<?php

/**
 * Plugin Name: Climbings Clinent Portal tweaks
 * Description: Add custom assets for Client Portal
 * Version: 1.0
 * Author: Climbings, Andrew Matin
 * Author URI: https://climbings.com
 **/

// Exit if Accessed Directly
if (!defined('ABSPATH')) {
  exit;
}

// Global Options Variable
$ccpt_options = get_option('ccpt_settings');


// Load Scripts
require_once(plugin_dir_path(__FILE__) . '/includes/climbings-cp-tweaks-scripts.php');


// Load Content
require_once(plugin_dir_path(__FILE__) . '/includes/climbings-cp-tweaks-content.php');


// Check if admin
if (is_admin()) {
  require_once(plugin_dir_path(__FILE__) . '/includes/climbings-cp-tweaks-fields.php');
}
