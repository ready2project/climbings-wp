<?php

// Check if admin and add admin scripts
if (is_admin()) {

  // Add Admin Scripts
  function yvg_add_admin_scripts()
  {
    wp_enqueue_style('ccpt-main-admin-style', plugins_url() . '/climbings-cp-tweaks/css/styles-admin.css');
    wp_enqueue_script('ccpt-main-admin-script', plugins_url() . '/climbings-cp-tweaks/js/main-admin.js');
  }

  add_action('admin_init', 'yvg_add_admin_scripts');
}



// Add Scripts
function ccpt_add_sripts()
{
  wp_enqueue_style('ccpt-main-style', plugins_url() . '/climbings-cp-tweaks/css/styles.css');
  wp_enqueue_script('ccpt-main-script', plugins_url() . '/climbings-cp-tweaks/js/main.js');

  // Enable jQuery for Client Portal Only
  $post_type = get_post_type();
  if ($post_type === 'leco_client' && is_single()) {
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);
  }
}

add_action('wp_enqueue_scripts', 'ccpt_add_sripts', 111);
