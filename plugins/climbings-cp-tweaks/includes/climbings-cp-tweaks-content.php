<?php

/**
 * Modify HTML Output
 * https://stackoverflow.com/questions/772510/wordpress-filter-to-modify-final-html-output
 */

// https://simplehtmldom.sourceforge.io/
require_once(plugin_dir_path(__FILE__) . '/utils/simplehtmldom/simple_html_dom.php');


/**
 * Change HTML Output
 */
ob_start();

add_action('shutdown', function () {

  // We'll need to get the number of ob levels we're in, so that we can iterate over each, collecting
  // that buffer's output into the final output.
  $levels = ob_get_level();
  $content = '';
  $post_type = get_post_type();
  $dom = new DOMDocument();

  for ($i = 0; $i < $levels; $i++) {
    $content .= ob_get_clean();
  }

  /**
   * If Page is Client Portal
   */
  if ($post_type === 'leco_client' && is_single()) {
    $html = str_get_html($content);

    /**
     * Update Header
     */
    $ccpt_subtitle = get_post_meta(get_the_ID(), 'ccpt_subtitle', true);
    $header = $html->find('.title-section');
    if ($header) {
      foreach ($html->find(".title-section") as $e) {
        $e->innertext .= '
        <div class="ccpt-subtitle">
          <div class="ccpt-container">
            <div class="ccpt-subtitle__content">'
          . $ccpt_subtitle .
          '</div>
            <div class="ccpt-arrow">
              <img src="/wp-content/plugins/climbings-cp-tweaks/img/dr-arrow.svg" alt="">
            </div>
          </div>
        </div>';
      }
    }

    /**
     * Update Phases
     */
    $phases = '';
    $phases_suffix = '';
    $phases_postfix = '';
    $phases_counter = 0;
    foreach ($html->find('.phases')[0]->children as $e) {

      if ($phases_counter === 3) {
        $phases_suffix = '<div class="ccpt-phase-primary">';
      } else {
        $phases_suffix = '';
      }

      if ($phases_counter === 4) {
        $phases_postfix = '</div>';
      } else {
        $phases_postfix = '';
      }

      $phases = $phases_suffix . '<div class="ccpt-container">' . $e->outertext . '</div>' . $phases_postfix;
      $e->outertext = $phases;

      $phases_counter++;
    }

    /**
     * Add Conversation
     */

    $ccpt_conversation = file_get_contents((plugin_dir_path(__FILE__) . '/layouts/conversation.html'));

    $main = $dom->getElementsByTagName('.phases');
    if ($main) {
      foreach ($html->find('.phases') as $e) {
        $main_content = $e->innertext;
        $e->innertext = $main_content . $ccpt_conversation;
      }
    }


    /**
     * Update Footer
     */
    $ccpt_footer = html_entity_decode(htmlspecialchars_decode(get_post_meta(get_the_ID(), 'ccpt_footer', true)));
    $ccpt_footer = file_get_contents((plugin_dir_path(__FILE__) . '/layouts/footer.html'));

    $footer = $dom->getElementsByTagName('footer');

    if ($footer) {
      foreach ($html->find('footer') as $e)
        $e->outertext = $ccpt_footer;
      $content = $html;
    }
  }

  // Apply any filters to the final output
  echo apply_filters('final_output', $content);
}, 0);
