<?php

/**
 * Hide Yoast metabox
 *
 * @return void
 */
function ccpt_remove_wp_seo_meta_box()
{
  remove_meta_box('wpseo_meta', 'leco_client', 'normal');
}
add_action('add_meta_boxes', 'ccpt_remove_wp_seo_meta_box', 100);


/**
 * Add custom fields
 *
 * @return void
 */
function ccpt_add_fields_metabox()
{
  add_meta_box('ccpt_video_fields', __('Climbings Custom Fields'), 'ccpt_fields_callback', 'leco_client', 'normal', 'default');
}

add_action('add_meta_boxes', 'ccpt_add_fields_metabox');

// Display metabox content
function ccpt_fields_callback($post)
{
  wp_nonce_field(basename(__FILE__), 'wp_ccpt_nonce');
  $ccpt_stored_meta = get_post_meta($post->ID);
?>
  <div class="wrap ccpt-form">

    <div class="form-group">
      <label for="ccpt_subtitle"><?php esc_html_e('Subtitle', 'ccpt_domain'); ?></label>
      <?php
      $content = get_post_meta($post->ID, 'ccpt_subtitle', true);
      $editor = 'ccpt_subtitle';
      $settings = array(
        'textarea_rows' => 5,
        'media_buttons' => false,
      );

      wp_editor($content, $editor, $settings);
      ?>
    </div>


    <div class="form-group">
      <div>
        <label for="ccpt_footer"><?php esc_html_e('Footer HTML', 'ccpt_domain'); ?></label>
      </div>
      <div>
        <textarea name="ccpt_footer" id="ccpt_footer" class="large-text" rows="10" cols="50"><?php if (!empty($ccpt_stored_meta['ccpt_footer'])) echo html_entity_decode(htmlspecialchars_decode($ccpt_stored_meta['ccpt_footer'][0])); ?></textarea>
      </div>
    </div>
  </div>
<?php
}


function ccpt_save($post_id)
{
  $is_autosave = wp_is_post_autosave($post_id);
  $is_revision = wp_is_post_revision($post_id);
  $is_valid_nonce = (isset($_POST['wp_ccpt_nonce']) && wp_verify_nonce($_POST['wp_ccpt_nonce'], basename(__FILE__))) ? 'true' : 'false';

  if ($is_autosave || $is_revision || !$is_valid_nonce) {
    return;
  }

  if (isset($_POST['ccpt_footer'])) {
    update_post_meta($post_id, 'ccpt_footer',  htmlentities(htmlspecialchars($_POST['ccpt_footer'])));
  }

  if (isset($_POST['ccpt_subtitle'])) {
    update_post_meta($post_id, 'ccpt_subtitle',  sanitize_text_field($_POST['ccpt_subtitle']));
  }
}

add_action('save_post', 'ccpt_save');
