<?php

/**
 * The template for displaying Careers single posts
 *
 *#single-post
 *
 * @package climbings
 */

get_header();
?>

<?php
while (have_posts()) :
  the_post();


  $action = get_field('action');

  if (empty($action)) {
    $action = 'mailto:team@climbings.com?subject=Careers - ' . get_the_title();
  }

?>

  <!-- Works Post Credentials  -->
  <main class="careers-post">
    <div class="container">


      <!-- Parent Page Title -->
      <div class="page__back">
        <h3><a href="<?php echo get_site_url(null, '/careers') ?>" title="Back to Careers Page">&lt; Back to Careers Page</a></h3>
      </div>


      <div class="careers-openings-card__col">
        <h3 class="page-card__title">
          <?php the_title(); ?>
        </h3>

        <ul class="careers-openings-tags-list">
          <li class="careers-openings-tags-list__item">
            <div class="careers-openings-tags-list__icon">
              <svg width="21" height="19">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-map-marked-alt"></use>
              </svg>
            </div>
            <div class="careers-openings-tags-list__text">
              <?php
              $location =  implode(', ', get_field('location'));
              echo $location;
              ?>
            </div>
          </li>
          <li class="careers-openings-tags-list__item">
            <div class="careers-openings-tags-list__icon">
              <svg width="18" height="18">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-clock"></use>
              </svg>
            </div>
            <div class="careers-openings-tags-list__text">
              <?php
              $time =  implode(', ', get_field('time'));
              echo $time;
              ?>
            </div>
          </li>
          <li class="careers-openings-tags-list__item">
            <div class="careers-openings-tags-list__icon">
              <svg width="18" height="19">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-rocket"></use>
              </svg>
            </div>
            <div class="careers-openings-tags-list__text">
              <?php
              $level =  implode(', ', get_field('level'));
              echo $level;
              ?>
            </div>
          </li>
        </ul>

        <div class="careers-openings-card__dsc">
          <p>
            <?php
            echo get_field('excerpt');
            ?>
          </p>
        </div>
      </div>



      <div class="careers-openings-card__action">
        <a class="btn btn-icon btn-lg" href="<?php echo $action; ?>">
          <span class="btn__text">Apply Now</span>
          <i class="i i-arrow-right"></i>
        </a>
      </div>

      <!-- Parent Page Title -->
      <div class="page__back pt-5">
        <h3><a href="<?php echo get_site_url(null, '/careers') ?>" title="Back to Careers Page">&lt; Back to Careers Page</a></h3>
      </div>

    </div>
  </main>


<?php
endwhile; // End of the loop.
?>



<?php
get_footer();
