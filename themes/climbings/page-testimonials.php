<?php

/**
 * Testimonials Page
 * @package climbings
 */

get_header();

$fv_uid_list = [];
?>

<!-- Testimonials -->
<section class="testimonials testimonials-page">
  <div class="container">

    <!-- Testimonials Header -->
    <header class="testimonials-header">
      <h2 class="title title-sm">
        <span>
          Inspirations

          <svg width="82" height="72">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#heart"></use>
          </svg>

          from
        </span>
        <span>
          our clients
        </span>
      </h2>

      <div class="subtitle">
        <?php the_field('testimonials_header', 'option'); ?>
      </div>
    </header>
  </div>



  <?php

  $dataTst = array_reverse(get_field('testimonials_slider', 'options'));
  $tabsTst = [];

  foreach ($dataTst as $item) {
    $type = $item['item']['type'];
    $tabsTst[0][] =  $item['item'];

    if (!empty($type)) {
      if (in_array('Web Design', $type)) {
        $tabsTst[1][] =  $item['item'];
      }

      if (in_array('App Design', $type)) {
        $tabsTst[2][] =  $item['item'];
      }

      if (in_array('Branding', $type)) {
        $tabsTst[3][] =  $item['item'];
      }

      if (in_array('Animation', $type)) {
        $tabsTst[4][] =  $item['item'];
      }

      if (in_array('Graphics Design', $type)) {
        $tabsTst[5][] =  $item['item'];
      }
    }
  }
  ksort($tabsTst);
  ?>

  <div class="container container-testimonials">

    <!-- Testimonials Filter -->
    <div class="testimonials-filter">

      <!-- Testimonials Menu -->
      <ul class="tab-menu testimonials-menu">

        <!-- Testimonials Menu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab1" class="tab-menu__link testimonials__link active">All Works</a>
        </li>

        <!-- Testimonials Menu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab2" class="tab-menu__link testimonials__link">Web Design</a>
        </li>

        <!-- Testimonials Menu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab3" class="tab-menu__link testimonials__link">App Design</a>
        </li>

        <!-- Testimonials Menu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab4" class="tab-menu__link testimonials__link">Branding</a>
        </li>

        <!-- Testimonials Menu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab5" class="tab-menu__link testimonials__link">Animation</a>
        </li>

        <!-- TestimonialsMenu Item -->
        <li class="tab-menu__item testimonials__item">
          <a href="#tst_tab6" class="tab-menu__link testimonials__link">Graphic Design</a>
        </li>

      </ul>
    </div><!-- /.testimonials-filter -->


    <!-- Testimonials Slider -->
    <div class="testimonials_tab-content">

      <?php
      $active = ' active';

      foreach ($tabsTst as $i => $items) :
        $idTstTab = $i;
        if ($idTstTab > 0) {
          $active = '';
        }
      ?>
        <!-- Testimonials Tab -->
        <div class="testimonials_tab-content__item<?php echo $active; ?>" id="tst_tab<?php echo $idTstTab + 1; ?>" data-tabid="<?php echo $idTstTab; ?>">

          <?php
          foreach ($items as $item) :
            // Get Clients Logo
            $logo = $item['logo']['url'];

            // Get Video
            $video = $item['video'];

            // Get Embed iframe code
            $fv_embed = $item['embed'];

            // Get thumbnail for iframe video
            $fv_emb_thumb = $item['embed_thumbnail']['url'];
          ?>
            <!-- Feedback Slide -->
            <div class="feedback__item">
              <div class="feedback">
                <?php if ($logo) : ?>
                  <div class="feedback__logo">
                    <img src="<?php echo $item['logo']['url']; ?>" alt="Climbings | Testimonials Logo">
                  </div>
                <?php endif; ?>
                <?php if ($video) :
                  $fv_uid = uniqid();
                  $fv_uid_list[] = [$fv_uid, $video];
                ?>
                  <div class="feedback-video">
                    <a class="feedback-video__play modal-video-link" href="#modal_feedback_<?php echo $fv_uid; ?>" data-target="modal_feedback_<?php echo $fv_uid; ?>">
                      <div class="feedback-video__btn">
                        <svg width="121" height="121">
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#play-feedback"></use>
                        </svg>
                      </div>
                      <div class="feedback-video__panel"></div>
                      <img class="feedback-video__thumb" src="https://img.youtube.com/vi/<?php echo $item['video']; ?>/0.jpg" alt="Video Thumbnail">
                    </a>
                  </div>
                <?php endif; ?>

                <?php if ($fv_embed) :
                  $fv_uid_emb = uniqid();
                  $fv_uid_emb_list[] = [$fv_uid_emb, $fv_embed];
                ?>
                  <!-- Feedback Video -->
                  <div class="feedback-video">
                    <a class="feedback-video__play modal-video-link" href="#modal_feedback_<?php echo $fv_uid_emb; ?>" data-target="modal_feedback_<?php echo $fv_uid_emb; ?>">
                      <div class="feedback-video__btn">
                        <svg width="121" height="121">
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#play-feedback"></use>
                        </svg>
                      </div>
                      <div class="feedback-video__panel"></div>
                      <img class="feedback-video__thumb" src="<?php echo $fv_emb_thumb; ?>" alt="Climbings | Video Thumbnail">
                    </a>
                  </div>
                <?php endif; ?>

                <div class="feedback__text">
                  <p>
                    <?php echo $item['feedback']; ?>
                  </p>
                </div>

                <p class="feedback__author"><?php echo $item['author']; ?></p>
                <p class="feedback__author-role"><?php echo $item['occupation']; ?></p>
              </div>
            </div>
          <?php
          endforeach; ?>
        </div> <!-- /.testimonials_tab-content__item -->
      <?php
      endforeach;
      ?>
    </div><!-- /.testimonials_tab-content -->

  </div><!-- /.container container-testimonials -->
</section><!-- /.testimonials -->

<?php
if (!empty($fv_uid_list)) :
  foreach ($fv_uid_list as $fv_item) :

?>

    <!-- Feedback Modal -->
    <div class="modal-video" id="modal_feedback_<?php echo $fv_item[0]; ?>" role="dialog" tabindex="-1" data-video="<?php echo $fv_item[1]; ?>">

      <!-- Contact Modal | Window-->
      <div class="modal-wnd">

        <!-- Contact Modal | Hide Button -->
        <a class="modal-video-hide" href="#" aria-label="Hide" data-target="modal_feedback_<?php echo $fv_item[0]; ?>"><span aria-hidden="true">&times;</span></a>

        <!-- Modal Body -->
        <div class="modal-wnd__body">
          <iframe loading="lazy" id="modal_feedback_<?php echo $fv_item[0]; ?>video" src="about:blank" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div><!-- /.modal-wnd__body -->
      </div>
    </div><!-- /.video-modal -->

<?php
  endforeach;
endif;
?>

<?php
// Embed iframe Modals
if (!empty($fv_uid_emb_list)) :
  foreach ($fv_uid_emb_list as $fv_item) :
?>

    <!-- Feedback Modal -->
    <div class="modal-video" id="modal_feedback_<?php echo $fv_item[0]; ?>" role="dialog" tabindex="-1">

      <!-- Contact Modal | Window-->
      <div class="modal-wnd">

        <!-- Contact Modal | Hide Button -->
        <a class="modal-video-hide" href="#" aria-label="Hide" data-target="modal_feedback_<?php echo $fv_item[0]; ?>"><span aria-hidden="true">&times;</span></a>

        <!-- Modal Body -->
        <div class="modal-wnd__body modal-wnd__body-iframe">
          <?php
          echo $fv_item[1];
          ?>
        </div><!-- /.modal-wnd__body -->
      </div>
    </div><!-- /.video-modal -->

<?php
  endforeach;
endif;
?>


<?php
get_footer();
