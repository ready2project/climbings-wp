<?php

/**
 * climbings functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package climbings
 */

function getFavFolder()
{
  return get_template_directory_uri() . '/assets/dist/favicons';
}


if (!function_exists('climbings_setup')) :


  add_theme_support('title-tag');


  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function climbings_setup()
  {
    /*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on climbings, use a find and replace
		 * to change 'climbings' to the name of your theme in all the template files.
		 */
    load_theme_textdomain('climbings', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
    add_theme_support('title-tag');

    /*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
      'menu-1' => esc_html__('Primary', 'climbings'),
    ));

    /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));

    // Set up the WordPress core custom background feature.
    add_theme_support('custom-background', apply_filters('climbings_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    )));

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support('custom-logo', array(
      'height'      => 250,
      'width'       => 250,
      'flex-width'  => true,
      'flex-height' => true,
    ));
  }
endif;
add_action('after_setup_theme', 'climbings_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function climbings_content_width()
{
  // This variable is intended to be overruled from themes.
  // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
  // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
  $GLOBALS['content_width'] = apply_filters('climbings_content_width', 640);
}
add_action('after_setup_theme', 'climbings_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function climbings_widgets_init()
{
  register_sidebar(array(
    'name'          => esc_html__('Sidebar', 'climbings'),
    'id'            => 'sidebar-1',
    'description'   => esc_html__('Add widgets here.', 'climbings'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ));
}
add_action('widgets_init', 'climbings_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function climbings_scripts()
{
  wp_enqueue_style('climbings-theme-styles', get_stylesheet_uri());
  wp_enqueue_style('climbings-styles', get_template_directory_uri() . '/assets/dist/styles/main.css', false, '1.2', 'all');
  wp_enqueue_script('climbings-scripts', get_template_directory_uri() . '/assets/dist/javascript/app.js', array(), '1.2', true);

  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_deregister_script('wp-embed');
  wp_deregister_script('jquery');
}
add_action('wp_enqueue_scripts', 'climbings_scripts');


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
  require get_template_directory() . '/inc/jetpack.php';
}






/**
 * Tweaks
 */
require get_template_directory() . '/inc/tweaks.php';




/**
 * fix: leco-client-portal: hide nginx message at the admin area
 */
add_action('admin_head', 'setCustomStyles');

function setCustomStyles()
{
  echo '<style>
		#nginx_rules {
			display: none;
		}
  </style>';
}


/**
 * fix: w3tc: footer comments
 */

add_filter('w3tc_can_print_comment', function ($w3tc_setting) {
  return false;
}, 10, 1);




/**
 * Custom Logo
 */
add_action('login_head', 'wp_login_logo_img_url');
function wp_login_logo_img_url()
{
  echo '
		<style>
			.login h1 a{ width: 206px; height: 50px; background-size: 206px 50px; background-image: url( ' . get_template_directory_uri() . '/assets/dist/images/logo-alt.svg) !important; }
		</style>';
}

add_filter('login_headerurl', 'wp_login_logo_link_url');
function wp_login_logo_link_url($url)
{
  return home_url();
}

add_filter('login_headertitle', 'wp_login_logo_title_attr');
function wp_login_logo_title_attr($title)
{
  $title = get_bloginfo('name');
  return $title;
}


/**
 * Options Pages
 */

if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
    'page_title'   => 'Content Settings',
    'menu_title'  => 'Content Settings',
    'menu_slug'   => 'content-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

  acf_add_options_sub_page(array(
    'page_title'   => 'CTA Section',
    'menu_title'  => 'CTA Section',
    'parent_slug'  => 'content-general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'   => 'Testimonials Section',
    'menu_title'  => 'Testimonials Section',
    'parent_slug'  => 'content-general-settings',
  ));
}


add_action(
  'after_setup_theme',
  function () {
    add_theme_support('html5', ['script', 'style']);
  }
);



// add_action( 'init', 'gp_register_taxonomy_for_object_type' );
// function gp_register_taxonomy_for_object_type() {
//     register_taxonomy_for_object_type( 'post_tag', 'works' );
// };




/**
 * Force type private for Subscribers and Contacts Modal
 *
 * @param [type] $post
 * @return void
 */
function force_type_private($post)
{
  if ($post['post_type'] == 'subscribers' || $post['post_type'] == 'contacts') {
    $post['post_status'] = 'private';
  }

  return $post;
}
add_filter('wp_insert_post_data', 'force_type_private');


/**
 * Contact 7
 *
 * https://contactform7.com/loading-javascript-and-stylesheet-only-when-it-is-necessary/
 */
// add_filter('wpcf7_load_js', '__return_false');
// add_filter('wpcf7_load_css', '__return_false');



/**
 * Add page slug to body class
 *
 * @param [type] $classes
 * @return void
 */
function climbings_slug_to_body($classes)
{
  global $post;

  if (isset($post)) {
    $classes[] = 'body-' . $post->post_name;
  }
  return $classes;
}

add_filter('body_class', 'climbings_slug_to_body');



/**
 * Get first image from a post
 *
 * @return void
 */
function get_post_image()
{
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+?src=[\'"]([^\'"]+)[\'"].*?>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if (empty($first_img)) {
    $first_img = "https://climbings.com/wp-content/themes/climbings/assets/dist/images/placeholder.jpg";
  }
  return $first_img;
}

/**
 * Get User Role
 *
 * @param [type] $post_authr_id
 * @return void
 */
function get_user_role($post_authr_id)
{
  $user = new WP_User($post_authr_id);
  return array_shift($user->roles);
}
