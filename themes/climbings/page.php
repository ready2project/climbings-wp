<?php

/**
 * Pages
 *
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package climbings
 */

get_header();
?>

<header class="page-header">
  <div class="container container-full">
    <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
    <p class="entry-lastupdate"><?php printf(__('Last updated on %s', 'textdomain'), get_the_modified_date()); ?></p>
  </div>
</header>

<?php
while (have_posts()) :
  the_post();
?>


  <main class="page-main">
    <div class="container container-sm">

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php climbings_post_thumbnail(); ?>

        <div class="entry-content">
          <?php
          the_content();
          ?>
        </div><!-- .entry-content -->

        <?php if (get_edit_post_link()) : ?>
          <footer class="entry-footer">
            <?php
            edit_post_link(
              sprintf(
                wp_kses(
                  /* translators: %s: Name of current post. Only visible to screen readers */
                  __('Edit <span class="screen-reader-text">%s</span>', 'climbings'),
                  array(
                    'span' => array(
                      'class' => array(),
                    ),
                  )
                ),
                get_the_title()
              ),
              '<span class="edit-link">',
              '</span>'
            );
            ?>
          </footer><!-- .entry-footer -->
        <?php endif; ?>
      </article><!-- #post-<?php the_ID(); ?> -->
    </div>
  </main>


<?php
endwhile; // End of the loop.
?>


<?php
get_footer();
