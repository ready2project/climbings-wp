<?php

/**
 * Main Landing page
 */

$button_start_a_project = get_field('button_start_a_project', 'option');

get_header();
?>

<!-- Careers Intro -->
<section class="careers-intro">
  <div class="container">

    <!-- Careers Intro Header -->
    <div class="careers-intro__header">
      <h1 class="subtitle">Careers at Climbings</h1>
      <h2 class="title">What's your next destination?</h2>
    </div>

    <!-- Careers Intro Planet -->
    <div class="careers-intro__img">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/careers/careers-intro.svg" alt="Climbings | Careers at Climbings | What's your next destination?" width="1021" height="923">
    </div>

    <!-- Careers Intro Planet - Mobile -->
    <div class="careers-intro__img-mobile">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/careers/planet.svg" alt="Climbings | Careers at Climbings | What's your next destination?" width="602" height="420">
    </div>

    <!-- Careers Intro Action -->
    <div class="careers-intro__action">
      <a class="btn btn-secondary btn-icon" href="https://www.linkedin.com/company/climbings/jobs/" target="_blank" rel="nofollow noopener noreferrer">
        <span class="btn__text">See Openings</span>
        <i class="i i-arrow-right"></i>
      </a>
    </div>
  </div>
</section><!-- /.careers-intro -->

<!-- Our Openings -->
<section class="careers-openings">
  <div class="container">

    <!-- Our Openings Header -->
    <header class="careers-openings__header">
      <div class="i-arrow-center">
        <i class="i-arrow-cta i-arrow-cta-rotate i-arrow-cta-red"></i>
      </div>

      <h2 class="page-section-title">Our Openings</h2>
      <div class="page-section-dsc">
        <p>
          After going through Discover process,
          we start designing eye catchy design that fits best for your brand and audience based on geography,
          age &amp; gender
        </p>
      </div>
    </header>

    <!-- Our Openings Content -->
    <div class="careers-openings__content">

      <?php
      $args = array(
        'post_type' => 'careers',
      );
      $the_query = new WP_Query($args);

      if ($the_query->have_posts()) {
        while ($the_query->have_posts()) :
          setup_postdata($the_query->the_post());

          $action = get_field('action');

          if (empty($action)) {
            // $action = 'mailto:team@climbings.com?subject=Careers - ' . get_the_title();

            $action = get_post_permalink();
          }
      ?>

          <!-- Our Openings Card -->
          <div class="careers-openings-card">
            <div class="careers-openings-card__col">
              <div class="careers-openings-card__title">
                <a href="<?php echo $action; ?>" class="careers-openings-card__link"><?php the_title(); ?></a>
              </div>

              <ul class="careers-openings-tags-list">
                <li class="careers-openings-tags-list__item">
                  <div class="careers-openings-tags-list__icon">
                    <svg width="21" height="19">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-map-marked-alt"></use>
                    </svg>
                  </div>
                  <div class="careers-openings-tags-list__text">
                    <?php
                    $location =  implode(', ', get_field('location'));
                    echo $location;
                    ?>
                  </div>
                </li>
                <li class="careers-openings-tags-list__item">
                  <div class="careers-openings-tags-list__icon">
                    <svg width="18" height="18">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-clock"></use>
                    </svg>
                  </div>
                  <div class="careers-openings-tags-list__text">
                    <?php
                    $time =  implode(', ', get_field('time'));
                    echo $time;
                    ?>
                  </div>
                </li>
                <li class="careers-openings-tags-list__item">
                  <div class="careers-openings-tags-list__icon">
                    <svg width="18" height="19">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-rocket"></use>
                    </svg>
                  </div>
                  <div class="careers-openings-tags-list__text">
                    <?php
                    $level =  implode(', ', get_field('level'));
                    echo $level;
                    ?>
                  </div>
                </li>
              </ul>

              <div class="careers-openings-card__dsc">
                <p>
                  <?php
                  $excerpt = get_field('excerpt');
                  echo wp_trim_words($excerpt, 30);
                  ?>
                </p>
              </div>
            </div>



            <div class="careers-openings-card__action">
              <a class="btn btn-icon" href="<?php echo $action; ?>">
                <span class="btn__text">Apply</span>
                <i class="i i-arrow-right"></i>
              </a>
            </div>
          </div><!-- /.careers-openings-card -->


      <?php
        endwhile;

        // Restore original Query & Post Data
        wp_reset_query();
        wp_reset_postdata();
      }
      ?>



    </div><!-- /.careers-openings__content -->

    <?php
    $enable_faq = get_field('enable_faq');

    if ($enable_faq) :
    ?>

      <!-- Our Openings Footer -->
      <footer class="careers-openings__footer">
        <i class="i-arrow-cta i-arrow-cta-red"></i>
      </footer>

    <?php
    endif;
    ?>


  </div>
</section><!-- /.careers-openings -->

<?php
$enable_faq = get_field('enable_faq');

if ($enable_faq) :
?>

  <!-- FAQ -->
  <section class="faq">
    <div class="container">

      <!-- FAQ Header -->
      <header class="faq__header">
        <h2 class="page-section-title">FAQ</h2>
      </header>

      <!-- FAQ Content -->
      <div class="faq__content">

        <!-- FAQ Tabs List -->
        <ul class="faq-tabs-list">
          <?php
          if (have_rows('faq')) :
            $counter = 1;
            while (have_rows('faq')) : the_row();
              if ($counter == 1) {
                $active = ' active';
              } else {
                $active = '';
              }

              $question = get_sub_field('question');
              $answer = get_sub_field('answer');
          ?>
              <li class="faq-tabs-list__item">
                <a href="#" class="faq-tabs-list__link<?php echo $active; ?>" data-tabid="<?php echo $counter; ?>"><?php echo $question; ?></a>
                <div class="faq-tabs__card faq-tabs__card-mobile<?php echo $active; ?>" data-tab="<?php echo $counter; ?>">
                  <?php echo $answer; ?>
                </div>
              </li>
          <?php
              $counter++;
            endwhile;
          endif;
          ?>
        </ul>

        <!-- FAQ Tabs Content -->
        <div class="faq-tabs__content">


          <?php
          if (have_rows('faq')) :
            $counter = 1;
            while (have_rows('faq')) : the_row();
              if ($counter === 1) {
                $active = ' active';
              } else {
                $active = '';
              }

              $answer = get_sub_field('answer');
          ?>
              <div class="faq-tabs__card<?php echo $active; ?>" data-tab="<?php echo $counter; ?>">
                <?php echo $answer; ?>
              </div>
          <?php
              $counter++;
            endwhile;
          endif;
          ?>


        </div>
      </div>
    </div>
  </section><!-- /.faq -->

<?php
endif;
?>


<?php
get_footer();
