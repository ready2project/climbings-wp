<?php

/**
 * Main Landing page
 */

$button_start_a_project = get_field('button_start_a_project', 'option');

get_header();
?>


<!-- Header -->
<header class="header header-dark">
  <div class="container container-full">
    <div class="row">
      <div class="col">
        <!-- Logo -->
        <a class="logo" href="<?php echo get_home_url(); ?>" title="Climbings. Index Page" accesskey="1">
          <svg width="206" height="50">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#logo"></use>
          </svg>

        </a>
      </div>
    </div>
  </div>
</header>


<!-- Intro -->
<section class="section-inverted intro">
  <div class="container">
    <div class="row">
      <div class="col d-flex justify-content-end">

        <!-- Subtitle -->
        <div class="intro-subtitle">
          <?php the_field('intro_subtitle'); ?>
        </div>

      </div>
    </div>
  </div>

  <!-- Title -->
  <header class="container intro-header">
    <h2 class="title">Climb High</h2>
  </header>

  <!-- Intro Menu -->
  <div class="container">

    <!-- Intro Animation Scene -->
    <div class="intro-scene" id="sceneIntro">

      <!-- Intro Menu -->
      <ul class="intro-menu">

        <!-- Menu Item -->
        <li class="intro-menu__item">
          <a href="<?php the_field('intro_buttons_link'); ?>" class="btn btn-inv intro-menu__link">
            <span class="btn__icon-plus">+</span>
            <span class="btn__text">Development</span>
          </a>
        </li>

        <!-- Menu Item -->
        <li class="intro-menu__item">
          <a href="<?php the_field('intro_buttons_link'); ?>" class="btn btn-inv intro-menu__link">
            <span class="btn__icon-plus">+</span>
            <span class="btn__text">Design</span>
          </a>
        </li>

        <!-- Menu Item -->
        <li class="intro-menu__item">
          <a href="<?php the_field('intro_buttons_link'); ?>" class="btn btn-inv intro-menu__link">
            <span class="btn__icon-plus">+</span>
            <span class="btn__text">Branding</span>
          </a>
        </li>

        <!-- Menu Item -->
        <li class="intro-menu__item">
          <a href="<?php the_field('intro_buttons_link'); ?>" class="btn btn-inv intro-menu__link">
            <span class="btn__icon-plus">+</span>
            <span class="btn__text">Strategy</span>
          </a>
        </li>

        <!-- Menu Item -->
        <li class="intro-menu__item">
          <a class="btn intro-menu__link btn-explore-more" href="<?php the_field('intro_explore_more_link'); ?>" target="_blank" rel="nofollow noopener noreferrer">
            <i class="i i-arrow-up"></i>
            <span class="btn__text">Explore More</span>
          </a>
        </li>

      </ul>

      <?php
      // Hero Animation
      get_template_part('template-parts/animation/intro-scene', 'none');
      ?>

      <!-- Intro Scene Shadow -->
      <div class="intro-scene-shadow"></div>

    </div>
  </div>

  <!-- Popup Explore More -->
  <div class="popup-explore-more">
    <a class="btn intro-menu__link btn-explore-more" href="<?php the_field('intro_explore_more_link'); ?>" target="_blank" rel="nofollow noopener noreferrer">
      <i class="i i-arrow-up"></i>
      <span class="btn__text">Explore More</span>
    </a>
  </div>
</section>



<!-- Partners -->
<section class="partners section-inverted">
  <div class="container">

    <!-- Partners Title -->
    <header class="partners__header">
      <h2 class="partners__title caps-heading">Clients we’ve worked with</h2>
    </header>

    <!-- Partners List -->
    <div class="partners__content">
      <ul class="partners-list">

        <!-- Partners Item -->
        <li class="partners-list__item">
          <a class="partners-list__link" href="https://webflow.com/" aria-label="Climbings. External Link: Webflow" target="_blank" rel="nofollow noopener noreferrer">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/partners/webflow.png" alt="Climbings | Partners">
          </a>
        </li>

        <!-- Partners Item -->
        <li class="partners-list__item">
          <a class="partners-list__link" href="https://www.intricately.com/" aria-label="Climbings. External Link: Intricately" target="_blank" rel="nofollow noopener noreferrer">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/partners/intricately.png" alt="Climbings | Partners">
          </a>
        </li>

        <!-- Partners Item -->
        <li class="partners-list__item">
          <a class="partners-list__link" href="https://www.piermontbank.com/" aria-label="Climbings. External Link: Piermont Bank" target="_blank" rel="nofollow noopener noreferrer">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/partners/piermontbank.png" alt="Climbings | Partners">
          </a>
        </li>

        <!-- Partners Item -->
        <li class="partners-list__item">
          <a class="partners-list__link" href="https://www.phdata.io/" aria-label="Climbings. External Link: phData" target="_blank" rel="nofollow noopener noreferrer">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/partners/phdata.png" alt="Climbings | Partners">
          </a>
        </li>

        <!-- Partners Item -->
        <li class="partners-list__item">
          <a class="partners-list__link" href="https://www.datonics.com/" aria-label="Climbings. External Link: Datonics" target="_blank" rel="nofollow noopener noreferrer">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/partners/daton.png" alt="Climbings | Partners">
          </a>
        </li>

      </ul><!-- /.partners-list -->
    </div>
  </div>
</section><!-- /.partners -->


<!-- Services -->
<section class="section-inverted services" id="sectionServices">
  <div class="container">

    <!-- Services Header -->
    <header class="services__header">
      <i class="i-arrow-cta i-arrow-cta-rotate"></i>
      <h2 class="title">Our Services</h2>
    </header>

    <!-- Services Tabs -->
    <div class="services-tabs">

      <!-- Services Tabs Navigation -->
      <div class="services-tabs__nav">


        <!-- Tab Button -->
        <a class="services-tabs__link" href="#tab_develop">

          <!-- Tabs Icon -->
          <div class="services-tabs__icon">
            <svg width="65" height="67">
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-services-brand"></use>
            </svg>
          </div>

          <!-- Tabs Content -->
          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Branding</h3>
            <p class="services-tabs__dsc">Software and web development for all platforms</p>
          </div>
        </a>

        <!-- Tab Button -->
        <a class="services-tabs__link active" href="#tab_design">

          <!-- Tabs Icon -->
          <div class="services-tabs__icon">
            <svg width="82" height="76">
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-services-design"></use>
            </svg>
          </div>

          <!-- Tabs Content -->
          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Design</h3>
            <p class="services-tabs__dsc">Carefully crafted design and user experience analysis</p>
          </div>
        </a>

        <!-- Tab Button -->
        <a class="services-tabs__link" href="#tab_strategy">

          <!-- Tabs Icon -->
          <div class="services-tabs__icon">
            <svg width="62" height="87">
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-services-dev"></use>
            </svg>
          </div>

          <!-- Tabs Content -->
          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Develop</h3>
            <p class="services-tabs__dsc">Need to develop business strategy? We've got it covered</p>
          </div>
        </a>
      </div>


      <!-- Services Tabs Content -->
      <div class="services-content">

        <!-- Tabs Content Item -->
        <div class="services-content__item active" id="tab_design">

          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Design</h3>
            <p class="services-tabs__dsc">Carefully crafted design and user experience analysis</p>
          </div>

          <ul class="services-list">
            <li class="services-list__item">User Experience Design</li>
            <li class="services-list__item">Wireframe</li>
            <li class="services-list__item">Prototype</li>
            <li class="services-list__item">App Design</li>
            <li class="services-list__item">Web Design</li>
            <li class="services-list__item">Animation</li>
            <li class="services-list__item">Brand Identity</li>
            <li class="services-list__item">Print Design</li>
            <li class="services-list__item">Interactive Presentation</li>
            <li class="services-list__item">Illustration</li>
          </ul>
        </div>

        <!-- Tabs Content Item -->
        <div class="services-content__item" id="tab_develop">

          <!-- Tabs Content -->
          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Develop</h3>
            <p class="services-tabs__dsc">Software and web development for all platforms</p>
          </div>

          <ul class="services-list">
            <li class="services-list__item">Strategy</li>
            <li class="services-list__item">Positioning</li>
            <li class="services-list__item">Art Direction</li>
            <li class="services-list__item">Stylescapes</li>
            <li class="services-list__item">Logo Design</li>
            <li class="services-list__item">Identity Design</li>
            <li class="services-list__item">Marketing Collateral</li>
          </ul>
        </div>

        <!-- Tabs Content Item -->
        <div class="services-content__item" id="tab_strategy">

          <!-- Tabs Content -->
          <div class="services-tabs__content">
            <h3 class="services-tabs__header">Strategy</h3>
            <p class="services-tabs__dsc">Need to develop business strategy? We’ve got it covered</p>
          </div>

          <ul class="services-list">
            <li class="services-list__item">Business Strategy</li>
            <li class="services-list__item">Content Writing</li>
            <li class="services-list__item">Presentation</li>
            <li class="services-list__item">Analysis</li>
            <li class="services-list__item">Marketing</li>
          </ul>
        </div>


        <!-- Services Action -->
        <div class="services__action">
          <a class="btn btn-primary btn-icon" href="https://climbings.com/get-started/">
            <span class="btn__text">Start a project</span>
            <i class="i i-arrow-right"></i>
          </a>
        </div><!-- /.services__action -->

      </div><!-- /.services-content -->
    </div><!-- /.services-tabs -->
  </div>

  <div class="services-bottom-bg"></div>
</section><!-- /.services -->


<!-- Our Process -->
<section class="process" id="sectionProcess">

  <!-- Our Process Header -->
  <div class="container">
    <header class="process__header">
      <i class="i-arrow-cta"></i>
      <h2 class="title title-sm">How do we climb to your goal?</h2>
    </header>

    <?php
    // Hero Animation
    get_template_part('template-parts/animation/process-scene', 'none');
    ?>
  </div>

  <div class="process-bottom-bg"></div>
</section>

<!-- Our Process Slider -->
<div class="process__content">
  <div class="container">

    <!-- Our Process Menu -->
    <ul class="process-menu">

      <!-- Menu Item -->
      <li class="process-menu__item">
        <a href="#" class="process-menu__link active" data-index="1">Discover</a>
      </li>

      <!-- Menu Item -->
      <li class="process-menu__item">
        <a href="#" class="process-menu__link" data-index="2">Branding</a>
      </li>

      <!-- Menu Item -->
      <li class="process-menu__item">
        <a href="#" class="process-menu__link" data-index="3">Design</a>
      </li>

      <!-- Menu Item -->
      <li class="process-menu__item">
        <a href="#" class="process-menu__link" data-index="4">Develop</a>
      </li>

      <!-- Menu Item -->
      <li class="process-menu__item">
        <a href="#" class="process-menu__link" data-index="5">Testing &amp; Launch</a>
      </li>

      <li class="hover"></li>
    </ul>

    <div class="process-slider-panel">
      <!-- Our Process Slider -->
      <div class="process-slider">

        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">

          <!-- Slide #1 Discover -->
          <div class="swiper-slide">

            <!-- Tab Content -->
            <div class="process-details__tab">
              <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-discover-alt.svg" alt="Climbings | Discover" width="1025" height="461">

              <div class="process-details__content mt-30">
                <h3 class="process-details__title">Requirements. Analysis. Wireframe.</h3>
                <p class="process-details__subtitle">
                  After going through Discover process, we start designing eye catchy design that fits best
                  for your brand and audience based on geography, age &amp; gender
                </p>
              </div>
            </div>

          </div>

          <!-- Slide #2 Branding -->
          <div class="swiper-slide">

            <!-- Tab Content -->
            <div class="process-details__tab">
              <div class="process-details__content">
                <h3 class="process-details__title">Branding</h3>
                <p class="process-details__subtitle">
                  After going through Discover process, we start designing eye catchy design that fits best
                  for your brand and audience based on geography, age &amp; gender
                </p>
              </div>

              <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-branding.svg" alt="Climbings | Branding" width="916" height="345">
            </div>

          </div>

          <!-- Slide #3 Design -->
          <div class="swiper-slide">

            <!-- Tab Content -->
            <div class="process-details__tab">
              <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-design.png" alt="Climbings | Design" width="1810" height="906">

              <div class="process-details__content">
                <h3 class="process-details__title">Diverse Design &amp; Animation skill</h3>
                <p class="process-details__subtitle">
                  After going through Discover process, we start designing eye catchy design that fits best
                  for your brand and audience based on geography, age &amp; gender
                </p>
              </div>
            </div>

          </div>

          <!-- Slide #4 Develop -->
          <div class="swiper-slide">

            <!-- Tab Content -->
            <div class="process-details__tab">
              <div class="process-details__content lg">
                <div class="process-details__platforms">
                  <h3 class="process-details__title">Genius Coding</h3>
                  <h4 class="process-details__title-secondary">For Any Playform</h4>
                  <p class="process-details__subtitle">
                    After going through Discover process, we start designing eye catchy design that fits best
                    for your brand and audience based on geography, age &amp; gender
                  </p>
                  <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-platforms-alt.svg" alt="Climbings | Platforms" width="940" height="80">
                </div>
                <div class="process-details__lang">
                  <h4 class="process-details__title-secondary">With Any Language.</h4>
                  <p class="process-details__subtitle">
                    After going through Discover process, we start designing eye catchy design that fits best
                    for your brand and audience based on geography, age &amp; gender
                  </p>
                  <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-lang-alt.svg" alt="Climbings | Process" width="940" height="52">
                </div>
              </div>
            </div>

          </div>

          <!-- Slide #5 Testing & Launch -->
          <div class="swiper-slide">

            <!-- Tab Content -->
            <div class="process-details__tab">

              <!-- Tab Content -->
              <div class="process-details__tab">
                <div class="process-testing__img">
                  <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-testing.svg" alt="Climbings | QA" width="887" height="469">
                </div>

                <div class="process-details__content mt-30">
                  <h3 class="process-details__title">We make sure it works as intended</h3>
                  <p class="process-details__subtitle">
                    After going through Discover process, we start designing eye catchy design that fits best
                    for your brand and audience based on geography, age &amp; gender
                  </p>
                </div>
              </div>


              <div class="process__launch-img">
                <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/process-launch-alt.svg" alt="Climbings | Launch" width="801" height="321">
              </div>

              <div class="process__launch-msg">
                <p class="primary">But wait! We aren’t done yet!</p>
                <p>We will provide <em>on-demand support</em> for the product we created with <em>love</em> for you</p>
              </div>

            </div>

          </div>
        </div><!-- /.swiper-wrapper -->

        <!-- Navigation buttons -->
        <div class="swiper-button-prev">
          <div class="btn btn-short"><i class="i i-arrow-left"></i></div>
        </div>

        <div class="swiper-button-next">
          <div class="btn btn-short"><i class="i i-arrow-right"></i></div>
        </div>
      </div><!-- /.process-slider -->

      <div class="process-slider__action">
        <div class="process__launch-btn">
          <a class="btn btn-icon btn-lg" href="https://climbings.com/get-started/">
            <span class="btn__text">Start a project</span>
            <i class="i i-arrow-right"></i>
          </a>
        </div>
      </div>

    </div>
  </div><!-- /.container -->

  <div class="works-top-bg"></div>
</div><!-- /.process__content -->

<?php if (current_user_can('administrator')) : ?>

  <!-- Our Process -->
  <section class="our-process">
    <div class="process__header">
      <div class="container">
        <i class="i-arrow-cta i-arrow-cta-rotate"></i>
        <h2 class="title">Our Process</h2>
        <h3 class="subtitle">
          Climbings is a global digital agency and design studio providing innovative, modern and aesthetic solution for your online business &amp; presence
        </h3>
      </div>
    </div>


    <div class="process__content">
      <div class="container">

        <!-- Our Process Menu -->
        <ul class="our-process-menu">
          <!-- Menu Item -->
          <li class="our-process-menu__item">
            <a href="#" class="our-process-menu__link active" data-slideindex="1">Branding</a>
          </li>

          <!-- Menu Item -->
          <li class="our-process-menu__item">
            <a href="#" class="our-process-menu__link" data-slideindex="2">Design</a>
          </li>

          <!-- Menu Item -->
          <li class="our-process-menu__item">
            <a href="#" class="our-process-menu__link" data-slideindex="3">Development</a>
          </li>
        </ul>


        <div class="our-process-menu-slider">

          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">

            <!-- Slide #1 Discover -->
            <div class="swiper-slide">

              <!-- Tab Content -->
              <div class="our-process-details__tab">
                <h3 class="our-process-details__title">
                  Branding Process
                </h3>
                <div class="our-process-details__dsc">
                  <p>
                    After going through Discover processs, we start designing eye catchy design that fits best for your brand and audience based on geography, age &amp; gennder
                  </p>
                </div>
                <div class="our-process-details__action">
                  <a class="btn btn-secondary btn-icon" href="<?php echo home_url('/works/'); ?>">
                    <span class="btn__text">See Our Branding Works</span>
                    <i class="i i-arrow-right"></i>
                  </a>
                </div>
              </div>

            </div>

            <!-- Slide #2 Design -->
            <div class="swiper-slide">

              <!-- Tab Content -->
              <div class="our-process-details__tab">
                <h3 class="our-process-details__title">
                  Development Process
                </h3>
                <div class="our-process-details__dsc">
                  <p>
                    After going through Discover processs, we start designing eye catchy design that fits best for your brand and audience based on geography, age &amp; gennder
                  </p>
                </div>
                <div class="our-process-details__action">
                  <a class="btn btn-secondary btn-icon" href="<?php echo home_url('/works/'); ?>">
                    <span class="btn__text">See Our Development Works</span>
                    <i class="i i-arrow-right"></i>
                  </a>
                </div>
              </div>
            </div>

            <!-- Slide #3 Develop -->
            <div class="swiper-slide">

              <!-- Tab Content -->
              <div class="our-process-details__tab">
                <h3 class="our-process-details__title">
                  Design Process
                </h3>
                <div class="our-process-details__dsc">
                  <p>
                    After going through Discover processs, we start designing eye catchy design that fits best for your brand and audience based on geography, age &amp; gennder
                  </p>
                </div>
                <div class="our-process-details__action">
                  <a class="btn btn-secondary btn-icon" href="<?php echo home_url('/works/'); ?>">
                    <span class="btn__text">See Our Design Works</span>
                    <i class="i i-arrow-right"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.swiper-wrapper -->
      </div><!-- /.process-slider -->

    </div>

    <!-- Process Progress Bar -->
    <div class="how-do-we-climb">
      <img class="how-do-we-climb_img" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/process/how-do-we-climb.svg" alt="Climbings | How do we climb">
    </div>


    <div class="our-process__action">
      <div class="process__launch-btn">
        <a class="btn btn-icon btn-lg" href="https://climbings.com/get-started/">
          <span class="btn__text">Start a project</span>
          <i class="i i-arrow-right"></i>
        </a>
      </div>
    </div>


    <div class="our-process-bottom-bg"></div>
  </section><!-- /.our-process -->

<?php endif; ?>


<?php
// Works
get_template_part('template-parts/home/section-works', 'none');
?>


<?php
// Testimonials
get_template_part('template-parts/home/section-testimonials', 'none');
?>

<?php
// Latest Insights
get_template_part('template-parts/blog/latest-insights', 'none');
?>

<?php
get_footer();
