<?php

/**
 * Blog Page
 * @package climbings
 */

get_header('lg');
?>


<!-- Blog Intro -->
<section class="blog-intro">
  <div class="blog-intro__content">

    <!-- Slider main container -->
    <div class="swiper-container blog-intro__slider">

      <!-- Additional required wrapper -->
      <div class="swiper-wrapper">


        <?php
        /**
         * Get Main Featured Posts (Intro)
         */
        $args = array(
          'category_name' => 'featured',
          'posts_per_page' => 10,
          'offset' => 0
        );

        $the_query = new WP_Query($args);

        if ($the_query->have_posts()) {
          while ($the_query->have_posts()) :
            setup_postdata($the_query->the_post());
            $post_date = get_the_date('j M, Y');
            $category = get_the_category();
        ?>

            <!-- Slides -->
            <div class="swiper-slide">

            <!-- Blog Intro Item -->
              <div class="blog-intro__item">
                <a class="blog-intro__link-img" href="<?php the_permalink(); ?>">
                  <div class="blog-intro__pict">
                    <?php echo the_post_thumbnail('full'); ?>
                    <div class="blog-intro__shadow"></div>
                  </div>
                </a>

                <div class="blog-intro__dsc">
                  <a class="blog-intro__link-rubric" href="<?php echo site_url('/category/' . $category[0]->slug . '/', 'https'); ?>"><?php echo $category[0]->cat_name; ?></a>
                  <a class="blog-intro__link-title" href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                  </a>
                </div>
              </div>
            </div>

        <?php
          endwhile;

          // Restore original Query & Post Data
          wp_reset_query();
          wp_reset_postdata();
        }
        ?>

      </div>

      <!-- Navigation buttons -->
      <div class="blog-intro__nav">
        <div class="container">
          <div class="swiper-button-prev">
            <div class="btn btn-short btn-alt"><i class="i i-arrow-left"></i></div>
          </div>

          <div class="swiper-button-next">
            <div class="btn btn-short btn-alt"><i class="i i-arrow-right"></i></div>
          </div>
        </div>
      </div><!-- /.blog-intro__nav -->
    </div>

  </div><!-- /.blog-intro__content -->
</section><!-- /.blog-intro -->

<!-- Blog Preview -->
<section class="blog-preview">
  <div class="container container-full">
    <div class="row">

      <?php
      /**
       * Get Main Featured Posts (Intro)
       */
      $args = array(
        'category_name' => 'featured',
        'posts_per_page' => 3,
        'offset' => 0
      );

      $the_query = new WP_Query($args);

      if ($the_query->have_posts()) {
        while ($the_query->have_posts()) :
          setup_postdata($the_query->the_post());
          $post_date = get_the_date('j M, Y');
          $category = get_the_category();
          $post_id = get_the_ID();
      ?>

          <!-- Column -->
          <div class="col-sm-4">

            <!-- Blog Preview Item -->
            <div class="blog-preview__item">

              <div class="blog-preview__thumb">
                <a class="blog-preview__link-img" href="<?php the_permalink(); ?>">
                  <?php echo the_post_thumbnail('thumbnail', ['class' => 'blog-preview__img']); ?>
                </a>
              </div>

              <div class="blog-preview__dsc">
                <a class="blog-preview__link-rubric" href="<?php echo site_url('/category/' . $category[0]->slug . '/', 'https'); ?>"><?php echo $category[0]->cat_name; ?></a>

                <a class="blog-preview__link-title" href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>

                <!-- Post Stats -->
                <ul class="post-stats">
                  <li class="post-stats__item">
                    <svg width="14" height="13" class="post-stats__icon">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-heart"></use>
                    </svg>
                    <div class="post-stats__val">12</div>
                  </li>

                  <li class="post-stats__item">
                    <svg width="16" height="11" class="post-stats__icon">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-eye"></use>
                    </svg>
                    <div class="post-stats__val"><?php echo pvc_get_post_views(get_the_ID()); ?></div>
                  </li>

                  <li class="post-stats__item">
                    <svg width="16" height="15" class="post-stats__icon">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-share"></use>
                    </svg>
                    <div class="post-stats__val">4</div>
                  </li>
                </ul>


                <?php
                // Check Author and show/hide author block
                $author_id = get_post_field('post_author', $post_id);
                $author_role = get_user_role($author_id);
                if ($author_role !== 'administrator') :
                ?>
                  <!-- Post Author -->
                  <a class="post-author-sm" href="<?php the_permalink(); ?>">
                    <div class="post-author-sm__thumb"><?php echo get_avatar(get_the_author_meta('ID')); ?></div>
                    <div class="post-author-sm__name"><?php the_author() ?></div>
                  </a>
                <?php endif; ?>

              </div>
            </div>
          </div><!-- /.col-sm-4 -->
      <?php
        endwhile;

        // Restore original Query & Post Data
        wp_reset_query();
        wp_reset_postdata();
      }
      ?>

    </div>
  </div>
</section><!-- /.blog-preview -->


<!-- Blog Section -->
<div class="blog-section  ptop-40">
  <div class="container container-full">
    <div class="row">
      <div class="col-md-8">

        <!-- Featured Posts -->
        <div class="featured-posts">

          <!-- Slider main container -->
          <div class="swiper-container featured-posts__slider">

            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">

              <?php
              /**
               * Get Featured Posts
               */
              $args = array(
                'category_name' => 'featured',
                'posts_per_page' => 5,
                'offset' => 0
              );

              $the_query = new WP_Query($args);

              if ($the_query->have_posts()) {
                while ($the_query->have_posts()) :
                  setup_postdata($the_query->the_post());
                  $post_date = get_the_date('j M, Y');
                  $category = get_the_category();
              ?>


                  <!-- Slides -->
                  <div class="swiper-slide">
                    <!-- Featured Posts Item -->
                    <div class="featured-posts__item">
                      <div class="featured-posts__content">
                        <div class="featured-posts__badge">
                          <a class="featured-posts__link" href="<?php the_permalink(); ?>">#<?php echo $category[0]->cat_name; ?></a>
                          <div class="featured-posts__text">
                            <?php the_title(); ?>
                          </div>
                        </div>

                        <div class="featured-posts__action">
                          <a class="btn btn-icon" href="<?php the_permalink(); ?>">
                            <span class="btn__text">View</span>
                            <i class="i i-arrow-right"></i>
                          </a>
                        </div>
                      </div>

                      <div class="featured-posts__thumb">
                        <?php echo the_post_thumbnail('large'); ?>
                      </div>
                    </div><!-- /.featured-posts__item -->
                  </div>


              <?php
                endwhile;

                // Restore original Query & Post Data
                wp_reset_query();
                wp_reset_postdata();
              }
              ?>

            </div>

            <!-- Navigation buttons -->
            <div class="featured-posts__nav">
              <div class="swiper-button-prev">
                <div class="btn btn-short btn-alt"><i class="i i-arrow-left"></i></div>
              </div>

              <div class="swiper-button-next">
                <div class="btn btn-short btn-alt"><i class="i i-arrow-right"></i></div>
              </div>
            </div>
          </div>

        </div><!-- /.featured-posts -->


        <!-- Recent Posts -->
        <div class="recent-posts">
          <h2 class="t2 t2-alert">Recent Posts</h2>

          <?php get_template_part('template-parts/blog/posts-scope', 'none'); ?>

          <div class="recent-posts__action">
            <a class="btn btn-icon" href="<?php echo site_url('/latest-articles/', 'https'); ?>">
              <span class="btn__text">See All Recent Posts</span>
              <i class="i i-arrow-right"></i>
            </a>
          </div>

        </div><!-- /.recent-posts -->

      </div>

      <!-- Blog Aside -->
      <div class="col-md-4">
        <aside class="blog-aside">

          <?php get_template_part('template-parts/blog/aside-newsletter', 'none'); ?>
          <?php get_template_part('template-parts/blog/aside-freebies', 'none'); ?>

        </aside><!-- /.blog-aside -->
      </div>
    </div>
  </div>
</div><!-- /.blog-section -->

<?php get_template_part('template-parts/blog/posts-similar', 'none'); ?>

<!-- Creative Talks -->
<section class="creative-posts">
  <div class="container container-full">
    <div class="row">
      <div class="col-md-8">

        <!-- Recent Posts -->
        <div class="recent-posts">
          <h2 class="t2 t2-alert">Creative Talks</h2>

          <?php get_template_part('template-parts/blog/posts-scope', 'none'); ?>

          <div class="recent-posts__action">
            <a class="btn btn-icon" href="<?php echo site_url('/latest-articles/', 'https'); ?>">
              <span class="btn__text">See All Recent Posts</span>
              <i class="i i-arrow-right"></i>
            </a>
          </div>
        </div><!-- /.recent-posts -->

      </div>

      <div class="col-md-4">
        <aside class="blog-aside">
          <?php get_template_part('template-parts/blog/aside-popular', 'none'); ?>
        </aside><!-- /.blog-aside -->
      </div>

    </div>
  </div>
</section><!-- /.bsc-creative -->




<?php
get_footer();
