<?php

/**
 * The template for displaying all single posts
 *
 *#single-post
 *
 * @package climbings
 */

get_header('lg');
?>


<!-- Post Main -->
<main class="post-main">
  <div class="container container-full">
    <div class="row">
      <div class="col-md-8">

        <?php
        while (have_posts()) :
          the_post();
          $post_date = get_the_date('j M, Y');

          $classThumb = '';
          if (has_post_thumbnail()) {
            $classThumb = ' thumb';
          }

          $category = get_the_category();
        ?>

          <header class="post-header">
            <div class="post-header__pict">
              <?php climbings_post_thumbnail(); ?>
              <div class="post-header__shadow"></div>
            </div>

            <div class="post-header__dsc<?php echo $classThumb; ?>">
              <a class="post-header__rubric" href="<?php echo site_url('/category/' . $category[0]->slug . '/', 'https'); ?>"><?php echo $category[0]->cat_name; ?></a>

              <?php the_title('<h1 class="post-header__title">', '</h1>'); ?>


              <?php

              /**
               * Check Author and show/hide author block
               */
              global $post;
              $post_author_id = $post->post_author;
              $author_role = get_user_role($post_author_id);

              if ($author_role !== 'administrator') :
              ?>
                <div class="post-info">

                  <!-- Post Stats -->
                  <ul class="post-stats">
                    <li class="post-stats__item">
                      <svg width="14" height="13" class="post-stats__icon">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-heart-alt"></use>
                      </svg>
                      <div class="post-stats__val">12</div>
                    </li>

                    <li class="post-stats__item">
                      <svg width="16" height="11" class="post-stats__icon">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-eye-alt"></use>
                      </svg>
                      <div class="post-stats__val"><?php echo pvc_get_post_views(get_the_ID()); ?></div>
                    </li>

                    <li class="post-stats__item">
                      <svg width="16" height="15" class="post-stats__icon">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-share-alt"></use>
                      </svg>
                      <div class="post-stats__val">4</div>
                    </li>
                  </ul>

                  <!-- Post Author -->
                  <a class="post-author-sm" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">
                    <div class="post-author-sm__thumb">
                      <?php echo get_avatar(get_the_author_meta('ID'), 200); ?>
                    </div>
                    <div class="post-author-sm__name"><?php the_author(); ?></div>
                  </a>
                </div>
              <?php endif; ?>

            </div>
          </header>


          <!-- Article -->
          <article class="article">
            <div class="container container-sm">

              <?php
              the_content(sprintf(
                wp_kses(
                  /* translators: %s: Name of current post. Only visible to screen readers */
                  __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'climbings'),
                  array(
                    'span' => array(
                      'class' => array(),
                    ),
                  )
                ),
                get_the_title()
              ));

              wp_link_pages(array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'climbings'),
                'after'  => '</div>',
              ));
              ?>

            </div>
          </article>


          <?php

          /**
           * Check Author and show/hide author block
           */
          global $post;
          $post_author_id = $post->post_author;
          $author_role = get_user_role($post_author_id);

          if ($author_role !== 'administrator') :
          ?>

            <!-- Author Info -->
            <section class="section-author">
              <div class="container container-sm">
                <div class="author__row">

                  <!-- Author Photo -->
                  <div class="author__avatar">
                    <?php echo get_avatar(get_the_author_meta('ID'), 140); ?>
                  </div>


                  <div class="author__dsc">

                    <!-- Author's name -->
                    <div class="author__title">
                      <a class="author__name" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">
                        <?php the_author() ?>
                      </a>
                    </div>

                    <!-- About author -->
                    <div class="author__about">
                      <p>

                      </p>
                    </div>

                    <!-- Author Social Link -->
                    <!-- <div class="social-attr">
                    <ul class="social-attr__list">
                      <li class="social-attr__item">
                        <a href="./index.html" class="social-attr__link" title="Social Link" target="_blank" rel="nofollow noopener noreferrer">
                          <i class="i-soc-sm i-soc-sm-alt i-soc-sm-fb"></i>
                        </a>
                      </li>
                      <li class="social-attr__item">
                        <a href="./index.html" class="social-attr__link" title="Social Link" target="_blank" rel="nofollow noopener noreferrer">
                          <i class="i-soc-sm i-soc-sm-alt i-soc-sm-tw"></i>
                        </a>
                      </li>
                      <li class="social-attr__item">
                        <a href="./index.html" class="social-attr__link" title="Social Link" target="_blank" rel="nofollow noopener noreferrer">
                          <i class="i-soc-sm i-soc-sm-alt i-soc-sm-in"></i>
                        </a>
                      </li>
                      <li class="social-attr__item">
                        <a href="./index.html" class="social-attr__link" title="Social Link" target="_blank" rel="nofollow noopener noreferrer">
                          <i class="i-soc-sm i-soc-sm-alt i-soc-sm-inst"></i>
                        </a>
                      </li>
                    </ul>
                  </div> -->



                  </div>
                </div>
              </div>
            </section>
          <?php endif; ?>



        <?php
        endwhile; // End of the loop.
        ?>







      </div><!-- /.col-md-8 -->

      <!-- Blog Aside-->
      <div class="col-md-4">
        <aside class="blog-aside">
          <?php get_template_part('template-parts/blog/aside-newsletter', 'none'); ?>
          <?php get_template_part('template-parts/blog/aside-freebies', 'none'); ?>
          <div class="mtop-40"></div>
          <?php get_template_part('template-parts/blog/aside-popular', 'none'); ?>
        </aside><!-- /.blog-aside -->
      </div><!-- /.col-md-4 -->

    </div><!-- /.row -->
  </div><!-- /.container -->


  <?php get_template_part('template-parts/blog/posts-similar', 'none'); ?>
</main><!-- /.post-main -->


<?php
get_footer();
