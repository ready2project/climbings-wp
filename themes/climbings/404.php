<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package climbings
 */

get_header('404');
?>

<section class="error-404 not-found">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="content-404">
          <div class="text-404">
            <header class="page-header">
              <h1 class="title">404</h1>
              <h2 class="title-sm">Not Found</h2>
            </header>
            <div class="err-msg">
              <p>Return to <a href="<?php echo get_home_url(); ?>">Home</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
