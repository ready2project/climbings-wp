<!-- Similar Posts -->
<section class="similar-posts">
  <div class="container container-full">
    <div class="similar-posts__wrapper">

      <header class="similar-posts__header">
        <h2 class="t2 t2-alt">Recent Posts</h2>
        <a class="btn" href="<?php echo site_url('/latest-articles/', 'https'); ?>">
          <span class="btn__text">See All Recent Posts</span>
          <i class="i i-arrow-right"></i>
        </a>
      </header>

      <div class="similar-posts__content posts-scope-alt posts-scope-sm">

        <div class="swiper-container similar-posts__slides">

          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">

            <?php
            $args = array(
              'post_type' => 'post',
              'posts_per_page' => 8,
            );
            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) {
              while ($the_query->have_posts()) :
                setup_postdata($the_query->the_post());
                $post_date = get_the_date('j M, Y');

                $excerpt = wp_strip_all_tags(get_the_excerpt(), true);
                $excerpt = substr($excerpt, 0, 140);
                $excerpt = substr($excerpt, 0, strrpos($excerpt, ' ')) . '...';

                $category = get_the_category();

                $post_id =  get_the_ID();
            ?>

                <!-- Slides -->
                <div class="swiper-slide">

                  <!-- Posts Scope Item -->
                  <div class="posts-scope__item">
                    <div class="posts-scope__thumb">
                      <?php

                      if (get_the_post_thumbnail($post_id) != '') {
                        echo the_post_thumbnail('large');
                      } else {
                        echo '<img src="';
                        echo get_post_image();
                        echo '" alt="Climbings | Post Thumbnail" />';
                      }

                      ?>
                    </div>
                    <a class="posts-scope__rubric" href="<?php echo site_url('/category/' . $category[0]->slug . '/', 'https'); ?>"><?php echo $category[0]->cat_name; ?></a>
                    <h3 class="posts-scope__title">
                      <a class="posts-scope__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                    <p class="posts-scope__excerpt">
                      <?php echo $excerpt; ?>
                    </p>

                    <div class="posts-scope__dsc">
                      <!-- Post Stats -->
                      <ul class="post-stats">
                        <li class="post-stats__item">
                          <svg width="14" height="13" class="post-stats__icon">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-heart-alt"></use>
                          </svg>
                          <div class="post-stats__val">12</div>
                        </li>

                        <li class="post-stats__item">
                          <svg width="16" height="11" class="post-stats__icon">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-eye-alt"></use>
                          </svg>
                          <div class="post-stats__val"><?php echo pvc_get_post_views(get_the_ID()); ?></div>
                        </li>

                        <li class="post-stats__item">
                          <svg width="16" height="15" class="post-stats__icon">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-share-alt"></use>
                          </svg>
                          <div class="post-stats__val">4</div>
                        </li>
                      </ul>

                      <!-- Post Author -->
                      <a class="post-author-sm" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>">
                        <div class="post-author-sm__thumb"><?php echo get_avatar(get_the_author_meta('ID')); ?></div>
                        <div class="post-author-sm__name"><?php the_author() ?></div>
                      </a>
                    </div><!-- /.posts-scope__dsc -->
                  </div><!-- /.posts-scope__item -->
                </div>

            <?php
              endwhile;

              // Restore original Query & Post Data
              wp_reset_query();
              wp_reset_postdata();
            }
            ?>

          </div><!-- /.similar-posts__slides -->


        </div>
      </div>

      <!-- Navigation buttons -->
      <div class="similar-posts__nav">
        <div class="similar-posts__btn-prev swiper-button-prev">
          <div class="btn btn-short btn-alt"><i class="i i-arrow-left"></i></div>
        </div>

        <div class="similar-posts__btn-next swiper-button-next">
          <div class="btn btn-short btn-alt"><i class="i i-arrow-right"></i></div>
        </div>
      </div>
    </div>
</section><!-- /.similar-posts -->