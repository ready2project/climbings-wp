<!-- Featured Posts -->
<section class="featured-posts inverted">
  <div class="container container-full">


    <?php


    /**
     * Get Main Featured Post
     */

    $args = array(
      'category_name' => 'featured',
      'posts_per_page' => 1,
      'offset' => 0
    );

    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) :
        setup_postdata($the_query->the_post());
        $post_date = get_the_date('j M, Y');
    ?>

        <div class="featured-posts__item">

          <!-- Featured Post Picture -->
          <div class="featured-posts__picture">
            <a href="<?php the_permalink(); ?>">
              <?php echo the_post_thumbnail('full'); ?>
            </a>
          </div>

          <!-- Featured Post Header -->
          <div class="featured-posts__header">

            <!-- Featured Post Title -->
            <a class="featured-posts__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

            <!-- Featured Post Credentials -->
            <div class="featured-posts__credentials">
              <div class="featured-posts__author-thumb"><?php echo get_avatar(get_the_author_meta('ID')); ?></div>
              <a class="featured-posts__author" href="<?php the_permalink(); ?>">
                <?php the_author() ?>
              </a>
              <p class="featured-posts__date"><?php echo $post_date; ?></p>
            </div>
          </div>
        </div>

    <?php
      endwhile;

      // Restore original Query & Post Data
      wp_reset_query();
      wp_reset_postdata();
    }
    ?>



    <div class="row featured-posts-secondary">

      <?php

      /**
       * Get Secondary Featured Post
       */

      $args = array(
        'category_name' => 'featured',
        'posts_per_page' => 2,
        'offset' => 1
      );

      $the_query = new WP_Query($args);

      if ($the_query->have_posts()) {
        while ($the_query->have_posts()) :
          setup_postdata($the_query->the_post());
          $post_date = get_the_date('j M, Y');

      ?>

          <!-- Featured Post Item -->
          <div class="col-md-6">


            <div class="featured-posts__item">

              <!-- Featured Post Picture -->
              <div class="featured-posts__picture">
                <a href="<?php the_permalink(); ?>">
                  <?php echo the_post_thumbnail('full'); ?>
                </a>
              </div>

              <!-- Featured Post Header -->
              <div class="featured-posts__header">

                <!-- Featured Post Title -->
                <a class="featured-posts__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                <!-- Featured Post Credentials -->
                <div class="featured-posts__credentials">
                  <div class="featured-posts__author-thumb"><?php echo get_avatar(get_the_author_meta('ID')); ?></div>
                  <a class="featured-posts__author" href="<?php the_permalink(); ?>">
                    <?php the_author() ?>
                  </a>
                  <p class="featured-posts__date"><?php echo $post_date; ?></p>
                </div>
              </div>
            </div>

          </div>

      <?php
        endwhile;

        // Restore original Query & Post Data
        wp_reset_query();
        wp_reset_postdata();
      }
      ?>
    </div><!-- end. Row -->

  </div>
</section>