<!-- Aside Freebies -->
<div class="aside-freebies">
  <div class="aside-freebies__header">
    <h3 class="aside-freebies__title">
      Free Resources we
      <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/aside/aside-heart-sm.svg" alt="" width="40" height="35">
      around the web
    </h3>
  </div>

  <ul class="aside-freebies-list">

    <?php
    /**
     * Get Featured Posts
     */
    $args = array(
      'category_name' => 'freebies',
      'posts_per_page' => 6,
      'offset' => 0
    );

    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) {
      while ($the_query->have_posts()) :
        setup_postdata($the_query->the_post());
        $post_date = get_the_date('j M, Y');
    ?>

        <!-- Item -->
        <li class="aside-freebies-list__item">
          <a class="aside-freebies-list__link" href="<?php the_permalink(); ?>">
            <span class="aside-freebies-list__thumb">
              <?php echo the_post_thumbnail('thumbnail'); ?>
            </span>

            <span class="aside-freebies-list__text">
              <span class="aside-freebies-list__title">
                <?php the_title(); ?>
              </span>
              <span class="btn btn-link">
                <span class="btn__text">View</span>
                <i class="i i-arrow-right"></i>
              </span>
            </span>
          </a>
        </li>


    <?php
      endwhile;

      // Restore original Query & Post Data
      wp_reset_query();
      wp_reset_postdata();
    }
    ?>

  </ul>

  <div class="aside-freebies__action">
    <a class="btn btn-alt btn-icon" href="<?php echo site_url('/category/freebies/', 'https'); ?>">
      <span class="btn__text">See All Recent Freebies</span>
      <i class="i i-arrow-right"></i>
    </a>
  </div>
</div><!-- /.aside-freebies -->