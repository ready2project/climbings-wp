<!-- Latest Insights -->
<section class="latest-insights">
  <div class="container">
    <header class="latest-insights__header">
      <h2 class="title title-sm">Latest Insights</h2>
      <div class="subtitle">
        Climbings is a global digital agency and design studio providing innovative, modern and aesthetic solution for your online business &amp; presence
      </div>
    </header>
  </div>
  <div class="container latest-insights-container">
    <div class="insight-slider">

      <!-- Additional required wrapper -->
      <div class="swiper-wrapper">

        <?php
        $args = array(
          'post_type' => 'post',
          'posts_per_page' => 8,
        );
        $the_query = new WP_Query($args);

        if ($the_query->have_posts()) {
          while ($the_query->have_posts()) :
            setup_postdata($the_query->the_post());
            $post_date = get_the_date('j M, Y');

            $excerpt = wp_strip_all_tags(get_the_excerpt(), true);
            $excerpt = substr($excerpt, 0, 140);
            $excerpt = substr($excerpt, 0, strrpos($excerpt, ' ')) . '...';

            $category = get_the_category();

            $post_id =  get_the_ID();
        ?>

            <!-- Slides -->
            <div class="swiper-slide">

              <!-- Posts Scope Item -->
              <div class="feedback">
                <div class="posts-scope__thumb">
                  <?php

                  if (get_the_post_thumbnail($post_id) != '') {
                    echo the_post_thumbnail('large');
                  } else {
                    echo '<img src="';
                    echo get_post_image();
                    echo '" alt="Climbings | Post Thumbnail" />';
                  }

                  ?>
                </div>

                <div class="feedback__content">
                  <h3 class="feedback__title">
                    <a class="feedback__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </h3>
                  <div class="latest-insights__text">
                    <p>
                      <?php echo $excerpt; ?>
                    </p>
                  </div>
                  <div class="feedback__date">
                    <?php the_date('d M, Y', '<p>', '</p>'); ?>
                  </div>
                </div>
              </div><!-- /.feedback -->
            </div>

        <?php
          endwhile;

          // Restore original Query & Post Data
          wp_reset_query();
          wp_reset_postdata();
        }
        ?>
      </div><!-- /.similar-posts__slides -->

      <!-- Navigation buttons -->
      <div class="swiper-button-prev">
        <div class="btn btn-short"><i class="i i-arrow-left"></i></div>
      </div>

      <div class="swiper-button-next">
        <div class="btn btn-short"><i class="i i-arrow-right"></i></div>
      </div>
    </div><!-- /.insight-slider -->
  </div>
</section><!-- /.similar-posts -->