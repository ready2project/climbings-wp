<!-- Aside Popular Now -->
<div class="aside-popular">
  <div class="aside-popular__header">
    <h3 class="t2 t2-alert">Popular Now</h3>
  </div>

  <div class="posts-scope-sm posts-scope-full aside-popular__content">


    <?php

    $args = array(
      'header' => '',
      'header_start' => '<h3 class="posts-scope__title">',
      'header_end' => '</h3>',
      'limit' => 5,
      'range' => 'last7days',
      'order_by' => 'avg',
      'post_html' => '

      <!-- Posts Scope Item -->
      <div class="posts-scope__item">
        <div class="posts-scope__thumb">
          {thumb}
        </div>
        <a class="posts-scope__rubric" href="{url}">{category}</a>
        <h3 class="posts-scope__title">
          <a class="posts-scope__link" href="{url}">{text_title}</a>
        </h3>
        <p class="posts-scope__excerpt">
          {summary}
        </p>
      </div><!-- /.posts-scope__item -->
      ',
      'thumbnail_width' => 370,
      'thumbnail_height' => 240,
      'excerpt_length' => 55,
    );

    wpp_get_mostpopular($args);

    ?>

  </div><!-- /.aside-popular__content -->

  <div class="aside-popular__action">
    <a class="btn btn-icon" href="<?php echo site_url('/latest-articles/', 'https'); ?>">
      <span class="btn__text">See All Recent Posts</span>
      <i class="i i-arrow-right"></i>
    </a>
  </div><!-- /.aside-popular__action -->
</div><!-- /.aside-popular -->