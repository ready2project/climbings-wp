

<!-- Inspiration Updates -->
<section class="recent-posts">
  <div class="container">

    <!-- Recent Posts Header -->
    <header class="recent-posts__header">
      <h2 class="title-sm recent-posts__title">Inspiration</h2>
      <a class="btn btn-primary btn-icon" href="<?php echo get_home_url(); ?>/category/inspiration/"><i class="icon icon-arrow-right"></i>Explore more</a>
    </header>


    <!-- Recent Posts Content -->
    <div class="recent-posts__content">
      <div class="row">


        <div class="row recent-posts__secondary">

          <?php

          /**
           * Get Inspiration Post
           */

          $args = array(
            'category_name' => 'inspiration',
            'posts_per_page' => 4,
            'offset' => 6
          );

          $the_query = new WP_Query($args);

          if ($the_query->have_posts()) {
            while ($the_query->have_posts()) :
              setup_postdata($the_query->the_post());
              $post_date = get_the_date('j M, Y');

          ?>

              <div class="col-md-6">

                <!-- Post Block -->
                <div class="post-block">
                  <a class="post-block__header" href="<?php the_permalink(); ?>">
                    <span class="post-block__thumb">
                      <?php echo the_post_thumbnail('full'); ?>
                    </span>
                  </a>

                  <div class="post-block__content">
                    <a class="post-block__link" href="<?php the_permalink(); ?>">
                      <span class="post-block__details">
                        <span class="post-block__title"><?php the_title(); ?></span>

                        <?php
                        $excerpt = wp_strip_all_tags(get_the_excerpt(), true);
                        $excerpt = substr($excerpt, 0, 150);
                        $excerpt = substr($excerpt, 0, strrpos($excerpt, ' ')) . '...';
                        ?>
                        <span class="post-block__text">
                          <?php echo $excerpt; ?>
                        </span>
                      </span>
                    </a>

                    <div class="post-block__credentials">
                      <div class="post-block-author__thumb"><?php echo get_avatar(get_the_author_meta('ID')); ?></div>
                      <div class="post-block-author__block">
                        <a class="post-block-author" href="<?php the_permalink(); ?>"><?php the_author() ?></a>
                        <p class="post-block__date"><?php echo $post_date; ?></p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

          <?php
            endwhile;

            // Restore original Query & Post Data
            wp_reset_query();
            wp_reset_postdata();
          }
          ?>

        </div>

      </div>
    </div>


    <!-- Recent Posts Footer -->
    <footer class="recent-posts__footer">
      <a class="btn btn-primary btn-icon" href="<?php echo get_home_url(); ?>/category/inspiration/"><i class="icon icon-arrow-right"></i>Explore more</a>
    </footer>
  </div>
</section>
