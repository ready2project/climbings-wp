<!-- Categories -->
<div class="aside-categories">
  <div class="aside-categories__body">
    <div class="aside-categories__header">
      <h3 class="aside-categories__title">Tags</h3>
    </div>

    <div class="aside-categories__content">
      <ul class="aside-categories__list">

        <?php
        $categories = get_tags();
        $categories_list = '';
        foreach ($categories as $category) {

          if ($category->name !== 'Uncategorized') {
            $categories_list .=
              '
              <li class="aside-categories__item">
                <a href=" '  . site_url('/tag/' . $category->slug . '/', 'https') . ' ">' . $category->name . '</a>
              </li>
              ';
          }
        }
        echo $categories_list;
        ?>
      </ul>
    </div>
  </div>
</div><!-- /.aside-categories -->