<!-- Aside Newsletter -->
<div class="aside-newsletter">
  <div class="aside-newsletter__body">
    <div class="aside-newsletter__header">
      <h3 class="aside-newsletter__title">Signup for our monthly newsletter</h3>
    </div>

    <div class="aside-newsletter__dsc">
      <p>
        Our only metric of success is when we see you succeed. Get started now and let's grow together.
      </p>
    </div>
  </div>

  <div class="aside-newsletter__action">

    <?php
      echo do_shortcode('[newsletter_form form="1"]');
    ?>

  </div>
</div><!-- /.aside-newsletter -->