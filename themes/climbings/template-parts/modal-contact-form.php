<!-- Contact Modal-->
<div class="modal contact-modal" id="contact-modal" role="dialog" tabindex="-1">

  <!-- Contact Modal | Window-->
  <div class="modal-wnd">

    <!-- Contact Modal | Hide Button -->
    <a class="modal-hide" href="#" aria-label="Hide" data-target="contact-modal"><span aria-hidden="true">&times;</span></a>

    <div class="contact-modal__thanks hide">
      <p>Thank you. It has been sent.</p>
    </div>

    <!-- Modal Body -->
    <div class="modal-wnd__body">
      <!-- Column -->
      <div class="contact-modal-col">
        <!-- Modal Header -->
        <div class="modal__header">
          <h3 class="modal__title">Contact Us</h3>
          <div class="modal_dsc">
            <p>
              Our only metric of success is when we see you succeed. Get started now and let's grow together.
            </p>
          </div>

        </div><!-- /.modal__header -->
        <div class="contact-modal__img">
          <svg width="351" height="353">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#modal-contact"></use>
          </svg>
        </div>
      </div><!-- /.contact-modal-col -->

      <!-- Modal Form -->
      <div class="contact-modal-form">

        <div class="contact-modal-form__thanks">
          <h2>Thank you</h2>
          <p>Your message has been successfully sent!</p>
        </div>

        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <input name="cmf_name" id="cmf_name" type="text" class="contact-modal-form__input floating-input" placeholder=" " maxlength="200">
          <label class="contact-modal-form__label floating-label" for="cmf_name">Your Full Name</label>
        </div>

        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <input name="cmf_email" id="cmf_email" type="text" class="contact-modal-form__input floating-input" placeholder=" " inputmode="email" maxlength="200">
          <label class="contact-modal-form__label floating-label" for="cmf_email">Enter your Email</label>
        </div>


        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <input name="cmf_company" id="cmf_company" type="text" class="contact-modal-form__input floating-input" placeholder=" " maxlength="200">
          <label class="contact-modal-form__label floating-label" for="cmf_company">Company</label>
        </div>

        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <input name="cmf_tel" id="cmf_tel" type="text" class="contact-modal-form__input floating-input tel-input" placeholder=" " inputmode="tel" maxlength="200">
          <label class="contact-modal-form__label floating-label" for="cmf_tel">Your Phone Number</label>
        </div>

        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <textarea name="cmf_msg" id="cmf_msg" type="text" class="contact-modal-form__text floating-input" placeholder=" "></textarea>
          <label class="contact-modal-form__label floating-label" for="cmf_msg"> Write your message here…</label>
        </div>

        <!-- Modal Form Field -->
        <div class="contact-modal-form__field">
          <?php
            $c7shortcode = get_field('shortcode', 'option');
            echo do_shortcode($c7shortcode);
          ?>
        </div>
      </div><!-- /.contact-modal__form -->
    </div><!-- /.modal-wnd__body -->
  </div>
</div><!-- /#contact-modal.modal contact-modal -->