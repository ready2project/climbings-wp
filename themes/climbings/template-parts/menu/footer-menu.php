<?php

/**
 * Template part for displaying Footer Menu
 *
 * @package climbings
 */


class Walker_Footer_Menu extends Walker
{

  // Tell Walker where to inherit it's parent and id values
  var $db_fields = array(
    'parent' => 'menu_item_parent',
    'id'     => 'db_id'
  );

  /**
   * At the start of each element, output a <li> and <a> tag structure.
   *
   * Note: Menu objects include url and title properties, so we will use those.
   */
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    $output .= sprintf(
      "\n<li class='footer-menu__item'><a class='footer-menu__link' href='%s'>%s</a></li>\n",
      $item->url,
      $item->title
    );
  }
}
?>

<!-- Footer Menu -->
<nav class="footer-nav">
  <?php wp_nav_menu(array(
    'menu' => 'Footer Menu',
    'container' => '',
    'menu_class' => 'footer-menu',
    'walker'  => new Walker_Footer_Menu()
  )); ?>
</nav><!-- /.footer-nav -->