<?php

/**
 * Template part for displaying Social Menu
 *
 * @package climbings
 */


class Walker_Social_Menu extends Walker
{

  // Tell Walker where to inherit it's parent and id values
  var $db_fields = array(
    'parent' => 'menu_item_parent',
    'id'     => 'db_id'
  );

  /**
   * At the start of each element, output a <li> and <a> tag structure.
   *
   * Note: Menu objects include url and title properties, so we will use those.
   */
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {

    $icon  = get_field('icon', $item->ID);

    $output .= sprintf(
      PHP_EOL . "<li class='cta-social__item'><a class='cta-social__link' href='%s' rel='nofollow noopener noreferrer' target='_blank'><img loading='lazy' src='%s' alt='%s' width='41' height='41'></a></li>" . PHP_EOL,
      $item->url,
      $icon['url'],
      $item->title
    );
  }
}

?>

<!-- Social Menu -->
<nav class="nav-social">
  <?php wp_nav_menu(array(
    'menu' => 'Social Menu',
    'container' => '',
    'menu_class' => 'cta-social__menu',
    'walker'  => new Walker_Social_Menu()
  )); ?>
</nav>
