<?php

/**
 * The template for displaying the Works section
 *
 * @package climbings
 */
?>

<?php if (current_user_can('administrator')) : ?>

  <!-- Mobile Works -->
  <div class="mobile-works">
    <div class="container">

      <header class="works__header">
        <h2 class="title">Works</h2>
      </header>

      <!-- Slider main container -->
      <div class="swiper-container mobile-works-slider">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
          <!-- Slides -->


          <?php
          $args = array(
            'post_type' => 'works',
          );
          $the_query = new WP_Query($args);

          if ($the_query->have_posts()) {
            $counter = 1;

            while ($the_query->have_posts()) :
              setup_postdata($the_query->the_post());
          ?>

              <!-- Mobile Slider Item -->
              <div class="swiper-slide">

                <!-- Tabs Card -->
                <div class="mobile-works-slide">

                  <!-- Card Header -->
                  <div class="works-card__header">
                    <h3 class="page-card__title">
                      <?php the_title(); ?>
                    </h3>

                    <div class="works-card__subtitle">
                      <p>
                        <?php the_field('subtitle'); ?>
                      </p>
                    </div>

                  </div>

                  <!-- Card Thumbnail -->
                  <div class="works-card__thumb">
                    <?php
                    $url = get_the_post_thumbnail_url();
                    ?>

                    <a href="<?php echo get_permalink($post->ID); ?>">
                      <?php if ($url) : ?>
                        <img loading="lazy" src="<?php echo $url; ?>" alt="Climbings | Works |  <?php the_title(); ?>">
                      <?php endif; ?>
                    </a>
                  </div>

                </div><!-- /.works-card -->


              </div>

          <?php
            endwhile;

            // Restore original Query & Post Data
            wp_reset_query();
            wp_reset_postdata();
          }
          ?>

        </div>
      </div>

    </div>
  </div><!-- /.mobile-works -->

  <!-- Works -->
  <section class="works">
    <div class="container">

      <!-- Tabs -->
      <div class="works-tabs">

        <!-- Tabs Nav -->
        <div class="works-tabs__nav">
          <header class="works__header">
            <h2 class="title">Works</h2>
          </header>

          <!-- Tabs List -->
          <ul class="works-tabs-list">


            <?php
            $args = array(
              'post_type' => 'works',
            );
            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) {
              $counter = 1;

              while ($the_query->have_posts()) :
                setup_postdata($the_query->the_post());

                if ($counter === 1) {
                  $active = ' active';
                } else {
                  $active = '';
                }
            ?>
                <li class="works-tabs-list__item">
                  <a class="works-tabs-list__link<?php echo $active; ?>" href="#works-tab-<?php the_ID(); ?>">
                    <?php the_title(); ?>
                  </a>
                </li>
            <?php

                $counter++;
              endwhile;

              // Restore original Query & Post Data
              wp_reset_query();
              wp_reset_postdata();
            }
            ?>

          </ul>

          <a class="btn btn-primary btn-lg btn-icon" href="<?php echo home_url('/works/'); ?>">
            <span class="btn__text">See All Projects</span>
            <i class="i i-arrow-right"></i>
          </a>

        </div><!-- /.works-tabs__nav -->

        <!-- Tabs Content -->
        <div class="works-tabs__content">
          <?php
          $args = array(
            'post_type' => 'works',
          );
          $the_query = new WP_Query($args);

          if ($the_query->have_posts()) {
            $counter = 1;

            while ($the_query->have_posts()) :
              setup_postdata($the_query->the_post());

              if ($counter === 1) {
                $active = ' active';
              } else {
                $active = '';
              }
          ?>

              <!-- Tabs Card -->
              <div class="works-card<?php echo $active; ?>" id="works-tab-<?php the_ID(); ?>">

                <!-- Card Header -->
                <div class="works-card__header">
                  <?php
                  $logo = get_field('logo');
                  if (!empty($logo['url'])) : ?>
                    <div class="works-card__logo">
                      <img src="<?php echo $logo['url']; ?>" alt="">
                    </div>
                  <?php endif; ?>

                  <h3 class="page-card__title">
                    <?php the_title(); ?>
                  </h3>

                  <div class="works-card__subtitle">
                    <p>
                      <?php the_field('subtitle'); ?>
                    </p>
                  </div>

                  <div class="works-card__dsc">
                    <?php the_field('description'); ?>
                  </div>
                </div>

                <!-- Card Content -->
                <div class="works-card__content">
                  <h3 class="caps-heading">Responsibilities</h3>

                  <?php
                  $resp = get_field('responsibilities');
                  ?>

                  <!-- Card Tags -->
                  <ul class="works-card-tags__list">
                    <?php foreach ($resp as $value) : ?>
                      <li class="works-card-tags__item">
                        <?php echo $value; ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>

                  <!-- Card Action -->
                  <div class="works-card__action">
                    <a class="btn btn-lg btn-icon" href="<?php echo get_permalink($post->ID); ?>" data-target="work-<?php the_ID(); ?>">
                      <span class="btn__text">View the Project</span>
                      <i class="i i-arrow-right"></i>
                    </a>
                  </div>

                  <!-- Card Thumbnail -->
                  <div class="works-card__thumb">
                    <?php
                    $url = get_the_post_thumbnail_url();
                    ?>

                    <?php if ($url) : ?>
                      <img loading="lazy" src="<?php echo $url; ?>" alt="Climbings | Works |  <?php the_title(); ?>">
                    <?php endif; ?>
                  </div>

                </div>
              </div><!-- /.works-card -->

          <?php
              $counter++;
            endwhile;

            // Restore original Query & Post Data
            wp_reset_query();
            wp_reset_postdata();
          }
          ?>

        </div><!-- /.works-tabs__content -->
      </div><!-- /.works-tabs -->

    </div>
  </section><!-- /.works -->
<?php endif; ?>
