<div class="intro-scene-box">
  <svg width="1100" height="970" viewBox="0 0 1100 970">
    <defs>
      <filter id="filter-11" x="-7.9%" y="-2.6%" width="111.9%" height="108.1%">
        <feOffset dx="-10" dy="10" in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="7.5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.0887510927 0" in="shadowBlurOuter1" />
      </filter>
      <linearGradient id="Gradient-0" x1="0" y1=".6" x2=".9" y2=".5">
        <stop offset="0" stop-color="#4df0fb" />
        <stop offset="1" stop-color="#1bd8e9" />
      </linearGradient>
      <linearGradient id="Gradient-1" x1=".1" y1=".6" x2="1" y2=".5">
        <stop offset="0" stop-color="#f2522d" />
        <stop offset=".5" stop-color="#f97449" />
        <stop offset="1" stop-color="#eb3b1d" />
      </linearGradient>
      <linearGradient id="Gradient-2" x1="442" y1="334.5" x2="397" y2="337" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#000" />
        <stop offset="1" stop-color="#000" stop-opacity="0" />
      </linearGradient>
      <linearGradient id="Gradient-3" x1="1" y1=".5" x2="0" y2=".5">
        <stop offset="0" stop-color="#1e3941" />
        <stop offset=".6" stop-color="#54aab1" />
        <stop offset="1" stop-color="#4ea5ad" />
      </linearGradient>
      <linearGradient id="Gradient-4" x1="1" y1=".5" x2="0" y2=".5">
        <stop offset="0" stop-color="#1e3941" />
        <stop offset=".6" stop-color="#54aab1" />
        <stop offset="1" stop-color="#4ea5ad" />
      </linearGradient>
      <linearGradient id="Gradient-5" x1="0" y1=".5" x2="1" y2=".5">
        <stop offset="0" stop-color="#4f4f4f" />
        <stop offset="1" stop-color="#121630" />
      </linearGradient>
      <linearGradient id="Gradient-6" x1="1" y1=".5" x2="0" y2=".6">
        <stop offset="0" stop-color="#e83c23" />
        <stop offset="1" stop-color="#db2a1e" />
      </linearGradient>
      <linearGradient id="Gradient-7" x1="445" y1="338" x2="484" y2="334.5" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#000" />
        <stop offset="1" stop-color="#000" stop-opacity="0" />
      </linearGradient>
      <linearGradient id="Gradient-8" x1=".5" y1="0" x2=".5" y2="1">
        <stop offset="0" stop-color="#69728a" stop-opacity="0" />
        <stop offset="1" stop-color="#0e132f" stop-opacity="0" />
        <stop offset="1" stop-color="#69728a" stop-opacity=".85" />
      </linearGradient>
      <linearGradient id="Gradient-9" x1="1" y1=".6" x2="0" y2=".6">
        <stop offset="0" stop-color="#69728a" stop-opacity="0" />
        <stop offset=".9" stop-color="#0e132f" stop-opacity="0" />
        <stop offset="1" stop-color="#69728a" stop-opacity=".8" />
      </linearGradient>
      <linearGradient id="Gradient-10" x1="1" y1=".6" x2="0" y2=".6">
        <stop offset="0" stop-color="#69728a" stop-opacity="0" />
        <stop offset=".1" stop-color="#0e132f" stop-opacity="0" />
        <stop offset=".9" stop-color="#0e132f" stop-opacity="0" />
        <stop offset="1" stop-color="#69728a" stop-opacity=".88" />
      </linearGradient>
    </defs>
    <g transform="translate(518.2,422.2) translate(-800,-600)">
      <g id="Vector-Book" fill="none" fill-rule="evenodd" opacity="0" transform="translate(813.4,787.5) scale(.5,.5) translate(-440.7,-350.5)">
        <g transform="translate(.5,0)">
          <path d="M474.5 622.5l389.6-34.6c8.8-0.8 15.3-8.5 14.5-17.3l-47.7-554.7c-0.8-8.8-8.5-15.3-17.3-14.5l-391.1 33.6l52 587.5Z" fill="#fff" opacity=".7" />
          <path d="M468.5 622.5l389.6-34.6c8.8-0.8 15.3-8.5 14.5-17.3l-47.7-554.7c-0.8-8.8-8.5-15.3-17.3-14.5l-391.1 33.6l52 587.5Z" fill="#fff" />
          <path d="M468.5 622.5l389.6-34.6c8.8-0.8 15.3-8.5 14.5-17.3l-31.7-368.7c-86.3 5.1-173.8 11.4-262.4 19.1c-46.9 4-94.9 9.4-144.1 16l34.1 385.5Z" fill="#FFEC62" opacity=".5" />
          <path d="M416.5 35l-373.6 32.5c-19.3 1.6-33.5 18.6-31.9 37.9l45.6 524.2c1.3 15.4 14.9 26.8 30.3 25.5l381.6-32.6l-52-587.5Z" fill="#fff" />
          <path d="M408.5 35l-388.6 33.8c-11 .9-19.1 10.6-18.2 21.6l47.8 550.2c.8 9.3 9.1 16.3 18.4 15.5l392.6-33.6l-52-587.5Z" fill="#fff" opacity=".8" />
          <path d="M416.5 35l-392.6 34.1c-8.8 .8-15.3 8.5-14.5 17.3l16.1 185.4l406.3-63.8l-15.3-173Z" fill="#FFEC62" />
          <path id="Path-10-Copy" d="M43.4 478.1l14.2 163.5c.8 8.8 8.5 15.3 17.3 14.5l393.6-33.6l-25.5-288.4l-399.6 144Z" fill="#f3552f" />
          <ellipse id="Oval" fill="#fff" rx="33" ry="33" transform="translate(118.5,590) scale(0,0)" />
          <g filter="url(#filter-11)" fill="#000">
            <path d="M416.5 35c-35.2 7.2-74.8 16.9-119 29c-76.9 21.1-157.7 44.4-242.5 69.8c-10.8 3.2-17.4 14.1-15.2 25.2l102.6 533.4c.9 4.8 5.4 8 10.2 7.2c56.1-9 120.9-23.1 194.4-42.1c77.3-20 117.8-31.7 121.5-35" />
          </g>
          <g fill="#fff">
            <path d="M416.5 35c-35.2 7.2-74.8 16.9-119 29c-76.9 21.1-157.7 44.4-242.5 69.8c-10.8 3.2-17.4 14.1-15.2 25.2l102.6 533.4c.9 4.8 5.4 8 10.2 7.2c56.1-9 120.9-23.1 194.4-42.1c77.3-20 117.8-31.7 121.5-35" />
          </g>
          <path fill="url(#Gradient-0)" d="M85.5 397l23.8 123.4l342.6-85.6l-11.7-131.5Z" />
          <path d="M416.5 35c-35.2 7.2-74.8 16.9-119 29c-78.2 21.5-160.5 45.2-246.8 71.1c-8.3 2.5-13.4 10.9-11.8 19.5l46.6 242.4c77.7-22 158.4-43.9 242-65.5c34.7-9 72.3-17.7 112.9-26.3l-23.9-270.2Z" fill="url(#Gradient-1)" />
          <path d="M416.5 35c-40.3 8.9-72.1 16.5-95.2 22.6c-5.2 1.4-13.1 3.5-23.8 6.4l84.5 584.4l86.5-25.9l-52-587.5Z" fill="url(#Gradient-2)" opacity=".2" />
        </g>
        <path id="Path-13" d="M81.9 169.5l146.4-42.1c6.9-2 14.2 2.1 16.2 9c.1 .4 .2 .7 .2 1.1c1.6 7.9-3.1 15.7-10.9 18l-146.9 42.5c-6.9 2-14.1-2-16.1-8.9c-0.1-0.4-0.2-0.9-0.3-1.3c-1.4-8.2 3.5-16 11.4-18.3Z" fill="#fff" transform="translate(72,187) scale(0,0) translate(-72,-187)" />
        <path id="Path-13-Copy" d="M83.2 206.5l97-27.9c4.6-1.3 9.4 1.3 10.7 6c.1 .2 .1 .4 .2 .7c1 5.2-2.1 10.4-7.3 11.9l-97.3 28.1c-4.5 1.4-9.3-1.3-10.6-5.8c-0.1-0.3-0.2-0.6-0.2-0.9c-1-5.4 2.3-10.6 7.5-12.1Z" fill="#f8bda6" transform="translate(75,219.5) scale(0,0) translate(-75,-219.5)" />
        <path id="Path-13-Copy-2" d="M88.2 231.5l64.2-18.4c4.6-1.3 9.4 1.3 10.7 6c.1 .2 .2 .4 .2 .7c1.1 5.2-2.1 10.4-7.2 11.9l-64.6 18.6c-4.5 1.4-9.3-1.3-10.6-5.8c-0.1-0.3-0.2-0.6-0.2-0.9c-1-5.4 2.3-10.6 7.5-12.1Z" fill="#f8bda6" transform="translate(81,244) scale(0,0) translate(-81,-244)" />
        <path d="M355.6 81.5l34.9-9.3c2.4-0.6 4.8 .8 5.5 3.2c0 0 0 .1 0 .2c.6 2.6-1 5.1-3.5 5.8l-35.1 9.4c-2.4 .6-4.8-0.8-5.4-3.1c-0.1-0.2-0.1-0.3-0.1-0.4c-0.5-2.6 1.1-5.2 3.7-5.8Z" fill="#2F71C7" />
        <path d="M355.6 81.5l34.9-9.3c2.4-0.6 4.8 .8 5.5 3.2c0 0 0 .1 0 .2c.6 2.6-1 5.1-3.5 5.8l-35.1 9.4c-2.4 .6-4.8-0.8-5.4-3.1c-0.1-0.2-0.1-0.3-0.1-0.4c-0.5-2.6 1.1-5.2 3.7-5.8Z" fill="#2F71C7" />
        <path id="Path-13-Copy-5" d="M162.2 311.6l225.7-62.6c4.7-1.3 9.5 1.5 10.8 6.1c0 .2 .1 .5 .1 .7c1.1 5.2-2.1 10.4-7.3 11.8l-226 62.8c-4.6 1.3-9.4-1.4-10.7-6c0-0.3-0.1-0.6-0.1-0.8c-1-5.4 2.3-10.6 7.5-12Z" fill="#f8bda6" transform="translate(155,323) scale(0,0) translate(-155,-323)" />
        <path id="Path-13-Copy-6" d="M167.2 334.5l166.2-46.5c4.6-1.3 9.5 1.5 10.8 6.1c0 .2 .1 .4 .1 .7c1.1 5.2-2.1 10.4-7.3 11.8l-166.5 46.8c-4.6 1.3-9.4-1.4-10.7-6c0-0.3-0.1-0.6-0.1-0.8c-1-5.4 2.3-10.6 7.5-12.1Z" fill="#f8bda6" transform="translate(160.5,347.5) scale(0,0) translate(-160.5,-347.5)" />
        <path id="Path-13-Copy-7" d="M128.3 401.2l269.1-70.8c4.7-1.2 9.5 1.6 10.8 6.3c0 .2 .1 .4 .1 .6c1 5.2-2.2 10.3-7.3 11.7l-269.4 71.1c-4.7 1.2-9.5-1.6-10.8-6.3c0-0.3-0.1-0.5-0.1-0.7c-1-5.4 2.3-10.5 7.6-11.9Z" fill="url(#Gradient-3)" opacity="0" transform="translate(264.5,375.2) translate(-264.5,-375.2)" />
        <path id="Path-13-Copy-8" d="M133.3 424.2l209.6-54.8c4.7-1.2 9.6 1.6 10.8 6.3c0 .2 .1 .4 .1 .6c1 5.2-2.2 10.3-7.3 11.6l-209.9 55.2c-4.7 1.2-9.6-1.6-10.8-6.3c0-0.3-0.1-0.5-0.1-0.7c-1-5.4 2.3-10.5 7.6-11.9Z" fill="url(#Gradient-4)" opacity="0" transform="translate(239.8,406.2) translate(-239.8,-406.2)" />
        <path id="Path-13-Copy-9" d="M136 455.2l155.1-41.9c2.5-0.7 5.1 .8 5.8 3.3c0 .2 .1 .3 .1 .4c.5 2.8-1.2 5.5-3.9 6.3l-155.4 42c-2.5 .7-5-0.8-5.7-3.3c-0.1-0.1-0.1-0.3-0.1-0.4c-0.5-2.9 1.3-5.7 4.1-6.4Z" fill="#51d7e3" opacity="0" />
        <path id="Path-13-Copy-10" d="M139 467.5l112.2-29.3c2.5-0.7 5.1 .8 5.8 3.4c0 .1 0 .2 0 .3c.6 2.8-1.1 5.5-3.9 6.2l-112.4 29.6c-2.5 .6-5.1-0.9-5.7-3.4c-0.1-0.1-0.1-0.3-0.1-0.4c-0.5-2.9 1.3-5.6 4.1-6.4Z" fill="#51d7e3" opacity="0" />
        <path id="Path-13-2" d="M109.6 251.8l127.6-34.5c13-3.5 26.4 4.2 30 17.2c.1 .6 .2 1.1 .3 1.7c2.9 14.5-5.9 28.7-20.2 32.6l-128.5 35.2c-12.9 3.5-26.2-4.1-29.7-16.9c-0.2-0.8-0.4-1.5-0.5-2.2c-2.6-14.8 6.5-29.2 21-33.1Z" fill="#fff" transform="translate(89,285.5) scale(0,0) translate(-89,-285.5)" />
        <ellipse id="Oval-2" fill="#fff" rx="24.5" ry="27" transform="translate(125.5,344) scale(0,0)" />
        <path d="M122 499.5l-65.5-344.5l345.5-95.5l43 359c-41.6 8.9-83.2 18.5-125 29c-65.9 16.5-131.9 33.9-198 52Z" stroke="#fff" stroke-width="2" />
        <ellipse id="Oval-3" fill="#fccf01" opacity=".5" rx="54" ry="67.5" transform="translate(396,541.5) scale(0,0)" />
        <ellipse id="Oval-4" fill="#fccf01" opacity=".5" rx="68" ry="67.5" transform="translate(298,567.5) scale(0,0)" />
        <ellipse id="Oval-5" fill="#fccf01" rx="68" ry="67.5" transform="translate(206,587.5) scale(0,0)" />
        <ellipse fill="url(#Gradient-5)" rx="40" ry="39.7" transform="translate(0,0) translate(502,141.5)" />
        <path fill="#fff" d="M484 597.5l-31.5-344.5l372.5-31l31 337.5Z" />
        <path id="Path-12" d="M469 622.5l60.5-163.5l76 20l31.5-111l56-4l70-101.5l86 28.5l24.1 279.5c.8 8.9-5.7 16.6-14.5 17.4c-96.9 8.3-195.2 18-295.1 29.1c-31.1 2.8-62.6 4.6-94.5 5.5Z" fill="#2F71C7" />
        <path id="Path-12-Copy" d="M469 622.5l67.5-103.5l76 11l33.5-66.5l54-3.5l75-69.5l84.5 12.5l13.7 167.6c.7 8.8-5.8 16.5-14.6 17.3c-96.8 8.3-195.2 18-295.1 29.1c-31.1 2.8-62.6 4.6-94.5 5.5Z" fill="url(#Gradient-6)" />
        <path id="Path-13-Copy-11" d="M651.5 530.4l173-21.3c5.2-0.6 10 2.9 10.9 8.1c.9 4.9-2.4 9.6-7.3 10.4c-0.2 0-0.3 .1-0.4 .1l-172.9 21.6c-5.3 .7-10.2-3-11.1-8.2c-0.9-5 2.4-9.7 7.4-10.6c.1 0 .3 0 .4-0.1Z" fill="#e79577" transform="translate(739.5,529.2) scale(0,0) translate(-739.5,-529.2)" />
        <path id="Path-13-Copy-12" d="M653.5 554.5l121.6-13.9c5.3-0.6 10.2 2.9 11.2 8.2c1 4.7-2.1 9.3-6.9 10.2c-0.2 .1-0.4 .1-0.6 .1l-122 14.3c-5.3 .6-10.2-3-11.1-8.3c-0.9-5 2.4-9.6 7.3-10.5c.2 0 .3-0.1 .5-0.1Z" fill="#e79577" transform="translate(716,557) scale(0,0) translate(-716,-557)" />
        <path d="M564.6 112l105.4-7.9c5.3-0.3 10 3.4 10.9 8.6c.8 4.7-2.3 9.1-7 9.9c-0.3 .1-0.5 .1-0.8 .1l-105.2 8.1c-5.5 .4-10.3-3.3-11.2-8.7c-0.9-4.7 2.3-9.2 7-10c.3-0.1 .6-0.1 .9-0.1Z" fill="#626262" />
        <path d="M567.5 136.9l70.6-6.1c5.3-0.5 10.2 3.2 11.2 8.5c.9 4.6-2.1 9.1-6.7 10c-0.3 0-0.6 .1-0.9 .1l-70.8 6.3c-5.4 .5-10.3-3.3-11.2-8.6c-0.9-4.8 2.3-9.3 7.1-10.1c.2-0.1 .5-0.1 .7-0.1Z" fill="#626262" />
        <ellipse id="Oval-7" fill="#fff" rx="32.5" ry="32.5" transform="translate(529.5,559.5) scale(0,0)" />
        <ellipse id="Oval-8" fill="#fff" rx="32.5" ry="32.5" transform="translate(603.5,553.5) scale(0,0)" />
        <path fill="url(#Gradient-7)" opacity=".2" d="M417 35l52 587.5l40-2l-54.5-588.5Z" />
        <path d="M758.7 28.5l35.8-1.3c2.5-0.1 4.8 1.8 5 4.4c.3 2.4-1.5 4.5-3.8 4.7c-0.1 0-0.2 0-0.3 .1l-36 1.4c-2.6 .1-4.8-1.9-5-4.5c-0.2-2.4 1.6-4.6 4.1-4.8c0 0 .1 0 .2 0Z" fill="#2F71C7" transform="translate(777,32.5) rotate(-4) translate(-777,-32.5)" />
      </g>
      <g id="Phone-annd-Code" fill="none" fill-rule="evenodd" opacity="0" transform="translate(677.7,310) translate(0,0) scale(1.5,1.5) translate(0,0)">
        <g opacity=".95" transform="translate(241.1,26.8) rotate(16.5)">
          <path d="M41.7 8.4l187.6 23.9c17.9 2.3 31.4 17.4 31.5 35.4l3.5 522.7c.1 19.9-15.9 36.1-35.8 36.2c-2.5 .1-5-0.2-7.5-0.7l-185.6-38.6c-16.6-3.5-28.5-18-28.7-34.9l-5.6-507.9c-0.2-19.9 15.8-36.2 35.6-36.4c1.7 0 3.3 .1 5 .3Z" fill="#121630" />
          <path d="M35.4 50.8l185.6-38.6c19.4-4 38.5 8.5 42.6 28c.5 2.4 .7 5 .7 7.5l-3.5 522.7c-0.1 18-13.6 33.2-31.5 35.4l-187.6 23.9c-19.7 2.5-37.8-11.4-40.3-31.1c-0.2-1.7-0.3-3.3-0.3-5l5.6-507.9c.2-16.9 12.1-31.4 28.7-34.9Z" fill="url(#Gradient-8)" transform="translate(132.6,319.1) scale(1,-1) translate(-132.6,-319.1)" />
          <path d="M35.5 50.7l185.2-40.2c19.4-4.2 38.6 8.1 42.8 27.5c.6 2.6 .8 5.3 .8 7.9l-3.3 521.7c-0.1 17.9-13.3 32.9-31 35.4l-187.5 26.2c-19.7 2.7-37.9-11-40.6-30.7c-0.3-1.8-0.4-3.6-0.4-5.4l5.6-507.7c.2-16.7 11.9-31.2 28.4-34.7Z" fill="url(#Gradient-9)" transform="translate(132.8,318) scale(1,-1) translate(-132.8,-318)" />
          <path d="M44.3 12.2l185.6 38.6c16.6 3.5 28.5 18 28.7 34.9l5.6 508.1c.2 19.9-15.8 36.2-35.6 36.4c-1.7 0-3.3-0.1-4.9-0.3l-187.3-23.2c-18-2.2-31.5-17.4-31.6-35.5l-3.8-523.5c-0.1-19.8 15.9-36.1 35.8-36.2c2.5 0 5 .2 7.5 .7Z" fill="url(#Gradient-10)" transform="translate(132.6,319.1) scale(-1,-1) translate(-132.6,-319.1)" />
        </g>
        <g transform="translate(98,51)">
          <path d="M6.8 505.3l137.8-483.1c3-10.6 14.1-16.8 24.7-13.8c.9 .3 1.8 .6 2.7 1l166.7 74.4c13.3 6 20.1 20.8 16 34.7l-147 497c-2.9 10.1-13.5 15.8-23.6 12.9c-1.2-0.4-2.5-0.9-3.6-1.6l-158.6-85.8c-12.8-6.9-19.1-21.8-15.1-35.7Z" fill="#101748" />
          <g fill="#18236a" transform="translate(24,38)">
            <g id="Code-2" opacity="0" transform="translate(150,280.1) translate(-149.2,-280.1)">
              <path id="Rectangle" d="M226.3 174.2l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-2" d="M202.3 252.3l24.9 11.8l-8.9 28.7l-25-11.7Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-4" d="M181.6 198.7l24.9 11.7l-8.9 28.8l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-5" d="M204.2 120.5l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-6" d="M228.6 45.2l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6" d="M185.3 187.4l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-2" d="M229.1 164.8l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-3" d="M252.6 87.6l19.8-22.6l4.3 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-4" d="M275.2 138.4l19.8-22.6l4.3 33.9Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-9" rx="12.7" ry="16" transform="translate(-0.8,0) translate(144.8,373.8)" />
              <path id="Rectangle-7" d="M104.8 338l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-8" d="M82.7 284.4l24.9 11.7l-8.9 28.7l-25-11.7Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-9" d="M147.7 313.5l24.9 11.8l-8.9 28.7l-25-11.7Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-10" d="M107.2 209l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-5" d="M63.9 351.2l19.7-22.6l4.3 33.9Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-11" d="M47.9 272.1l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-12" d="M59.2 233.5l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-13" d="M72.3 196.8l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-6" d="M29 339l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-7" d="M131.2 251.4l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-8" d="M153.8 302.2l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-14" d="M110.9 427.5l25 11.7l-9 28.8l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-9" d="M114.7 416.2l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-10" rx="12.7" ry="16" transform="translate(-0.8,0) translate(108.1,485.9)" />
              <path id="Path-6-10" d="M83.2 531.1l19.7-22.6l4.3 33.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-15" d="M222.5 305.1l25 11.7l-9 28.8l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-16" d="M245.6 227.9l24.9 11.7l-8.9 28.8l-25-11.8Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-11" rx="12.7" ry="16" transform="translate(-0.8,0) translate(203.7,48)" />
              <path id="Rectangle-17" d="M85 153.5l25 11.7l-9 28.8l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-18" d="M107.6 75.3l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-19" d="M118.9 36.7l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-20" d="M172.6 104.5l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-21" d="M132.1 0l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-11" d="M88.8 142.2l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-12" d="M132.6 119.6l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-13" d="M143.9 80l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Path-6-14" d="M110.9 195.8l19.8-22.5l4.2 33.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-15" d="M178.7 93.2l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-12" rx="12.7" ry="16" transform="translate(-0.8,0) translate(163.7,424.7)" />
              <ellipse id="Oval-13" rx="12.7" ry="16" transform="translate(-0.8,0) translate(197.6,307.9)" />
              <path id="Rectangle-22" d="M166.5 364.4l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-16" d="M203.7 371.9l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-23" d="M187.7 418.1l24.9 11.7l-8.9 28.7l-25-11.7Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-24" d="M165.5 493.4l25 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-17" d="M168.8 484.9l19.8-22.6l4.3 33.9Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-25" d="M119.4 519.8l25 11.7l-9 28.7l-24.9-11.7Z" transform="translate(-0.8,0)" />
              <path id="Rectangle-26" d="M16.3 387.9l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-0.8,0)" />
              <path id="Path-6-18" d="M20.1 376.6l19.7-22.6l4.3 33.9Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-14" rx="12.7" ry="16" transform="translate(-0.8,0) translate(13.5,446.3)" />
              <ellipse id="Oval-15" rx="12.7" ry="16" transform="translate(-0.8,0) translate(69,385.1)" />
              <ellipse id="Oval-16" rx="12.7" ry="16" transform="translate(-0.8,0) translate(46.4,460.4)" />
              <path id="Path-6-19" d="M74.2 445.4l19.8-22.6l4.2 33.9Z" transform="translate(-0.8,0)" />
              <ellipse id="Oval-17" rx="12.7" ry="16" transform="translate(-0.8,0) translate(266.3,210)" />
            </g>
            <g id="Code-1" opacity="0" transform="translate(176.1,296.6) translate(-136.8,-276.8)">
              <ellipse rx="12.7" ry="16" transform="translate(-39.4,-19.8) translate(243.7,285.3)" />
              <ellipse rx="12.7" ry="16" transform="translate(-39.4,-19.8) translate(300.2,93.2)" />
              <path d="M215.5 81.9l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M269.1 149.7l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M240.4 125.2l19.8-22.6l4.2 33.9Z" transform="translate(-39.4,-19.8)" />
              <path d="M207.4 241l19.8-22.6l4.3 33.9Z" transform="translate(-39.4,-19.8)" />
              <ellipse rx="12.7" ry="16" transform="translate(-39.4,-19.8) translate(178.7,257.1)" />
              <path d="M94 245.8l24.9 11.7l-8.9 28.7l-25-11.7Z" transform="translate(-39.4,-19.8)" />
              <path d="M107.6 328.6l19.8-22.6l4.3 33.9Z" transform="translate(-39.4,-19.8)" />
              <path d="M118.9 289.1l19.8-22.6l4.3 33.9Z" transform="translate(-39.4,-19.8)" />
              <ellipse rx="12.7" ry="16" transform="translate(-39.4,-19.8) translate(169.8,164.8)" />
              <path d="M129.8 129l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M156.1 42.4l19.8-22.6l4.2 33.9Z" transform="translate(-39.4,-19.8)" />
              <path d="M149.1 182.7l24.9 11.7l-8.9 28.8l-25-11.8Z" transform="translate(-39.4,-19.8)" />
              <ellipse rx="12.7" ry="16" transform="translate(-39.4,-19.8) translate(141.1,500)" />
              <path d="M172.6 353.1l19.8-22.6l4.2 33.9Z" transform="translate(-39.4,-19.8)" />
              <path d="M143 442.5l24.9 11.8l-8.9 28.7l-25-11.7Z" transform="translate(-39.4,-19.8)" />
              <path d="M200.4 380.4l24.9 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M145.3 562.1l19.8-22.6l4.2 33.9Z" transform="translate(-39.4,-19.8)" />
              <path d="M48.3 403l25 11.8l-9 28.7l-24.9-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M93 378.5l25 11.8l-8.9 28.7l-25-11.8Z" transform="translate(-39.4,-19.8)" />
              <path d="M52.8 514.2l19.7-22.6l4.3 33.9Z" transform="translate(-39.4,-19.8)" />
            </g>
          </g>
        </g>
        <g id="App-Screen" transform="translate(93.1,42) translate(0,0) scale(1.015,1.015) translate(0,0)">
          <path d="M11.1 503.3l57.7-201.8l203.7 107.4l-60.6 204.6c-3.1 10.5-14.1 16.6-24.7 13.4c-1.3-0.3-2.6-0.9-3.8-1.5l-156.5-84.6c-13.4-7.3-19.9-22.8-15.8-37.5Z" fill="#fff" />
          <path d="M68.8 301.2l79.4-278c3.2-11.1 14.7-17.6 25.8-14.4c1 .3 1.9 .6 2.8 1l164.6 73.4c13.9 6.2 21 21.7 16.7 36.3l-85.5 289.1l-203.8-107.4Z" fill="#2F71C7" />
          <path id="Rectangle-36" d="M263.9 129.3c5.9 3 8.8 9.8 7 16.2l-43.2 149c-1.5 5-6.7 7.9-11.7 6.4c-0.6-0.1-1.1-0.4-1.7-0.6c-5.9-3.1-8.8-9.8-6.9-16.2l43.1-149.1c1.5-5 6.7-7.8 11.7-6.4c.6 .2 1.1 .4 1.7 .7Z" fill="#fff" transform="translate(206.8,301.3) scale(0,0) translate(-206.8,-301.3)" />
          <path id="Rectangle-Copy" d="M178.9 199.1c5.9 3.1 8.8 9.8 6.9 16.2l-15.3 50.6c-1.6 5-6.9 7.9-11.9 6.3c-0.6-0.1-1.1-0.3-1.6-0.6c-5.9-3-8.8-9.8-6.9-16.2l15.3-50.5c1.5-5.1 6.9-7.9 11.9-6.4c.6 .2 1.1 .4 1.6 .6Z" fill-opacity=".4" fill="#d8d8d8" transform="translate(149.6,272.6) scale(0,0) translate(-149.6,-272.6)" />
          <path id="Rectangle-Copy-2" d="M214.9 185.6c5.8 3 8.7 9.8 6.7 16.1l-23.9 77.2c-1.6 5.1-7 7.9-12.1 6.4c-0.5-0.2-1-0.4-1.5-0.7c-5.9-3-8.7-9.8-6.8-16.1l24-77.2c1.6-5.1 7-7.9 12-6.3c.6 .1 1.1 .3 1.6 .6Z" fill-opacity=".6" fill="#d8d8d8" transform="translate(176.7,285.7) scale(0,0) translate(-176.7,-285.7)" />
          <g transform="translate(0,448.8)">
            <g fill="#fccf01">
              <path d="M196.5 171.3c5.6-9.6 8.3-23.6 8.2-42c-0.2-27.7-23.5-56-53.2-56c-29.6 0-50.5 27.1-39.2 69c.2 .7 .4 1.7 .8 3l83.4 26Z" transform="translate(156.9,122.3) rotate(11) translate(-156.9,-122.3)" />
              <path d="M135.4 138.7c5.9-17.1 8.9-29.3 8.8-36.8c-0.2-27.7-24-61.5-53.7-61.5c-29.6 0-50.3 29.9-39 71.8c.2 .7 28.2 9.5 83.9 26.5Z" fill-opacity=".5" transform="translate(96.3,89.5) rotate(11) translate(-96.3,-89.5)" />
              <path d="M63.4 105.9c9.8 2.9 19.1 5.8 28.1 8.6c-12.9-24.8-19.4-39.9-19.4-45.1c-0.2-27.7-24-61.5-53.7-61.5c-1 0-2.8-1.1-2.9 0c-10.3 84.7-9.6 80.4 47.9 98Z" fill-opacity=".3" transform="translate(50.9,60.9) rotate(11) translate(-50.9,-60.9)" />
            </g>
            <g fill="#fff" transform="translate(48.8,45.4)">
              <g transform="translate(42,37.8) translate(-40.4,-35.2)">
                <path d="M9.9 3.2l67.8 35.3c3.6 1.9 5.5 6 4.4 10c-0.8 3-3.9 4.8-6.9 4c-0.4-0.1-0.8-0.3-1.1-0.5l-67.9-35.2c-3.6-1.9-5.4-6.1-4.4-10c.8-3 3.9-4.8 6.9-4c.4 .1 .8 .2 1.2 .4Z" transform="translate(-1.5,-2.6)" />
                <path d="M21.1 33.4l50.1 26c3.4 1.9 5.2 5.9 4.1 9.7c-0.8 2.9-3.8 4.6-6.7 3.8c-0.4-0.1-0.8-0.3-1.1-0.4l-50-26.1c-3.5-1.8-5.3-5.9-4.2-9.6c.8-3 3.8-4.7 6.8-3.9c.3 .1 .7 .3 1 .5Z" transform="translate(-1.5,-2.6)" />
              </g>
              <ellipse id="Oval-26" rx="21.9" ry="27.6" transform="translate(104.3,73.5) scale(0,0)" />
            </g>
          </g>
          <g transform="translate(43.6,332.6)">
            <path d="M34.9 2.9l169.6 86.3c4.5 2.3 6.7 7.5 5.3 12.3l-26 87.4c-0.8 2.7-3.8 4.3-6.5 3.5c-0.3-0.1-0.7-0.2-1-0.4l-170.2-89.2c-3.6-1.8-5.3-5.9-4.2-9.7l25.6-87.1c.8-2.7 3.7-4.3 6.5-3.5c.3 .1 .6 .2 .9 .4Z" fill="#00d4f0" opacity=".85" />
            <g transform="translate(88.7,103.5) translate(-86.9,-87.8)">
              <path id="Path-5" d="M66.7 25l28.7 53.7l25.5 16.2l11 33.9l29.7 5.7l14.1 56.8l-168.8-88.1c-4-2.1-6-6.8-4.7-11.1l22.8-76.5l41.7 9.4Z" fill="#001ef0" transform="translate(-1.8,-15.6)" />
              <path id="Path-5-Copy" d="M57.9 59.9l27.1 39.7l30.2 18.7l8.3 17.8l33.9 13l18.3 42.2l-168.8-88.1c-4-2.1-6-6.7-4.7-11.1l12.9-43.6l42.8 11.4Z" fill="#e43b0b" transform="translate(-1.8,-15.6)" />
            </g>
            <g transform="translate(43.8,44.8)">
              <g transform="translate(42,37.8) translate(-40.4,-35.2)">
                <path d="M9.9 3.2l67.8 35.3c3.6 1.9 5.5 6 4.4 10c-0.8 3-3.9 4.8-6.9 4c-0.4-0.1-0.8-0.3-1.1-0.5l-67.9-35.2c-3.6-1.9-5.4-6.1-4.4-10c.8-3 3.9-4.8 6.9-4c.4 .1 .8 .2 1.2 .4Z" fill="#b2bbf6" transform="translate(-1.5,-2.6)" />
                <path d="M21.1 33.4l50.1 26c3.4 1.9 5.2 5.9 4.1 9.7c-0.8 2.9-3.8 4.6-6.7 3.8c-0.4-0.1-0.8-0.3-1.1-0.4l-50-26.1c-3.5-1.8-5.3-5.9-4.2-9.6c.8-3 3.8-4.7 6.8-3.9c.3 .1 .7 .3 1 .5Z" fill="#b2bbf6" transform="translate(-1.5,-2.6)" />
              </g>
              <ellipse id="Oval-27" fill="#fff" rx="21.9" ry="27.6" transform="translate(104.3,73.5) scale(0,0)" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
</div>