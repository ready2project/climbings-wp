<?php

/**
 * The template for displaying the CTA Animated section
 *
 * Contains the closing of the #content div and all content after.
 *
 *
 * @package climbings
 */

$cta_header = get_field('cta_header', 'option');
$cta_subheader = get_field('cta_subheader', 'option');
$cta_social_title = get_field('cta_social_title', 'option');
$cta_action_button = get_field('cta_action_button', 'option');
?>

<!-- CTA -->
<section class="cta" id="sceneCTA">
  <div class="container">
    <div class="row">
      <div class="col">
        <i class="i-arrow-cta i-arrow-cta-rotate i-arrow-cta-blue"></i>
        <!-- Scene -->
        <div class="cta__scene">

          <svg xmlns="http://www.w3.org/2000/svg" width="800" height="800" viewBox="0 0 800 800">
            <defs />
            <g transform="translate(455.3,127.2) translate(-640,0)">
              <g transform="translate(597,274.2) translate(-277,-274.2)">
                <g id="Sun" fill="#fccf01" fill-rule="evenodd" transform="translate(0,0) translate(0,0) translate(0,0)">
                  <ellipse rx="126.5" ry="126.5" fill="#ffec61" transform="translate(0,0) translate(126.5,126.5)" />
                </g>
                <g id="Tree" transform="translate(277.1,548.3) translate(-148.2,-306.4)">
                  <g fill="#c7c692" fill-rule="evenodd" transform="translate(273.5,240.6) scale(-1,1)">
                    <path id="_a0" d="M-6.9-76.5l-31.4 14l180.2 52l55.6-23c0 0-204.4-43-204.4-43Z" fill="#c7c692" opacity="0" transform="translate(84.3,6.8) scale(-1,1) translate(22.3,69.5)" style="mix-blend-mode: multiply;" />
                  </g>
                  <g id="Left-Branch" fill="none" fill-rule="evenodd" transform="translate(175.7,133.1) scale(0,0) translate(0,-81.2)">
                    <path id="Fill-31" fill="#02256c" d="M0 59.1l72.1-59.1l-63.8 75.9l-8.3-16.8" />
                    <path id="Fill-32" fill="#ffec61" d="M26.3 81.2l54.5-73.9l-8.7-7.3l-63.8 76l18 5.2" />
                  </g>
                  <g id="Main-branch" fill="none" fill-rule="evenodd" transform="translate(173.2,254.1) scale(0,0) translate(-31.1,-289.1)">
                    <path id="Fill-33" fill="#ffec61" d="M62.2 275.5l-17.4-275.5h-13.7v289.1l31.1-13.6" />
                    <path id="Fill-34" fill="#02256c" d="M0 275.5l17.5-275.5h13.6v289.1l-31.1-13.6" />
                  </g>
                  <g id="Right-Branch" fill="none" fill-rule="evenodd" transform="translate(178,167.7) scale(0,0) translate(-73.3,-77.3)">
                    <path id="Fill-64" fill="#ffec61" d="M73.3 53l-64.6-53l63.8 75.9l.8-22.9" />
                    <path id="Fill-65" fill="#02256c" d="M51.7 77.3l-51.7-70l8.7-7.3l63.8 76l-20.8 1.3" />
                  </g>
                  <g fill="none" fill-rule="evenodd" transform="translate(354,-197.2) scale(-1,1)">
                    <g id="Left-Tree" transform="translate(282.9,219) scale(0,0) translate(-75.4,-66.1)">
                      <path fill="#2f71c7" d="M0 63.5l48.8-63.5l-9.7 68.3l-39.1-4.8" />
                      <path fill="#6897d6" d="M48.8 0l79.1 14.6l-19 11.3l-60.1-25.9" />
                      <path fill="#ccdcf1" d="M108.9 25.9l-69.8 42.4l9.7-68.3l60.1 25.9" />
                      <path fill="#2f71c7" d="M127.9 14.6l23 64l-42-52.7l19-11.3" stroke="#6897d6" />
                      <path fill="#9ab8e4" d="M150.9 78.6l-111.8-10.3l69.8-42.4l42 52.7" />
                      <path fill="#2f71c7" d="M0 63.5l21 60.5l18.1-55.7l-39.1-4.8" stroke="#6897d6" stroke-width="2" />
                      <path fill="#9ab8e4" d="M21 124l89.6-31.3l-71.5-24.4l-18.1 55.7" />
                      <path fill="#2f71c7" d="M150.9 78.6l-40.6 53.7l.3-39.6l40.3-14.1" stroke="#6897d6" />
                      <path fill="#6897d6" d="M110.3 132.3l-27.9-16.1l28.2-23.5l-0.3 39.6" />
                      <path fill="#2f71c7" d="M82.4 116.2l-61.4 7.8l89.6-31.3l-28.2 23.5" stroke="#6897d6" stroke-width="2" />
                      <path fill="#2f71c7" d="M21 124l89.3 8.3l-27.9-16.1l-61.4 7.8" stroke="#6897d6" />
                      <path fill="#2f71c7" d="M150.9 78.6l-111.8-10.3l71.5 24.4l40.3-14.1" />
                    </g>
                    <g id="Mid-Tree" transform="translate(186.7,197.2) scale(0,0) translate(-110.3,-197.2)">
                      <g transform="translate(2.4,0)">
                        <path fill="#ccdcf1" d="M47.8 0l-47.8 70.3l38.1-18.6l9.7-51.7" />
                        <path fill="#ccdcf1" d="M47.8 0l90.8 18.5l12.7 18.6l-103.5-37.1" />
                        <path fill="#9ab8e4" d="M151.3 37.1l-113.2 14.6l9.7-51.7l103.5 37.1" />
                        <path fill="#6897d6" d="M138.6 18.5l50.8 40.1l-38.1-21.5l-12.7-18.6" stroke="#6897d6" />
                        <path fill="#2f71c7" d="M189.4 58.6l-18.5 71.3l-19.6-92.8l38.1 21.5" stroke="#6897d6" />
                        <path fill="#6897d6" d="M170.9 129.9l-132.8-78.2l113.2-14.6l19.6 92.8" />
                        <path fill="#6897d6" d="M0 70.3l18.5 88.8l19.6-107.4l-38.1 18.6" />
                        <path fill="#2f71c7" d="M18.5 159.1l152.4-29.2l-132.8-78.2l-19.6 107.4" stroke="#6897d6" stroke-width="2" />
                        <path fill="#2f71c7" d="M130.8 164l-1.9 29.8l-110.4-34.7l112.3 4.9" stroke="#6897d6" />
                        <path fill="#2f71c7" d="M128.9 193.8l42-63.9l-40.1 34.1l-1.9 29.8" />
                      </g>
                      <path fill="#2f71c7" d="M9.9 105.9l-9.9 65.9l61.8 .2l-40.8-12.9l-11.1-53.2" />
                      <path fill="#6897d6" d="M0 171.8l50.3 25.4l11.5-25.2l-61.8-0.2" />
                      <path fill="#2f71c7" d="M50.3 197.2l43.7-15.1l-32.2-10.1l-11.5 25.2" />
                      <path fill="#2f71c7" d="M184.3 87.4l36.4 37.1l-47.4 5.4l11-42.5" stroke="#6897d6" />
                      <path fill="#6897d6" d="M220.7 124.5l-27.9 51.7l-19.5-46.3l47.4-5.4" />
                      <path fill="#2f71c7" d="M192.8 176.2l-46.5-5.1l27-41.2l19.5 46.3" stroke="#6897d6" />
                      <path fill="#6897d6" d="M173.3 129.9l-40 34.1l-112.3-4.9l152.3-29.2" />
                    </g>
                    <g id="Right-Tree" transform="translate(108.3,287.2) rotate(1) scale(0,0) translate(-76.3,-67.2)">
                      <path fill="#9ab8e4" d="M49 1.3l-33.3 14.4l25.7 24.5l7.6-38.9" />
                      <path fill="#ccdcf1" d="M49 1.3l62.1-1.3l-69.7 40.2l7.6-38.9" />
                      <path fill="#2f71c7" d="M111.1 0l41.5 58.4l-37.7 7.5l-3.8-65.9" />
                      <path fill="#9ab8e4" d="M114.9 65.9l-73.5-25.7l69.7-40.2l3.8 65.9" />
                      <path fill="#2f71c7" d="M15.7 15.7l-15.7 67.8l41.4-43.3l-25.7-24.5" stroke="#6897d6" />
                      <path fill="#6897d6" d="M0 83.5l114.9-17.6l-73.5-25.7l-41.4 43.3" />
                      <path fill="#2f71c7" d="M152.6 58.4l-31.4 67.8l-6.3-60.3l37.7-7.5" stroke="#6897d6" stroke-width="2" />
                      <path fill="#2f71c7" d="M56.5 134.4l-37-18.9l-19.5-32l56.5 50.9" stroke="#6897d6" />
                      <path fill="#6897d6" d="M121.2 126.2l-64.7 8.2l58.4-68.5l6.3 60.3" />
                      <path fill="#2f71c7" d="M0 83.5l56.5 50.9l58.4-68.5l-114.9 17.6" stroke="#6897d6" stroke-width="2" />
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </svg>
        </div>

        <!-- CTA Header -->
        <header class="cta__header">
          <h2 class="title title-sm"><?php echo $cta_header; ?></h2>
          <h3 class="subtitle"><?php echo $cta_subheader; ?></h3>
        </header>

        <!-- CTA Action -->
        <div class="cta__action">

          <a class="btn btn-primary btn-icon modal-link" data-target="contact-modal" href="#">
            <span class="btn__text">Drop us a line</span>
            <i class="i i-arrow-right"></i>
          </a>
          <!-- <a class="btn btn-primary btn-icon" href="mailto:team@climbings.com?subject=Drop%20us%20a%20line">
              <span class="btn__text">Drop us a line</span>
              <i class="i i-arrow-right"></i>
            </a> -->
          <a href="https://climbings.com/get-started/" class="btn btn-icon">
            <span class="btn__text">Start a Project</span>
            <i class="i i-arrow-right"></i>
          </a>
        </div>


        <!-- CTA Social Menu-->
        <div class="cta-social">
          <!-- Social Header -->
          <div class="cta-social__header">
            <h3 class="cta-social__title"><?php echo $cta_social_title; ?></h3>
          </div>

          <!-- Social Menu -->
          <ul class="cta-social__menu">

            <!-- Item -->
            <li class="cta-social__item">
              <a href="https://dribbble.com/climbings" class="cta-social__link" target="_blank" title="Social Link" rel="nofollow noopener noreferrer">
                <svg width="35" height="35">
                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-soc-dribbble"></use>
                </svg>
              </a>
            </li>

            <!-- Item -->
            <li class="cta-social__item">
              <a href="https://www.behance.net/climbings" class="cta-social__link" target="_blank" title="Social Link" rel="nofollow noopener noreferrer">
                <svg width="35" height="35">
                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-soc-behance"></use>
                </svg>
              </a>
            </li>

            <!-- Item -->
            <li class="cta-social__item">
              <a href="http://instagram.com/climbingsstudio" class="cta-social__link" target="_blank" title="Social Link" rel="nofollow noopener noreferrer">
                <svg width="35" height="35">
                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-soc-inst"></use>
                </svg>
              </a>
            </li>

            <!-- Item -->
            <!-- <li class="cta-social__item">
              <a href="http://instagram.com/climbingsstudio" class="cta-social__link" target="_blank" title="Social Link" rel="nofollow noopener noreferrer">
                <svg width="35" height="35">
                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-soc-facebook"></use>
                </svg>
              </a>
            </li> -->

          </ul>
        </div><!-- /.cta-social -->
      </div>
    </div>
  </div>
</section>