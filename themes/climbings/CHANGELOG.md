# Changelog

# 1.2

- Works (https://docs.google.com/document/d/1MO0DLImNMZDs6sPk7CpMOHi5aRwoOXpNU2eQJiN-WHM/edit?usp=sharing)
- Careers (https://docs.google.com/document/d/1LLDIDvjEyik06Aq3efVz72X1dgvnv2xEDMthqYiB8Y4/edit?usp=sharing)
- Testimonials (https://docs.google.com/document/d/1X_s_oCTNXI2Y6iAEofcKU8q6Xl0qOIGoHlJOlz3FtYQ/edit?usp=sharing)
- Blog (https://docs.google.com/document/d/1BLMshk8gQl3TioaMnYiGsuWbjZ8QH7WUNKk3WUC-8dc/edit?usp=sharing)

# 1.1

- Technical Documentation (https://docs.google.com/document/d/1e3zFOqwApCClLxm04ZXZwEeUF-HEmLgoUFmpIqnXdIk/edit?usp=sharing)