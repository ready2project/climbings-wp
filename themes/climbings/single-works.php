<?php

/**
 * The template for displaying Works single posts
 *
 *#single-post
 *
 * @package climbings
 */

get_header();
?>

<?php
while (have_posts()) :
  the_post();
?>


  <!-- Works Post Credentials  -->
  <main class="works-post">
    <div class="container">

      <!-- Parent Page Title -->
      <div class="page__back">
        <h3><a href="<?php echo get_site_url(null, '/works') ?>" title="Back to Works Page">&lt; Back to Works Page</a></h3>
      </div>


      <!-- Tabs Card -->
      <div class="works-card">

        <!-- Card Header -->
        <div class="works-card__header">
          <h3 class="page-card__title"><?php the_title(); ?></h3>

          <div class="works-card__subtitle">
            <p>
              <?php the_field('subtitle'); ?>
            </p>
          </div>

          <div class="works-card__dsc">
            <?php the_field('description'); ?>
          </div>
        </div>

        <!-- Card Content -->
        <div class="works-card__content">
          <h3 class="caps-heading">Responsibilities</h3>

          <?php
          $resp = get_field('responsibilities');
          ?>

          <!-- Card Tags -->
          <ul class="works-card-tags__list">
            <?php foreach ($resp as $value) : ?>
              <li class="works-card-tags__item">
                <?php echo $value; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div><!-- /.works-card -->

      <!-- Preview -->
      <div class="work__preview">
        <?php
        $screens = get_field('screens');
        foreach ($screens as $item) :
        ?>
          <img loading="lazy" src="<?php echo $item['url']; ?>" alt="Climbings | Works |  <?php the_title(); ?>">
        <?php endforeach; ?>
      </div>


      <!-- Parent Page Title -->
      <div class="page__back pt-5">
        <h3><a href="<?php echo get_site_url(null, '/works') ?>" title="Back to Works Page">&lt; Back to Works Page</a></h3>
      </div>


    </div>
  </main>


<?php
endwhile; // End of the loop.
?>



<?php
get_footer();
