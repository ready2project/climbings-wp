<?php

/**
 * The template for displaying search results pages
 *
 *#search-result
 *
 * @package climbings
 */

get_header('lg');
?>




<!-- Post Main -->
<main class="post-main">
  <div class="container container-full">
    <div class="row">
      <div class="col-md-8">
        <div class="recent-posts-archive">

          <?php if (have_posts()) : ?>
            <h2 class="t2 t2-alert">
              <?php
              /* translators: %s: search query. */
              printf(esc_html__('Search Results for: %s', 'climbings'), '<span>' . get_search_query() . '</span>');
              ?>
            </h2>

            <!-- Posts Scope -->
            <div class="posts-scope">

              <?php
              /* Start the Loop */
              while (have_posts()) :
                the_post();

                $excerpt = wp_strip_all_tags(get_the_excerpt(), true);
                $excerpt = substr($excerpt, 0, 140);
                $excerpt = substr($excerpt, 0, strrpos($excerpt, ' ')) . '...';

                $category = get_the_category();
              ?>


                <!-- Posts Scope Item -->
                <div class="posts-scope__item">
                  <div class="posts-scope__thumb">
                    <?php climbings_post_thumbnail(); ?>
                  </div>
                  <a class="posts-scope__rubric" href="<?php echo site_url('/category/' . $category[0]->slug . '/', 'https'); ?>"><?php echo $category[0]->cat_name; ?></a>
                  <h3 class="posts-scope__title">
                    <a class="posts-scope__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </h3>
                  <p class="posts-scope__excerpt"><?php echo $excerpt; ?></p>

                  <div class="posts-scope__dsc">
                    <!-- Post Stats -->
                    <ul class="post-stats">
                      <li class="post-stats__item">
                        <svg width="14" height="13" class="post-stats__icon">
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-heart"></use>
                        </svg>
                        <div class="post-stats__val">12</div>
                      </li>
                      <li class="post-stats__item">
                        <svg width="16" height="11" class="post-stats__icon">
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-eye"></use>
                        </svg>
                        <div class="post-stats__val"><?php echo pvc_get_post_views(get_the_ID()); ?></div>
                      </li>
                      <li class="post-stats__item">
                        <svg width="16" height="15" class="post-stats__icon">
                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#icon-post-share"></use>
                        </svg>
                        <div class="post-stats__val">4</div>
                      </li>
                    </ul>
                    <!-- Post Author -->
                    <a class="post-author-sm" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
                      <div class="post-author-sm__thumb"><?php echo get_avatar(get_the_author_meta('ID'), 200); ?></div>
                      <div class="post-author-sm__name"><?php the_author(); ?></div>
                    </a>
                  </div><!-- /.posts-scope__dsc -->
                </div><!-- /.posts-scope__item -->
              <?php
              endwhile;
              the_posts_navigation();
              ?>
            </div><!-- /.posts-scope -->
            <div class="recent-posts__action">
              <a class="btn btn-icon" href="<?php echo site_url('/latest-articles/', 'https'); ?>">
                <span class="btn__text">See All Search Results</span>
                <i class="i i-arrow-right"></i>
              </a>
            </div>

          <?php
          else :
          ?>
            <h2 class="t2 t2-alert">
              <?php
              /* translators: %s: search query. */
              printf(esc_html__('Search Results for: %s', 'climbings'), '<span>' . get_search_query() . '</span>');
              ?>
            </h2>

            <p class="search-err"><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'climbings'); ?></p>
          <?php
          endif;
          ?>
        </div><!-- /.recent-posts-archive -->
      </div><!-- /.col-md-8 -->

      <!-- Blog Aside-->
      <div class="col-md-4">
        <aside class="blog-aside">
          <?php get_template_part('template-parts/blog/aside-newsletter', 'none'); ?>
          <?php get_template_part('template-parts/blog/aside-freebies', 'none'); ?>
          <div class="mtop-40"></div>
          <?php get_template_part('template-parts/blog/aside-popular', 'none'); ?>
        </aside><!-- /.blog-aside -->
      </div><!-- /.col-md-4 -->

    </div><!-- /.row -->
  </div><!-- /.container -->

</main><!-- /.post-main -->


<?php
get_footer();
