/**
 * Home: Christmas Tree
 */

const sceneTree = () => {



  // Get all tabs
  const redLight = document.getElementById("treeLightsRed");
  const blueLight = document.getElementById("treeLightsBlue");

  if (redLight && blueLight) {


    setInterval(() => {
      redLight.classList.toggle("active");
      blueLight.classList.toggle("active");
    }, 800);
  }


};

export default sceneTree;
