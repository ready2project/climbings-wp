/**
 * FAQ Tabs
 */

const tabsFAQ = () => {
  // Get all tabs
  const tabsServices = document.querySelectorAll('.faq-tabs-list__link')
  const tabsServicesContent = document.querySelectorAll('.faq-tabs__card')

  if (tabsServices) {
    const tabsServicesLen = tabsServices.length
    for (let i = 0; i < tabsServicesLen; i++) {
      /**
       * Set Active Status
       */
      tabsServices[i].addEventListener('mousemove', function (e) {
        e.preventDefault()
        tabsServices.forEach((el) => {
          el.classList.remove('active')
        })
        tabsServicesContent.forEach((el) => {
          el.classList.remove('active')
        })

        const obj = e.currentTarget
        obj.classList.add('active')

        let link = obj.dataset.tabid
        const actualTab = document.querySelectorAll(`[data-tab="${link}"]`)

        const actualTabLen = actualTab.length
        for (let id = 0; id < actualTabLen; id++) {
          actualTab[id].classList.add('active')
        }
      })

      tabsServices[i].addEventListener('click', (e) => {
        e.preventDefault()
      })
    }
  }
}

export default tabsFAQ
