/*
 * Modal
 * ========================================================================== */

// Plugins
import pluginC7Hook from './plugin-c7-hook'

const Modal = () => {
  /**
   * Hide Popup by Close Button
   */
  const btnHide = document.querySelectorAll('.modal-hide')
  if (btnHide) {
    const btnHideLen = btnHide.length
    for (let i = 0; i < btnHideLen; i++) {
      btnHide[i].addEventListener('click', (e) => {
        e.preventDefault()

        const dataTarget = e.currentTarget.dataset.target
        const parent = document.getElementById(dataTarget)
        parent.classList.remove('active')
      })
    }
  }

  /**
   * Hide Popup bay Background
   */
  const modal = document.querySelectorAll('.modal')
  if (modal) {
    const modalLen = modal.length
    for (let i = 0; i < modalLen; i++) {
      modal[i].addEventListener('click', function (e) {
        const obj = e.target
        if (obj !== e.currentTarget) return
        obj.classList.remove('active')
      })
    }
  }

  /**
   * Show Modal
   */
  const btnShow = document.querySelectorAll('.modal-link')
  if (btnShow) {
    const btnShowLen = btnShow.length
    for (let i = 0; i < btnShowLen; i++) {
      // Show Event
      btnShow[i].addEventListener(
        'show',
        function (e) {
          // Add Contact 7 Scripts for Contact Popup
          const btnAction = e.currentTarget
          if (btnAction.dataset.target === 'contact-modal') {
            // Plugin: Contact 7 Scripts
            pluginC7Hook()
          }
        },
        false
      )

      // Click Event
      btnShow[i].addEventListener('click', (e) => {
        e.preventDefault()

        let dataTarget = e.currentTarget.dataset.target

        const modal = document.getElementById(dataTarget)

        if (modal) {
          modal.classList.add('active')

          const event = new Event('show')
          btnShow[i].dispatchEvent(event)
        }
      })
    }
  }
}

export default Modal
