/*
 * Modal | Video
 * ========================================================================== */

const ModalVideo = () => {
  /**
   * Hide Popup by Close Button
   */
  const btnHide = document.querySelectorAll('.modal-video-hide')
  if (btnHide) {
    const btnHideLen = btnHide.length
    for (let i = 0; i < btnHideLen; i++) {
      btnHide[i].addEventListener('click', (e) => {
        e.preventDefault()

        const el = e.currentTarget
        const dataTarget = el.dataset.target

        const yutubevideo = document.getElementById(dataTarget + 'video')
        if (yutubevideo) {
          yutubevideo.src = ''
        }

        const parent = document.getElementById(dataTarget)
        parent.classList.remove('active')

        const iframeBox = parent.querySelector('.modal-wnd__body-iframe')
        if (iframeBox) {
          const embed = iframeBox.innerHTML
          iframeBox.innerHTML = ''
          iframeBox.innerHTML = embed
        }
      })
    }
  }

  /**
   * Hide Popup bay Background
   */
  const modal = document.querySelectorAll('.modal-video')
  if (modal) {
    const modalLen = modal.length
    for (let i = 0; i < modalLen; i++) {
      modal[i].addEventListener('click', function (e) {
        const obj = e.target
        if (obj !== e.currentTarget) return
        obj.classList.remove('active')

        const yutubevideo = document.getElementById(obj.id + 'video')
        if (yutubevideo) {
          yutubevideo.src = ''
        }

        const iframeBox = obj.querySelector('.modal-wnd__body-iframe')
        if (iframeBox) {
          const embed = iframeBox.innerHTML
          iframeBox.innerHTML = ''
          iframeBox.innerHTML = embed
        }
      })
    }
  }

  /**
   * Show Modal
   */
  const btnShow = document.querySelectorAll('.modal-video-link')
  if (btnShow) {
    const btnShowLen = btnShow.length
    for (let i = 0; i < btnShowLen; i++) {
      // Click Event
      btnShow[i].addEventListener('click', (e) => {
        e.preventDefault()

        let dataTarget = e.currentTarget.dataset.target
        const modal = document.getElementById(dataTarget)

        if (modal) {
          modal.classList.add('active')
          const videoSrc = modal.dataset.video

          if (videoSrc) {
            const yutubevideo = document.getElementById(dataTarget + 'video')
            yutubevideo.src =
              'https://www.youtube.com/embed/' + videoSrc + '?autoplay=1'
          }
        }
      })
    }
  }
}

export default ModalVideo
