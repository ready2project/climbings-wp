/**
 * Blog: Intro Slider
 */
import Swiper, {Navigation} from 'swiper'
Swiper.use([Navigation])

const sliderBlog = () => {
  /**
   * Intro Slider
   */
  const elSliderIntro = document.querySelector('.blog-intro__slider')
  if (elSliderIntro) {
    const sliderIntro = new Swiper(elSliderIntro, {
      loop: true,
      centeredSlides: true,
      spaceBetween: 0,
      slidesPerView: 3,
      preloadImages: false,
      lazy: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          spaceBetween: 0,
        },
      },
    })
  }

  /**
   * Featured Posts Slider
   */
  const elSliderFeatured = document.querySelector('.featured-posts__slider')
  if (elSliderFeatured) {
    const SliderFeatured = new Swiper(elSliderFeatured, {
      loop: true,
      centeredSlides: true,
      spaceBetween: 0,
      slidesPerView: 1,
      preloadImages: false,
      lazy: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })
  }

  /**
   * Recent Posts Slider
   */
  const elSimilarPosts = document.querySelector('.similar-posts__slides')
  if (elSimilarPosts) {
    const SliderSim = new Swiper(elSimilarPosts, {
      loop: true,
      centeredSlides: false,
      spaceBetween: 45,
      preloadImages: false,
      lazy: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.similar-posts__btn-next',
        prevEl: '.similar-posts__btn-prev',
      },
      breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 45,
        },
      },
    })
  }
}

export default sliderBlog
