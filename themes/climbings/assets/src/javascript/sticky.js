const StickyMenu = () => {
  const elSticky = document.querySelector('.sticky')
  if (elSticky) {
    const sticky = new Sticky('.sticky')
  }
}

export default StickyMenu
