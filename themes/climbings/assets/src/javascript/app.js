/**
 * Load methods, helpers, polyfills etc.
 */
import initNav from './nav'

// Action
import btnExploreMore from './btn-explore-more'

// Modal
import Modal from './modal'
import ModalContact from './modal-contact'
import ModalVideo from './modal-video'

// Tabs
import initProcessTabs from './tabs-process'
import initOurProcessTabs from './tabs-our-process'
import initServiceTabs from './tabs-services'
import initWorksTabs from './tabs-works'
import initTestimonialsTabs from './tabs-testimonials'
import tabsFAQ from './tabs-faq'

// Scenes
import * as KeyshapeJS from './vendors/keyshapejs.min'

import sceneCTA from './scene-cta'
import sceneIntro from './scene-intro'
import sceneProcess from './scene-process'

// Sliders
import sliderBlog from './slider-blog'
import sliderWorksList from './slider-works-list'
import sliderWorks from './slider-works'
import sliderInsight from './slider-insight.js'

// Sticky
import stickyMenu from './sticky'

/**
 * The DOMContentLoaded event fires when the initial HTML document has been completely loaded and parsed,
 * without waiting for stylesheets, images, and subframes to finish loading.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event
 */
document.addEventListener('DOMContentLoaded', () => {
  initNav()

  // Tabs
  initProcessTabs()
  initOurProcessTabs()
  initServiceTabs()
  initWorksTabs()
  initTestimonialsTabs()
  tabsFAQ()

  // Animation
  sceneIntro()
  sceneCTA()
  sceneProcess()

  // Modal
  Modal()
  ModalContact()
  ModalVideo()

  // Sliders
  sliderBlog()
  sliderWorksList()
  sliderWorks()
  sliderInsight()

  // Action
  btnExploreMore()

  // Sticky
  stickyMenu()
})
