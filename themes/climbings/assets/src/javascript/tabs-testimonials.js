/**
 * Home: Testimonials Tabs
 */

import Swiper, {Navigation} from 'swiper'
Swiper.use([Navigation])

const initTestimonialsTabs = () => {
  /**
   * More/Less Content
   */

  function shrinkFeedbackText() {
    const feedbackText = document.querySelectorAll('.feedback__text')
    if (feedbackText) {
      feedbackText.forEach((el) => {
        const text = el.innerHTML
        const textHeight = el.clientHeight

        if (textHeight > 260) {
          el.classList.add('feedback__text-excerpt')

          const textAction = el.querySelector('.feedback-text-action')
          if (textAction) {
            textAction.addEventListener('click', (e) => {
              e.preventDefault()

              el.classList.remove('feedback__text-excerpt')
            })
          }
        }
      })
    }
  }

  shrinkFeedbackText()

  // Get all tabs
  const tabsServices = document.querySelectorAll('.testimonials__link')
  const tabsServicesContent = document.querySelectorAll(
    '.testimonials_tab-content__item'
  )

  if (tabsServices) {
    let tabsServicesContentLen = tabsServicesContent.length
    let slider = []

    for (let i = 0; i < tabsServicesContentLen; i++) {
      // Testimonials Slider
      const testimonialsSlider = tabsServicesContent[i].querySelector(
        '.testimonials-slider'
      )

      if (testimonialsSlider) {
        slider[i] = new Swiper(testimonialsSlider, {
          loop: false,
          variableWidth: true,
          slidesPerView: 1,
          spaceBetween: 20,

          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },

          breakpoints: {
            768: {
              slidesPerView: 3,
              spaceBetween: 30,
            },

            528: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
          },
        })
      }
    }

    const tabsServicesLen = tabsServices.length
    for (let i = 0; i < tabsServicesLen; i++) {
      /**
       * Set Active Status
       */
      tabsServices[i].addEventListener('mousemove', (e) => {
        e.preventDefault()

        tabsServices.forEach((el) => {
          el.classList.remove('active')
        })

        tabsServicesContent.forEach((el) => {
          el.classList.remove('active')
        })

        const obj = e.currentTarget
        obj.classList.add('active')

        let link = obj.hash
        if (link) {
          const actualTab = document.querySelector(link)

          if (actualTab) {
            actualTab.classList.add('active')

            // Check slider (Is it on home page?)
            if (slider.length > 0) {
              // Update slider
              slider[actualTab.dataset.tabid].update(true)
            }
          }
        }

        shrinkFeedbackText()
      })

      tabsServices[i].addEventListener('click', (e) => {
        e.preventDefault()
      })
    }
  }
}

export default initTestimonialsTabs
