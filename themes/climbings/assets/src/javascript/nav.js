/**
 * Navigation
 */

import {getClientHeight} from './helpers/helpers'

const initNav = () => {
  /**
   * Button Toggle
   */
  const btnNav = document.getElementById('btnNav')
  const navBar = document.querySelector('.nav')
  const header = document.querySelector('.header')
  const headerAnchor = document.querySelector('.header .container')

  if (navBar) {
    /**
     * Nav Action
     */
    if (btnNav) {
      btnNav.addEventListener('click', function (e) {
        e.stopPropagation()

        e.preventDefault()
        btnNav.classList.toggle('active')
        document.body.classList.toggle('main-menu-active')
      })
    }

    /**
     * Correct Nav Button Position
     */
    var navBarStyle = navBar.style
    navBarStyle.display = 'flex'

    /**
     * Event: Scroll (Short Menu)
     */
    if (header) {
      const setNavBtn = () => {
        var rect = headerAnchor.getBoundingClientRect()

        navBarStyle.right = ''
        if (window.innerWidth > 1200) {
          navBarStyle.right = rect.left + 15 + 'px'
        }
      }

      if (headerAnchor) {
        setNavBtn()
      }

      window.addEventListener('resize', function () {
        setNavBtn()
      })

      const navScroll = () => {
        var docScrollTop = document.body.scrollTop
        var fixed_position = navBar.getBoundingClientRect().top + docScrollTop
        var fixed_height = navBar.clientHeight

        var toCross_position = header.getBoundingClientRect().top + docScrollTop
        var toCross_height = header.clientHeight

        if (fixed_position + fixed_height < toCross_position) {
          navBar.classList.add('nav-short')
        } else if (fixed_position > toCross_position + toCross_height) {
          navBar.classList.add('nav-short')
        } else {
          navBar.classList.remove('nav-short')
        }
      }

      navScroll()

      window.addEventListener('scroll', (e) => {
        navScroll()
      })

      /**
       * Event: Keypress
       */
      document.onkeydown = (e) => {
        if (document.body.classList.contains('main-menu-active')) {
          // Hide menu by Escape
          var keyEsc = 27

          switch (e.keyCode) {
            case keyEsc:
              e.preventDefault()

              btnNav.click()
              break
          }
        }
      }

      /**
       * Event: Blur
       */
      window.addEventListener('click', () => {
        if (document.body.classList.contains('main-menu-active')) {
          btnNav.click()
        }
      })

      var mainMenu = document.getElementById('mainMenu')

      if (mainMenu) {
        mainMenu.addEventListener('click', (e) => {
          e.stopPropagation()
        })
      }
    } // end. if header & nav

    /**
     * Button 'Start a Project' behaviour
     */

    var areasRestrictedNavStart = document.querySelectorAll(
      '.area-startbtn-hide, .header'
    )
    var areasRestrictedNavStartLength = areasRestrictedNavStart.length

    const btnNavStartScroll = () => {
      // Indicator Nav Position
      var fixed_position =
        navBar.getBoundingClientRect().top + document.body.scrollTop
      var fixed_height = getClientHeight(navBar)

      if (areasRestrictedNavStartLength > 0) {
        for (var i = 0; i < areasRestrictedNavStartLength; i++) {
          var elCross = areasRestrictedNavStart[i]
          var toCross_position =
            elCross.getBoundingClientRect().top + document.body.scrollTop
          var toCross_height = getClientHeight(elCross)

          if (fixed_position + fixed_height < toCross_position) {
            navBar.classList.add('nav-short')
          } else if (fixed_position > toCross_position + toCross_height) {
            navBar.classList.add('nav-short')
          } else {
            navBar.classList.remove('nav-short')
            break
          }
        }
      }
    }

    btnNavStartScroll()

    window.addEventListener('scroll', function (e) {
      btnNavStartScroll()
    })

    /**
     * Hide menu after click
     */
    const menuLink = document.querySelectorAll('.main-menu__link')

    if (menuLink) {
      menuLink.forEach((el) => {
        el.addEventListener('click', (e) => {
          btnNav.click()
        })
      })
    }
  }
}

export default initNav
