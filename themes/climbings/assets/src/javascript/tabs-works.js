/**
 * Home: Services Tabs
 */

const initWorksTabs = () => {
  // Get all tabs
  const tabsServices = document.querySelectorAll('.works-tabs-list__link')
  const tabsServicesContent = document.querySelectorAll('.works-card')

  if (tabsServices) {
    const tabsServicesLength = tabsServices.length
    for (let i = 0; i < tabsServicesLength; i++) {
      /**
       * Set Active Status
       */
      tabsServices[i].addEventListener('mousemove', (e) => {
        tabsServices.forEach((el) => {
          el.classList.remove('active')
        })

        tabsServicesContent.forEach((el) => {
          el.classList.remove('active')
        })

        var obj = e.currentTarget
        obj.classList.add('active')

        var link = obj.hash
        var actualTab = document.querySelector(link)
        actualTab.classList.add('active')
      })

      tabsServices[i].addEventListener('click', (e) => {
        e.preventDefault()
      })
    }
  }
}

export default initWorksTabs
