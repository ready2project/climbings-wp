/**
 * Home Page: Parallax Effect
 */

import { getViewHeight, getClientHeight } from './helpers/helpers';

const initHomeParallax = () => {

  var areaParallax = document.querySelectorAll('.area-parallax');
  var areaParallaxLength = areaParallax.length;
  var viewHeight = getViewHeight();

  if (areaParallax.length > 0) {

    var screenWidth = window.innerWidth;
    var parallaxBreakPoint = 1900;

    if (screenWidth > parallaxBreakPoint) {

      /**
       * Set Properties for Areas
       */
      for (var i = 0; i < areaParallaxLength; i++) {
        var el = areaParallax[i];
        el.style.position = 'relative';
        el.style.zIndex = i;
      }


      const parallaxArea = () => {
        var offset = 0;

        // var areaHeight = areaParallax[0].offsetHeight;
        var areaHeight = getClientHeight(areaParallax[0]);

        var areaPos = areaHeight - viewHeight;
        var scrollPos = window.scrollY;

        if (scrollPos >= areaPos) {
          if (!areaParallax[0].classList.contains('fixed')) {
            document.body.style.paddingTop = areaHeight + 'px';
            areaParallax[0].style.bottom = '0';
            areaParallax[0].classList.add('fixed');
          }
        } else {
          document.body.style.paddingTop = offset + 'px';
          areaParallax[0].style.bottom = 'auto';
          areaParallax[0].classList.remove('fixed');
        }
      }


      const parallaxArea2 = () => {
        if (areaParallax[0].classList.contains('fixed')) {
          // var offset = areaParallax[0].offsetHeight;
          // var areaHeight = areaParallax[1].offsetHeight + offset;

          var offset = getClientHeight(areaParallax[0]);
          var areaHeight = getClientHeight(areaParallax[1]) + offset;


          var areaPos = areaHeight - viewHeight;
          var scrollPos = window.scrollY;

          if (scrollPos >= areaPos) {
            if (!areaParallax[1].classList.contains('fixed')) {
              document.body.style.paddingTop = areaHeight + 'px';
              areaParallax[1].style.bottom = '0';
              areaParallax[1].classList.add('fixed');
            }
          } else {
            document.body.style.paddingTop = offset + 'px';
            areaParallax[1].style.bottom = 'auto';
            areaParallax[1].classList.remove('fixed');
          }
        }
      }

      // Init
      parallaxArea();
      // parallaxArea2();

      /**
       * Event: Scroll
       */
      window.addEventListener('scroll', function () {
        parallaxArea();
        // parallaxArea2();
      });

      /**
       * Event: Resize
       */
      window.addEventListener('resize', function () {
        areaParallaxLength = areaParallax.length;
        viewHeight = getViewHeight();
        screenWidth = window.innerWidth;
      });

    }
  }


}
export default initHomeParallax;

