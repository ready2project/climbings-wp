/**
 * Home: Services Tabs
 */
import Swiper, {Navigation} from 'swiper'
Swiper.use([Navigation])

const initOurProcessTabs = () => {
  // Get tabs
  const tabsOurProcess = document.querySelectorAll('.our-process-menu__link')

  if (tabsOurProcess) {
    /**
     * Clear Tabs Active Status
     */
    function clearTabsStatus() {
      tabsOurProcess.forEach((el) => {
        el.classList.remove('active')
      })
    }

    /**
     * Home: Our Process Slider
     */
    const sliderProcess = new Swiper('.our-process-menu-slider', {
      loop: false,
      slidesPerView: 1,
      preloadImages: false,
      lazy: true,
      autoHeight: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })

    /**
     * Slider Action
     */
    sliderProcess.on('slideChange', () => {
      var slideId = sliderProcess.activeIndex

      // Clear Active Tab Status
      clearTabsStatus()

      // Active Tab
      tabsOurProcess[slideId].classList.add('active')
    })

    /**
     * Tabs Action
     */
    var tabsOurProcessLength = tabsOurProcess.length
    for (var i = 0; i < tabsOurProcessLength; i++) {
      tabsOurProcess[i].addEventListener('click', (e) => {
        e.preventDefault()
      })

      tabsOurProcess[i].addEventListener('mouseover', (e) => {
        e.preventDefault()

        // Clear Active Tab Status
        clearTabsStatus()

        // Active Tab
        var obj = e.currentTarget
        obj.classList.add('active')

        // Show Start Points
        var points = obj.dataset.slideindex

        // Slider
        sliderProcess.slideTo(points - 1)
      })
    }
  }
}

export default initOurProcessTabs
