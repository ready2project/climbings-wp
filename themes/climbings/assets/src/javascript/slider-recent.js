/**
 * Home: Recent Slider
 */
import Swiper from 'swiper'

/**
 * Recent Posts
 */
const sliderRecent = () => {
  const recentSlider = document.querySelector('.similar-posts')
  if (recentSlider) {
    const swiperRecent = new Swiper('.similar-posts', {
      loop: true,
      variableWidth: true,
      slidesPerView: 1,
      spaceBetween: 30,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },

        528: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    })
  }

  /**
   * Works Slider
   */
  const recentWorksSlider = document.querySelector('.recent-works__slider')
  if (recentWorksSlider) {
    const swiperRecent = new Swiper('.recent-works__slider', {
      loop: false,
      variableWidth: true,
      slidesPerView: 1,
      spaceBetween: 30,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },

        528: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    })
  }
}

export default sliderRecent
