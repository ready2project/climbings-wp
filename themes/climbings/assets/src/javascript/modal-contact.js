/**
 * Submit Form for Sell Home Page
 */

import IMask from 'imask'

export default () => {

  /**
   * Phone Mask
   */
  const phone = document.querySelector('.tel-input')

  if (phone) {
    let maskOptions = {
      mask: '(000) 000-0000',
    }

    let mask = IMask(phone, maskOptions)
  }

  // Contact Modal
  const cmf_name = document.querySelector('#cmf_name')
  const cmf_email = document.querySelector('#cmf_email')
  const cmf_company = document.querySelector('#cmf_company')
  const cmf_tel = document.querySelector('#cmf_tel')
  const cmf_msg = document.querySelector('#cmf_msg')

  const c7cmf_name = document.querySelector('#c7cmf_name')
  const c7cmf_email = document.querySelector('#c7cmf_email')
  const c7cmf_company = document.querySelector('#c7cmf_company')
  const c7cmf_tel = document.querySelector('#c7cmf_tel')
  const c7cmf_msg = document.querySelector('#c7cmf_msg')

  /**
   * Contact 7 Forms
   */
  const btnSubmit = document.querySelector('.wpcf7-submit')
  const thanksPanel = document.querySelector('.contact-modal__thanks')

  if (btnSubmit) {
    btnSubmit.addEventListener('click', (e) => {
      btnSubmit.value = 'Sending'
    })
  }

  const wpcf7Form = document.querySelector('.wpcf7')
  if (wpcf7Form) {
    /**
     * Submit Event
     */
    wpcf7Form.addEventListener(
      'wpcf7submit',
      function (event) {
        let data = event.detail

        let valErrElems = document.querySelectorAll('.val-err')
        if (valErrElems) {
          for (let item of valErrElems) {
            item.classList.remove('val-err')
          }
        }

        // Validate Fields
        if (data.status === 'validation_failed') {
          // Invalid Fields
          let fields = data.apiResponse.invalid_fields

          if (fields) {
            fields.forEach((element) => {
              const item = document.querySelector(
                '#' + element.idref.replace('c7', '')
              )

              if (item) {
                item.classList.add('val-err')
              }
            })
          }
        }

        setTimeout(() => {
          btnSubmit.value = 'Submit'
        }, 100)
      },
      false
    )

    wpcf7Form.addEventListener('wpcf7mailsent', function (event) {
      if (thanksPanel) {
        thanksPanel.classList.remove('hide')
      }

      // Clear Form
      setTimeout(() => {
        let valErrElems = document.querySelectorAll('.val-err')
        if (valErrElems) {
          for (let item of valErrElems) {
            item.classList.remove('val-err')
          }
        }

        if (cmf_name && c7cmf_name) {
          cmf_name.value = ''
          c7cmf_name.value = ''
        }

        if (cmf_email && c7cmf_email) {
          cmf_email.value = ''
          c7cmf_email.value = ''
        }

        if (cmf_company && c7cmf_company) {
          cmf_company.value = ''
          c7cmf_company.value = ''
        }

        if (cmf_tel && c7cmf_tel) {
          cmf_tel.value = ''
          c7cmf_tel.value = ''
        }

        if (cmf_msg && c7cmf_msg) {
          cmf_msg.value = ''
          c7cmf_msg.value = ''
        }

        const outputMsg = document.querySelector('.wpcf7-response-output')
        if (outputMsg) {
          outputMsg.style.display = 'none'
        }

        thanksPanel.classList.add('hide')
      }, 15000)
    })
  }

  /**
   * Contact 7 Integration with Modal Inputs
   */
  if (cmf_name && c7cmf_name) {
    cmf_name.value = ''
    c7cmf_name.value = ''

    cmf_name.addEventListener('input', (e) => {
      c7cmf_name.value = e.target.value
    })
  }

  if (cmf_email && c7cmf_email) {
    cmf_email.value = ''
    c7cmf_email.value = ''

    cmf_email.addEventListener('input', (e) => {
      c7cmf_email.value = e.target.value
    })
  }

  if (cmf_company && c7cmf_company) {
    cmf_company.value = ''
    c7cmf_company.value = ''

    cmf_company.addEventListener('input', (e) => {
      c7cmf_company.value = e.target.value
    })
  }

  if (cmf_tel && c7cmf_tel) {
    cmf_tel.value = ''
    c7cmf_tel.value = ''

    cmf_tel.addEventListener('input', (e) => {
      c7cmf_tel.value = e.target.value
    })
  }

  if (cmf_msg && c7cmf_msg) {
    cmf_msg.value = ''
    c7cmf_msg.value = ''

    cmf_msg.addEventListener('input', (e) => {
      c7cmf_msg.value = e.target.value
    })
  }
  // }
}
