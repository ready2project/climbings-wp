import {checkVisible} from './helpers/helpers'

const sceneCTA = () => {
  if (!KeyshapeJS.version.indexOf('1.') != 0) {
    /**
     * Animation for CTA Section
     */

    const sceneCTA = document.getElementById('sceneCTA')
    let sceneCTAlaunch = true

    /**
     * Animation Frames
     */
    const getCTASceneAnimation = () => {
      window.ks = document.ks = KeyshapeJS
      ;(function (ks) {
        ks.animate(
          '#Sun',
          [
            {
              p: 'mpath',
              t: [1500, 4200],
              v: ['0%', '100%'],
              e: [[0], [0]],
              mp: 'M0,0L340,0',
            },
            {
              p: 'scaleX',
              t: [1500, 2200, 3200, 4200],
              v: [1, 1.3, 1.2, 1],
              e: [[0], [0], [0], [0]],
            },
            {
              p: 'scaleY',
              t: [1500, 2200, 3200, 4200],
              v: [1, 1.3, 1.2, 1],
              e: [[0], [0], [0], [0]],
            },
          ],
          '#Tree',
          [
            {
              p: 'skewX',
              t: [4900, 5000, 5100, 5200, 5300, 5500],
              v: [0, 6, -6, 6, -6, 0],
              e: [
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [0],
              ],
            },
            {
              p: 'skewY',
              t: [4900, 5000, 5100, 5200, 5300, 5500],
              v: [0, 0, 0, 0, 0, 0],
              e: [
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [0],
              ],
            },
          ],
          '#_a0',
          [
            {
              p: 'opacity',
              t: [500, 800, 2300, 3000, 3600],
              v: [0, 1, 0.9, 0.5, 1],
              e: [[0], [0], [0], [0], [0]],
            },
            {
              p: 'd',
              t: [1500, 2500, 2900, 3500, 4000],
              v: [
                "path('M-6.9,-76.5L-38.3,-62.5L141.9,-10.5L197.5,-33.5C197.5,-33.5,-6.9,-76.5,-6.9,-76.5Z')",
                "path('M-6.4,-77.1L-70.4,-77.1L10.6,-16.1L54.6,-37.1C54.6,-37.1,-6.4,-77.1,-6.4,-77.1Z')",
                "path('M-8.6,-76L-69.4,-76L-89.2,-36.1L-8.6,-37.8C-8.6,-37.8,-8.6,-76,-8.6,-76Z')",
                "path('M-7.4,-75.6L-68.4,-75.6L-160.4,-38.1L-94.7,-23.5C-94.7,-23.5,-7.4,-75.6,-7.4,-75.6Z')",
                "path('M-5.4,-76.2L-69.4,-76.2L-197.3,-14.4L-107.4,-7.1C-107.4,-7.1,-5.4,-76.2,-5.4,-76.2Z')",
              ],
              e: [
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [1, 0, 0, 1, 1],
                [0],
              ],
            },
          ],
          '#Left-Branch',
          [
            {
              p: 'scaleX',
              t: [900, 1300],
              v: [0, -1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
            {
              p: 'scaleY',
              t: [900, 1300],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
          ],
          '#Fill-31',
          [
            {
              p: 'fill',
              t: [2000, 4700],
              v: ['#02256c', '#ffec61'],
              e: [[1, 0, 0, 1, 1], [0]],
            },
          ],
          '#Fill-32',
          [
            {
              p: 'fill',
              t: [1500, 2000, 4400],
              v: ['#ffec61', '#ffec61', '#02256c'],
              e: [[0], [1, 0, 1, 1, 1], [0]],
            },
          ],
          '#Main-branch',
          [
            {
              p: 'scaleX',
              t: [400, 1000],
              v: [0, -1],
              e: [[1, 0, 2.7, 1, 0], [0]],
            },
            {p: 'scaleY', t: [400, 1000], v: [0, 1], e: [[1, 0, 2.7, 1, 0], [0]]},
          ],
          '#Fill-33',
          [
            {
              p: 'fill',
              t: [1500, 2000, 4400],
              v: ['#ffec61', '#ffec61', '#02256c'],
              e: [[0], [1, 0, 1, 1, 1], [0]],
            },
          ],
          '#Fill-34',
          [
            {
              p: 'fill',
              t: [1500, 2000, 4700],
              v: ['#02256c', '#02256c', '#ffec61'],
              e: [[0], [1, 0, 1, 1, 1], [0]],
            },
          ],
          '#Right-Branch',
          [
            {
              p: 'scaleX',
              t: [800, 1200],
              v: [0, -1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
            {
              p: 'scaleY',
              t: [800, 1200],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
          ],
          '#Fill-64',
          [
            {
              p: 'fill',
              t: [2000, 4400],
              v: ['#ffec61', '#02256c'],
              e: [[1, 0, 1, 1, 1], [0]],
            },
          ],
          '#Fill-65',
          [
            {
              p: 'fill',
              t: [1300, 1500, 2000, 4700],
              v: ['#02256c', '#ffec61', '#02256c', '#ffec61'],
              e: [[0], [0], [1, 0, 1, 1, 1], [0]],
            },
          ],
          '#Left-Tree',
          [
            {
              p: 'scaleX',
              t: [1000, 1500],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
            {
              p: 'scaleY',
              t: [1000, 1500],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
          ],
          '#Mid-Tree',
          [
            {
              p: 'scaleX',
              t: [700, 1000],
              v: [0, 1],
              e: [[1, 0, 2, 0.5, 0.3], [0]],
            },
            {
              p: 'scaleY',
              t: [700, 1000],
              v: [0, 1],
              e: [[1, 0, 2, 0.5, 0.3], [0]],
            },
          ],
          '#Right-Tree',
          [
            {
              p: 'scaleX',
              t: [1200, 1700],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
            {
              p: 'scaleY',
              t: [1200, 1700],
              v: [0, 1],
              e: [[1, 1, 0, 0.4, 0.8], [0]],
            },
          ],
          {autoremove: false}
        ).range(0, 7000)
        if (
          document.location.search
            .substr(1)
            .split('&')
            .indexOf('global=paused') >= 0
        )
          ks.globalPause()
      })(KeyshapeJS)
    }

    /**
     * Check Visibility
     */
    window.addEventListener('scroll', () => {
      showCTASceneAnimation()
    })

    /**
     * Show Animation
     */
    const showCTASceneAnimation = () => {
      // Animation Trigger
      const offset = -100

      if (sceneCTAlaunch) {
        if (checkVisible(sceneCTA, offset)) {
          getCTASceneAnimation()
          sceneCTAlaunch = false
        }
      }
    }

    showCTASceneAnimation()
  }
}

export default sceneCTA
