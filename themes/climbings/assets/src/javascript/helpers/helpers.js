/**
 * Helpers
 */

/**
 * Run a callback functions on scroll and after scrolling has stopped
 * Based on https://codepen.io/cferdinandi/pen/BGpPoo
 */
const scrollMaster = (callbackScrollOut, callbackScrollOn) => {

  // Make sure a valid callback was provided
  if (!callbackScrollOut || typeof callbackScrollOut !== 'function') return;

  // Setup scrolling variable
  var isScrolling;
  var timeOut = 150;

  // Listen for scroll events
  window.addEventListener('scroll', function (event) {

    callbackScrollOn();

    // Clear our timeout throughout the scroll
    window.clearTimeout(isScrolling);

    // Set a timeout to run after scrolling ends
    isScrolling = setTimeout(function () {

      // Run the callback
      callbackScrollOut();

    }, timeOut);

  }, false);
};

/**
 * Get Element Height with Margin
 *
 * @param {Element} el
 */
const getClientHeight = (el) => {
  if (el) {
    var styles = window.getComputedStyle(el);
    var marginTop = parseFloat(styles['marginTop']) || 0;
    var marginBottom = parseFloat(styles['marginBottom']) || 0;

    return Math.ceil(el.offsetHeight + marginTop + marginBottom);
  }
}


/**
 * Get Page Height
 */
const getViewHeight = () => Math.max(document.documentElement.clientHeight, window.innerHeight);


/**
 * Check Element Visibility
 *
 * @param {Element} el
 */
const checkVisible = (el, offset) => {
  if (el) {

    if (isNaN(offset)) {
      offset = 0;
    }

    let rect = el.getBoundingClientRect();
    let viewHeight = getViewHeight();

    return !(rect.bottom < offset || rect.top - viewHeight >= offset);
  }
}


export {checkVisible, getClientHeight, getViewHeight, scrollMaster};
