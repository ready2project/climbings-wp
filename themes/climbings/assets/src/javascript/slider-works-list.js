/**
 * Home: Works Slider
 */
import Swiper, {Scrollbar} from 'swiper'
Swiper.use([Scrollbar])

import initWorksTabs from './tabs-works'

const sliderWorksList = () => {
  const elSlider = document.querySelector('.works-list-slider')

  if (elSlider) {
    const elData = document.querySelector('.works-data')
    const elDataItems = elData.querySelectorAll('.filter-all')
    let items = []

    /**
     * Scroll to active work
     */
    const worksPosts = document.querySelector('.works-posts')

    /**
     * Hide Works
     */
    const hideWorks = () => {
      if (worksPosts) {
        const worksPostsLinks = worksPosts.querySelectorAll(
          '.works-tabs-list__link'
        )
        const worksPostsCards = worksPosts.querySelectorAll('.works-card')

        if (worksPostsLinks && worksPostsCards) {
          worksPostsCards.forEach((el) => {
            el.classList.remove('active')
            el.classList.add('hide')
          })

          worksPostsLinks.forEach((el) => {
            let hash = el.hash
            let card = worksPosts.querySelector(hash)

            if (card) {
              card.classList.remove('hide')
            }
          })
        }
      }
    }

    const updateSlider = (target) => {
      target.classList.add('active')

      if (sliderWorks.slides) {
        sliderWorks.removeAllSlides()
        let filterClass = '.filter-' + target.hash.split('#')[1]

        const elFilteredItems = document.querySelectorAll(filterClass)
        let filteredItems = []

        elFilteredItems.forEach((el) => {
          let item = el.innerHTML
          filteredItems.push(item)
        })

        sliderWorks.addSlide(1, filteredItems)
        sliderWorks.update(true)
        initWorksTabs()
      } else {
        sliderWorks = new Swiper(elSlider, {
          direction: 'vertical',
          mousewheel: true,
          mousewheelControl: true,
          slidesPerView: 'auto',
          autoHeight: true,
          scrollbar: {
            el: '.swiper-scrollbar',
            hide: false,
            draggable: true,
          },
        })

        elSlider.style.height = ''

        // Restyle Slider
        const elActionPanel = document.querySelector('.works-tabs-list__action')
        elActionPanel.style.display = ''

        const elWrapper = elSlider.querySelector('.swiper-wrapper')
        elWrapper.style.flexDirection = ''

        const elScrollBar = elSlider.querySelector('.swiper-scrollbar')
        elScrollBar.style.display = ''
      }


      if (worksPosts) {
        const worksPostsLinks = worksPosts.querySelectorAll(
          '.works-tabs-list__link'
        )

        if (worksPostsLinks) {
          worksPostsLinks.forEach((el) => {
            el.addEventListener('mousemove', (e) => {
              window.scrollTo(0, 0)
            })
          })
        }
      }
    }

    elDataItems.forEach((el) => {
      let item = el.innerHTML
      items.push(item)
    })

    /**
     * Vertical Slider
     */
    let sliderWorks = new Swiper(elSlider, {
      direction: 'vertical',
      mousewheel: true,
      mousewheelControl: true,
      slidesPerView: 'auto',
      autoHeight: true,
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
        draggable: true,
      },
    })

    sliderWorks.addSlide(1, items)
    sliderWorks.update(true)
    initWorksTabs()

    const btnExpand = document.querySelector('.btn-works-list-expand')
    if (btnExpand) {
      btnExpand.addEventListener('click', (e) => {
        e.preventDefault()

        sliderWorks.destroy(true, true)
        elSlider.style.height = 'auto'

        // Restyle Slider
        const elActionPanel = document.querySelector('.works-tabs-list__action')
        elActionPanel.style.display = 'none'

        const elWrapper = elSlider.querySelector('.swiper-wrapper')
        elWrapper.style.flexDirection = 'column'

        const elScrollBar = elSlider.querySelector('.swiper-scrollbar')
        elScrollBar.style.display = 'none'
      })
    }

    /**
     * Filter
     */
    const elFilter = document.querySelector('.works-posts__filter')
    const elFilterItems = elFilter.querySelectorAll('.tab-menu__link')

    const elFilterItemsLen = elFilterItems.length
    for (let i = 0; i < elFilterItemsLen; i++) {
      elFilterItems[i].addEventListener('mousemove', (e) => {
        e.preventDefault()
        const target = e.currentTarget

        elFilterItems.forEach((el) => {
          el.classList.remove('active')
        })

        updateSlider(target)
        hideWorks()
      })

      elFilterItems[i].addEventListener('click', (e) => {
        e.preventDefault()
      })


      if (worksPosts) {
        const worksPostsLinks = worksPosts.querySelectorAll(
          '.works-tabs-list__link'
        )

        if (worksPostsLinks) {
          worksPostsLinks.forEach((el) => {
            el.addEventListener('mousemove', (e) => {
              window.scrollTo(0, 0)
            })
          })
        }
      }
    }

    /**
     * Mobile Filter
     */
    const elMobFilter = document.querySelector('.works-filter')

    if (elMobFilter) {
      const elMobFilterBtn = elMobFilter.querySelector('.btn')
      const elMobFilterBtnText = elMobFilterBtn.querySelector('.btn__text')
      const elMobFilterOptions = elMobFilter.querySelector(
        '.works-filter__options'
      )
      const elMobFilterLinks = elMobFilter.querySelectorAll(
        '.works-filter__link '
      )

      if (elMobFilterBtn && elMobFilterOptions && elMobFilterLinks) {
        elMobFilterBtn.addEventListener('click', (e) => {
          e.preventDefault()
          let status = elMobFilterOptions.classList.contains('active')

          if (status) {
            elMobFilterOptions.classList.remove('active')
            elMobFilterBtn.classList.remove('active')
            return
          } else {
            elMobFilterOptions.classList.add('active')
            elMobFilterBtn.classList.add('active')
          }
        })

        elMobFilterLinks.forEach((el) => {
          el.addEventListener('click', (e) => {
            e.preventDefault

            elMobFilterLinks.forEach((el) => {
              el.classList.remove('active')
            })

            const target = e.currentTarget
            elMobFilterBtnText.innerHTML = target.innerHTML
            elMobFilterOptions.classList.remove('active')
            elMobFilterBtn.classList.remove('active')

            updateSlider(target)
            hideWorks()
          })
        })
      }
    }
  }
}

export default sliderWorksList
