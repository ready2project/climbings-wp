/**
 * Home: Services Tabs
 */

const initServiceTabs = () => {
  // Get all tabs
  const tabsServices = document.querySelectorAll('.services-tabs__link')
  const tabsServicesContent = document.querySelectorAll(
    '.services-content__item'
  )

  if (tabsServices) {
    const tabsServicesLength = tabsServices.length
    for (let i = 0; i < tabsServicesLength; i++) {
      /**
       * Set Active Status
       */
      tabsServices[i].addEventListener('mousemove', (e) => {
        e.preventDefault()

        tabsServices.forEach((el) => {
          el.classList.remove('active')
        })

        tabsServicesContent.forEach((el) => {
          el.classList.remove('active')
        })

        const obj = e.currentTarget
        obj.classList.add('active')

        let link = obj.hash
        if (link) {
          const actualTab = document.querySelector(link)

          if (actualTab) {
            actualTab.classList.add('active')
          }
        }
      })

      tabsServices[i].addEventListener('click', (e) => {
        e.preventDefault()
      })
    }
  }
}

export default initServiceTabs
