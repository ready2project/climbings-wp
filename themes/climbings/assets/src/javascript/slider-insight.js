/**
 * Home: Works Slider (Mobile)
 */
import Swiper, {Navigation} from 'swiper'
Swiper.use([Navigation])

const sliderInsight = () => {
  const insSlider = document.querySelector('.insight-slider')
  if (insSlider) {
    const sliderIns = new Swiper(insSlider, {
      loop: false,
      variableWidth: true,
      slidesPerView: 1,
      spaceBetween: 20,

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      breakpoints: {
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },

        528: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    })
  }
}

export default sliderInsight
