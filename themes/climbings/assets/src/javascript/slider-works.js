/**
 * Home: Works Slider (Mobile)
 */
import Swiper from 'swiper'

const sliderWorks = () => {
  const worksSlider = document.querySelector('.mobile-works-slider')
  if (worksSlider) {
    const sliderWorks = new Swiper(worksSlider, {
      loop: true,
      centeredSlides: true,
      spaceBetween: 20,
      slidesPerView: 1,
      preloadImages: false,
      lazy: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
    })
  }
}

export default sliderWorks
