import {checkVisible} from './helpers/helpers'

const sceneProcess = () => {
  if (!KeyshapeJS.version.indexOf('1.') != 0) {
    /**
     * Animation for Our Process Section
     */

    var sceneProcess = document.getElementById('sceneProcess')
    var sceneProcesslaunch = true

    /**
     * Show Animation
     */
    const showProcessSceneAnimation = () => {
      var offset = -300

      if (sceneProcesslaunch) {
        if (checkVisible(sceneProcess, offset)) {
          getProcessSceneAnimation()
          sceneProcesslaunch = false
        }
      }
    }

    /**
     * Animation Frames
     */
    const getProcessSceneAnimation = () => {
      window.ks = document.ks = KeyshapeJS
      ;(function (ks) {
        ks.animate("#Sun-1",[{p:'fillOpacity',t:[0,1000,3500,4500,7000,8000,10500],v:[.1,.3,.1,.3,.1,.3,.1],e:[[1,0,0,.6,1],[0],[1,0,0,.6,1],[0],[1,0,0,.6,1],[0],[0]]}],
        "#Sun-2",[{p:'fillOpacity',t:[0,500,1500,3500,4000,5000,7000,7500,8500,10500],v:[.1,0,.3,.1,0,.3,.1,0,.3,.1],e:[[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0]]}],
        "#Sun-3",[{p:'fillOpacity',t:[0,1000,2000,3500,4500,5500,7000,8000,9000,10500],v:[.1,0,.3,.1,0,.3,.1,0,.3,.1],e:[[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0]]}],
        "#Sun-4",[{p:'fillOpacity',t:[0,1500,2500,3500,5000,6000,7000,8500,9500,10500],v:[.1,0,.3,.1,0,.3,.1,0,.3,.1],e:[[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0],[1,0,0,.6,1],[0],[0]]}],
        "#Rocket",[{p:'mpath',t:[0,1000,2000,3000,4000,5000,6000,7000,8000,10000],v:['0%','6.8%','13.6%','20.4%','27.2%','34%','40.8%','47.6%','54.4%','100%'],e:[[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[1,.4,0,.6,1],[0]],mp:"M747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,60.5"},{p:'scaleX',t:[9000,10000],v:[1,0],e:[[1,.4,0,0,1],[0]]},{p:'scaleY',t:[9000,10000],v:[1,0],e:[[1,.4,0,0,1],[0]]}],
        "#Rocket-2",[{p:'opacity',t:[0,500,10000,10500],v:[1,0,0,1],e:[[0],[0],[0],[0]]}],
        "#Text-Launch",[{p:'opacity',t:[8000,8500],v:[0,1],e:[[0],[0]]}],
        "#Text-Test",[{p:'opacity',t:[7000,7500],v:[0,1],e:[[0],[0]]}],
        "#Text-Develop",[{p:'opacity',t:[5000,5500],v:[0,1],e:[[0],[0]]}],
        "#Text-Design",[{p:'opacity',t:[3000,3500],v:[0,1],e:[[0],[0]]}],
        "#Text-Discover",[{p:'opacity',t:[1000,1500],v:[0,1],e:[[1,.4,0,0,1],[0]]}],
        "#Flag---1",[{p:'rotate',t:[0,1000],v:[-10,0],e:[[1,.4,0,0,1],[0]]},{p:'scaleX',t:[0,1000],v:[0,1],e:[[1,.4,0,0,1],[0]]},{p:'scaleY',t:[0,1000],v:[0,1],e:[[1,.4,0,0,1],[0]]}],
        "#Flag---2",[{p:'rotate',t:[2000,3000],v:[-10,0],e:[[1,.4,0,0,1],[0]]},{p:'scaleX',t:[2000,3000],v:[0,1],e:[[1,.4,0,0,1],[0]]},{p:'scaleY',t:[2000,3000],v:[0,1],e:[[1,.4,0,0,1],[0]]}],
        "#Flag---3",[{p:'rotate',t:[4000,5000],v:[-10,0],e:[[1,.4,0,0,1],[0]]},{p:'scaleX',t:[4000,5000],v:[0,1],e:[[1,.4,0,0,1],[0]]},{p:'scaleY',t:[4000,5000],v:[0,1],e:[[1,.4,0,0,1],[0]]}],
        "#Flag---4",[{p:'rotate',t:[6000,7000],v:[-10,0],e:[[1,.4,0,0,1],[0]]},{p:'scaleX',t:[6000,7000],v:[0,1],e:[[1,.4,0,0,1],[0]]},{p:'scaleY',t:[6000,7000],v:[0,1],e:[[1,.4,0,0,1],[0]]}],
        {autoremove:false})
          .range(0, 10500)
          .loop(true)
        if (
          document.location.search
            .substr(1)
            .split('&')
            .indexOf('global=paused') >= 0
        )
          ks.globalPause()
      })(KeyshapeJS)
    }

    /**
     * Check Visibility
     */
    window.addEventListener('scroll', function () {
      showProcessSceneAnimation()
    })

    showProcessSceneAnimation()
  }
}

export default sceneProcess
