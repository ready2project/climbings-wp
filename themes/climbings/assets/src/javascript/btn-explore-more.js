/**
 * Button: Explore More
 */

const btnExploreMore = () => {
  const elBtnExploreMore = document.querySelector('.btn-explore-more')
  const elPopupExploreMore = document.querySelector('.popup-explore-more')

  if (elBtnExploreMore && elPopupExploreMore) {
    if ('IntersectionObserver' in window) {
      let observer = new IntersectionObserver(
        (entries) => {
          if (entries[0].isIntersecting === true) {
            elPopupExploreMore.classList.remove('active')
          } else {
            elPopupExploreMore.classList.add('active')
          }
        },
        {threshold: [0]}
      )

      observer.observe(elBtnExploreMore)
    }
  }
}

export default btnExploreMore
