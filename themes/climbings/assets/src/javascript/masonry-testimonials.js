/**
 * Masonry Testimonials
 */

const masonryTestimonials = () => {
  const elGrid = document.querySelector('.testimonials_tab-content__item')

  if (elGrid) {
    const msnry = new Masonry(elGrid, {
      // options
      itemSelector: '.feedback',
    })
  }
}

export default masonryTestimonials
