(function () {
  'use strict';

  /**
   * Helpers
   */
  /**
   * Get Element Height with Margin
   *
   * @param {Element} el
   */


  var getClientHeight = function getClientHeight(el) {
    if (el) {
      var styles = window.getComputedStyle(el);
      var marginTop = parseFloat(styles['marginTop']) || 0;
      var marginBottom = parseFloat(styles['marginBottom']) || 0;
      return Math.ceil(el.offsetHeight + marginTop + marginBottom);
    }
  };
  /**
   * Get Page Height
   */


  var getViewHeight = function getViewHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight);
  };
  /**
   * Check Element Visibility
   *
   * @param {Element} el
   */


  var checkVisible = function checkVisible(el, offset) {
    if (el) {
      if (isNaN(offset)) {
        offset = 0;
      }

      var rect = el.getBoundingClientRect();
      var viewHeight = getViewHeight();
      return !(rect.bottom < offset || rect.top - viewHeight >= offset);
    }
  };

  /**
   * Navigation
   */

  var initNav = function initNav() {
    /**
     * Button Toggle
     */
    var btnNav = document.getElementById('btnNav');
    var navBar = document.querySelector('.nav');
    var header = document.querySelector('.header');
    var headerAnchor = document.querySelector('.header .container');

    if (navBar) {
      /**
       * Nav Action
       */
      if (btnNav) {
        btnNav.addEventListener('click', function (e) {
          e.stopPropagation();
          e.preventDefault();
          btnNav.classList.toggle('active');
          document.body.classList.toggle('main-menu-active');
        });
      }
      /**
       * Correct Nav Button Position
       */


      var navBarStyle = navBar.style;
      navBarStyle.display = 'flex';
      /**
       * Event: Scroll (Short Menu)
       */

      if (header) {
        var setNavBtn = function setNavBtn() {
          var rect = headerAnchor.getBoundingClientRect();
          navBarStyle.right = '';

          if (window.innerWidth > 1200) {
            navBarStyle.right = rect.left + 15 + 'px';
          }
        };

        if (headerAnchor) {
          setNavBtn();
        }

        window.addEventListener('resize', function () {
          setNavBtn();
        });

        var navScroll = function navScroll() {
          var docScrollTop = document.body.scrollTop;
          var fixed_position = navBar.getBoundingClientRect().top + docScrollTop;
          var fixed_height = navBar.clientHeight;
          var toCross_position = header.getBoundingClientRect().top + docScrollTop;
          var toCross_height = header.clientHeight;

          if (fixed_position + fixed_height < toCross_position) {
            navBar.classList.add('nav-short');
          } else if (fixed_position > toCross_position + toCross_height) {
            navBar.classList.add('nav-short');
          } else {
            navBar.classList.remove('nav-short');
          }
        };

        navScroll();
        window.addEventListener('scroll', function (e) {
          navScroll();
        });
        /**
         * Event: Keypress
         */

        document.onkeydown = function (e) {
          if (document.body.classList.contains('main-menu-active')) {
            // Hide menu by Escape
            var keyEsc = 27;

            switch (e.keyCode) {
              case keyEsc:
                e.preventDefault();
                btnNav.click();
                break;
            }
          }
        };
        /**
         * Event: Blur
         */


        window.addEventListener('click', function () {
          if (document.body.classList.contains('main-menu-active')) {
            btnNav.click();
          }
        });
        var mainMenu = document.getElementById('mainMenu');

        if (mainMenu) {
          mainMenu.addEventListener('click', function (e) {
            e.stopPropagation();
          });
        }
      } // end. if header & nav

      /**
       * Button 'Start a Project' behaviour
       */


      var areasRestrictedNavStart = document.querySelectorAll('.area-startbtn-hide, .header');
      var areasRestrictedNavStartLength = areasRestrictedNavStart.length;

      var btnNavStartScroll = function btnNavStartScroll() {
        // Indicator Nav Position
        var fixed_position = navBar.getBoundingClientRect().top + document.body.scrollTop;
        var fixed_height = getClientHeight(navBar);

        if (areasRestrictedNavStartLength > 0) {
          for (var i = 0; i < areasRestrictedNavStartLength; i++) {
            var elCross = areasRestrictedNavStart[i];
            var toCross_position = elCross.getBoundingClientRect().top + document.body.scrollTop;
            var toCross_height = getClientHeight(elCross);

            if (fixed_position + fixed_height < toCross_position) {
              navBar.classList.add('nav-short');
            } else if (fixed_position > toCross_position + toCross_height) {
              navBar.classList.add('nav-short');
            } else {
              navBar.classList.remove('nav-short');
              break;
            }
          }
        }
      };

      btnNavStartScroll();
      window.addEventListener('scroll', function (e) {
        btnNavStartScroll();
      });
      /**
       * Hide menu after click
       */

      var menuLink = document.querySelectorAll('.main-menu__link');

      if (menuLink) {
        menuLink.forEach(function (el) {
          el.addEventListener('click', function (e) {
            btnNav.click();
          });
        });
      }
    }
  };

  /**
   * Button: Explore More
   */
  var btnExploreMore = function btnExploreMore() {
    var elBtnExploreMore = document.querySelector('.btn-explore-more');
    var elPopupExploreMore = document.querySelector('.popup-explore-more');

    if (elBtnExploreMore && elPopupExploreMore) {
      if ('IntersectionObserver' in window) {
        var observer = new IntersectionObserver(function (entries) {
          if (entries[0].isIntersecting === true) {
            elPopupExploreMore.classList.remove('active');
          } else {
            elPopupExploreMore.classList.add('active');
          }
        }, {
          threshold: [0]
        });
        observer.observe(elBtnExploreMore);
      }
    }
  };

  /*
   * Modal
   * ========================================================================== */

  var Modal = function Modal() {
    /**
     * Hide Popup by Close Button
     */
    var btnHide = document.querySelectorAll('.modal-hide');

    if (btnHide) {
      var btnHideLen = btnHide.length;

      for (var i = 0; i < btnHideLen; i++) {
        btnHide[i].addEventListener('click', function (e) {
          e.preventDefault();
          var dataTarget = e.currentTarget.dataset.target;
          var parent = document.getElementById(dataTarget);
          parent.classList.remove('active');
        });
      }
    }
    /**
     * Hide Popup bay Background
     */


    var modal = document.querySelectorAll('.modal');

    if (modal) {
      var modalLen = modal.length;

      for (var _i = 0; _i < modalLen; _i++) {
        modal[_i].addEventListener('click', function (e) {
          var obj = e.target;
          if (obj !== e.currentTarget) return;
          obj.classList.remove('active');
        });
      }
    }
    /**
     * Show Modal
     */


    var btnShow = document.querySelectorAll('.modal-link');

    if (btnShow) {
      var btnShowLen = btnShow.length;

      var _loop = function _loop(_i2) {
        // Show Event
        btnShow[_i2].addEventListener('show', function (e) {
          // Add Contact 7 Scripts for Contact Popup
          var btnAction = e.currentTarget;

          if (btnAction.dataset.target === 'contact-modal') ;
        }, false); // Click Event


        btnShow[_i2].addEventListener('click', function (e) {
          e.preventDefault();
          var dataTarget = e.currentTarget.dataset.target;
          var modal = document.getElementById(dataTarget);

          if (modal) {
            modal.classList.add('active');
            var event = new Event('show');

            btnShow[_i2].dispatchEvent(event);
          }
        });
      };

      for (var _i2 = 0; _i2 < btnShowLen; _i2++) {
        _loop(_i2);
      }
    }
  };

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  function _typeof$1(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof$1 = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof$1 = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof$1(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    var target = _objectWithoutPropertiesLoose(source, excluded);

    var key, i;

    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (_typeof(call) === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }

    return object;
  }

  function _get(target, property, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get;
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);

        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);

        if (desc.get) {
          return desc.get.call(receiver);
        }

        return desc.value;
      };
    }

    return _get(target, property, receiver || target);
  }

  function set(target, property, value, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.set) {
      set = Reflect.set;
    } else {
      set = function set(target, property, value, receiver) {
        var base = _superPropBase(target, property);

        var desc;

        if (base) {
          desc = Object.getOwnPropertyDescriptor(base, property);

          if (desc.set) {
            desc.set.call(receiver, value);
            return true;
          } else if (!desc.writable) {
            return false;
          }
        }

        desc = Object.getOwnPropertyDescriptor(receiver, property);

        if (desc) {
          if (!desc.writable) {
            return false;
          }

          desc.value = value;
          Object.defineProperty(receiver, property, desc);
        } else {
          _defineProperty(receiver, property, value);
        }

        return true;
      };
    }

    return set(target, property, value, receiver);
  }

  function _set(target, property, value, receiver, isStrict) {
    var s = set(target, property, value, receiver || target);

    if (!s && isStrict) {
      throw new Error('failed to set property');
    }

    return value;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$1(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _unsupportedIterableToArray$1(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray$1(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$1(o, minLen);
  }

  function _arrayLikeToArray$1(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  /** Checks if value is string */

  function isString(str) {
    return typeof str === 'string' || str instanceof String;
  }
  /**
    Direction
    @prop {string} NONE
    @prop {string} LEFT
    @prop {string} FORCE_LEFT
    @prop {string} RIGHT
    @prop {string} FORCE_RIGHT
  */


  var DIRECTION = {
    NONE: 'NONE',
    LEFT: 'LEFT',
    FORCE_LEFT: 'FORCE_LEFT',
    RIGHT: 'RIGHT',
    FORCE_RIGHT: 'FORCE_RIGHT'
  };
  /** */


  function forceDirection(direction) {
    switch (direction) {
      case DIRECTION.LEFT:
        return DIRECTION.FORCE_LEFT;

      case DIRECTION.RIGHT:
        return DIRECTION.FORCE_RIGHT;

      default:
        return direction;
    }
  }
  /** Escapes regular expression control chars */


  function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
  } // cloned from https://github.com/epoberezkin/fast-deep-equal with small changes


  function objectIncludes(b, a) {
    if (a === b) return true;
    var arrA = Array.isArray(a),
        arrB = Array.isArray(b),
        i;

    if (arrA && arrB) {
      if (a.length != b.length) return false;

      for (i = 0; i < a.length; i++) {
        if (!objectIncludes(a[i], b[i])) return false;
      }

      return true;
    }

    if (arrA != arrB) return false;

    if (a && b && _typeof$1(a) === 'object' && _typeof$1(b) === 'object') {
      var dateA = a instanceof Date,
          dateB = b instanceof Date;
      if (dateA && dateB) return a.getTime() == b.getTime();
      if (dateA != dateB) return false;
      var regexpA = a instanceof RegExp,
          regexpB = b instanceof RegExp;
      if (regexpA && regexpB) return a.toString() == b.toString();
      if (regexpA != regexpB) return false;
      var keys = Object.keys(a); // if (keys.length !== Object.keys(b).length) return false;

      for (i = 0; i < keys.length; i++) {
        if (!Object.prototype.hasOwnProperty.call(b, keys[i])) return false;
      }

      for (i = 0; i < keys.length; i++) {
        if (!objectIncludes(b[keys[i]], a[keys[i]])) return false;
      }

      return true;
    } else if (a && b && typeof a === 'function' && typeof b === 'function') {
      return a.toString() === b.toString();
    }

    return false;
  }

  /** Provides details of changing input */

  var ActionDetails = /*#__PURE__*/function () {
    /** Current input value */

    /** Current cursor position */

    /** Old input value */

    /** Old selection */
    function ActionDetails(value, cursorPos, oldValue, oldSelection) {
      _classCallCheck(this, ActionDetails);

      this.value = value;
      this.cursorPos = cursorPos;
      this.oldValue = oldValue;
      this.oldSelection = oldSelection; // double check if left part was changed (autofilling, other non-standard input triggers)

      while (this.value.slice(0, this.startChangePos) !== this.oldValue.slice(0, this.startChangePos)) {
        --this.oldSelection.start;
      }
    }
    /**
      Start changing position
      @readonly
    */


    _createClass(ActionDetails, [{
      key: "startChangePos",
      get: function get() {
        return Math.min(this.cursorPos, this.oldSelection.start);
      }
      /**
        Inserted symbols count
        @readonly
      */

    }, {
      key: "insertedCount",
      get: function get() {
        return this.cursorPos - this.startChangePos;
      }
      /**
        Inserted symbols
        @readonly
      */

    }, {
      key: "inserted",
      get: function get() {
        return this.value.substr(this.startChangePos, this.insertedCount);
      }
      /**
        Removed symbols count
        @readonly
      */

    }, {
      key: "removedCount",
      get: function get() {
        // Math.max for opposite operation
        return Math.max(this.oldSelection.end - this.startChangePos || // for Delete
        this.oldValue.length - this.value.length, 0);
      }
      /**
        Removed symbols
        @readonly
      */

    }, {
      key: "removed",
      get: function get() {
        return this.oldValue.substr(this.startChangePos, this.removedCount);
      }
      /**
        Unchanged head symbols
        @readonly
      */

    }, {
      key: "head",
      get: function get() {
        return this.value.substring(0, this.startChangePos);
      }
      /**
        Unchanged tail symbols
        @readonly
      */

    }, {
      key: "tail",
      get: function get() {
        return this.value.substring(this.startChangePos + this.insertedCount);
      }
      /**
        Remove direction
        @readonly
      */

    }, {
      key: "removeDirection",
      get: function get() {
        if (!this.removedCount || this.insertedCount) return DIRECTION.NONE; // align right if delete at right or if range removed (event with backspace)

        return this.oldSelection.end === this.cursorPos || this.oldSelection.start === this.cursorPos ? DIRECTION.RIGHT : DIRECTION.LEFT;
      }
    }]);

    return ActionDetails;
  }();

  /**
    Provides details of changing model value
    @param {Object} [details]
    @param {string} [details.inserted] - Inserted symbols
    @param {boolean} [details.skip] - Can skip chars
    @param {number} [details.removeCount] - Removed symbols count
    @param {number} [details.tailShift] - Additional offset if any changes occurred before tail
  */

  var ChangeDetails = /*#__PURE__*/function () {
    /** Inserted symbols */

    /** Can skip chars */

    /** Additional offset if any changes occurred before tail */

    /** Raw inserted is used by dynamic mask */
    function ChangeDetails(details) {
      _classCallCheck(this, ChangeDetails);

      Object.assign(this, {
        inserted: '',
        rawInserted: '',
        skip: false,
        tailShift: 0
      }, details);
    }
    /**
      Aggregate changes
      @returns {ChangeDetails} `this`
    */


    _createClass(ChangeDetails, [{
      key: "aggregate",
      value: function aggregate(details) {
        this.rawInserted += details.rawInserted;
        this.skip = this.skip || details.skip;
        this.inserted += details.inserted;
        this.tailShift += details.tailShift;
        return this;
      }
      /** Total offset considering all changes */

    }, {
      key: "offset",
      get: function get() {
        return this.tailShift + this.inserted.length;
      }
    }]);

    return ChangeDetails;
  }();

  /** Provides details of continuous extracted tail */

  var ContinuousTailDetails = /*#__PURE__*/function () {
    /** Tail value as string */

    /** Tail start position */

    /** Start position */
    function ContinuousTailDetails() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var stop = arguments.length > 2 ? arguments[2] : undefined;

      _classCallCheck(this, ContinuousTailDetails);

      this.value = value;
      this.from = from;
      this.stop = stop;
    }

    _createClass(ContinuousTailDetails, [{
      key: "toString",
      value: function toString() {
        return this.value;
      }
    }, {
      key: "extend",
      value: function extend(tail) {
        this.value += String(tail);
      }
    }, {
      key: "appendTo",
      value: function appendTo(masked) {
        return masked.append(this.toString(), {
          tail: true
        }).aggregate(masked._appendPlaceholder());
      }
    }, {
      key: "state",
      get: function get() {
        return {
          value: this.value,
          from: this.from,
          stop: this.stop
        };
      },
      set: function set(state) {
        Object.assign(this, state);
      }
    }, {
      key: "shiftBefore",
      value: function shiftBefore(pos) {
        if (this.from >= pos || !this.value.length) return '';
        var shiftChar = this.value[0];
        this.value = this.value.slice(1);
        return shiftChar;
      }
    }]);

    return ContinuousTailDetails;
  }();

  /**
   * Applies mask on element.
   * @constructor
   * @param {HTMLInputElement|HTMLTextAreaElement|MaskElement} el - Element to apply mask
   * @param {Object} opts - Custom mask options
   * @return {InputMask}
   */
  function IMask(el) {
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {}; // currently available only for input-like elements

    return new IMask.InputMask(el, opts);
  }

  /** Supported mask type */

  /** Provides common masking stuff */

  var Masked = /*#__PURE__*/function () {
    // $Shape<MaskedOptions>; TODO after fix https://github.com/facebook/flow/issues/4773

    /** @type {Mask} */

    /** */
    // $FlowFixMe no ideas

    /** Transforms value before mask processing */

    /** Validates if value is acceptable */

    /** Does additional processing in the end of editing */

    /** Format typed value to string */

    /** Parse strgin to get typed value */

    /** Enable characters overwriting */

    /** */
    function Masked(opts) {
      _classCallCheck(this, Masked);

      this._value = '';

      this._update(Object.assign({}, Masked.DEFAULTS, opts));

      this.isInitialized = true;
    }
    /** Sets and applies new options */


    _createClass(Masked, [{
      key: "updateOptions",
      value: function updateOptions(opts) {
        if (!Object.keys(opts).length) return;
        this.withValueRefresh(this._update.bind(this, opts));
      }
      /**
        Sets new options
        @protected
      */

    }, {
      key: "_update",
      value: function _update(opts) {
        Object.assign(this, opts);
      }
      /** Mask state */

    }, {
      key: "state",
      get: function get() {
        return {
          _value: this.value
        };
      },
      set: function set(state) {
        this._value = state._value;
      }
      /** Resets value */

    }, {
      key: "reset",
      value: function reset() {
        this._value = '';
      }
      /** */

    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(value) {
        this.resolve(value);
      }
      /** Resolve new value */

    }, {
      key: "resolve",
      value: function resolve(value) {
        this.reset();
        this.append(value, {
          input: true
        }, '');
        this.doCommit();
        return this.value;
      }
      /** */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.value;
      },
      set: function set(value) {
        this.reset();
        this.append(value, {}, '');
        this.doCommit();
      }
      /** */

    }, {
      key: "typedValue",
      get: function get() {
        return this.doParse(this.value);
      },
      set: function set(value) {
        this.value = this.doFormat(value);
      }
      /** Value that includes raw user input */

    }, {
      key: "rawInputValue",
      get: function get() {
        return this.extractInput(0, this.value.length, {
          raw: true
        });
      },
      set: function set(value) {
        this.reset();
        this.append(value, {
          raw: true
        }, '');
        this.doCommit();
      }
      /** */

    }, {
      key: "isComplete",
      get: function get() {
        return true;
      }
      /** Finds nearest input position in direction */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos, direction) {
        return cursorPos;
      }
      /** Extracts value in range considering flags */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return this.value.slice(fromPos, toPos);
      }
      /** Extracts tail in range */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return new ContinuousTailDetails(this.extractInput(fromPos, toPos), fromPos);
      }
      /** Appends tail */
      // $FlowFixMe no ideas

    }, {
      key: "appendTail",
      value: function appendTail(tail) {
        if (isString(tail)) tail = new ContinuousTailDetails(String(tail));
        return tail.appendTo(this);
      }
      /** Appends char */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        if (!ch) return new ChangeDetails();
        this._value += ch;
        return new ChangeDetails({
          inserted: ch,
          rawInserted: ch
        });
      }
      /** Appends char */

    }, {
      key: "_appendChar",
      value: function _appendChar(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var checkTail = arguments.length > 2 ? arguments[2] : undefined;
        var consistentState = this.state;

        var details = this._appendCharRaw(this.doPrepare(ch, flags), flags);

        if (details.inserted) {
          var consistentTail;
          var appended = this.doValidate(flags) !== false;

          if (appended && checkTail != null) {
            // validation ok, check tail
            var beforeTailState = this.state;

            if (this.overwrite) {
              consistentTail = checkTail.state;
              checkTail.shiftBefore(this.value.length);
            }

            var tailDetails = this.appendTail(checkTail);
            appended = tailDetails.rawInserted === checkTail.toString(); // if ok, rollback state after tail

            if (appended && tailDetails.inserted) this.state = beforeTailState;
          } // revert all if something went wrong


          if (!appended) {
            details = new ChangeDetails();
            this.state = consistentState;
            if (checkTail && consistentTail) checkTail.state = consistentTail;
          }
        }

        return details;
      }
      /** Appends optional placeholder at end */

    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        return new ChangeDetails();
      }
      /** Appends symbols considering flags */
      // $FlowFixMe no ideas

    }, {
      key: "append",
      value: function append(str, flags, tail) {
        if (!isString(str)) throw new Error('value should be string');
        var details = new ChangeDetails();
        var checkTail = isString(tail) ? new ContinuousTailDetails(String(tail)) : tail;
        if (flags && flags.tail) flags._beforeTailState = this.state;

        for (var ci = 0; ci < str.length; ++ci) {
          details.aggregate(this._appendChar(str[ci], flags, checkTail));
        } // append tail but aggregate only tailShift


        if (checkTail != null) {
          details.tailShift += this.appendTail(checkTail).tailShift; // TODO it's a good idea to clear state after appending ends
          // but it causes bugs when one append calls another (when dynamic dispatch set rawInputValue)
          // this._resetBeforeTailState();
        }

        return details;
      }
      /** */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        this._value = this.value.slice(0, fromPos) + this.value.slice(toPos);
        return new ChangeDetails();
      }
      /** Calls function and reapplies current value */

    }, {
      key: "withValueRefresh",
      value: function withValueRefresh(fn) {
        if (this._refreshing || !this.isInitialized) return fn();
        this._refreshing = true;
        var rawInput = this.rawInputValue;
        var value = this.value;
        var ret = fn();
        this.rawInputValue = rawInput; // append lost trailing chars at end

        if (this.value && this.value !== value && value.indexOf(this.value) === 0) {
          this.append(value.slice(this.value.length), {}, '');
        }

        delete this._refreshing;
        return ret;
      }
      /** */

    }, {
      key: "runIsolated",
      value: function runIsolated(fn) {
        if (this._isolated || !this.isInitialized) return fn(this);
        this._isolated = true;
        var state = this.state;
        var ret = fn(this);
        this.state = state;
        delete this._isolated;
        return ret;
      }
      /**
        Prepares string before mask processing
        @protected
      */

    }, {
      key: "doPrepare",
      value: function doPrepare(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.prepare ? this.prepare(str, this, flags) : str;
      }
      /**
        Validates if value is acceptable
        @protected
      */

    }, {
      key: "doValidate",
      value: function doValidate(flags) {
        return (!this.validate || this.validate(this.value, this, flags)) && (!this.parent || this.parent.doValidate(flags));
      }
      /**
        Does additional processing in the end of editing
        @protected
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        if (this.commit) this.commit(this.value, this);
      }
      /** */

    }, {
      key: "doFormat",
      value: function doFormat(value) {
        return this.format ? this.format(value, this) : value;
      }
      /** */

    }, {
      key: "doParse",
      value: function doParse(str) {
        return this.parse ? this.parse(str, this) : str;
      }
      /** */

    }, {
      key: "splice",
      value: function splice(start, deleteCount, inserted, removeDirection) {
        var tailPos = start + deleteCount;
        var tail = this.extractTail(tailPos);
        var startChangePos = this.nearestInputPos(start, removeDirection);
        var changeDetails = new ChangeDetails({
          tailShift: startChangePos - start // adjust tailShift if start was aligned

        }).aggregate(this.remove(startChangePos)).aggregate(this.append(inserted, {
          input: true
        }, tail));
        return changeDetails;
      }
    }]);

    return Masked;
  }();

  Masked.DEFAULTS = {
    format: function format(v) {
      return v;
    },
    parse: function parse(v) {
      return v;
    }
  };
  IMask.Masked = Masked;

  /** Get Masked class by mask type */

  function maskedClass(mask) {
    if (mask == null) {
      throw new Error('mask property should be defined');
    } // $FlowFixMe


    if (mask instanceof RegExp) return IMask.MaskedRegExp; // $FlowFixMe

    if (isString(mask)) return IMask.MaskedPattern; // $FlowFixMe

    if (mask instanceof Date || mask === Date) return IMask.MaskedDate; // $FlowFixMe

    if (mask instanceof Number || typeof mask === 'number' || mask === Number) return IMask.MaskedNumber; // $FlowFixMe

    if (Array.isArray(mask) || mask === Array) return IMask.MaskedDynamic; // $FlowFixMe

    if (IMask.Masked && mask.prototype instanceof IMask.Masked) return mask; // $FlowFixMe

    if (mask instanceof Function) return IMask.MaskedFunction; // $FlowFixMe

    if (mask instanceof IMask.Masked) return mask.constructor;
    console.warn('Mask not found for mask', mask); // eslint-disable-line no-console
    // $FlowFixMe

    return IMask.Masked;
  }
  /** Creates new {@link Masked} depending on mask type */


  function createMask(opts) {
    // $FlowFixMe
    if (IMask.Masked && opts instanceof IMask.Masked) return opts;
    opts = Object.assign({}, opts);
    var mask = opts.mask; // $FlowFixMe

    if (IMask.Masked && mask instanceof IMask.Masked) return mask;
    var MaskedClass = maskedClass(mask);
    if (!MaskedClass) throw new Error('Masked class is not found for provided mask, appropriate module needs to be import manually before creating mask.');
    return new MaskedClass(opts);
  }

  IMask.createMask = createMask;

  var DEFAULT_INPUT_DEFINITIONS = {
    '0': /\d/,
    'a': /[\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]/,
    // http://stackoverflow.com/a/22075070
    '*': /./
  };
  /** */

  var PatternInputDefinition = /*#__PURE__*/function () {
    /** */

    /** */

    /** */

    /** */

    /** */

    /** */
    function PatternInputDefinition(opts) {
      _classCallCheck(this, PatternInputDefinition);

      var mask = opts.mask,
          blockOpts = _objectWithoutProperties(opts, ["mask"]);

      this.masked = createMask({
        mask: mask
      });
      Object.assign(this, blockOpts);
    }

    _createClass(PatternInputDefinition, [{
      key: "reset",
      value: function reset() {
        this._isFilled = false;
        this.masked.reset();
      }
    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        if (fromPos === 0 && toPos >= 1) {
          this._isFilled = false;
          return this.masked.remove(fromPos, toPos);
        }

        return new ChangeDetails();
      }
    }, {
      key: "value",
      get: function get() {
        return this.masked.value || (this._isFilled && !this.isOptional ? this.placeholderChar : '');
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.masked.unmaskedValue;
      }
    }, {
      key: "isComplete",
      get: function get() {
        return Boolean(this.masked.value) || this.isOptional;
      }
    }, {
      key: "_appendChar",
      value: function _appendChar(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        if (this._isFilled) return new ChangeDetails();
        var state = this.masked.state; // simulate input

        var details = this.masked._appendChar(str, flags);

        if (details.inserted && this.doValidate(flags) === false) {
          details.inserted = details.rawInserted = '';
          this.masked.state = state;
        }

        if (!details.inserted && !this.isOptional && !this.lazy && !flags.input) {
          details.inserted = this.placeholderChar;
        }

        details.skip = !details.inserted && !this.isOptional;
        this._isFilled = Boolean(details.inserted);
        return details;
      }
    }, {
      key: "append",
      value: function append() {
        var _this$masked;

        return (_this$masked = this.masked).append.apply(_this$masked, arguments);
      }
    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        var details = new ChangeDetails();
        if (this._isFilled || this.isOptional) return details;
        this._isFilled = true;
        details.inserted = this.placeholderChar;
        return details;
      }
    }, {
      key: "extractTail",
      value: function extractTail() {
        var _this$masked2;

        return (_this$masked2 = this.masked).extractTail.apply(_this$masked2, arguments);
      }
    }, {
      key: "appendTail",
      value: function appendTail() {
        var _this$masked3;

        return (_this$masked3 = this.masked).appendTail.apply(_this$masked3, arguments);
      }
    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var flags = arguments.length > 2 ? arguments[2] : undefined;
        return this.masked.extractInput(fromPos, toPos, flags);
      }
    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE;
        var minPos = 0;
        var maxPos = this.value.length;
        var boundPos = Math.min(Math.max(cursorPos, minPos), maxPos);

        switch (direction) {
          case DIRECTION.LEFT:
          case DIRECTION.FORCE_LEFT:
            return this.isComplete ? boundPos : minPos;

          case DIRECTION.RIGHT:
          case DIRECTION.FORCE_RIGHT:
            return this.isComplete ? boundPos : maxPos;

          case DIRECTION.NONE:
          default:
            return boundPos;
        }
      }
    }, {
      key: "doValidate",
      value: function doValidate() {
        var _this$masked4, _this$parent;

        return (_this$masked4 = this.masked).doValidate.apply(_this$masked4, arguments) && (!this.parent || (_this$parent = this.parent).doValidate.apply(_this$parent, arguments));
      }
    }, {
      key: "doCommit",
      value: function doCommit() {
        this.masked.doCommit();
      }
    }, {
      key: "state",
      get: function get() {
        return {
          masked: this.masked.state,
          _isFilled: this._isFilled
        };
      },
      set: function set(state) {
        this.masked.state = state.masked;
        this._isFilled = state._isFilled;
      }
    }]);

    return PatternInputDefinition;
  }();

  var PatternFixedDefinition = /*#__PURE__*/function () {
    /** */

    /** */

    /** */

    /** */
    function PatternFixedDefinition(opts) {
      _classCallCheck(this, PatternFixedDefinition);

      Object.assign(this, opts);
      this._value = '';
    }

    _createClass(PatternFixedDefinition, [{
      key: "value",
      get: function get() {
        return this._value;
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.isUnmasking ? this.value : '';
      }
    }, {
      key: "reset",
      value: function reset() {
        this._isRawInput = false;
        this._value = '';
      }
    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._value.length;
        this._value = this._value.slice(0, fromPos) + this._value.slice(toPos);
        if (!this._value) this._isRawInput = false;
        return new ChangeDetails();
      }
    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE;
        var minPos = 0;
        var maxPos = this._value.length;

        switch (direction) {
          case DIRECTION.LEFT:
          case DIRECTION.FORCE_LEFT:
            return minPos;

          case DIRECTION.NONE:
          case DIRECTION.RIGHT:
          case DIRECTION.FORCE_RIGHT:
          default:
            return maxPos;
        }
      }
    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._value.length;
        var flags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        return flags.raw && this._isRawInput && this._value.slice(fromPos, toPos) || '';
      }
    }, {
      key: "isComplete",
      get: function get() {
        return true;
      }
    }, {
      key: "_appendChar",
      value: function _appendChar(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var details = new ChangeDetails();
        if (this._value) return details;
        var appended = this["char"] === str[0];
        var isResolved = appended && (this.isUnmasking || flags.input || flags.raw) && !flags.tail;
        if (isResolved) details.rawInserted = this["char"];
        this._value = details.inserted = this["char"];
        this._isRawInput = isResolved && (flags.raw || flags.input);
        return details;
      }
    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        var details = new ChangeDetails();
        if (this._value) return details;
        this._value = details.inserted = this["char"];
        return details;
      }
    }, {
      key: "extractTail",
      value: function extractTail() {
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return new ContinuousTailDetails('');
      } // $FlowFixMe no ideas

    }, {
      key: "appendTail",
      value: function appendTail(tail) {
        if (isString(tail)) tail = new ContinuousTailDetails(String(tail));
        return tail.appendTo(this);
      }
    }, {
      key: "append",
      value: function append(str, flags, tail) {
        var details = this._appendChar(str, flags);

        if (tail != null) {
          details.tailShift += this.appendTail(tail).tailShift;
        }

        return details;
      }
    }, {
      key: "doCommit",
      value: function doCommit() {}
    }, {
      key: "state",
      get: function get() {
        return {
          _value: this._value,
          _isRawInput: this._isRawInput
        };
      },
      set: function set(state) {
        Object.assign(this, state);
      }
    }]);

    return PatternFixedDefinition;
  }();

  var ChunksTailDetails = /*#__PURE__*/function () {
    /** */
    function ChunksTailDetails() {
      var chunks = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      _classCallCheck(this, ChunksTailDetails);

      this.chunks = chunks;
      this.from = from;
    }

    _createClass(ChunksTailDetails, [{
      key: "toString",
      value: function toString() {
        return this.chunks.map(String).join('');
      } // $FlowFixMe no ideas

    }, {
      key: "extend",
      value: function extend(tailChunk) {
        if (!String(tailChunk)) return;
        if (isString(tailChunk)) tailChunk = new ContinuousTailDetails(String(tailChunk));
        var lastChunk = this.chunks[this.chunks.length - 1];
        var extendLast = lastChunk && ( // if stops are same or tail has no stop
        lastChunk.stop === tailChunk.stop || tailChunk.stop == null) && // if tail chunk goes just after last chunk
        tailChunk.from === lastChunk.from + lastChunk.toString().length;

        if (tailChunk instanceof ContinuousTailDetails) {
          // check the ability to extend previous chunk
          if (extendLast) {
            // extend previous chunk
            lastChunk.extend(tailChunk.toString());
          } else {
            // append new chunk
            this.chunks.push(tailChunk);
          }
        } else if (tailChunk instanceof ChunksTailDetails) {
          if (tailChunk.stop == null) {
            // unwrap floating chunks to parent, keeping `from` pos
            var firstTailChunk;

            while (tailChunk.chunks.length && tailChunk.chunks[0].stop == null) {
              firstTailChunk = tailChunk.chunks.shift();
              firstTailChunk.from += tailChunk.from;
              this.extend(firstTailChunk);
            }
          } // if tail chunk still has value


          if (tailChunk.toString()) {
            // if chunks contains stops, then popup stop to container
            tailChunk.stop = tailChunk.blockIndex;
            this.chunks.push(tailChunk);
          }
        }
      }
    }, {
      key: "appendTo",
      value: function appendTo(masked) {
        // $FlowFixMe
        if (!(masked instanceof IMask.MaskedPattern)) {
          var tail = new ContinuousTailDetails(this.toString());
          return tail.appendTo(masked);
        }

        var details = new ChangeDetails();

        for (var ci = 0; ci < this.chunks.length && !details.skip; ++ci) {
          var chunk = this.chunks[ci];

          var lastBlockIter = masked._mapPosToBlock(masked.value.length);

          var stop = chunk.stop;
          var chunkBlock = void 0;

          if (stop != null && ( // if block not found or stop is behind lastBlock
          !lastBlockIter || lastBlockIter.index <= stop)) {
            if (chunk instanceof ChunksTailDetails || // for continuous block also check if stop is exist
            masked._stops.indexOf(stop) >= 0) {
              details.aggregate(masked._appendPlaceholder(stop));
            }

            chunkBlock = chunk instanceof ChunksTailDetails && masked._blocks[stop];
          }

          if (chunkBlock) {
            var tailDetails = chunkBlock.appendTail(chunk);
            tailDetails.skip = false; // always ignore skip, it will be set on last

            details.aggregate(tailDetails);
            masked._value += tailDetails.inserted; // get not inserted chars

            var remainChars = chunk.toString().slice(tailDetails.rawInserted.length);
            if (remainChars) details.aggregate(masked.append(remainChars, {
              tail: true
            }));
          } else {
            details.aggregate(masked.append(chunk.toString(), {
              tail: true
            }));
          }
        }

        return details;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          chunks: this.chunks.map(function (c) {
            return c.state;
          }),
          from: this.from,
          stop: this.stop,
          blockIndex: this.blockIndex
        };
      },
      set: function set(state) {
        var chunks = state.chunks,
            props = _objectWithoutProperties(state, ["chunks"]);

        Object.assign(this, props);
        this.chunks = chunks.map(function (cstate) {
          var chunk = "chunks" in cstate ? new ChunksTailDetails() : new ContinuousTailDetails(); // $FlowFixMe already checked above

          chunk.state = cstate;
          return chunk;
        });
      }
    }, {
      key: "shiftBefore",
      value: function shiftBefore(pos) {
        if (this.from >= pos || !this.chunks.length) return '';
        var chunkShiftPos = pos - this.from;
        var ci = 0;

        while (ci < this.chunks.length) {
          var chunk = this.chunks[ci];
          var shiftChar = chunk.shiftBefore(chunkShiftPos);

          if (chunk.toString()) {
            // chunk still contains value
            // but not shifted - means no more available chars to shift
            if (!shiftChar) break;
            ++ci;
          } else {
            // clean if chunk has no value
            this.chunks.splice(ci, 1);
          }

          if (shiftChar) return shiftChar;
        }

        return '';
      }
    }]);

    return ChunksTailDetails;
  }();

  /** Masking by RegExp */

  var MaskedRegExp = /*#__PURE__*/function (_Masked) {
    _inherits(MaskedRegExp, _Masked);

    var _super = _createSuper(MaskedRegExp);

    function MaskedRegExp() {
      _classCallCheck(this, MaskedRegExp);

      return _super.apply(this, arguments);
    }

    _createClass(MaskedRegExp, [{
      key: "_update",
      value:
      /**
        @override
        @param {Object} opts
      */
      function _update(opts) {
        if (opts.mask) opts.validate = function (value) {
          return value.search(opts.mask) >= 0;
        };

        _get(_getPrototypeOf(MaskedRegExp.prototype), "_update", this).call(this, opts);
      }
    }]);

    return MaskedRegExp;
  }(Masked);

  IMask.MaskedRegExp = MaskedRegExp;

  /**
    Pattern mask
    @param {Object} opts
    @param {Object} opts.blocks
    @param {Object} opts.definitions
    @param {string} opts.placeholderChar
    @param {boolean} opts.lazy
  */

  var MaskedPattern = /*#__PURE__*/function (_Masked) {
    _inherits(MaskedPattern, _Masked);

    var _super = _createSuper(MaskedPattern);
    /** */

    /** */

    /** Single char for empty input */

    /** Show placeholder only when needed */


    function MaskedPattern() {
      var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      _classCallCheck(this, MaskedPattern); // TODO type $Shape<MaskedPatternOptions>={} does not work


      opts.definitions = Object.assign({}, DEFAULT_INPUT_DEFINITIONS, opts.definitions);
      return _super.call(this, Object.assign({}, MaskedPattern.DEFAULTS, opts));
    }
    /**
      @override
      @param {Object} opts
    */


    _createClass(MaskedPattern, [{
      key: "_update",
      value: function _update() {
        var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        opts.definitions = Object.assign({}, this.definitions, opts.definitions);

        _get(_getPrototypeOf(MaskedPattern.prototype), "_update", this).call(this, opts);

        this._rebuildMask();
      }
      /** */

    }, {
      key: "_rebuildMask",
      value: function _rebuildMask() {
        var _this = this;

        var defs = this.definitions;
        this._blocks = [];
        this._stops = [];
        this._maskedBlocks = {};
        var pattern = this.mask;
        if (!pattern || !defs) return;
        var unmaskingBlock = false;
        var optionalBlock = false;

        for (var i = 0; i < pattern.length; ++i) {
          if (this.blocks) {
            var _ret = function () {
              var p = pattern.slice(i);
              var bNames = Object.keys(_this.blocks).filter(function (bName) {
                return p.indexOf(bName) === 0;
              }); // order by key length

              bNames.sort(function (a, b) {
                return b.length - a.length;
              }); // use block name with max length

              var bName = bNames[0];

              if (bName) {
                // $FlowFixMe no ideas
                var maskedBlock = createMask(Object.assign({
                  parent: _this,
                  lazy: _this.lazy,
                  placeholderChar: _this.placeholderChar,
                  overwrite: _this.overwrite
                }, _this.blocks[bName]));

                if (maskedBlock) {
                  _this._blocks.push(maskedBlock); // store block index


                  if (!_this._maskedBlocks[bName]) _this._maskedBlocks[bName] = [];

                  _this._maskedBlocks[bName].push(_this._blocks.length - 1);
                }

                i += bName.length - 1;
                return "continue";
              }
            }();

            if (_ret === "continue") continue;
          }

          var _char = pattern[i];

          var _isInput = (_char in defs);

          if (_char === MaskedPattern.STOP_CHAR) {
            this._stops.push(this._blocks.length);

            continue;
          }

          if (_char === '{' || _char === '}') {
            unmaskingBlock = !unmaskingBlock;
            continue;
          }

          if (_char === '[' || _char === ']') {
            optionalBlock = !optionalBlock;
            continue;
          }

          if (_char === MaskedPattern.ESCAPE_CHAR) {
            ++i;
            _char = pattern[i];
            if (!_char) break;
            _isInput = false;
          }

          var def = _isInput ? new PatternInputDefinition({
            parent: this,
            lazy: this.lazy,
            placeholderChar: this.placeholderChar,
            mask: defs[_char],
            isOptional: optionalBlock
          }) : new PatternFixedDefinition({
            "char": _char,
            isUnmasking: unmaskingBlock
          });

          this._blocks.push(def);
        }
      }
      /**
        @override
      */

    }, {
      key: "state",
      get: function get() {
        return Object.assign({}, _get(_getPrototypeOf(MaskedPattern.prototype), "state", this), {
          _blocks: this._blocks.map(function (b) {
            return b.state;
          })
        });
      },
      set: function set(state) {
        var _blocks = state._blocks,
            maskedState = _objectWithoutProperties(state, ["_blocks"]);

        this._blocks.forEach(function (b, bi) {
          return b.state = _blocks[bi];
        });

        _set(_getPrototypeOf(MaskedPattern.prototype), "state", maskedState, this, true);
      }
      /**
        @override
      */

    }, {
      key: "reset",
      value: function reset() {
        _get(_getPrototypeOf(MaskedPattern.prototype), "reset", this).call(this);

        this._blocks.forEach(function (b) {
          return b.reset();
        });
      }
      /**
        @override
      */

    }, {
      key: "isComplete",
      get: function get() {
        return this._blocks.every(function (b) {
          return b.isComplete;
        });
      }
      /**
        @override
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        this._blocks.forEach(function (b) {
          return b.doCommit();
        });

        _get(_getPrototypeOf(MaskedPattern.prototype), "doCommit", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._blocks.reduce(function (str, b) {
          return str += b.unmaskedValue;
        }, '');
      },
      set: function set(unmaskedValue) {
        _set(_getPrototypeOf(MaskedPattern.prototype), "unmaskedValue", unmaskedValue, this, true);
      }
      /**
        @override
      */

    }, {
      key: "value",
      get: function get() {
        // TODO return _value when not in change?
        return this._blocks.reduce(function (str, b) {
          return str += b.value;
        }, '');
      },
      set: function set(value) {
        _set(_getPrototypeOf(MaskedPattern.prototype), "value", value, this, true);
      }
      /**
        @override
      */

    }, {
      key: "appendTail",
      value: function appendTail(tail) {
        return _get(_getPrototypeOf(MaskedPattern.prototype), "appendTail", this).call(this, tail).aggregate(this._appendPlaceholder());
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var blockIter = this._mapPosToBlock(this.value.length);

        var details = new ChangeDetails();
        if (!blockIter) return details;

        for (var bi = blockIter.index;; ++bi) {
          var _block = this._blocks[bi];
          if (!_block) break;

          var blockDetails = _block._appendChar(ch, flags);

          var skip = blockDetails.skip;
          details.aggregate(blockDetails);
          if (skip || blockDetails.rawInserted) break; // go next char
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var _this2 = this;

        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var chunkTail = new ChunksTailDetails();
        if (fromPos === toPos) return chunkTail;

        this._forEachBlocksInRange(fromPos, toPos, function (b, bi, bFromPos, bToPos) {
          var blockChunk = b.extractTail(bFromPos, bToPos);
          blockChunk.stop = _this2._findStopBefore(bi);
          blockChunk.from = _this2._blockStartPos(bi);
          if (blockChunk instanceof ChunksTailDetails) blockChunk.blockIndex = bi;
          chunkTail.extend(blockChunk);
        });

        return chunkTail;
      }
      /**
        @override
      */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var flags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        if (fromPos === toPos) return '';
        var input = '';

        this._forEachBlocksInRange(fromPos, toPos, function (b, _, fromPos, toPos) {
          input += b.extractInput(fromPos, toPos, flags);
        });

        return input;
      }
    }, {
      key: "_findStopBefore",
      value: function _findStopBefore(blockIndex) {
        var stopBefore;

        for (var si = 0; si < this._stops.length; ++si) {
          var stop = this._stops[si];
          if (stop <= blockIndex) stopBefore = stop;else break;
        }

        return stopBefore;
      }
      /** Appends placeholder depending on laziness */

    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder(toBlockIndex) {
        var _this3 = this;

        var details = new ChangeDetails();
        if (this.lazy && toBlockIndex == null) return details;

        var startBlockIter = this._mapPosToBlock(this.value.length);

        if (!startBlockIter) return details;
        var startBlockIndex = startBlockIter.index;
        var endBlockIndex = toBlockIndex != null ? toBlockIndex : this._blocks.length;

        this._blocks.slice(startBlockIndex, endBlockIndex).forEach(function (b) {
          if (!b.lazy || toBlockIndex != null) {
            // $FlowFixMe `_blocks` may not be present
            var args = b._blocks != null ? [b._blocks.length] : [];

            var bDetails = b._appendPlaceholder.apply(b, args);

            _this3._value += bDetails.inserted;
            details.aggregate(bDetails);
          }
        });

        return details;
      }
      /** Finds block in pos */

    }, {
      key: "_mapPosToBlock",
      value: function _mapPosToBlock(pos) {
        var accVal = '';

        for (var bi = 0; bi < this._blocks.length; ++bi) {
          var _block2 = this._blocks[bi];
          var blockStartPos = accVal.length;
          accVal += _block2.value;

          if (pos <= accVal.length) {
            return {
              index: bi,
              offset: pos - blockStartPos
            };
          }
        }
      }
      /** */

    }, {
      key: "_blockStartPos",
      value: function _blockStartPos(blockIndex) {
        return this._blocks.slice(0, blockIndex).reduce(function (pos, b) {
          return pos += b.value.length;
        }, 0);
      }
      /** */

    }, {
      key: "_forEachBlocksInRange",
      value: function _forEachBlocksInRange(fromPos) {
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var fn = arguments.length > 2 ? arguments[2] : undefined;

        var fromBlockIter = this._mapPosToBlock(fromPos);

        if (fromBlockIter) {
          var toBlockIter = this._mapPosToBlock(toPos); // process first block


          var isSameBlock = toBlockIter && fromBlockIter.index === toBlockIter.index;
          var fromBlockStartPos = fromBlockIter.offset;
          var fromBlockEndPos = toBlockIter && isSameBlock ? toBlockIter.offset : this._blocks[fromBlockIter.index].value.length;
          fn(this._blocks[fromBlockIter.index], fromBlockIter.index, fromBlockStartPos, fromBlockEndPos);

          if (toBlockIter && !isSameBlock) {
            // process intermediate blocks
            for (var bi = fromBlockIter.index + 1; bi < toBlockIter.index; ++bi) {
              fn(this._blocks[bi], bi, 0, this._blocks[bi].value.length);
            } // process last block


            fn(this._blocks[toBlockIter.index], toBlockIter.index, 0, toBlockIter.offset);
          }
        }
      }
      /**
        @override
      */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        var removeDetails = _get(_getPrototypeOf(MaskedPattern.prototype), "remove", this).call(this, fromPos, toPos);

        this._forEachBlocksInRange(fromPos, toPos, function (b, _, bFromPos, bToPos) {
          removeDetails.aggregate(b.remove(bFromPos, bToPos));
        });

        return removeDetails;
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE; // TODO refactor - extract alignblock

        var beginBlockData = this._mapPosToBlock(cursorPos) || {
          index: 0,
          offset: 0
        };
        var beginBlockOffset = beginBlockData.offset,
            beginBlockIndex = beginBlockData.index;
        var beginBlock = this._blocks[beginBlockIndex];
        if (!beginBlock) return cursorPos;
        var beginBlockCursorPos = beginBlockOffset; // if position inside block - try to adjust it

        if (beginBlockCursorPos !== 0 && beginBlockCursorPos < beginBlock.value.length) {
          beginBlockCursorPos = beginBlock.nearestInputPos(beginBlockOffset, forceDirection(direction));
        }

        var cursorAtRight = beginBlockCursorPos === beginBlock.value.length;
        var cursorAtLeft = beginBlockCursorPos === 0; //  cursor is INSIDE first block (not at bounds)

        if (!cursorAtLeft && !cursorAtRight) return this._blockStartPos(beginBlockIndex) + beginBlockCursorPos;
        var searchBlockIndex = cursorAtRight ? beginBlockIndex + 1 : beginBlockIndex;

        if (direction === DIRECTION.NONE) {
          // NONE direction used to calculate start input position if no chars were removed
          // FOR NONE:
          // -
          // input|any
          // ->
          //  any|input
          // <-
          //  filled-input|any
          // check if first block at left is input
          if (searchBlockIndex > 0) {
            var blockIndexAtLeft = searchBlockIndex - 1;
            var blockAtLeft = this._blocks[blockIndexAtLeft];
            var blockInputPos = blockAtLeft.nearestInputPos(0, DIRECTION.NONE); // is input

            if (!blockAtLeft.value.length || blockInputPos !== blockAtLeft.value.length) {
              return this._blockStartPos(searchBlockIndex);
            }
          } // ->


          var firstInputAtRight = searchBlockIndex;

          for (var bi = firstInputAtRight; bi < this._blocks.length; ++bi) {
            var blockAtRight = this._blocks[bi];

            var _blockInputPos = blockAtRight.nearestInputPos(0, DIRECTION.NONE);

            if (!blockAtRight.value.length || _blockInputPos !== blockAtRight.value.length) {
              return this._blockStartPos(bi) + _blockInputPos;
            }
          } // <-
          // find first non-fixed symbol


          for (var _bi = searchBlockIndex - 1; _bi >= 0; --_bi) {
            var _block3 = this._blocks[_bi];

            var _blockInputPos2 = _block3.nearestInputPos(0, DIRECTION.NONE); // is input


            if (!_block3.value.length || _blockInputPos2 !== _block3.value.length) {
              return this._blockStartPos(_bi) + _block3.value.length;
            }
          }

          return cursorPos;
        }

        if (direction === DIRECTION.LEFT || direction === DIRECTION.FORCE_LEFT) {
          // -
          //  any|filled-input
          // <-
          //  any|first not empty is not-len-aligned
          //  not-0-aligned|any
          // ->
          //  any|not-len-aligned or end
          // check if first block at right is filled input
          var firstFilledBlockIndexAtRight;

          for (var _bi2 = searchBlockIndex; _bi2 < this._blocks.length; ++_bi2) {
            if (this._blocks[_bi2].value) {
              firstFilledBlockIndexAtRight = _bi2;
              break;
            }
          }

          if (firstFilledBlockIndexAtRight != null) {
            var filledBlock = this._blocks[firstFilledBlockIndexAtRight];

            var _blockInputPos3 = filledBlock.nearestInputPos(0, DIRECTION.RIGHT);

            if (_blockInputPos3 === 0 && filledBlock.unmaskedValue.length) {
              // filled block is input
              return this._blockStartPos(firstFilledBlockIndexAtRight) + _blockInputPos3;
            }
          } // <-
          // find this vars


          var firstFilledInputBlockIndex = -1;
          var firstEmptyInputBlockIndex; // TODO consider nested empty inputs

          for (var _bi3 = searchBlockIndex - 1; _bi3 >= 0; --_bi3) {
            var _block4 = this._blocks[_bi3];

            var _blockInputPos4 = _block4.nearestInputPos(_block4.value.length, DIRECTION.FORCE_LEFT);

            if (!_block4.value || _blockInputPos4 !== 0) firstEmptyInputBlockIndex = _bi3;

            if (_blockInputPos4 !== 0) {
              if (_blockInputPos4 !== _block4.value.length) {
                // aligned inside block - return immediately
                return this._blockStartPos(_bi3) + _blockInputPos4;
              } else {
                // found filled
                firstFilledInputBlockIndex = _bi3;
                break;
              }
            }
          }

          if (direction === DIRECTION.LEFT) {
            // try find first empty input before start searching position only when not forced
            for (var _bi4 = firstFilledInputBlockIndex + 1; _bi4 <= Math.min(searchBlockIndex, this._blocks.length - 1); ++_bi4) {
              var _block5 = this._blocks[_bi4];

              var _blockInputPos5 = _block5.nearestInputPos(0, DIRECTION.NONE);

              var blockAlignedPos = this._blockStartPos(_bi4) + _blockInputPos5;

              if (blockAlignedPos > cursorPos) break; // if block is not lazy input

              if (_blockInputPos5 !== _block5.value.length) return blockAlignedPos;
            }
          } // process overflow


          if (firstFilledInputBlockIndex >= 0) {
            return this._blockStartPos(firstFilledInputBlockIndex) + this._blocks[firstFilledInputBlockIndex].value.length;
          } // for lazy if has aligned left inside fixed and has came to the start - use start position


          if (direction === DIRECTION.FORCE_LEFT || this.lazy && !this.extractInput() && !isInput(this._blocks[searchBlockIndex])) {
            return 0;
          }

          if (firstEmptyInputBlockIndex != null) {
            return this._blockStartPos(firstEmptyInputBlockIndex);
          } // find first input


          for (var _bi5 = searchBlockIndex; _bi5 < this._blocks.length; ++_bi5) {
            var _block6 = this._blocks[_bi5];

            var _blockInputPos6 = _block6.nearestInputPos(0, DIRECTION.NONE); // is input


            if (!_block6.value.length || _blockInputPos6 !== _block6.value.length) {
              return this._blockStartPos(_bi5) + _blockInputPos6;
            }
          }

          return 0;
        }

        if (direction === DIRECTION.RIGHT || direction === DIRECTION.FORCE_RIGHT) {
          // ->
          //  any|not-len-aligned and filled
          //  any|not-len-aligned
          // <-
          //  not-0-aligned or start|any
          var firstInputBlockAlignedIndex;
          var firstInputBlockAlignedPos;

          for (var _bi6 = searchBlockIndex; _bi6 < this._blocks.length; ++_bi6) {
            var _block7 = this._blocks[_bi6];

            var _blockInputPos7 = _block7.nearestInputPos(0, DIRECTION.NONE);

            if (_blockInputPos7 !== _block7.value.length) {
              firstInputBlockAlignedPos = this._blockStartPos(_bi6) + _blockInputPos7;
              firstInputBlockAlignedIndex = _bi6;
              break;
            }
          }

          if (firstInputBlockAlignedIndex != null && firstInputBlockAlignedPos != null) {
            for (var _bi7 = firstInputBlockAlignedIndex; _bi7 < this._blocks.length; ++_bi7) {
              var _block8 = this._blocks[_bi7];

              var _blockInputPos8 = _block8.nearestInputPos(0, DIRECTION.FORCE_RIGHT);

              if (_blockInputPos8 !== _block8.value.length) {
                return this._blockStartPos(_bi7) + _blockInputPos8;
              }
            }

            return direction === DIRECTION.FORCE_RIGHT ? this.value.length : firstInputBlockAlignedPos;
          }

          for (var _bi8 = Math.min(searchBlockIndex, this._blocks.length - 1); _bi8 >= 0; --_bi8) {
            var _block9 = this._blocks[_bi8];

            var _blockInputPos9 = _block9.nearestInputPos(_block9.value.length, DIRECTION.LEFT);

            if (_blockInputPos9 !== 0) {
              var alignedPos = this._blockStartPos(_bi8) + _blockInputPos9;

              if (alignedPos >= cursorPos) return alignedPos;
              break;
            }
          }
        }

        return cursorPos;
      }
      /** Get block by name */

    }, {
      key: "maskedBlock",
      value: function maskedBlock(name) {
        return this.maskedBlocks(name)[0];
      }
      /** Get all blocks by name */

    }, {
      key: "maskedBlocks",
      value: function maskedBlocks(name) {
        var _this4 = this;

        var indices = this._maskedBlocks[name];
        if (!indices) return [];
        return indices.map(function (gi) {
          return _this4._blocks[gi];
        });
      }
    }]);

    return MaskedPattern;
  }(Masked);

  MaskedPattern.DEFAULTS = {
    lazy: true,
    placeholderChar: '_'
  };
  MaskedPattern.STOP_CHAR = '`';
  MaskedPattern.ESCAPE_CHAR = '\\';
  MaskedPattern.InputDefinition = PatternInputDefinition;
  MaskedPattern.FixedDefinition = PatternFixedDefinition;

  function isInput(block) {
    if (!block) return false;
    var value = block.value;
    return !value || block.nearestInputPos(0, DIRECTION.NONE) !== value.length;
  }

  IMask.MaskedPattern = MaskedPattern;

  /** Pattern which accepts ranges */

  var MaskedRange = /*#__PURE__*/function (_MaskedPattern) {
    _inherits(MaskedRange, _MaskedPattern);

    var _super = _createSuper(MaskedRange);

    function MaskedRange() {
      _classCallCheck(this, MaskedRange);

      return _super.apply(this, arguments);
    }

    _createClass(MaskedRange, [{
      key: "_matchFrom",
      get:
      /**
        Optionally sets max length of pattern.
        Used when pattern length is longer then `to` param length. Pads zeros at start in this case.
      */

      /** Min bound */

      /** Max bound */

      /** */
      function get() {
        return this.maxLength - String(this.from).length;
      }
      /**
        @override
      */

    }, {
      key: "_update",
      value: function _update(opts) {
        // TODO type
        opts = Object.assign({
          to: this.to || 0,
          from: this.from || 0
        }, opts);
        var maxLength = String(opts.to).length;
        if (opts.maxLength != null) maxLength = Math.max(maxLength, opts.maxLength);
        opts.maxLength = maxLength;
        var fromStr = String(opts.from).padStart(maxLength, '0');
        var toStr = String(opts.to).padStart(maxLength, '0');
        var sameCharsCount = 0;

        while (sameCharsCount < toStr.length && toStr[sameCharsCount] === fromStr[sameCharsCount]) {
          ++sameCharsCount;
        }

        opts.mask = toStr.slice(0, sameCharsCount).replace(/0/g, '\\0') + '0'.repeat(maxLength - sameCharsCount);

        _get(_getPrototypeOf(MaskedRange.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "isComplete",
      get: function get() {
        return _get(_getPrototypeOf(MaskedRange.prototype), "isComplete", this) && Boolean(this.value);
      }
    }, {
      key: "boundaries",
      value: function boundaries(str) {
        var minstr = '';
        var maxstr = '';

        var _ref = str.match(/^(\D*)(\d*)(\D*)/) || [],
            _ref2 = _slicedToArray(_ref, 3),
            placeholder = _ref2[1],
            num = _ref2[2];

        if (num) {
          minstr = '0'.repeat(placeholder.length) + num;
          maxstr = '9'.repeat(placeholder.length) + num;
        }

        minstr = minstr.padEnd(this.maxLength, '0');
        maxstr = maxstr.padEnd(this.maxLength, '9');
        return [minstr, maxstr];
      }
      /**
        @override
      */

    }, {
      key: "doPrepare",
      value: function doPrepare(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        str = _get(_getPrototypeOf(MaskedRange.prototype), "doPrepare", this).call(this, str, flags).replace(/\D/g, '');
        if (!this.autofix) return str;
        var fromStr = String(this.from).padStart(this.maxLength, '0');
        var toStr = String(this.to).padStart(this.maxLength, '0');
        var val = this.value;
        var prepStr = '';

        for (var ci = 0; ci < str.length; ++ci) {
          var nextVal = val + prepStr + str[ci];

          var _this$boundaries = this.boundaries(nextVal),
              _this$boundaries2 = _slicedToArray(_this$boundaries, 2),
              minstr = _this$boundaries2[0],
              maxstr = _this$boundaries2[1];

          if (Number(maxstr) < this.from) prepStr += fromStr[nextVal.length - 1];else if (Number(minstr) > this.to) prepStr += toStr[nextVal.length - 1];else prepStr += str[ci];
        }

        return prepStr;
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _get2;

        var str = this.value;
        var firstNonZero = str.search(/[^0]/);
        if (firstNonZero === -1 && str.length <= this._matchFrom) return true;

        var _this$boundaries3 = this.boundaries(str),
            _this$boundaries4 = _slicedToArray(_this$boundaries3, 2),
            minstr = _this$boundaries4[0],
            maxstr = _this$boundaries4[1];

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.from <= Number(maxstr) && Number(minstr) <= this.to && (_get2 = _get(_getPrototypeOf(MaskedRange.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args));
      }
    }]);

    return MaskedRange;
  }(MaskedPattern);

  IMask.MaskedRange = MaskedRange;

  /** Date mask */

  var MaskedDate = /*#__PURE__*/function (_MaskedPattern) {
    _inherits(MaskedDate, _MaskedPattern);

    var _super = _createSuper(MaskedDate);
    /** Pattern mask for date according to {@link MaskedDate#format} */

    /** Start date */

    /** End date */

    /** */

    /**
      @param {Object} opts
    */


    function MaskedDate(opts) {
      _classCallCheck(this, MaskedDate);

      return _super.call(this, Object.assign({}, MaskedDate.DEFAULTS, opts));
    }
    /**
      @override
    */


    _createClass(MaskedDate, [{
      key: "_update",
      value: function _update(opts) {
        if (opts.mask === Date) delete opts.mask;
        if (opts.pattern) opts.mask = opts.pattern;
        var blocks = opts.blocks;
        opts.blocks = Object.assign({}, MaskedDate.GET_DEFAULT_BLOCKS()); // adjust year block

        if (opts.min) opts.blocks.Y.from = opts.min.getFullYear();
        if (opts.max) opts.blocks.Y.to = opts.max.getFullYear();

        if (opts.min && opts.max && opts.blocks.Y.from === opts.blocks.Y.to) {
          opts.blocks.m.from = opts.min.getMonth() + 1;
          opts.blocks.m.to = opts.max.getMonth() + 1;

          if (opts.blocks.m.from === opts.blocks.m.to) {
            opts.blocks.d.from = opts.min.getDate();
            opts.blocks.d.to = opts.max.getDate();
          }
        }

        Object.assign(opts.blocks, blocks); // add autofix

        Object.keys(opts.blocks).forEach(function (bk) {
          var b = opts.blocks[bk];
          if (!('autofix' in b)) b.autofix = opts.autofix;
        });

        _get(_getPrototypeOf(MaskedDate.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _get2;

        var date = this.date;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedDate.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args)) && (!this.isComplete || this.isDateExist(this.value) && date != null && (this.min == null || this.min <= date) && (this.max == null || date <= this.max));
      }
      /** Checks if date is exists */

    }, {
      key: "isDateExist",
      value: function isDateExist(str) {
        return this.format(this.parse(str, this), this).indexOf(str) >= 0;
      }
      /** Parsed Date */

    }, {
      key: "date",
      get: function get() {
        return this.typedValue;
      },
      set: function set(date) {
        this.typedValue = date;
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get() {
        return this.isComplete ? _get(_getPrototypeOf(MaskedDate.prototype), "typedValue", this) : null;
      },
      set: function set(value) {
        _set(_getPrototypeOf(MaskedDate.prototype), "typedValue", value, this, true);
      }
    }]);

    return MaskedDate;
  }(MaskedPattern);

  MaskedDate.DEFAULTS = {
    pattern: 'd{.}`m{.}`Y',
    format: function format(date) {
      var day = String(date.getDate()).padStart(2, '0');
      var month = String(date.getMonth() + 1).padStart(2, '0');
      var year = date.getFullYear();
      return [day, month, year].join('.');
    },
    parse: function parse(str) {
      var _str$split = str.split('.'),
          _str$split2 = _slicedToArray(_str$split, 3),
          day = _str$split2[0],
          month = _str$split2[1],
          year = _str$split2[2];

      return new Date(year, month - 1, day);
    }
  };

  MaskedDate.GET_DEFAULT_BLOCKS = function () {
    return {
      d: {
        mask: MaskedRange,
        from: 1,
        to: 31,
        maxLength: 2
      },
      m: {
        mask: MaskedRange,
        from: 1,
        to: 12,
        maxLength: 2
      },
      Y: {
        mask: MaskedRange,
        from: 1900,
        to: 9999
      }
    };
  };

  IMask.MaskedDate = MaskedDate;

  /**
    Generic element API to use with mask
    @interface
  */

  var MaskElement = /*#__PURE__*/function () {
    function MaskElement() {
      _classCallCheck(this, MaskElement);
    }

    _createClass(MaskElement, [{
      key: "selectionStart",
      get:
      /** */

      /** */

      /** */

      /** Safely returns selection start */
      function get() {
        var start;

        try {
          start = this._unsafeSelectionStart;
        } catch (e) {}

        return start != null ? start : this.value.length;
      }
      /** Safely returns selection end */

    }, {
      key: "selectionEnd",
      get: function get() {
        var end;

        try {
          end = this._unsafeSelectionEnd;
        } catch (e) {}

        return end != null ? end : this.value.length;
      }
      /** Safely sets element selection */

    }, {
      key: "select",
      value: function select(start, end) {
        if (start == null || end == null || start === this.selectionStart && end === this.selectionEnd) return;

        try {
          this._unsafeSelect(start, end);
        } catch (e) {}
      }
      /** Should be overriden in subclasses */

    }, {
      key: "_unsafeSelect",
      value: function _unsafeSelect(start, end) {}
      /** Should be overriden in subclasses */

    }, {
      key: "isActive",
      get: function get() {
        return false;
      }
      /** Should be overriden in subclasses */

    }, {
      key: "bindEvents",
      value: function bindEvents(handlers) {}
      /** Should be overriden in subclasses */

    }, {
      key: "unbindEvents",
      value: function unbindEvents() {}
    }]);

    return MaskElement;
  }();

  IMask.MaskElement = MaskElement;

  /** Bridge between HTMLElement and {@link Masked} */

  var HTMLMaskElement = /*#__PURE__*/function (_MaskElement) {
    _inherits(HTMLMaskElement, _MaskElement);

    var _super = _createSuper(HTMLMaskElement);
    /** Mapping between HTMLElement events and mask internal events */

    /** HTMLElement to use mask on */

    /**
      @param {HTMLInputElement|HTMLTextAreaElement} input
    */


    function HTMLMaskElement(input) {
      var _this;

      _classCallCheck(this, HTMLMaskElement);

      _this = _super.call(this);
      _this.input = input;
      _this._handlers = {};
      return _this;
    }
    /** */
    // $FlowFixMe https://github.com/facebook/flow/issues/2839


    _createClass(HTMLMaskElement, [{
      key: "rootElement",
      get: function get() {
        return this.input.getRootNode ? this.input.getRootNode() : document;
      }
      /**
        Is element in focus
        @readonly
      */

    }, {
      key: "isActive",
      get: function get() {
        //$FlowFixMe
        return this.input === this.rootElement.activeElement;
      }
      /**
        Returns HTMLElement selection start
        @override
      */

    }, {
      key: "_unsafeSelectionStart",
      get: function get() {
        return this.input.selectionStart;
      }
      /**
        Returns HTMLElement selection end
        @override
      */

    }, {
      key: "_unsafeSelectionEnd",
      get: function get() {
        return this.input.selectionEnd;
      }
      /**
        Sets HTMLElement selection
        @override
      */

    }, {
      key: "_unsafeSelect",
      value: function _unsafeSelect(start, end) {
        this.input.setSelectionRange(start, end);
      }
      /**
        HTMLElement value
        @override
      */

    }, {
      key: "value",
      get: function get() {
        return this.input.value;
      },
      set: function set(value) {
        this.input.value = value;
      }
      /**
        Binds HTMLElement events to mask internal events
        @override
      */

    }, {
      key: "bindEvents",
      value: function bindEvents(handlers) {
        var _this2 = this;

        Object.keys(handlers).forEach(function (event) {
          return _this2._toggleEventHandler(HTMLMaskElement.EVENTS_MAP[event], handlers[event]);
        });
      }
      /**
        Unbinds HTMLElement events to mask internal events
        @override
      */

    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        var _this3 = this;

        Object.keys(this._handlers).forEach(function (event) {
          return _this3._toggleEventHandler(event);
        });
      }
      /** */

    }, {
      key: "_toggleEventHandler",
      value: function _toggleEventHandler(event, handler) {
        if (this._handlers[event]) {
          this.input.removeEventListener(event, this._handlers[event]);
          delete this._handlers[event];
        }

        if (handler) {
          this.input.addEventListener(event, handler);
          this._handlers[event] = handler;
        }
      }
    }]);

    return HTMLMaskElement;
  }(MaskElement);

  HTMLMaskElement.EVENTS_MAP = {
    selectionChange: 'keydown',
    input: 'input',
    drop: 'drop',
    click: 'click',
    focus: 'focus',
    commit: 'blur'
  };
  IMask.HTMLMaskElement = HTMLMaskElement;

  var HTMLContenteditableMaskElement = /*#__PURE__*/function (_HTMLMaskElement) {
    _inherits(HTMLContenteditableMaskElement, _HTMLMaskElement);

    var _super = _createSuper(HTMLContenteditableMaskElement);

    function HTMLContenteditableMaskElement() {
      _classCallCheck(this, HTMLContenteditableMaskElement);

      return _super.apply(this, arguments);
    }

    _createClass(HTMLContenteditableMaskElement, [{
      key: "_unsafeSelectionStart",
      get:
      /**
        Returns HTMLElement selection start
        @override
      */
      function get() {
        var root = this.rootElement;
        var selection = root.getSelection && root.getSelection();
        return selection && selection.anchorOffset;
      }
      /**
        Returns HTMLElement selection end
        @override
      */

    }, {
      key: "_unsafeSelectionEnd",
      get: function get() {
        var root = this.rootElement;
        var selection = root.getSelection && root.getSelection();
        return selection && this._unsafeSelectionStart + String(selection).length;
      }
      /**
        Sets HTMLElement selection
        @override
      */

    }, {
      key: "_unsafeSelect",
      value: function _unsafeSelect(start, end) {
        if (!this.rootElement.createRange) return;
        var range = this.rootElement.createRange();
        range.setStart(this.input.firstChild || this.input, start);
        range.setEnd(this.input.lastChild || this.input, end);
        var root = this.rootElement;
        var selection = root.getSelection && root.getSelection();

        if (selection) {
          selection.removeAllRanges();
          selection.addRange(range);
        }
      }
      /**
        HTMLElement value
        @override
      */

    }, {
      key: "value",
      get: function get() {
        // $FlowFixMe
        return this.input.textContent;
      },
      set: function set(value) {
        this.input.textContent = value;
      }
    }]);

    return HTMLContenteditableMaskElement;
  }(HTMLMaskElement);

  IMask.HTMLContenteditableMaskElement = HTMLContenteditableMaskElement;

  /** Listens to element events and controls changes between element and {@link Masked} */

  var InputMask = /*#__PURE__*/function () {
    /**
      View element
      @readonly
    */

    /**
      Internal {@link Masked} model
      @readonly
    */

    /**
      @param {MaskElement|HTMLInputElement|HTMLTextAreaElement} el
      @param {Object} opts
    */
    function InputMask(el, opts) {
      _classCallCheck(this, InputMask);

      this.el = el instanceof MaskElement ? el : el.isContentEditable && el.tagName !== 'INPUT' && el.tagName !== 'TEXTAREA' ? new HTMLContenteditableMaskElement(el) : new HTMLMaskElement(el);
      this.masked = createMask(opts);
      this._listeners = {};
      this._value = '';
      this._unmaskedValue = '';
      this._saveSelection = this._saveSelection.bind(this);
      this._onInput = this._onInput.bind(this);
      this._onChange = this._onChange.bind(this);
      this._onDrop = this._onDrop.bind(this);
      this._onFocus = this._onFocus.bind(this);
      this._onClick = this._onClick.bind(this);
      this.alignCursor = this.alignCursor.bind(this);
      this.alignCursorFriendly = this.alignCursorFriendly.bind(this);

      this._bindEvents(); // refresh


      this.updateValue();

      this._onChange();
    }
    /** Read or update mask */


    _createClass(InputMask, [{
      key: "mask",
      get: function get() {
        return this.masked.mask;
      },
      set: function set(mask) {
        if (this.maskEquals(mask)) return;

        if (!(mask instanceof IMask.Masked) && this.masked.constructor === maskedClass(mask)) {
          this.masked.updateOptions({
            mask: mask
          });
          return;
        }

        var masked = createMask({
          mask: mask
        });
        masked.unmaskedValue = this.masked.unmaskedValue;
        this.masked = masked;
      }
      /** Raw value */

    }, {
      key: "maskEquals",
      value: function maskEquals(mask) {
        return mask == null || mask === this.masked.mask || mask === Date && this.masked instanceof MaskedDate;
      }
    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(str) {
        this.masked.value = str;
        this.updateControl();
        this.alignCursor();
      }
      /** Unmasked value */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._unmaskedValue;
      },
      set: function set(str) {
        this.masked.unmaskedValue = str;
        this.updateControl();
        this.alignCursor();
      }
      /** Typed unmasked value */

    }, {
      key: "typedValue",
      get: function get() {
        return this.masked.typedValue;
      },
      set: function set(val) {
        this.masked.typedValue = val;
        this.updateControl();
        this.alignCursor();
      }
      /**
        Starts listening to element events
        @protected
      */

    }, {
      key: "_bindEvents",
      value: function _bindEvents() {
        this.el.bindEvents({
          selectionChange: this._saveSelection,
          input: this._onInput,
          drop: this._onDrop,
          click: this._onClick,
          focus: this._onFocus,
          commit: this._onChange
        });
      }
      /**
        Stops listening to element events
        @protected
       */

    }, {
      key: "_unbindEvents",
      value: function _unbindEvents() {
        if (this.el) this.el.unbindEvents();
      }
      /**
        Fires custom event
        @protected
       */

    }, {
      key: "_fireEvent",
      value: function _fireEvent(ev) {
        for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        var listeners = this._listeners[ev];
        if (!listeners) return;
        listeners.forEach(function (l) {
          return l.apply(void 0, args);
        });
      }
      /**
        Current selection start
        @readonly
      */

    }, {
      key: "selectionStart",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionStart;
      }
      /** Current cursor position */

    }, {
      key: "cursorPos",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionEnd;
      },
      set: function set(pos) {
        if (!this.el || !this.el.isActive) return;
        this.el.select(pos, pos);

        this._saveSelection();
      }
      /**
        Stores current selection
        @protected
      */

    }, {
      key: "_saveSelection",
      value: function _saveSelection()
      /* ev */
      {
        if (this.value !== this.el.value) {
          console.warn('Element value was changed outside of mask. Syncronize mask using `mask.updateValue()` to work properly.'); // eslint-disable-line no-console
        }

        this._selection = {
          start: this.selectionStart,
          end: this.cursorPos
        };
      }
      /** Syncronizes model value from view */

    }, {
      key: "updateValue",
      value: function updateValue() {
        this.masked.value = this.el.value;
        this._value = this.masked.value;
      }
      /** Syncronizes view from model value, fires change events */

    }, {
      key: "updateControl",
      value: function updateControl() {
        var newUnmaskedValue = this.masked.unmaskedValue;
        var newValue = this.masked.value;
        var isChanged = this.unmaskedValue !== newUnmaskedValue || this.value !== newValue;
        this._unmaskedValue = newUnmaskedValue;
        this._value = newValue;
        if (this.el.value !== newValue) this.el.value = newValue;
        if (isChanged) this._fireChangeEvents();
      }
      /** Updates options with deep equal check, recreates @{link Masked} model if mask type changes */

    }, {
      key: "updateOptions",
      value: function updateOptions(opts) {
        var mask = opts.mask,
            restOpts = _objectWithoutProperties(opts, ["mask"]);

        var updateMask = !this.maskEquals(mask);
        var updateOpts = !objectIncludes(this.masked, restOpts);
        if (updateMask) this.mask = mask;
        if (updateOpts) this.masked.updateOptions(restOpts);
        if (updateMask || updateOpts) this.updateControl();
      }
      /** Updates cursor */

    }, {
      key: "updateCursor",
      value: function updateCursor(cursorPos) {
        if (cursorPos == null) return;
        this.cursorPos = cursorPos; // also queue change cursor for mobile browsers

        this._delayUpdateCursor(cursorPos);
      }
      /**
        Delays cursor update to support mobile browsers
        @private
      */

    }, {
      key: "_delayUpdateCursor",
      value: function _delayUpdateCursor(cursorPos) {
        var _this = this;

        this._abortUpdateCursor();

        this._changingCursorPos = cursorPos;
        this._cursorChanging = setTimeout(function () {
          if (!_this.el) return; // if was destroyed

          _this.cursorPos = _this._changingCursorPos;

          _this._abortUpdateCursor();
        }, 10);
      }
      /**
        Fires custom events
        @protected
      */

    }, {
      key: "_fireChangeEvents",
      value: function _fireChangeEvents() {
        this._fireEvent('accept', this._inputEvent);

        if (this.masked.isComplete) this._fireEvent('complete', this._inputEvent);
      }
      /**
        Aborts delayed cursor update
        @private
      */

    }, {
      key: "_abortUpdateCursor",
      value: function _abortUpdateCursor() {
        if (this._cursorChanging) {
          clearTimeout(this._cursorChanging);
          delete this._cursorChanging;
        }
      }
      /** Aligns cursor to nearest available position */

    }, {
      key: "alignCursor",
      value: function alignCursor() {
        this.cursorPos = this.masked.nearestInputPos(this.cursorPos, DIRECTION.LEFT);
      }
      /** Aligns cursor only if selection is empty */

    }, {
      key: "alignCursorFriendly",
      value: function alignCursorFriendly() {
        if (this.selectionStart !== this.cursorPos) return; // skip if range is selected

        this.alignCursor();
      }
      /** Adds listener on custom event */

    }, {
      key: "on",
      value: function on(ev, handler) {
        if (!this._listeners[ev]) this._listeners[ev] = [];

        this._listeners[ev].push(handler);

        return this;
      }
      /** Removes custom event listener */

    }, {
      key: "off",
      value: function off(ev, handler) {
        if (!this._listeners[ev]) return this;

        if (!handler) {
          delete this._listeners[ev];
          return this;
        }

        var hIndex = this._listeners[ev].indexOf(handler);

        if (hIndex >= 0) this._listeners[ev].splice(hIndex, 1);
        return this;
      }
      /** Handles view input event */

    }, {
      key: "_onInput",
      value: function _onInput(e) {
        this._inputEvent = e;

        this._abortUpdateCursor(); // fix strange IE behavior


        if (!this._selection) return this.updateValue();
        var details = new ActionDetails( // new state
        this.el.value, this.cursorPos, // old state
        this.value, this._selection);
        var oldRawValue = this.masked.rawInputValue;
        var offset = this.masked.splice(details.startChangePos, details.removed.length, details.inserted, details.removeDirection).offset; // force align in remove direction only if no input chars were removed
        // otherwise we still need to align with NONE (to get out from fixed symbols for instance)

        var removeDirection = oldRawValue === this.masked.rawInputValue ? details.removeDirection : DIRECTION.NONE;
        var cursorPos = this.masked.nearestInputPos(details.startChangePos + offset, removeDirection);
        this.updateControl();
        this.updateCursor(cursorPos);
        delete this._inputEvent;
      }
      /** Handles view change event and commits model value */

    }, {
      key: "_onChange",
      value: function _onChange() {
        if (this.value !== this.el.value) {
          this.updateValue();
        }

        this.masked.doCommit();
        this.updateControl();

        this._saveSelection();
      }
      /** Handles view drop event, prevents by default */

    }, {
      key: "_onDrop",
      value: function _onDrop(ev) {
        ev.preventDefault();
        ev.stopPropagation();
      }
      /** Restore last selection on focus */

    }, {
      key: "_onFocus",
      value: function _onFocus(ev) {
        this.alignCursorFriendly();
      }
      /** Restore last selection on focus */

    }, {
      key: "_onClick",
      value: function _onClick(ev) {
        this.alignCursorFriendly();
      }
      /** Unbind view events and removes element reference */

    }, {
      key: "destroy",
      value: function destroy() {
        this._unbindEvents(); // $FlowFixMe why not do so?


        this._listeners.length = 0; // $FlowFixMe

        delete this.el;
      }
    }]);

    return InputMask;
  }();

  IMask.InputMask = InputMask;

  /** Pattern which validates enum values */

  var MaskedEnum = /*#__PURE__*/function (_MaskedPattern) {
    _inherits(MaskedEnum, _MaskedPattern);

    var _super = _createSuper(MaskedEnum);

    function MaskedEnum() {
      _classCallCheck(this, MaskedEnum);

      return _super.apply(this, arguments);
    }

    _createClass(MaskedEnum, [{
      key: "_update",
      value:
      /**
        @override
        @param {Object} opts
      */
      function _update(opts) {
        // TODO type
        if (opts["enum"]) opts.mask = '*'.repeat(opts["enum"][0].length);

        _get(_getPrototypeOf(MaskedEnum.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _this = this,
            _get2;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this["enum"].some(function (e) {
          return e.indexOf(_this.unmaskedValue) >= 0;
        }) && (_get2 = _get(_getPrototypeOf(MaskedEnum.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args));
      }
    }]);

    return MaskedEnum;
  }(MaskedPattern);

  IMask.MaskedEnum = MaskedEnum;

  /**
    Number mask
    @param {Object} opts
    @param {string} opts.radix - Single char
    @param {string} opts.thousandsSeparator - Single char
    @param {Array<string>} opts.mapToRadix - Array of single chars
    @param {number} opts.min
    @param {number} opts.max
    @param {number} opts.scale - Digits after point
    @param {boolean} opts.signed - Allow negative
    @param {boolean} opts.normalizeZeros - Flag to remove leading and trailing zeros in the end of editing
    @param {boolean} opts.padFractionalZeros - Flag to pad trailing zeros after point in the end of editing
  */

  var MaskedNumber = /*#__PURE__*/function (_Masked) {
    _inherits(MaskedNumber, _Masked);

    var _super = _createSuper(MaskedNumber);
    /** Single char */

    /** Single char */

    /** Array of single chars */

    /** */

    /** */

    /** Digits after point */

    /** */

    /** Flag to remove leading and trailing zeros in the end of editing */

    /** Flag to pad trailing zeros after point in the end of editing */


    function MaskedNumber(opts) {
      _classCallCheck(this, MaskedNumber);

      return _super.call(this, Object.assign({}, MaskedNumber.DEFAULTS, opts));
    }
    /**
      @override
    */


    _createClass(MaskedNumber, [{
      key: "_update",
      value: function _update(opts) {
        _get(_getPrototypeOf(MaskedNumber.prototype), "_update", this).call(this, opts);

        this._updateRegExps();
      }
      /** */

    }, {
      key: "_updateRegExps",
      value: function _updateRegExps() {
        // use different regexp to process user input (more strict, input suffix) and tail shifting
        var start = '^' + (this.allowNegative ? '[+|\\-]?' : '');
        var midInput = '(0|([1-9]+\\d*))?';
        var mid = '\\d*';
        var end = (this.scale ? '(' + escapeRegExp(this.radix) + '\\d{0,' + this.scale + '})?' : '') + '$';
        this._numberRegExpInput = new RegExp(start + midInput + end);
        this._numberRegExp = new RegExp(start + mid + end);
        this._mapToRadixRegExp = new RegExp('[' + this.mapToRadix.map(escapeRegExp).join('') + ']', 'g');
        this._thousandsSeparatorRegExp = new RegExp(escapeRegExp(this.thousandsSeparator), 'g');
      }
      /** */

    }, {
      key: "_removeThousandsSeparators",
      value: function _removeThousandsSeparators(value) {
        return value.replace(this._thousandsSeparatorRegExp, '');
      }
      /** */

    }, {
      key: "_insertThousandsSeparators",
      value: function _insertThousandsSeparators(value) {
        // https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
        var parts = value.split(this.radix);
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandsSeparator);
        return parts.join(this.radix);
      }
      /**
        @override
      */

    }, {
      key: "doPrepare",
      value: function doPrepare(str) {
        var _get2;

        for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedNumber.prototype), "doPrepare", this)).call.apply(_get2, [this, this._removeThousandsSeparators(str.replace(this._mapToRadixRegExp, this.radix))].concat(args));
      }
      /** */

    }, {
      key: "_separatorsCount",
      value: function _separatorsCount(to) {
        var extendOnSeparators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var count = 0;

        for (var pos = 0; pos < to; ++pos) {
          if (this._value.indexOf(this.thousandsSeparator, pos) === pos) {
            ++count;
            if (extendOnSeparators) to += this.thousandsSeparator.length;
          }
        }

        return count;
      }
      /** */

    }, {
      key: "_separatorsCountFromSlice",
      value: function _separatorsCountFromSlice() {
        var slice = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._value;
        return this._separatorsCount(this._removeThousandsSeparators(slice).length, true);
      }
      /**
        @override
      */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var flags = arguments.length > 2 ? arguments[2] : undefined;

        var _this$_adjustRangeWit = this._adjustRangeWithSeparators(fromPos, toPos);

        var _this$_adjustRangeWit2 = _slicedToArray(_this$_adjustRangeWit, 2);

        fromPos = _this$_adjustRangeWit2[0];
        toPos = _this$_adjustRangeWit2[1];
        return this._removeThousandsSeparators(_get(_getPrototypeOf(MaskedNumber.prototype), "extractInput", this).call(this, fromPos, toPos, flags));
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        if (!this.thousandsSeparator) return _get(_getPrototypeOf(MaskedNumber.prototype), "_appendCharRaw", this).call(this, ch, flags);
        var prevBeforeTailValue = flags.tail && flags._beforeTailState ? flags._beforeTailState._value : this._value;

        var prevBeforeTailSeparatorsCount = this._separatorsCountFromSlice(prevBeforeTailValue);

        this._value = this._removeThousandsSeparators(this.value);

        var appendDetails = _get(_getPrototypeOf(MaskedNumber.prototype), "_appendCharRaw", this).call(this, ch, flags);

        this._value = this._insertThousandsSeparators(this._value);
        var beforeTailValue = flags.tail && flags._beforeTailState ? flags._beforeTailState._value : this._value;

        var beforeTailSeparatorsCount = this._separatorsCountFromSlice(beforeTailValue);

        appendDetails.tailShift += (beforeTailSeparatorsCount - prevBeforeTailSeparatorsCount) * this.thousandsSeparator.length;
        appendDetails.skip = !appendDetails.rawInserted && ch === this.thousandsSeparator;
        return appendDetails;
      }
      /** */

    }, {
      key: "_findSeparatorAround",
      value: function _findSeparatorAround(pos) {
        if (this.thousandsSeparator) {
          var searchFrom = pos - this.thousandsSeparator.length + 1;
          var separatorPos = this.value.indexOf(this.thousandsSeparator, searchFrom);
          if (separatorPos <= pos) return separatorPos;
        }

        return -1;
      }
    }, {
      key: "_adjustRangeWithSeparators",
      value: function _adjustRangeWithSeparators(from, to) {
        var separatorAroundFromPos = this._findSeparatorAround(from);

        if (separatorAroundFromPos >= 0) from = separatorAroundFromPos;

        var separatorAroundToPos = this._findSeparatorAround(to);

        if (separatorAroundToPos >= 0) to = separatorAroundToPos + this.thousandsSeparator.length;
        return [from, to];
      }
      /**
        @override
      */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        var _this$_adjustRangeWit3 = this._adjustRangeWithSeparators(fromPos, toPos);

        var _this$_adjustRangeWit4 = _slicedToArray(_this$_adjustRangeWit3, 2);

        fromPos = _this$_adjustRangeWit4[0];
        toPos = _this$_adjustRangeWit4[1];
        var valueBeforePos = this.value.slice(0, fromPos);
        var valueAfterPos = this.value.slice(toPos);

        var prevBeforeTailSeparatorsCount = this._separatorsCount(valueBeforePos.length);

        this._value = this._insertThousandsSeparators(this._removeThousandsSeparators(valueBeforePos + valueAfterPos));

        var beforeTailSeparatorsCount = this._separatorsCountFromSlice(valueBeforePos);

        return new ChangeDetails({
          tailShift: (beforeTailSeparatorsCount - prevBeforeTailSeparatorsCount) * this.thousandsSeparator.length
        });
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos, direction) {
        if (!this.thousandsSeparator) return cursorPos;

        switch (direction) {
          case DIRECTION.NONE:
          case DIRECTION.LEFT:
          case DIRECTION.FORCE_LEFT:
            {
              var separatorAtLeftPos = this._findSeparatorAround(cursorPos - 1);

              if (separatorAtLeftPos >= 0) {
                var separatorAtLeftEndPos = separatorAtLeftPos + this.thousandsSeparator.length;

                if (cursorPos < separatorAtLeftEndPos || this.value.length <= separatorAtLeftEndPos || direction === DIRECTION.FORCE_LEFT) {
                  return separatorAtLeftPos;
                }
              }

              break;
            }

          case DIRECTION.RIGHT:
          case DIRECTION.FORCE_RIGHT:
            {
              var separatorAtRightPos = this._findSeparatorAround(cursorPos);

              if (separatorAtRightPos >= 0) {
                return separatorAtRightPos + this.thousandsSeparator.length;
              }
            }
        }

        return cursorPos;
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate(flags) {
        var regexp = flags.input ? this._numberRegExpInput : this._numberRegExp; // validate as string

        var valid = regexp.test(this._removeThousandsSeparators(this.value));

        if (valid) {
          // validate as number
          var number = this.number;
          valid = valid && !isNaN(number) && ( // check min bound for negative values
          this.min == null || this.min >= 0 || this.min <= this.number) && ( // check max bound for positive values
          this.max == null || this.max <= 0 || this.number <= this.max);
        }

        return valid && _get(_getPrototypeOf(MaskedNumber.prototype), "doValidate", this).call(this, flags);
      }
      /**
        @override
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        if (this.value) {
          var number = this.number;
          var validnum = number; // check bounds

          if (this.min != null) validnum = Math.max(validnum, this.min);
          if (this.max != null) validnum = Math.min(validnum, this.max);
          if (validnum !== number) this.unmaskedValue = String(validnum);
          var formatted = this.value;
          if (this.normalizeZeros) formatted = this._normalizeZeros(formatted);
          if (this.padFractionalZeros) formatted = this._padFractionalZeros(formatted);
          this._value = formatted;
        }

        _get(_getPrototypeOf(MaskedNumber.prototype), "doCommit", this).call(this);
      }
      /** */

    }, {
      key: "_normalizeZeros",
      value: function _normalizeZeros(value) {
        var parts = this._removeThousandsSeparators(value).split(this.radix); // remove leading zeros


        parts[0] = parts[0].replace(/^(\D*)(0*)(\d*)/, function (match, sign, zeros, num) {
          return sign + num;
        }); // add leading zero

        if (value.length && !/\d$/.test(parts[0])) parts[0] = parts[0] + '0';

        if (parts.length > 1) {
          parts[1] = parts[1].replace(/0*$/, ''); // remove trailing zeros

          if (!parts[1].length) parts.length = 1; // remove fractional
        }

        return this._insertThousandsSeparators(parts.join(this.radix));
      }
      /** */

    }, {
      key: "_padFractionalZeros",
      value: function _padFractionalZeros(value) {
        if (!value) return value;
        var parts = value.split(this.radix);
        if (parts.length < 2) parts.push('');
        parts[1] = parts[1].padEnd(this.scale, '0');
        return parts.join(this.radix);
      }
      /**
        @override
      */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._removeThousandsSeparators(this._normalizeZeros(this.value)).replace(this.radix, '.');
      },
      set: function set(unmaskedValue) {
        _set(_getPrototypeOf(MaskedNumber.prototype), "unmaskedValue", unmaskedValue.replace('.', this.radix), this, true);
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get() {
        return Number(this.unmaskedValue);
      },
      set: function set(n) {
        _set(_getPrototypeOf(MaskedNumber.prototype), "unmaskedValue", String(n), this, true);
      }
      /** Parsed Number */

    }, {
      key: "number",
      get: function get() {
        return this.typedValue;
      },
      set: function set(number) {
        this.typedValue = number;
      }
      /**
        Is negative allowed
        @readonly
      */

    }, {
      key: "allowNegative",
      get: function get() {
        return this.signed || this.min != null && this.min < 0 || this.max != null && this.max < 0;
      }
    }]);

    return MaskedNumber;
  }(Masked);

  MaskedNumber.DEFAULTS = {
    radix: ',',
    thousandsSeparator: '',
    mapToRadix: ['.'],
    scale: 2,
    signed: false,
    normalizeZeros: true,
    padFractionalZeros: false
  };
  IMask.MaskedNumber = MaskedNumber;

  /** Masking by custom Function */

  var MaskedFunction = /*#__PURE__*/function (_Masked) {
    _inherits(MaskedFunction, _Masked);

    var _super = _createSuper(MaskedFunction);

    function MaskedFunction() {
      _classCallCheck(this, MaskedFunction);

      return _super.apply(this, arguments);
    }

    _createClass(MaskedFunction, [{
      key: "_update",
      value:
      /**
        @override
        @param {Object} opts
      */
      function _update(opts) {
        if (opts.mask) opts.validate = opts.mask;

        _get(_getPrototypeOf(MaskedFunction.prototype), "_update", this).call(this, opts);
      }
    }]);

    return MaskedFunction;
  }(Masked);

  IMask.MaskedFunction = MaskedFunction;

  /** Dynamic mask for choosing apropriate mask in run-time */

  var MaskedDynamic = /*#__PURE__*/function (_Masked) {
    _inherits(MaskedDynamic, _Masked);

    var _super = _createSuper(MaskedDynamic);
    /** Currently chosen mask */

    /** Compliled {@link Masked} options */

    /** Chooses {@link Masked} depending on input value */

    /**
      @param {Object} opts
    */


    function MaskedDynamic(opts) {
      var _this;

      _classCallCheck(this, MaskedDynamic);

      _this = _super.call(this, Object.assign({}, MaskedDynamic.DEFAULTS, opts));
      _this.currentMask = null;
      return _this;
    }
    /**
      @override
    */


    _createClass(MaskedDynamic, [{
      key: "_update",
      value: function _update(opts) {
        _get(_getPrototypeOf(MaskedDynamic.prototype), "_update", this).call(this, opts);

        if ('mask' in opts) {
          // mask could be totally dynamic with only `dispatch` option
          this.compiledMasks = Array.isArray(opts.mask) ? opts.mask.map(function (m) {
            return createMask(m);
          }) : [];
        }
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var details = this._applyDispatch(ch, flags);

        if (this.currentMask) {
          details.aggregate(this.currentMask._appendChar(ch, flags));
        }

        return details;
      }
    }, {
      key: "_applyDispatch",
      value: function _applyDispatch() {
        var appended = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var prevValueBeforeTail = flags.tail && flags._beforeTailState != null ? flags._beforeTailState._value : this.value;
        var inputValue = this.rawInputValue;
        var insertValue = flags.tail && flags._beforeTailState != null ? // $FlowFixMe - tired to fight with type system
        flags._beforeTailState._rawInputValue : inputValue;
        var tailValue = inputValue.slice(insertValue.length);
        var prevMask = this.currentMask;
        var details = new ChangeDetails();
        var prevMaskState = prevMask && prevMask.state; // clone flags to prevent overwriting `_beforeTailState`

        this.currentMask = this.doDispatch(appended, Object.assign({}, flags)); // restore state after dispatch

        if (this.currentMask) {
          if (this.currentMask !== prevMask) {
            // if mask changed reapply input
            this.currentMask.reset();

            if (insertValue) {
              // $FlowFixMe - it's ok, we don't change current mask above
              var d = this.currentMask.append(insertValue, {
                raw: true
              });
              details.tailShift = d.inserted.length - prevValueBeforeTail.length;
            }

            if (tailValue) {
              // $FlowFixMe - it's ok, we don't change current mask above
              details.tailShift += this.currentMask.append(tailValue, {
                raw: true,
                tail: true
              }).tailShift;
            }
          } else {
            // Dispatch can do something bad with state, so
            // restore prev mask state
            this.currentMask.state = prevMaskState;
          }
        }

        return details;
      }
    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        var details = this._applyDispatch.apply(this, arguments);

        if (this.currentMask) {
          details.aggregate(this.currentMask._appendPlaceholder());
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "doDispatch",
      value: function doDispatch(appended) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.dispatch(appended, this, flags);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _get2, _this$currentMask;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedDynamic.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args)) && (!this.currentMask || (_this$currentMask = this.currentMask).doValidate.apply(_this$currentMask, args));
      }
      /**
        @override
      */

    }, {
      key: "reset",
      value: function reset() {
        if (this.currentMask) this.currentMask.reset();
        this.compiledMasks.forEach(function (m) {
          return m.reset();
        });
      }
      /**
        @override
      */

    }, {
      key: "value",
      get: function get() {
        return this.currentMask ? this.currentMask.value : '';
      },
      set: function set(value) {
        _set(_getPrototypeOf(MaskedDynamic.prototype), "value", value, this, true);
      }
      /**
        @override
      */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.currentMask ? this.currentMask.unmaskedValue : '';
      },
      set: function set(unmaskedValue) {
        _set(_getPrototypeOf(MaskedDynamic.prototype), "unmaskedValue", unmaskedValue, this, true);
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get() {
        return this.currentMask ? this.currentMask.typedValue : '';
      } // probably typedValue should not be used with dynamic
      ,
      set: function set(value) {
        var unmaskedValue = String(value); // double check it

        if (this.currentMask) {
          this.currentMask.typedValue = value;
          unmaskedValue = this.currentMask.unmaskedValue;
        }

        this.unmaskedValue = unmaskedValue;
      }
      /**
        @override
      */

    }, {
      key: "isComplete",
      get: function get() {
        return !!this.currentMask && this.currentMask.isComplete;
      }
      /**
        @override
      */

    }, {
      key: "remove",
      value: function remove() {
        var details = new ChangeDetails();

        if (this.currentMask) {
          var _this$currentMask2;

          details.aggregate((_this$currentMask2 = this.currentMask).remove.apply(_this$currentMask2, arguments)) // update with dispatch
          .aggregate(this._applyDispatch());
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "state",
      get: function get() {
        return Object.assign({}, _get(_getPrototypeOf(MaskedDynamic.prototype), "state", this), {
          _rawInputValue: this.rawInputValue,
          compiledMasks: this.compiledMasks.map(function (m) {
            return m.state;
          }),
          currentMaskRef: this.currentMask,
          currentMask: this.currentMask && this.currentMask.state
        });
      },
      set: function set(state) {
        var compiledMasks = state.compiledMasks,
            currentMaskRef = state.currentMaskRef,
            currentMask = state.currentMask,
            maskedState = _objectWithoutProperties(state, ["compiledMasks", "currentMaskRef", "currentMask"]);

        this.compiledMasks.forEach(function (m, mi) {
          return m.state = compiledMasks[mi];
        });

        if (currentMaskRef != null) {
          this.currentMask = currentMaskRef;
          this.currentMask.state = currentMask;
        }

        _set(_getPrototypeOf(MaskedDynamic.prototype), "state", maskedState, this, true);
      }
      /**
        @override
      */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var _this$currentMask3;

        return this.currentMask ? (_this$currentMask3 = this.currentMask).extractInput.apply(_this$currentMask3, arguments) : '';
      }
      /**
        @override
      */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var _this$currentMask4, _get3;

        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return this.currentMask ? (_this$currentMask4 = this.currentMask).extractTail.apply(_this$currentMask4, args) : (_get3 = _get(_getPrototypeOf(MaskedDynamic.prototype), "extractTail", this)).call.apply(_get3, [this].concat(args));
      }
      /**
        @override
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        if (this.currentMask) this.currentMask.doCommit();

        _get(_getPrototypeOf(MaskedDynamic.prototype), "doCommit", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos() {
        var _this$currentMask5, _get4;

        for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          args[_key3] = arguments[_key3];
        }

        return this.currentMask ? (_this$currentMask5 = this.currentMask).nearestInputPos.apply(_this$currentMask5, args) : (_get4 = _get(_getPrototypeOf(MaskedDynamic.prototype), "nearestInputPos", this)).call.apply(_get4, [this].concat(args));
      }
    }, {
      key: "overwrite",
      get: function get() {
        return this.currentMask ? this.currentMask.overwrite : _get(_getPrototypeOf(MaskedDynamic.prototype), "overwrite", this);
      },
      set: function set(overwrite) {
        console.warn('"overwrite" option is not available in dynamic mask, use this option in siblings');
      }
    }]);

    return MaskedDynamic;
  }(Masked);

  MaskedDynamic.DEFAULTS = {
    dispatch: function dispatch(appended, masked, flags) {
      if (!masked.compiledMasks.length) return;
      var inputValue = masked.rawInputValue; // simulate input

      var inputs = masked.compiledMasks.map(function (m, index) {
        m.reset();
        m.append(inputValue, {
          raw: true
        });
        m.append(appended, flags);
        var weight = m.rawInputValue.length;
        return {
          weight: weight,
          index: index
        };
      }); // pop masks with longer values first

      inputs.sort(function (i1, i2) {
        return i2.weight - i1.weight;
      });
      return masked.compiledMasks[inputs[0].index];
    }
  };
  IMask.MaskedDynamic = MaskedDynamic;

  /** Mask pipe source and destination types */

  var PIPE_TYPE = {
    MASKED: 'value',
    UNMASKED: 'unmaskedValue',
    TYPED: 'typedValue'
  };
  /** Creates new pipe function depending on mask type, source and destination options */

  function createPipe(mask) {
    var from = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : PIPE_TYPE.MASKED;
    var to = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : PIPE_TYPE.MASKED;
    var masked = createMask(mask);
    return function (value) {
      return masked.runIsolated(function (m) {
        m[from] = value;
        return m[to];
      });
    };
  }
  /** Pipes value through mask depending on mask type, source and destination options */


  function pipe(value) {
    for (var _len = arguments.length, pipeArgs = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      pipeArgs[_key - 1] = arguments[_key];
    }

    return createPipe.apply(void 0, pipeArgs)(value);
  }

  IMask.PIPE_TYPE = PIPE_TYPE;
  IMask.createPipe = createPipe;
  IMask.pipe = pipe;

  try {
    globalThis.IMask = IMask;
  } catch (e) {}

  var ModalContact = (function () {
    /**
     * Phone Mask
     */
    var phone = document.querySelector('.tel-input');

    if (phone) {
      var maskOptions = {
        mask: '(000) 000-0000'
      };
      var mask = IMask(phone, maskOptions);
    } // Contact Modal


    var cmf_name = document.querySelector('#cmf_name');
    var cmf_email = document.querySelector('#cmf_email');
    var cmf_company = document.querySelector('#cmf_company');
    var cmf_tel = document.querySelector('#cmf_tel');
    var cmf_msg = document.querySelector('#cmf_msg');
    var c7cmf_name = document.querySelector('#c7cmf_name');
    var c7cmf_email = document.querySelector('#c7cmf_email');
    var c7cmf_company = document.querySelector('#c7cmf_company');
    var c7cmf_tel = document.querySelector('#c7cmf_tel');
    var c7cmf_msg = document.querySelector('#c7cmf_msg');
    /**
     * Contact 7 Forms
     */

    var btnSubmit = document.querySelector('.wpcf7-submit');
    var thanksPanel = document.querySelector('.contact-modal__thanks');

    if (btnSubmit) {
      btnSubmit.addEventListener('click', function (e) {
        btnSubmit.value = 'Sending';
      });
    }

    var wpcf7Form = document.querySelector('.wpcf7');

    if (wpcf7Form) {
      /**
       * Submit Event
       */
      wpcf7Form.addEventListener('wpcf7submit', function (event) {
        var data = event.detail;
        var valErrElems = document.querySelectorAll('.val-err');

        if (valErrElems) {
          var _iterator = _createForOfIteratorHelper(valErrElems),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var item = _step.value;
              item.classList.remove('val-err');
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        } // Validate Fields


        if (data.status === 'validation_failed') {
          // Invalid Fields
          var fields = data.apiResponse.invalid_fields;

          if (fields) {
            fields.forEach(function (element) {
              var item = document.querySelector('#' + element.idref.replace('c7', ''));

              if (item) {
                item.classList.add('val-err');
              }
            });
          }
        }

        setTimeout(function () {
          btnSubmit.value = 'Submit';
        }, 100);
      }, false);
      wpcf7Form.addEventListener('wpcf7mailsent', function (event) {
        if (thanksPanel) {
          thanksPanel.classList.remove('hide');
        } // Clear Form


        setTimeout(function () {
          var valErrElems = document.querySelectorAll('.val-err');

          if (valErrElems) {
            var _iterator2 = _createForOfIteratorHelper(valErrElems),
                _step2;

            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var item = _step2.value;
                item.classList.remove('val-err');
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }
          }

          if (cmf_name && c7cmf_name) {
            cmf_name.value = '';
            c7cmf_name.value = '';
          }

          if (cmf_email && c7cmf_email) {
            cmf_email.value = '';
            c7cmf_email.value = '';
          }

          if (cmf_company && c7cmf_company) {
            cmf_company.value = '';
            c7cmf_company.value = '';
          }

          if (cmf_tel && c7cmf_tel) {
            cmf_tel.value = '';
            c7cmf_tel.value = '';
          }

          if (cmf_msg && c7cmf_msg) {
            cmf_msg.value = '';
            c7cmf_msg.value = '';
          }

          var outputMsg = document.querySelector('.wpcf7-response-output');

          if (outputMsg) {
            outputMsg.style.display = 'none';
          }

          thanksPanel.classList.add('hide');
        }, 15000);
      });
    }
    /**
     * Contact 7 Integration with Modal Inputs
     */


    if (cmf_name && c7cmf_name) {
      cmf_name.value = '';
      c7cmf_name.value = '';
      cmf_name.addEventListener('input', function (e) {
        c7cmf_name.value = e.target.value;
      });
    }

    if (cmf_email && c7cmf_email) {
      cmf_email.value = '';
      c7cmf_email.value = '';
      cmf_email.addEventListener('input', function (e) {
        c7cmf_email.value = e.target.value;
      });
    }

    if (cmf_company && c7cmf_company) {
      cmf_company.value = '';
      c7cmf_company.value = '';
      cmf_company.addEventListener('input', function (e) {
        c7cmf_company.value = e.target.value;
      });
    }

    if (cmf_tel && c7cmf_tel) {
      cmf_tel.value = '';
      c7cmf_tel.value = '';
      cmf_tel.addEventListener('input', function (e) {
        c7cmf_tel.value = e.target.value;
      });
    }

    if (cmf_msg && c7cmf_msg) {
      cmf_msg.value = '';
      c7cmf_msg.value = '';
      cmf_msg.addEventListener('input', function (e) {
        c7cmf_msg.value = e.target.value;
      });
    } // }

  });

  /*
   * Modal | Video
   * ========================================================================== */
  var ModalVideo = function ModalVideo() {
    /**
     * Hide Popup by Close Button
     */
    var btnHide = document.querySelectorAll('.modal-video-hide');

    if (btnHide) {
      var btnHideLen = btnHide.length;

      for (var i = 0; i < btnHideLen; i++) {
        btnHide[i].addEventListener('click', function (e) {
          e.preventDefault();
          var el = e.currentTarget;
          var dataTarget = el.dataset.target;
          var yutubevideo = document.getElementById(dataTarget + 'video');

          if (yutubevideo) {
            yutubevideo.src = '';
          }

          var parent = document.getElementById(dataTarget);
          parent.classList.remove('active');
          var iframeBox = parent.querySelector('.modal-wnd__body-iframe');

          if (iframeBox) {
            var embed = iframeBox.innerHTML;
            iframeBox.innerHTML = '';
            iframeBox.innerHTML = embed;
          }
        });
      }
    }
    /**
     * Hide Popup bay Background
     */


    var modal = document.querySelectorAll('.modal-video');

    if (modal) {
      var modalLen = modal.length;

      for (var _i = 0; _i < modalLen; _i++) {
        modal[_i].addEventListener('click', function (e) {
          var obj = e.target;
          if (obj !== e.currentTarget) return;
          obj.classList.remove('active');
          var yutubevideo = document.getElementById(obj.id + 'video');

          if (yutubevideo) {
            yutubevideo.src = '';
          }

          var iframeBox = obj.querySelector('.modal-wnd__body-iframe');

          if (iframeBox) {
            var embed = iframeBox.innerHTML;
            iframeBox.innerHTML = '';
            iframeBox.innerHTML = embed;
          }
        });
      }
    }
    /**
     * Show Modal
     */


    var btnShow = document.querySelectorAll('.modal-video-link');

    if (btnShow) {
      var btnShowLen = btnShow.length;

      for (var _i2 = 0; _i2 < btnShowLen; _i2++) {
        // Click Event
        btnShow[_i2].addEventListener('click', function (e) {
          e.preventDefault();
          var dataTarget = e.currentTarget.dataset.target;
          var modal = document.getElementById(dataTarget);

          if (modal) {
            modal.classList.add('active');
            var videoSrc = modal.dataset.video;

            if (videoSrc) {
              var yutubevideo = document.getElementById(dataTarget + 'video');
              yutubevideo.src = 'https://www.youtube.com/embed/' + videoSrc + '?autoplay=1';
            }
          }
        });
      }
    }
  };

  /**
   * SSR Window 3.0.0
   * Better handling for window object in SSR environment
   * https://github.com/nolimits4web/ssr-window
   *
   * Copyright 2020, Vladimir Kharlampidi
   *
   * Licensed under MIT
   *
   * Released on: November 9, 2020
   */

  /* eslint-disable no-param-reassign */
  function isObject(obj) {
    return obj !== null && _typeof(obj) === 'object' && 'constructor' in obj && obj.constructor === Object;
  }

  function extend(target, src) {
    if (target === void 0) {
      target = {};
    }

    if (src === void 0) {
      src = {};
    }

    Object.keys(src).forEach(function (key) {
      if (typeof target[key] === 'undefined') target[key] = src[key];else if (isObject(src[key]) && isObject(target[key]) && Object.keys(src[key]).length > 0) {
        extend(target[key], src[key]);
      }
    });
  }

  var ssrDocument = {
    body: {},
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    activeElement: {
      blur: function blur() {},
      nodeName: ''
    },
    querySelector: function querySelector() {
      return null;
    },
    querySelectorAll: function querySelectorAll() {
      return [];
    },
    getElementById: function getElementById() {
      return null;
    },
    createEvent: function createEvent() {
      return {
        initEvent: function initEvent() {}
      };
    },
    createElement: function createElement() {
      return {
        children: [],
        childNodes: [],
        style: {},
        setAttribute: function setAttribute() {},
        getElementsByTagName: function getElementsByTagName() {
          return [];
        }
      };
    },
    createElementNS: function createElementNS() {
      return {};
    },
    importNode: function importNode() {
      return null;
    },
    location: {
      hash: '',
      host: '',
      hostname: '',
      href: '',
      origin: '',
      pathname: '',
      protocol: '',
      search: ''
    }
  };

  function getDocument() {
    var doc = typeof document !== 'undefined' ? document : {};
    extend(doc, ssrDocument);
    return doc;
  }

  var ssrWindow = {
    document: ssrDocument,
    navigator: {
      userAgent: ''
    },
    location: {
      hash: '',
      host: '',
      hostname: '',
      href: '',
      origin: '',
      pathname: '',
      protocol: '',
      search: ''
    },
    history: {
      replaceState: function replaceState() {},
      pushState: function pushState() {},
      go: function go() {},
      back: function back() {}
    },
    CustomEvent: function CustomEvent() {
      return this;
    },
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    getComputedStyle: function getComputedStyle() {
      return {
        getPropertyValue: function getPropertyValue() {
          return '';
        }
      };
    },
    Image: function Image() {},
    Date: function Date() {},
    screen: {},
    setTimeout: function setTimeout() {},
    clearTimeout: function clearTimeout() {},
    matchMedia: function matchMedia() {
      return {};
    },
    requestAnimationFrame: function requestAnimationFrame(callback) {
      if (typeof setTimeout === 'undefined') {
        callback();
        return null;
      }

      return setTimeout(callback, 0);
    },
    cancelAnimationFrame: function cancelAnimationFrame(id) {
      if (typeof setTimeout === 'undefined') {
        return;
      }

      clearTimeout(id);
    }
  };

  function getWindow() {
    var win = typeof window !== 'undefined' ? window : {};
    extend(win, ssrWindow);
    return win;
  }

  /**
   * Dom7 3.0.0
   * Minimalistic JavaScript library for DOM manipulation, with a jQuery-compatible API
   * https://framework7.io/docs/dom7.html
   *
   * Copyright 2020, Vladimir Kharlampidi
   *
   * Licensed under MIT
   *
   * Released on: November 9, 2020
   */

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  function _getPrototypeOf$1(o) {
    _getPrototypeOf$1 = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf$1(o);
  }

  function _setPrototypeOf$1(o, p) {
    _setPrototypeOf$1 = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf$1(o, p);
  }

  function _isNativeReflectConstruct$1() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct$1()) {
      _construct = Reflect.construct;
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf$1(instance, Class.prototype);
        return instance;
      };
    }

    return _construct.apply(null, arguments);
  }

  function _isNativeFunction(fn) {
    return Function.toString.call(fn).indexOf("[native code]") !== -1;
  }

  function _wrapNativeSuper(Class) {
    var _cache = typeof Map === "function" ? new Map() : undefined;

    _wrapNativeSuper = function _wrapNativeSuper(Class) {
      if (Class === null || !_isNativeFunction(Class)) return Class;

      if (typeof Class !== "function") {
        throw new TypeError("Super expression must either be null or a function");
      }

      if (typeof _cache !== "undefined") {
        if (_cache.has(Class)) return _cache.get(Class);

        _cache.set(Class, Wrapper);
      }

      function Wrapper() {
        return _construct(Class, arguments, _getPrototypeOf$1(this).constructor);
      }

      Wrapper.prototype = Object.create(Class.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      return _setPrototypeOf$1(Wrapper, Class);
    };

    return _wrapNativeSuper(Class);
  }

  function _assertThisInitialized$1(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }
  /* eslint-disable no-proto */


  function makeReactive(obj) {
    var proto = obj.__proto__;
    Object.defineProperty(obj, '__proto__', {
      get: function get() {
        return proto;
      },
      set: function set(value) {
        proto.__proto__ = value;
      }
    });
  }

  var Dom7 = /*#__PURE__*/function (_Array) {
    _inheritsLoose(Dom7, _Array);

    function Dom7(items) {
      var _this;

      _this = _Array.call.apply(_Array, [this].concat(items)) || this;
      makeReactive(_assertThisInitialized$1(_this));
      return _this;
    }

    return Dom7;
  }( /*#__PURE__*/_wrapNativeSuper(Array));

  function arrayFlat(arr) {
    if (arr === void 0) {
      arr = [];
    }

    var res = [];
    arr.forEach(function (el) {
      if (Array.isArray(el)) {
        res.push.apply(res, arrayFlat(el));
      } else {
        res.push(el);
      }
    });
    return res;
  }

  function arrayFilter(arr, callback) {
    return Array.prototype.filter.call(arr, callback);
  }

  function arrayUnique(arr) {
    var uniqueArray = [];

    for (var i = 0; i < arr.length; i += 1) {
      if (uniqueArray.indexOf(arr[i]) === -1) uniqueArray.push(arr[i]);
    }

    return uniqueArray;
  }

  function qsa(selector, context) {
    if (typeof selector !== 'string') {
      return [selector];
    }

    var a = [];
    var res = context.querySelectorAll(selector);

    for (var i = 0; i < res.length; i += 1) {
      a.push(res[i]);
    }

    return a;
  }

  function $(selector, context) {
    var window = getWindow();
    var document = getDocument();
    var arr = [];

    if (!context && selector instanceof Dom7) {
      return selector;
    }

    if (!selector) {
      return new Dom7(arr);
    }

    if (typeof selector === 'string') {
      var html = selector.trim();

      if (html.indexOf('<') >= 0 && html.indexOf('>') >= 0) {
        var toCreate = 'div';
        if (html.indexOf('<li') === 0) toCreate = 'ul';
        if (html.indexOf('<tr') === 0) toCreate = 'tbody';
        if (html.indexOf('<td') === 0 || html.indexOf('<th') === 0) toCreate = 'tr';
        if (html.indexOf('<tbody') === 0) toCreate = 'table';
        if (html.indexOf('<option') === 0) toCreate = 'select';
        var tempParent = document.createElement(toCreate);
        tempParent.innerHTML = html;

        for (var i = 0; i < tempParent.childNodes.length; i += 1) {
          arr.push(tempParent.childNodes[i]);
        }
      } else {
        arr = qsa(selector.trim(), context || document);
      } // arr = qsa(selector, document);

    } else if (selector.nodeType || selector === window || selector === document) {
      arr.push(selector);
    } else if (Array.isArray(selector)) {
      if (selector instanceof Dom7) return selector;
      arr = selector;
    }

    return new Dom7(arrayUnique(arr));
  }

  $.fn = Dom7.prototype;

  function addClass() {
    for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
      classes[_key] = arguments[_key];
    }

    var classNames = arrayFlat(classes.map(function (c) {
      return c.split(' ');
    }));
    this.forEach(function (el) {
      var _el$classList;

      (_el$classList = el.classList).add.apply(_el$classList, classNames);
    });
    return this;
  }

  function removeClass() {
    for (var _len2 = arguments.length, classes = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      classes[_key2] = arguments[_key2];
    }

    var classNames = arrayFlat(classes.map(function (c) {
      return c.split(' ');
    }));
    this.forEach(function (el) {
      var _el$classList2;

      (_el$classList2 = el.classList).remove.apply(_el$classList2, classNames);
    });
    return this;
  }

  function toggleClass() {
    for (var _len3 = arguments.length, classes = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      classes[_key3] = arguments[_key3];
    }

    var classNames = arrayFlat(classes.map(function (c) {
      return c.split(' ');
    }));
    this.forEach(function (el) {
      classNames.forEach(function (className) {
        el.classList.toggle(className);
      });
    });
  }

  function hasClass() {
    for (var _len4 = arguments.length, classes = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      classes[_key4] = arguments[_key4];
    }

    var classNames = arrayFlat(classes.map(function (c) {
      return c.split(' ');
    }));
    return arrayFilter(this, function (el) {
      return classNames.filter(function (className) {
        return el.classList.contains(className);
      }).length > 0;
    }).length > 0;
  }

  function attr(attrs, value) {
    if (arguments.length === 1 && typeof attrs === 'string') {
      // Get attr
      if (this[0]) return this[0].getAttribute(attrs);
      return undefined;
    } // Set attrs


    for (var i = 0; i < this.length; i += 1) {
      if (arguments.length === 2) {
        // String
        this[i].setAttribute(attrs, value);
      } else {
        // Object
        for (var attrName in attrs) {
          this[i][attrName] = attrs[attrName];
          this[i].setAttribute(attrName, attrs[attrName]);
        }
      }
    }

    return this;
  }

  function removeAttr(attr) {
    for (var i = 0; i < this.length; i += 1) {
      this[i].removeAttribute(attr);
    }

    return this;
  }

  function transform(transform) {
    for (var i = 0; i < this.length; i += 1) {
      this[i].style.transform = transform;
    }

    return this;
  }

  function transition(duration) {
    for (var i = 0; i < this.length; i += 1) {
      this[i].style.transitionDuration = typeof duration !== 'string' ? duration + "ms" : duration;
    }

    return this;
  }

  function on() {
    for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      args[_key5] = arguments[_key5];
    }

    var eventType = args[0],
        targetSelector = args[1],
        listener = args[2],
        capture = args[3];

    if (typeof args[1] === 'function') {
      eventType = args[0];
      listener = args[1];
      capture = args[2];
      targetSelector = undefined;
    }

    if (!capture) capture = false;

    function handleLiveEvent(e) {
      var target = e.target;
      if (!target) return;
      var eventData = e.target.dom7EventData || [];

      if (eventData.indexOf(e) < 0) {
        eventData.unshift(e);
      }

      if ($(target).is(targetSelector)) listener.apply(target, eventData);else {
        var _parents = $(target).parents(); // eslint-disable-line


        for (var k = 0; k < _parents.length; k += 1) {
          if ($(_parents[k]).is(targetSelector)) listener.apply(_parents[k], eventData);
        }
      }
    }

    function handleEvent(e) {
      var eventData = e && e.target ? e.target.dom7EventData || [] : [];

      if (eventData.indexOf(e) < 0) {
        eventData.unshift(e);
      }

      listener.apply(this, eventData);
    }

    var events = eventType.split(' ');
    var j;

    for (var i = 0; i < this.length; i += 1) {
      var el = this[i];

      if (!targetSelector) {
        for (j = 0; j < events.length; j += 1) {
          var event = events[j];
          if (!el.dom7Listeners) el.dom7Listeners = {};
          if (!el.dom7Listeners[event]) el.dom7Listeners[event] = [];
          el.dom7Listeners[event].push({
            listener: listener,
            proxyListener: handleEvent
          });
          el.addEventListener(event, handleEvent, capture);
        }
      } else {
        // Live events
        for (j = 0; j < events.length; j += 1) {
          var _event = events[j];
          if (!el.dom7LiveListeners) el.dom7LiveListeners = {};
          if (!el.dom7LiveListeners[_event]) el.dom7LiveListeners[_event] = [];

          el.dom7LiveListeners[_event].push({
            listener: listener,
            proxyListener: handleLiveEvent
          });

          el.addEventListener(_event, handleLiveEvent, capture);
        }
      }
    }

    return this;
  }

  function off() {
    for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
      args[_key6] = arguments[_key6];
    }

    var eventType = args[0],
        targetSelector = args[1],
        listener = args[2],
        capture = args[3];

    if (typeof args[1] === 'function') {
      eventType = args[0];
      listener = args[1];
      capture = args[2];
      targetSelector = undefined;
    }

    if (!capture) capture = false;
    var events = eventType.split(' ');

    for (var i = 0; i < events.length; i += 1) {
      var event = events[i];

      for (var j = 0; j < this.length; j += 1) {
        var el = this[j];
        var handlers = void 0;

        if (!targetSelector && el.dom7Listeners) {
          handlers = el.dom7Listeners[event];
        } else if (targetSelector && el.dom7LiveListeners) {
          handlers = el.dom7LiveListeners[event];
        }

        if (handlers && handlers.length) {
          for (var k = handlers.length - 1; k >= 0; k -= 1) {
            var handler = handlers[k];

            if (listener && handler.listener === listener) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            } else if (listener && handler.listener && handler.listener.dom7proxy && handler.listener.dom7proxy === listener) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            } else if (!listener) {
              el.removeEventListener(event, handler.proxyListener, capture);
              handlers.splice(k, 1);
            }
          }
        }
      }
    }

    return this;
  }

  function trigger() {
    var window = getWindow();

    for (var _len9 = arguments.length, args = new Array(_len9), _key9 = 0; _key9 < _len9; _key9++) {
      args[_key9] = arguments[_key9];
    }

    var events = args[0].split(' ');
    var eventData = args[1];

    for (var i = 0; i < events.length; i += 1) {
      var event = events[i];

      for (var j = 0; j < this.length; j += 1) {
        var el = this[j];

        if (window.CustomEvent) {
          var evt = new window.CustomEvent(event, {
            detail: eventData,
            bubbles: true,
            cancelable: true
          });
          el.dom7EventData = args.filter(function (data, dataIndex) {
            return dataIndex > 0;
          });
          el.dispatchEvent(evt);
          el.dom7EventData = [];
          delete el.dom7EventData;
        }
      }
    }

    return this;
  }

  function transitionEnd(callback) {
    var dom = this;

    function fireCallBack(e) {
      if (e.target !== this) return;
      callback.call(this, e);
      dom.off('transitionend', fireCallBack);
    }

    if (callback) {
      dom.on('transitionend', fireCallBack);
    }

    return this;
  }

  function outerWidth(includeMargins) {
    if (this.length > 0) {
      if (includeMargins) {
        var _styles = this.styles();

        return this[0].offsetWidth + parseFloat(_styles.getPropertyValue('margin-right')) + parseFloat(_styles.getPropertyValue('margin-left'));
      }

      return this[0].offsetWidth;
    }

    return null;
  }

  function outerHeight(includeMargins) {
    if (this.length > 0) {
      if (includeMargins) {
        var _styles2 = this.styles();

        return this[0].offsetHeight + parseFloat(_styles2.getPropertyValue('margin-top')) + parseFloat(_styles2.getPropertyValue('margin-bottom'));
      }

      return this[0].offsetHeight;
    }

    return null;
  }

  function offset() {
    if (this.length > 0) {
      var window = getWindow();
      var document = getDocument();
      var el = this[0];
      var box = el.getBoundingClientRect();
      var body = document.body;
      var clientTop = el.clientTop || body.clientTop || 0;
      var clientLeft = el.clientLeft || body.clientLeft || 0;
      var scrollTop = el === window ? window.scrollY : el.scrollTop;
      var scrollLeft = el === window ? window.scrollX : el.scrollLeft;
      return {
        top: box.top + scrollTop - clientTop,
        left: box.left + scrollLeft - clientLeft
      };
    }

    return null;
  }

  function styles() {
    var window = getWindow();
    if (this[0]) return window.getComputedStyle(this[0], null);
    return {};
  }

  function css(props, value) {
    var window = getWindow();
    var i;

    if (arguments.length === 1) {
      if (typeof props === 'string') {
        // .css('width')
        if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(props);
      } else {
        // .css({ width: '100px' })
        for (i = 0; i < this.length; i += 1) {
          for (var _prop in props) {
            this[i].style[_prop] = props[_prop];
          }
        }

        return this;
      }
    }

    if (arguments.length === 2 && typeof props === 'string') {
      // .css('width', '100px')
      for (i = 0; i < this.length; i += 1) {
        this[i].style[props] = value;
      }

      return this;
    }

    return this;
  }

  function each(callback) {
    if (!callback) return this;
    this.forEach(function (el, index) {
      callback.apply(el, [el, index]);
    });
    return this;
  }

  function filter(callback) {
    var result = arrayFilter(this, callback);
    return $(result);
  }

  function html(html) {
    if (typeof html === 'undefined') {
      return this[0] ? this[0].innerHTML : null;
    }

    for (var i = 0; i < this.length; i += 1) {
      this[i].innerHTML = html;
    }

    return this;
  }

  function text(text) {
    if (typeof text === 'undefined') {
      return this[0] ? this[0].textContent.trim() : null;
    }

    for (var i = 0; i < this.length; i += 1) {
      this[i].textContent = text;
    }

    return this;
  }

  function is(selector) {
    var window = getWindow();
    var document = getDocument();
    var el = this[0];
    var compareWith;
    var i;
    if (!el || typeof selector === 'undefined') return false;

    if (typeof selector === 'string') {
      if (el.matches) return el.matches(selector);
      if (el.webkitMatchesSelector) return el.webkitMatchesSelector(selector);
      if (el.msMatchesSelector) return el.msMatchesSelector(selector);
      compareWith = $(selector);

      for (i = 0; i < compareWith.length; i += 1) {
        if (compareWith[i] === el) return true;
      }

      return false;
    }

    if (selector === document) {
      return el === document;
    }

    if (selector === window) {
      return el === window;
    }

    if (selector.nodeType || selector instanceof Dom7) {
      compareWith = selector.nodeType ? [selector] : selector;

      for (i = 0; i < compareWith.length; i += 1) {
        if (compareWith[i] === el) return true;
      }

      return false;
    }

    return false;
  }

  function index() {
    var child = this[0];
    var i;

    if (child) {
      i = 0; // eslint-disable-next-line

      while ((child = child.previousSibling) !== null) {
        if (child.nodeType === 1) i += 1;
      }

      return i;
    }

    return undefined;
  }

  function eq(index) {
    if (typeof index === 'undefined') return this;
    var length = this.length;

    if (index > length - 1) {
      return $([]);
    }

    if (index < 0) {
      var returnIndex = length + index;
      if (returnIndex < 0) return $([]);
      return $([this[returnIndex]]);
    }

    return $([this[index]]);
  }

  function append() {
    var newChild;
    var document = getDocument();

    for (var k = 0; k < arguments.length; k += 1) {
      newChild = k < 0 || arguments.length <= k ? undefined : arguments[k];

      for (var i = 0; i < this.length; i += 1) {
        if (typeof newChild === 'string') {
          var tempDiv = document.createElement('div');
          tempDiv.innerHTML = newChild;

          while (tempDiv.firstChild) {
            this[i].appendChild(tempDiv.firstChild);
          }
        } else if (newChild instanceof Dom7) {
          for (var j = 0; j < newChild.length; j += 1) {
            this[i].appendChild(newChild[j]);
          }
        } else {
          this[i].appendChild(newChild);
        }
      }
    }

    return this;
  }

  function prepend(newChild) {
    var document = getDocument();
    var i;
    var j;

    for (i = 0; i < this.length; i += 1) {
      if (typeof newChild === 'string') {
        var tempDiv = document.createElement('div');
        tempDiv.innerHTML = newChild;

        for (j = tempDiv.childNodes.length - 1; j >= 0; j -= 1) {
          this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
        }
      } else if (newChild instanceof Dom7) {
        for (j = 0; j < newChild.length; j += 1) {
          this[i].insertBefore(newChild[j], this[i].childNodes[0]);
        }
      } else {
        this[i].insertBefore(newChild, this[i].childNodes[0]);
      }
    }

    return this;
  }

  function next(selector) {
    if (this.length > 0) {
      if (selector) {
        if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) {
          return $([this[0].nextElementSibling]);
        }

        return $([]);
      }

      if (this[0].nextElementSibling) return $([this[0].nextElementSibling]);
      return $([]);
    }

    return $([]);
  }

  function nextAll(selector) {
    var nextEls = [];
    var el = this[0];
    if (!el) return $([]);

    while (el.nextElementSibling) {
      var _next = el.nextElementSibling; // eslint-disable-line

      if (selector) {
        if ($(_next).is(selector)) nextEls.push(_next);
      } else nextEls.push(_next);

      el = _next;
    }

    return $(nextEls);
  }

  function prev(selector) {
    if (this.length > 0) {
      var el = this[0];

      if (selector) {
        if (el.previousElementSibling && $(el.previousElementSibling).is(selector)) {
          return $([el.previousElementSibling]);
        }

        return $([]);
      }

      if (el.previousElementSibling) return $([el.previousElementSibling]);
      return $([]);
    }

    return $([]);
  }

  function prevAll(selector) {
    var prevEls = [];
    var el = this[0];
    if (!el) return $([]);

    while (el.previousElementSibling) {
      var _prev = el.previousElementSibling; // eslint-disable-line

      if (selector) {
        if ($(_prev).is(selector)) prevEls.push(_prev);
      } else prevEls.push(_prev);

      el = _prev;
    }

    return $(prevEls);
  }

  function parent(selector) {
    var parents = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      if (this[i].parentNode !== null) {
        if (selector) {
          if ($(this[i].parentNode).is(selector)) parents.push(this[i].parentNode);
        } else {
          parents.push(this[i].parentNode);
        }
      }
    }

    return $(parents);
  }

  function parents(selector) {
    var parents = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      var _parent = this[i].parentNode; // eslint-disable-line

      while (_parent) {
        if (selector) {
          if ($(_parent).is(selector)) parents.push(_parent);
        } else {
          parents.push(_parent);
        }

        _parent = _parent.parentNode;
      }
    }

    return $(parents);
  }

  function closest(selector) {
    var closest = this; // eslint-disable-line

    if (typeof selector === 'undefined') {
      return $([]);
    }

    if (!closest.is(selector)) {
      closest = closest.parents(selector).eq(0);
    }

    return closest;
  }

  function find(selector) {
    var foundElements = [];

    for (var i = 0; i < this.length; i += 1) {
      var found = this[i].querySelectorAll(selector);

      for (var j = 0; j < found.length; j += 1) {
        foundElements.push(found[j]);
      }
    }

    return $(foundElements);
  }

  function children(selector) {
    var children = []; // eslint-disable-line

    for (var i = 0; i < this.length; i += 1) {
      var childNodes = this[i].children;

      for (var j = 0; j < childNodes.length; j += 1) {
        if (!selector || $(childNodes[j]).is(selector)) {
          children.push(childNodes[j]);
        }
      }
    }

    return $(children);
  }

  function remove() {
    for (var i = 0; i < this.length; i += 1) {
      if (this[i].parentNode) this[i].parentNode.removeChild(this[i]);
    }

    return this;
  }

  var Methods = {
    addClass: addClass,
    removeClass: removeClass,
    hasClass: hasClass,
    toggleClass: toggleClass,
    attr: attr,
    removeAttr: removeAttr,
    transform: transform,
    transition: transition,
    on: on,
    off: off,
    trigger: trigger,
    transitionEnd: transitionEnd,
    outerWidth: outerWidth,
    outerHeight: outerHeight,
    styles: styles,
    offset: offset,
    css: css,
    each: each,
    html: html,
    text: text,
    is: is,
    index: index,
    eq: eq,
    append: append,
    prepend: prepend,
    next: next,
    nextAll: nextAll,
    prev: prev,
    prevAll: prevAll,
    parent: parent,
    parents: parents,
    closest: closest,
    find: find,
    children: children,
    filter: filter,
    remove: remove
  };
  Object.keys(Methods).forEach(function (methodName) {
    $.fn[methodName] = Methods[methodName];
  });

  function deleteProps(obj) {
    var object = obj;
    Object.keys(object).forEach(function (key) {
      try {
        object[key] = null;
      } catch (e) {// no getter for object
      }

      try {
        delete object[key];
      } catch (e) {// something got wrong
      }
    });
  }

  function nextTick(callback, delay) {
    if (delay === void 0) {
      delay = 0;
    }

    return setTimeout(callback, delay);
  }

  function now() {
    return Date.now();
  }

  function getTranslate(el, axis) {
    if (axis === void 0) {
      axis = 'x';
    }

    var window = getWindow();
    var matrix;
    var curTransform;
    var transformMatrix;
    var curStyle = window.getComputedStyle(el, null);

    if (window.WebKitCSSMatrix) {
      curTransform = curStyle.transform || curStyle.webkitTransform;

      if (curTransform.split(',').length > 6) {
        curTransform = curTransform.split(', ').map(function (a) {
          return a.replace(',', '.');
        }).join(', ');
      } // Some old versions of Webkit choke when 'none' is passed; pass
      // empty string instead in this case


      transformMatrix = new window.WebKitCSSMatrix(curTransform === 'none' ? '' : curTransform);
    } else {
      transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
      matrix = transformMatrix.toString().split(',');
    }

    if (axis === 'x') {
      // Latest Chrome and webkits Fix
      if (window.WebKitCSSMatrix) curTransform = transformMatrix.m41; // Crazy IE10 Matrix
      else if (matrix.length === 16) curTransform = parseFloat(matrix[12]); // Normal Browsers
        else curTransform = parseFloat(matrix[4]);
    }

    if (axis === 'y') {
      // Latest Chrome and webkits Fix
      if (window.WebKitCSSMatrix) curTransform = transformMatrix.m42; // Crazy IE10 Matrix
      else if (matrix.length === 16) curTransform = parseFloat(matrix[13]); // Normal Browsers
        else curTransform = parseFloat(matrix[5]);
    }

    return curTransform || 0;
  }

  function isObject$1(o) {
    return _typeof(o) === 'object' && o !== null && o.constructor && o.constructor === Object;
  }

  function extend$1() {
    var to = Object(arguments.length <= 0 ? undefined : arguments[0]);

    for (var i = 1; i < arguments.length; i += 1) {
      var nextSource = i < 0 || arguments.length <= i ? undefined : arguments[i];

      if (nextSource !== undefined && nextSource !== null) {
        var keysArray = Object.keys(Object(nextSource));

        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex += 1) {
          var nextKey = keysArray[nextIndex];
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);

          if (desc !== undefined && desc.enumerable) {
            if (isObject$1(to[nextKey]) && isObject$1(nextSource[nextKey])) {
              extend$1(to[nextKey], nextSource[nextKey]);
            } else if (!isObject$1(to[nextKey]) && isObject$1(nextSource[nextKey])) {
              to[nextKey] = {};
              extend$1(to[nextKey], nextSource[nextKey]);
            } else {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
    }

    return to;
  }

  function bindModuleMethods(instance, obj) {
    Object.keys(obj).forEach(function (key) {
      if (isObject$1(obj[key])) {
        Object.keys(obj[key]).forEach(function (subKey) {
          if (typeof obj[key][subKey] === 'function') {
            obj[key][subKey] = obj[key][subKey].bind(instance);
          }
        });
      }

      instance[key] = obj[key];
    });
  }

  var support;

  function calcSupport() {
    var window = getWindow();
    var document = getDocument();
    return {
      touch: !!('ontouchstart' in window || window.DocumentTouch && document instanceof window.DocumentTouch),
      pointerEvents: !!window.PointerEvent && 'maxTouchPoints' in window.navigator && window.navigator.maxTouchPoints >= 0,
      observer: function checkObserver() {
        return 'MutationObserver' in window || 'WebkitMutationObserver' in window;
      }(),
      passiveListener: function checkPassiveListener() {
        var supportsPassive = false;

        try {
          var opts = Object.defineProperty({}, 'passive', {
            // eslint-disable-next-line
            get: function get() {
              supportsPassive = true;
            }
          });
          window.addEventListener('testPassiveListener', null, opts);
        } catch (e) {// No support
        }

        return supportsPassive;
      }(),
      gestures: function checkGestures() {
        return 'ongesturestart' in window;
      }()
    };
  }

  function getSupport() {
    if (!support) {
      support = calcSupport();
    }

    return support;
  }

  var device;

  function calcDevice(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        userAgent = _ref.userAgent;

    var support = getSupport();
    var window = getWindow();
    var platform = window.navigator.platform;
    var ua = userAgent || window.navigator.userAgent;
    var device = {
      ios: false,
      android: false
    };
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/); // eslint-disable-line

    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
    var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
    var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
    var windows = platform === 'Win32';
    var macos = platform === 'MacIntel'; // iPadOs 13 fix

    var iPadScreens = ['1024x1366', '1366x1024', '834x1194', '1194x834', '834x1112', '1112x834', '768x1024', '1024x768', '820x1180', '1180x820', '810x1080', '1080x810'];

    if (!ipad && macos && support.touch && iPadScreens.indexOf(screenWidth + "x" + screenHeight) >= 0) {
      ipad = ua.match(/(Version)\/([\d.]+)/);
      if (!ipad) ipad = [0, 1, '13_0_0'];
      macos = false;
    } // Android


    if (android && !windows) {
      device.os = 'android';
      device.android = true;
    }

    if (ipad || iphone || ipod) {
      device.os = 'ios';
      device.ios = true;
    } // Export object


    return device;
  }

  function getDevice(overrides) {
    if (overrides === void 0) {
      overrides = {};
    }

    if (!device) {
      device = calcDevice(overrides);
    }

    return device;
  }

  var browser;

  function calcBrowser() {
    var window = getWindow();

    function isSafari() {
      var ua = window.navigator.userAgent.toLowerCase();
      return ua.indexOf('safari') >= 0 && ua.indexOf('chrome') < 0 && ua.indexOf('android') < 0;
    }

    return {
      isEdge: !!window.navigator.userAgent.match(/Edge/g),
      isSafari: isSafari(),
      isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window.navigator.userAgent)
    };
  }

  function getBrowser() {
    if (!browser) {
      browser = calcBrowser();
    }

    return browser;
  }

  var Resize = {
    name: 'resize',
    create: function create() {
      var swiper = this;
      extend$1(swiper, {
        resize: {
          resizeHandler: function resizeHandler() {
            if (!swiper || swiper.destroyed || !swiper.initialized) return;
            swiper.emit('beforeResize');
            swiper.emit('resize');
          },
          orientationChangeHandler: function orientationChangeHandler() {
            if (!swiper || swiper.destroyed || !swiper.initialized) return;
            swiper.emit('orientationchange');
          }
        }
      });
    },
    on: {
      init: function init(swiper) {
        var window = getWindow(); // Emit resize

        window.addEventListener('resize', swiper.resize.resizeHandler); // Emit orientationchange

        window.addEventListener('orientationchange', swiper.resize.orientationChangeHandler);
      },
      destroy: function destroy(swiper) {
        var window = getWindow();
        window.removeEventListener('resize', swiper.resize.resizeHandler);
        window.removeEventListener('orientationchange', swiper.resize.orientationChangeHandler);
      }
    }
  };

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }
  var Observer = {
    attach: function attach(target, options) {
      if (options === void 0) {
        options = {};
      }

      var window = getWindow();
      var swiper = this;
      var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
      var observer = new ObserverFunc(function (mutations) {
        // The observerUpdate event should only be triggered
        // once despite the number of mutations.  Additional
        // triggers are redundant and are very costly
        if (mutations.length === 1) {
          swiper.emit('observerUpdate', mutations[0]);
          return;
        }

        var observerUpdate = function observerUpdate() {
          swiper.emit('observerUpdate', mutations[0]);
        };

        if (window.requestAnimationFrame) {
          window.requestAnimationFrame(observerUpdate);
        } else {
          window.setTimeout(observerUpdate, 0);
        }
      });
      observer.observe(target, {
        attributes: typeof options.attributes === 'undefined' ? true : options.attributes,
        childList: typeof options.childList === 'undefined' ? true : options.childList,
        characterData: typeof options.characterData === 'undefined' ? true : options.characterData
      });
      swiper.observer.observers.push(observer);
    },
    init: function init() {
      var swiper = this;
      if (!swiper.support.observer || !swiper.params.observer) return;

      if (swiper.params.observeParents) {
        var containerParents = swiper.$el.parents();

        for (var i = 0; i < containerParents.length; i += 1) {
          swiper.observer.attach(containerParents[i]);
        }
      } // Observe container


      swiper.observer.attach(swiper.$el[0], {
        childList: swiper.params.observeSlideChildren
      }); // Observe wrapper

      swiper.observer.attach(swiper.$wrapperEl[0], {
        attributes: false
      });
    },
    destroy: function destroy() {
      var swiper = this;
      swiper.observer.observers.forEach(function (observer) {
        observer.disconnect();
      });
      swiper.observer.observers = [];
    }
  };
  var Observer$1 = {
    name: 'observer',
    params: {
      observer: false,
      observeParents: false,
      observeSlideChildren: false
    },
    create: function create() {
      var swiper = this;
      bindModuleMethods(swiper, {
        observer: _extends({}, Observer, {
          observers: []
        })
      });
    },
    on: {
      init: function init(swiper) {
        swiper.observer.init();
      },
      destroy: function destroy(swiper) {
        swiper.observer.destroy();
      }
    }
  };

  var modular = {
    useParams: function useParams(instanceParams) {
      var instance = this;
      if (!instance.modules) return;
      Object.keys(instance.modules).forEach(function (moduleName) {
        var module = instance.modules[moduleName]; // Extend params

        if (module.params) {
          extend$1(instanceParams, module.params);
        }
      });
    },
    useModules: function useModules(modulesParams) {
      if (modulesParams === void 0) {
        modulesParams = {};
      }

      var instance = this;
      if (!instance.modules) return;
      Object.keys(instance.modules).forEach(function (moduleName) {
        var module = instance.modules[moduleName];
        var moduleParams = modulesParams[moduleName] || {}; // Add event listeners

        if (module.on && instance.on) {
          Object.keys(module.on).forEach(function (moduleEventName) {
            instance.on(moduleEventName, module.on[moduleEventName]);
          });
        } // Module create callback


        if (module.create) {
          module.create.bind(instance)(moduleParams);
        }
      });
    }
  };

  /* eslint-disable no-underscore-dangle */
  var eventsEmitter = {
    on: function on(events, handler, priority) {
      var self = this;
      if (typeof handler !== 'function') return self;
      var method = priority ? 'unshift' : 'push';
      events.split(' ').forEach(function (event) {
        if (!self.eventsListeners[event]) self.eventsListeners[event] = [];
        self.eventsListeners[event][method](handler);
      });
      return self;
    },
    once: function once(events, handler, priority) {
      var self = this;
      if (typeof handler !== 'function') return self;

      function onceHandler() {
        self.off(events, onceHandler);

        if (onceHandler.__emitterProxy) {
          delete onceHandler.__emitterProxy;
        }

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        handler.apply(self, args);
      }

      onceHandler.__emitterProxy = handler;
      return self.on(events, onceHandler, priority);
    },
    onAny: function onAny(handler, priority) {
      var self = this;
      if (typeof handler !== 'function') return self;
      var method = priority ? 'unshift' : 'push';

      if (self.eventsAnyListeners.indexOf(handler) < 0) {
        self.eventsAnyListeners[method](handler);
      }

      return self;
    },
    offAny: function offAny(handler) {
      var self = this;
      if (!self.eventsAnyListeners) return self;
      var index = self.eventsAnyListeners.indexOf(handler);

      if (index >= 0) {
        self.eventsAnyListeners.splice(index, 1);
      }

      return self;
    },
    off: function off(events, handler) {
      var self = this;
      if (!self.eventsListeners) return self;
      events.split(' ').forEach(function (event) {
        if (typeof handler === 'undefined') {
          self.eventsListeners[event] = [];
        } else if (self.eventsListeners[event]) {
          self.eventsListeners[event].forEach(function (eventHandler, index) {
            if (eventHandler === handler || eventHandler.__emitterProxy && eventHandler.__emitterProxy === handler) {
              self.eventsListeners[event].splice(index, 1);
            }
          });
        }
      });
      return self;
    },
    emit: function emit() {
      var self = this;
      if (!self.eventsListeners) return self;
      var events;
      var data;
      var context;

      for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      if (typeof args[0] === 'string' || Array.isArray(args[0])) {
        events = args[0];
        data = args.slice(1, args.length);
        context = self;
      } else {
        events = args[0].events;
        data = args[0].data;
        context = args[0].context || self;
      }

      data.unshift(context);
      var eventsArray = Array.isArray(events) ? events : events.split(' ');
      eventsArray.forEach(function (event) {
        if (self.eventsAnyListeners && self.eventsAnyListeners.length) {
          self.eventsAnyListeners.forEach(function (eventHandler) {
            eventHandler.apply(context, [event].concat(data));
          });
        }

        if (self.eventsListeners && self.eventsListeners[event]) {
          self.eventsListeners[event].forEach(function (eventHandler) {
            eventHandler.apply(context, data);
          });
        }
      });
      return self;
    }
  };

  function updateSize() {
    var swiper = this;
    var width;
    var height;
    var $el = swiper.$el;

    if (typeof swiper.params.width !== 'undefined' && swiper.params.width !== null) {
      width = swiper.params.width;
    } else {
      width = $el[0].clientWidth;
    }

    if (typeof swiper.params.height !== 'undefined' && swiper.params.height !== null) {
      height = swiper.params.height;
    } else {
      height = $el[0].clientHeight;
    }

    if (width === 0 && swiper.isHorizontal() || height === 0 && swiper.isVertical()) {
      return;
    } // Subtract paddings


    width = width - parseInt($el.css('padding-left') || 0, 10) - parseInt($el.css('padding-right') || 0, 10);
    height = height - parseInt($el.css('padding-top') || 0, 10) - parseInt($el.css('padding-bottom') || 0, 10);
    if (Number.isNaN(width)) width = 0;
    if (Number.isNaN(height)) height = 0;
    extend$1(swiper, {
      width: width,
      height: height,
      size: swiper.isHorizontal() ? width : height
    });
  }

  function updateSlides() {
    var swiper = this;
    var window = getWindow();
    var params = swiper.params;
    var $wrapperEl = swiper.$wrapperEl,
        swiperSize = swiper.size,
        rtl = swiper.rtlTranslate,
        wrongRTL = swiper.wrongRTL;
    var isVirtual = swiper.virtual && params.virtual.enabled;
    var previousSlidesLength = isVirtual ? swiper.virtual.slides.length : swiper.slides.length;
    var slides = $wrapperEl.children("." + swiper.params.slideClass);
    var slidesLength = isVirtual ? swiper.virtual.slides.length : slides.length;
    var snapGrid = [];
    var slidesGrid = [];
    var slidesSizesGrid = [];

    function slidesForMargin(slideEl, slideIndex) {
      if (!params.cssMode) return true;

      if (slideIndex === slides.length - 1) {
        return false;
      }

      return true;
    }

    var offsetBefore = params.slidesOffsetBefore;

    if (typeof offsetBefore === 'function') {
      offsetBefore = params.slidesOffsetBefore.call(swiper);
    }

    var offsetAfter = params.slidesOffsetAfter;

    if (typeof offsetAfter === 'function') {
      offsetAfter = params.slidesOffsetAfter.call(swiper);
    }

    var previousSnapGridLength = swiper.snapGrid.length;
    var previousSlidesGridLength = swiper.slidesGrid.length;
    var spaceBetween = params.spaceBetween;
    var slidePosition = -offsetBefore;
    var prevSlideSize = 0;
    var index = 0;

    if (typeof swiperSize === 'undefined') {
      return;
    }

    if (typeof spaceBetween === 'string' && spaceBetween.indexOf('%') >= 0) {
      spaceBetween = parseFloat(spaceBetween.replace('%', '')) / 100 * swiperSize;
    }

    swiper.virtualSize = -spaceBetween; // reset margins

    if (rtl) slides.css({
      marginLeft: '',
      marginTop: ''
    });else slides.css({
      marginRight: '',
      marginBottom: ''
    });
    var slidesNumberEvenToRows;

    if (params.slidesPerColumn > 1) {
      if (Math.floor(slidesLength / params.slidesPerColumn) === slidesLength / swiper.params.slidesPerColumn) {
        slidesNumberEvenToRows = slidesLength;
      } else {
        slidesNumberEvenToRows = Math.ceil(slidesLength / params.slidesPerColumn) * params.slidesPerColumn;
      }

      if (params.slidesPerView !== 'auto' && params.slidesPerColumnFill === 'row') {
        slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, params.slidesPerView * params.slidesPerColumn);
      }
    } // Calc slides


    var slideSize;
    var slidesPerColumn = params.slidesPerColumn;
    var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
    var numFullColumns = Math.floor(slidesLength / params.slidesPerColumn);

    for (var i = 0; i < slidesLength; i += 1) {
      slideSize = 0;
      var slide = slides.eq(i);

      if (params.slidesPerColumn > 1) {
        // Set slides order
        var newSlideOrderIndex = void 0;
        var column = void 0;
        var row = void 0;

        if (params.slidesPerColumnFill === 'row' && params.slidesPerGroup > 1) {
          var groupIndex = Math.floor(i / (params.slidesPerGroup * params.slidesPerColumn));
          var slideIndexInGroup = i - params.slidesPerColumn * params.slidesPerGroup * groupIndex;
          var columnsInGroup = groupIndex === 0 ? params.slidesPerGroup : Math.min(Math.ceil((slidesLength - groupIndex * slidesPerColumn * params.slidesPerGroup) / slidesPerColumn), params.slidesPerGroup);
          row = Math.floor(slideIndexInGroup / columnsInGroup);
          column = slideIndexInGroup - row * columnsInGroup + groupIndex * params.slidesPerGroup;
          newSlideOrderIndex = column + row * slidesNumberEvenToRows / slidesPerColumn;
          slide.css({
            '-webkit-box-ordinal-group': newSlideOrderIndex,
            '-moz-box-ordinal-group': newSlideOrderIndex,
            '-ms-flex-order': newSlideOrderIndex,
            '-webkit-order': newSlideOrderIndex,
            order: newSlideOrderIndex
          });
        } else if (params.slidesPerColumnFill === 'column') {
          column = Math.floor(i / slidesPerColumn);
          row = i - column * slidesPerColumn;

          if (column > numFullColumns || column === numFullColumns && row === slidesPerColumn - 1) {
            row += 1;

            if (row >= slidesPerColumn) {
              row = 0;
              column += 1;
            }
          }
        } else {
          row = Math.floor(i / slidesPerRow);
          column = i - row * slidesPerRow;
        }

        slide.css("margin-" + (swiper.isHorizontal() ? 'top' : 'left'), row !== 0 && params.spaceBetween && params.spaceBetween + "px");
      }

      if (slide.css('display') === 'none') continue; // eslint-disable-line

      if (params.slidesPerView === 'auto') {
        var slideStyles = window.getComputedStyle(slide[0], null);
        var currentTransform = slide[0].style.transform;
        var currentWebKitTransform = slide[0].style.webkitTransform;

        if (currentTransform) {
          slide[0].style.transform = 'none';
        }

        if (currentWebKitTransform) {
          slide[0].style.webkitTransform = 'none';
        }

        if (params.roundLengths) {
          slideSize = swiper.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true);
        } else {
          // eslint-disable-next-line
          if (swiper.isHorizontal()) {
            var width = parseFloat(slideStyles.getPropertyValue('width') || 0);
            var paddingLeft = parseFloat(slideStyles.getPropertyValue('padding-left') || 0);
            var paddingRight = parseFloat(slideStyles.getPropertyValue('padding-right') || 0);
            var marginLeft = parseFloat(slideStyles.getPropertyValue('margin-left') || 0);
            var marginRight = parseFloat(slideStyles.getPropertyValue('margin-right') || 0);
            var boxSizing = slideStyles.getPropertyValue('box-sizing');

            if (boxSizing && boxSizing === 'border-box') {
              slideSize = width + marginLeft + marginRight;
            } else {
              var _slide$ = slide[0],
                  clientWidth = _slide$.clientWidth,
                  offsetWidth = _slide$.offsetWidth;
              slideSize = width + paddingLeft + paddingRight + marginLeft + marginRight + (offsetWidth - clientWidth);
            }
          } else {
            var height = parseFloat(slideStyles.getPropertyValue('height') || 0);
            var paddingTop = parseFloat(slideStyles.getPropertyValue('padding-top') || 0);
            var paddingBottom = parseFloat(slideStyles.getPropertyValue('padding-bottom') || 0);
            var marginTop = parseFloat(slideStyles.getPropertyValue('margin-top') || 0);
            var marginBottom = parseFloat(slideStyles.getPropertyValue('margin-bottom') || 0);

            var _boxSizing = slideStyles.getPropertyValue('box-sizing');

            if (_boxSizing && _boxSizing === 'border-box') {
              slideSize = height + marginTop + marginBottom;
            } else {
              var _slide$2 = slide[0],
                  clientHeight = _slide$2.clientHeight,
                  offsetHeight = _slide$2.offsetHeight;
              slideSize = height + paddingTop + paddingBottom + marginTop + marginBottom + (offsetHeight - clientHeight);
            }
          }
        }

        if (currentTransform) {
          slide[0].style.transform = currentTransform;
        }

        if (currentWebKitTransform) {
          slide[0].style.webkitTransform = currentWebKitTransform;
        }

        if (params.roundLengths) slideSize = Math.floor(slideSize);
      } else {
        slideSize = (swiperSize - (params.slidesPerView - 1) * spaceBetween) / params.slidesPerView;
        if (params.roundLengths) slideSize = Math.floor(slideSize);

        if (slides[i]) {
          if (swiper.isHorizontal()) {
            slides[i].style.width = slideSize + "px";
          } else {
            slides[i].style.height = slideSize + "px";
          }
        }
      }

      if (slides[i]) {
        slides[i].swiperSlideSize = slideSize;
      }

      slidesSizesGrid.push(slideSize);

      if (params.centeredSlides) {
        slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
        if (prevSlideSize === 0 && i !== 0) slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
        if (i === 0) slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
        if (Math.abs(slidePosition) < 1 / 1000) slidePosition = 0;
        if (params.roundLengths) slidePosition = Math.floor(slidePosition);
        if (index % params.slidesPerGroup === 0) snapGrid.push(slidePosition);
        slidesGrid.push(slidePosition);
      } else {
        if (params.roundLengths) slidePosition = Math.floor(slidePosition);
        if ((index - Math.min(swiper.params.slidesPerGroupSkip, index)) % swiper.params.slidesPerGroup === 0) snapGrid.push(slidePosition);
        slidesGrid.push(slidePosition);
        slidePosition = slidePosition + slideSize + spaceBetween;
      }

      swiper.virtualSize += slideSize + spaceBetween;
      prevSlideSize = slideSize;
      index += 1;
    }

    swiper.virtualSize = Math.max(swiper.virtualSize, swiperSize) + offsetAfter;
    var newSlidesGrid;

    if (rtl && wrongRTL && (params.effect === 'slide' || params.effect === 'coverflow')) {
      $wrapperEl.css({
        width: swiper.virtualSize + params.spaceBetween + "px"
      });
    }

    if (params.setWrapperSize) {
      if (swiper.isHorizontal()) $wrapperEl.css({
        width: swiper.virtualSize + params.spaceBetween + "px"
      });else $wrapperEl.css({
        height: swiper.virtualSize + params.spaceBetween + "px"
      });
    }

    if (params.slidesPerColumn > 1) {
      swiper.virtualSize = (slideSize + params.spaceBetween) * slidesNumberEvenToRows;
      swiper.virtualSize = Math.ceil(swiper.virtualSize / params.slidesPerColumn) - params.spaceBetween;
      if (swiper.isHorizontal()) $wrapperEl.css({
        width: swiper.virtualSize + params.spaceBetween + "px"
      });else $wrapperEl.css({
        height: swiper.virtualSize + params.spaceBetween + "px"
      });

      if (params.centeredSlides) {
        newSlidesGrid = [];

        for (var _i = 0; _i < snapGrid.length; _i += 1) {
          var slidesGridItem = snapGrid[_i];
          if (params.roundLengths) slidesGridItem = Math.floor(slidesGridItem);
          if (snapGrid[_i] < swiper.virtualSize + snapGrid[0]) newSlidesGrid.push(slidesGridItem);
        }

        snapGrid = newSlidesGrid;
      }
    } // Remove last grid elements depending on width


    if (!params.centeredSlides) {
      newSlidesGrid = [];

      for (var _i2 = 0; _i2 < snapGrid.length; _i2 += 1) {
        var _slidesGridItem = snapGrid[_i2];
        if (params.roundLengths) _slidesGridItem = Math.floor(_slidesGridItem);

        if (snapGrid[_i2] <= swiper.virtualSize - swiperSize) {
          newSlidesGrid.push(_slidesGridItem);
        }
      }

      snapGrid = newSlidesGrid;

      if (Math.floor(swiper.virtualSize - swiperSize) - Math.floor(snapGrid[snapGrid.length - 1]) > 1) {
        snapGrid.push(swiper.virtualSize - swiperSize);
      }
    }

    if (snapGrid.length === 0) snapGrid = [0];

    if (params.spaceBetween !== 0) {
      if (swiper.isHorizontal()) {
        if (rtl) slides.filter(slidesForMargin).css({
          marginLeft: spaceBetween + "px"
        });else slides.filter(slidesForMargin).css({
          marginRight: spaceBetween + "px"
        });
      } else slides.filter(slidesForMargin).css({
        marginBottom: spaceBetween + "px"
      });
    }

    if (params.centeredSlides && params.centeredSlidesBounds) {
      var allSlidesSize = 0;
      slidesSizesGrid.forEach(function (slideSizeValue) {
        allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
      });
      allSlidesSize -= params.spaceBetween;
      var maxSnap = allSlidesSize - swiperSize;
      snapGrid = snapGrid.map(function (snap) {
        if (snap < 0) return -offsetBefore;
        if (snap > maxSnap) return maxSnap + offsetAfter;
        return snap;
      });
    }

    if (params.centerInsufficientSlides) {
      var _allSlidesSize = 0;
      slidesSizesGrid.forEach(function (slideSizeValue) {
        _allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
      });
      _allSlidesSize -= params.spaceBetween;

      if (_allSlidesSize < swiperSize) {
        var allSlidesOffset = (swiperSize - _allSlidesSize) / 2;
        snapGrid.forEach(function (snap, snapIndex) {
          snapGrid[snapIndex] = snap - allSlidesOffset;
        });
        slidesGrid.forEach(function (snap, snapIndex) {
          slidesGrid[snapIndex] = snap + allSlidesOffset;
        });
      }
    }

    extend$1(swiper, {
      slides: slides,
      snapGrid: snapGrid,
      slidesGrid: slidesGrid,
      slidesSizesGrid: slidesSizesGrid
    });

    if (slidesLength !== previousSlidesLength) {
      swiper.emit('slidesLengthChange');
    }

    if (snapGrid.length !== previousSnapGridLength) {
      if (swiper.params.watchOverflow) swiper.checkOverflow();
      swiper.emit('snapGridLengthChange');
    }

    if (slidesGrid.length !== previousSlidesGridLength) {
      swiper.emit('slidesGridLengthChange');
    }

    if (params.watchSlidesProgress || params.watchSlidesVisibility) {
      swiper.updateSlidesOffset();
    }
  }

  function updateAutoHeight(speed) {
    var swiper = this;
    var activeSlides = [];
    var newHeight = 0;
    var i;

    if (typeof speed === 'number') {
      swiper.setTransition(speed);
    } else if (speed === true) {
      swiper.setTransition(swiper.params.speed);
    } // Find slides currently in view


    if (swiper.params.slidesPerView !== 'auto' && swiper.params.slidesPerView > 1) {
      if (swiper.params.centeredSlides) {
        swiper.visibleSlides.each(function (slide) {
          activeSlides.push(slide);
        });
      } else {
        for (i = 0; i < Math.ceil(swiper.params.slidesPerView); i += 1) {
          var index = swiper.activeIndex + i;
          if (index > swiper.slides.length) break;
          activeSlides.push(swiper.slides.eq(index)[0]);
        }
      }
    } else {
      activeSlides.push(swiper.slides.eq(swiper.activeIndex)[0]);
    } // Find new height from highest slide in view


    for (i = 0; i < activeSlides.length; i += 1) {
      if (typeof activeSlides[i] !== 'undefined') {
        var height = activeSlides[i].offsetHeight;
        newHeight = height > newHeight ? height : newHeight;
      }
    } // Update Height


    if (newHeight) swiper.$wrapperEl.css('height', newHeight + "px");
  }

  function updateSlidesOffset() {
    var swiper = this;
    var slides = swiper.slides;

    for (var i = 0; i < slides.length; i += 1) {
      slides[i].swiperSlideOffset = swiper.isHorizontal() ? slides[i].offsetLeft : slides[i].offsetTop;
    }
  }

  function updateSlidesProgress(translate) {
    if (translate === void 0) {
      translate = this && this.translate || 0;
    }

    var swiper = this;
    var params = swiper.params;
    var slides = swiper.slides,
        rtl = swiper.rtlTranslate;
    if (slides.length === 0) return;
    if (typeof slides[0].swiperSlideOffset === 'undefined') swiper.updateSlidesOffset();
    var offsetCenter = -translate;
    if (rtl) offsetCenter = translate; // Visible Slides

    slides.removeClass(params.slideVisibleClass);
    swiper.visibleSlidesIndexes = [];
    swiper.visibleSlides = [];

    for (var i = 0; i < slides.length; i += 1) {
      var slide = slides[i];
      var slideProgress = (offsetCenter + (params.centeredSlides ? swiper.minTranslate() : 0) - slide.swiperSlideOffset) / (slide.swiperSlideSize + params.spaceBetween);

      if (params.watchSlidesVisibility || params.centeredSlides && params.autoHeight) {
        var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
        var slideAfter = slideBefore + swiper.slidesSizesGrid[i];
        var isVisible = slideBefore >= 0 && slideBefore < swiper.size - 1 || slideAfter > 1 && slideAfter <= swiper.size || slideBefore <= 0 && slideAfter >= swiper.size;

        if (isVisible) {
          swiper.visibleSlides.push(slide);
          swiper.visibleSlidesIndexes.push(i);
          slides.eq(i).addClass(params.slideVisibleClass);
        }
      }

      slide.progress = rtl ? -slideProgress : slideProgress;
    }

    swiper.visibleSlides = $(swiper.visibleSlides);
  }

  function updateProgress(translate) {
    var swiper = this;

    if (typeof translate === 'undefined') {
      var multiplier = swiper.rtlTranslate ? -1 : 1; // eslint-disable-next-line

      translate = swiper && swiper.translate && swiper.translate * multiplier || 0;
    }

    var params = swiper.params;
    var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
    var progress = swiper.progress,
        isBeginning = swiper.isBeginning,
        isEnd = swiper.isEnd;
    var wasBeginning = isBeginning;
    var wasEnd = isEnd;

    if (translatesDiff === 0) {
      progress = 0;
      isBeginning = true;
      isEnd = true;
    } else {
      progress = (translate - swiper.minTranslate()) / translatesDiff;
      isBeginning = progress <= 0;
      isEnd = progress >= 1;
    }

    extend$1(swiper, {
      progress: progress,
      isBeginning: isBeginning,
      isEnd: isEnd
    });
    if (params.watchSlidesProgress || params.watchSlidesVisibility || params.centeredSlides && params.autoHeight) swiper.updateSlidesProgress(translate);

    if (isBeginning && !wasBeginning) {
      swiper.emit('reachBeginning toEdge');
    }

    if (isEnd && !wasEnd) {
      swiper.emit('reachEnd toEdge');
    }

    if (wasBeginning && !isBeginning || wasEnd && !isEnd) {
      swiper.emit('fromEdge');
    }

    swiper.emit('progress', progress);
  }

  function updateSlidesClasses() {
    var swiper = this;
    var slides = swiper.slides,
        params = swiper.params,
        $wrapperEl = swiper.$wrapperEl,
        activeIndex = swiper.activeIndex,
        realIndex = swiper.realIndex;
    var isVirtual = swiper.virtual && params.virtual.enabled;
    slides.removeClass(params.slideActiveClass + " " + params.slideNextClass + " " + params.slidePrevClass + " " + params.slideDuplicateActiveClass + " " + params.slideDuplicateNextClass + " " + params.slideDuplicatePrevClass);
    var activeSlide;

    if (isVirtual) {
      activeSlide = swiper.$wrapperEl.find("." + params.slideClass + "[data-swiper-slide-index=\"" + activeIndex + "\"]");
    } else {
      activeSlide = slides.eq(activeIndex);
    } // Active classes


    activeSlide.addClass(params.slideActiveClass);

    if (params.loop) {
      // Duplicate to all looped slides
      if (activeSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ")[data-swiper-slide-index=\"" + realIndex + "\"]").addClass(params.slideDuplicateActiveClass);
      } else {
        $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + "[data-swiper-slide-index=\"" + realIndex + "\"]").addClass(params.slideDuplicateActiveClass);
      }
    } // Next Slide


    var nextSlide = activeSlide.nextAll("." + params.slideClass).eq(0).addClass(params.slideNextClass);

    if (params.loop && nextSlide.length === 0) {
      nextSlide = slides.eq(0);
      nextSlide.addClass(params.slideNextClass);
    } // Prev Slide


    var prevSlide = activeSlide.prevAll("." + params.slideClass).eq(0).addClass(params.slidePrevClass);

    if (params.loop && prevSlide.length === 0) {
      prevSlide = slides.eq(-1);
      prevSlide.addClass(params.slidePrevClass);
    }

    if (params.loop) {
      // Duplicate to all looped slides
      if (nextSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ")[data-swiper-slide-index=\"" + nextSlide.attr('data-swiper-slide-index') + "\"]").addClass(params.slideDuplicateNextClass);
      } else {
        $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + "[data-swiper-slide-index=\"" + nextSlide.attr('data-swiper-slide-index') + "\"]").addClass(params.slideDuplicateNextClass);
      }

      if (prevSlide.hasClass(params.slideDuplicateClass)) {
        $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ")[data-swiper-slide-index=\"" + prevSlide.attr('data-swiper-slide-index') + "\"]").addClass(params.slideDuplicatePrevClass);
      } else {
        $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + "[data-swiper-slide-index=\"" + prevSlide.attr('data-swiper-slide-index') + "\"]").addClass(params.slideDuplicatePrevClass);
      }
    }

    swiper.emitSlidesClasses();
  }

  function updateActiveIndex(newActiveIndex) {
    var swiper = this;
    var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
    var slidesGrid = swiper.slidesGrid,
        snapGrid = swiper.snapGrid,
        params = swiper.params,
        previousIndex = swiper.activeIndex,
        previousRealIndex = swiper.realIndex,
        previousSnapIndex = swiper.snapIndex;
    var activeIndex = newActiveIndex;
    var snapIndex;

    if (typeof activeIndex === 'undefined') {
      for (var i = 0; i < slidesGrid.length; i += 1) {
        if (typeof slidesGrid[i + 1] !== 'undefined') {
          if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1] - (slidesGrid[i + 1] - slidesGrid[i]) / 2) {
            activeIndex = i;
          } else if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1]) {
            activeIndex = i + 1;
          }
        } else if (translate >= slidesGrid[i]) {
          activeIndex = i;
        }
      } // Normalize slideIndex


      if (params.normalizeSlideIndex) {
        if (activeIndex < 0 || typeof activeIndex === 'undefined') activeIndex = 0;
      }
    }

    if (snapGrid.indexOf(translate) >= 0) {
      snapIndex = snapGrid.indexOf(translate);
    } else {
      var skip = Math.min(params.slidesPerGroupSkip, activeIndex);
      snapIndex = skip + Math.floor((activeIndex - skip) / params.slidesPerGroup);
    }

    if (snapIndex >= snapGrid.length) snapIndex = snapGrid.length - 1;

    if (activeIndex === previousIndex) {
      if (snapIndex !== previousSnapIndex) {
        swiper.snapIndex = snapIndex;
        swiper.emit('snapIndexChange');
      }

      return;
    } // Get real index


    var realIndex = parseInt(swiper.slides.eq(activeIndex).attr('data-swiper-slide-index') || activeIndex, 10);
    extend$1(swiper, {
      snapIndex: snapIndex,
      realIndex: realIndex,
      previousIndex: previousIndex,
      activeIndex: activeIndex
    });
    swiper.emit('activeIndexChange');
    swiper.emit('snapIndexChange');

    if (previousRealIndex !== realIndex) {
      swiper.emit('realIndexChange');
    }

    if (swiper.initialized || swiper.params.runCallbacksOnInit) {
      swiper.emit('slideChange');
    }
  }

  function updateClickedSlide(e) {
    var swiper = this;
    var params = swiper.params;
    var slide = $(e.target).closest("." + params.slideClass)[0];
    var slideFound = false;

    if (slide) {
      for (var i = 0; i < swiper.slides.length; i += 1) {
        if (swiper.slides[i] === slide) slideFound = true;
      }
    }

    if (slide && slideFound) {
      swiper.clickedSlide = slide;

      if (swiper.virtual && swiper.params.virtual.enabled) {
        swiper.clickedIndex = parseInt($(slide).attr('data-swiper-slide-index'), 10);
      } else {
        swiper.clickedIndex = $(slide).index();
      }
    } else {
      swiper.clickedSlide = undefined;
      swiper.clickedIndex = undefined;
      return;
    }

    if (params.slideToClickedSlide && swiper.clickedIndex !== undefined && swiper.clickedIndex !== swiper.activeIndex) {
      swiper.slideToClickedSlide();
    }
  }

  var update = {
    updateSize: updateSize,
    updateSlides: updateSlides,
    updateAutoHeight: updateAutoHeight,
    updateSlidesOffset: updateSlidesOffset,
    updateSlidesProgress: updateSlidesProgress,
    updateProgress: updateProgress,
    updateSlidesClasses: updateSlidesClasses,
    updateActiveIndex: updateActiveIndex,
    updateClickedSlide: updateClickedSlide
  };

  function getSwiperTranslate(axis) {
    if (axis === void 0) {
      axis = this.isHorizontal() ? 'x' : 'y';
    }

    var swiper = this;
    var params = swiper.params,
        rtl = swiper.rtlTranslate,
        translate = swiper.translate,
        $wrapperEl = swiper.$wrapperEl;

    if (params.virtualTranslate) {
      return rtl ? -translate : translate;
    }

    if (params.cssMode) {
      return translate;
    }

    var currentTranslate = getTranslate($wrapperEl[0], axis);
    if (rtl) currentTranslate = -currentTranslate;
    return currentTranslate || 0;
  }

  function setTranslate(translate, byController) {
    var swiper = this;
    var rtl = swiper.rtlTranslate,
        params = swiper.params,
        $wrapperEl = swiper.$wrapperEl,
        wrapperEl = swiper.wrapperEl,
        progress = swiper.progress;
    var x = 0;
    var y = 0;
    var z = 0;

    if (swiper.isHorizontal()) {
      x = rtl ? -translate : translate;
    } else {
      y = translate;
    }

    if (params.roundLengths) {
      x = Math.floor(x);
      y = Math.floor(y);
    }

    if (params.cssMode) {
      wrapperEl[swiper.isHorizontal() ? 'scrollLeft' : 'scrollTop'] = swiper.isHorizontal() ? -x : -y;
    } else if (!params.virtualTranslate) {
      $wrapperEl.transform("translate3d(" + x + "px, " + y + "px, " + z + "px)");
    }

    swiper.previousTranslate = swiper.translate;
    swiper.translate = swiper.isHorizontal() ? x : y; // Check if we need to update progress

    var newProgress;
    var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();

    if (translatesDiff === 0) {
      newProgress = 0;
    } else {
      newProgress = (translate - swiper.minTranslate()) / translatesDiff;
    }

    if (newProgress !== progress) {
      swiper.updateProgress(translate);
    }

    swiper.emit('setTranslate', swiper.translate, byController);
  }

  function minTranslate() {
    return -this.snapGrid[0];
  }

  function maxTranslate() {
    return -this.snapGrid[this.snapGrid.length - 1];
  }

  function translateTo(translate, speed, runCallbacks, translateBounds, internal) {
    if (translate === void 0) {
      translate = 0;
    }

    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    if (translateBounds === void 0) {
      translateBounds = true;
    }

    var swiper = this;
    var params = swiper.params,
        wrapperEl = swiper.wrapperEl;

    if (swiper.animating && params.preventInteractionOnTransition) {
      return false;
    }

    var minTranslate = swiper.minTranslate();
    var maxTranslate = swiper.maxTranslate();
    var newTranslate;
    if (translateBounds && translate > minTranslate) newTranslate = minTranslate;else if (translateBounds && translate < maxTranslate) newTranslate = maxTranslate;else newTranslate = translate; // Update progress

    swiper.updateProgress(newTranslate);

    if (params.cssMode) {
      var isH = swiper.isHorizontal();

      if (speed === 0) {
        wrapperEl[isH ? 'scrollLeft' : 'scrollTop'] = -newTranslate;
      } else {
        // eslint-disable-next-line
        if (wrapperEl.scrollTo) {
          var _wrapperEl$scrollTo;

          wrapperEl.scrollTo((_wrapperEl$scrollTo = {}, _wrapperEl$scrollTo[isH ? 'left' : 'top'] = -newTranslate, _wrapperEl$scrollTo.behavior = 'smooth', _wrapperEl$scrollTo));
        } else {
          wrapperEl[isH ? 'scrollLeft' : 'scrollTop'] = -newTranslate;
        }
      }

      return true;
    }

    if (speed === 0) {
      swiper.setTransition(0);
      swiper.setTranslate(newTranslate);

      if (runCallbacks) {
        swiper.emit('beforeTransitionStart', speed, internal);
        swiper.emit('transitionEnd');
      }
    } else {
      swiper.setTransition(speed);
      swiper.setTranslate(newTranslate);

      if (runCallbacks) {
        swiper.emit('beforeTransitionStart', speed, internal);
        swiper.emit('transitionStart');
      }

      if (!swiper.animating) {
        swiper.animating = true;

        if (!swiper.onTranslateToWrapperTransitionEnd) {
          swiper.onTranslateToWrapperTransitionEnd = function transitionEnd(e) {
            if (!swiper || swiper.destroyed) return;
            if (e.target !== this) return;
            swiper.$wrapperEl[0].removeEventListener('transitionend', swiper.onTranslateToWrapperTransitionEnd);
            swiper.$wrapperEl[0].removeEventListener('webkitTransitionEnd', swiper.onTranslateToWrapperTransitionEnd);
            swiper.onTranslateToWrapperTransitionEnd = null;
            delete swiper.onTranslateToWrapperTransitionEnd;

            if (runCallbacks) {
              swiper.emit('transitionEnd');
            }
          };
        }

        swiper.$wrapperEl[0].addEventListener('transitionend', swiper.onTranslateToWrapperTransitionEnd);
        swiper.$wrapperEl[0].addEventListener('webkitTransitionEnd', swiper.onTranslateToWrapperTransitionEnd);
      }
    }

    return true;
  }

  var translate = {
    getTranslate: getSwiperTranslate,
    setTranslate: setTranslate,
    minTranslate: minTranslate,
    maxTranslate: maxTranslate,
    translateTo: translateTo
  };

  function setTransition(duration, byController) {
    var swiper = this;

    if (!swiper.params.cssMode) {
      swiper.$wrapperEl.transition(duration);
    }

    swiper.emit('setTransition', duration, byController);
  }

  function transitionStart(runCallbacks, direction) {
    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    var activeIndex = swiper.activeIndex,
        params = swiper.params,
        previousIndex = swiper.previousIndex;
    if (params.cssMode) return;

    if (params.autoHeight) {
      swiper.updateAutoHeight();
    }

    var dir = direction;

    if (!dir) {
      if (activeIndex > previousIndex) dir = 'next';else if (activeIndex < previousIndex) dir = 'prev';else dir = 'reset';
    }

    swiper.emit('transitionStart');

    if (runCallbacks && activeIndex !== previousIndex) {
      if (dir === 'reset') {
        swiper.emit('slideResetTransitionStart');
        return;
      }

      swiper.emit('slideChangeTransitionStart');

      if (dir === 'next') {
        swiper.emit('slideNextTransitionStart');
      } else {
        swiper.emit('slidePrevTransitionStart');
      }
    }
  }

  function transitionEnd$1(runCallbacks, direction) {
    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    var activeIndex = swiper.activeIndex,
        previousIndex = swiper.previousIndex,
        params = swiper.params;
    swiper.animating = false;
    if (params.cssMode) return;
    swiper.setTransition(0);
    var dir = direction;

    if (!dir) {
      if (activeIndex > previousIndex) dir = 'next';else if (activeIndex < previousIndex) dir = 'prev';else dir = 'reset';
    }

    swiper.emit('transitionEnd');

    if (runCallbacks && activeIndex !== previousIndex) {
      if (dir === 'reset') {
        swiper.emit('slideResetTransitionEnd');
        return;
      }

      swiper.emit('slideChangeTransitionEnd');

      if (dir === 'next') {
        swiper.emit('slideNextTransitionEnd');
      } else {
        swiper.emit('slidePrevTransitionEnd');
      }
    }
  }

  var transition$1 = {
    setTransition: setTransition,
    transitionStart: transitionStart,
    transitionEnd: transitionEnd$1
  };

  function slideTo(index, speed, runCallbacks, internal) {
    if (index === void 0) {
      index = 0;
    }

    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    if (typeof index !== 'number' && typeof index !== 'string') {
      throw new Error("The 'index' argument cannot have type other than 'number' or 'string'. [" + _typeof(index) + "] given.");
    }

    if (typeof index === 'string') {
      /**
       * The `index` argument converted from `string` to `number`.
       * @type {number}
       */
      var indexAsNumber = parseInt(index, 10);
      /**
       * Determines whether the `index` argument is a valid `number`
       * after being converted from the `string` type.
       * @type {boolean}
       */

      var isValidNumber = isFinite(indexAsNumber);

      if (!isValidNumber) {
        throw new Error("The passed-in 'index' (string) couldn't be converted to 'number'. [" + index + "] given.");
      } // Knowing that the converted `index` is a valid number,
      // we can update the original argument's value.


      index = indexAsNumber;
    }

    var swiper = this;
    var slideIndex = index;
    if (slideIndex < 0) slideIndex = 0;
    var params = swiper.params,
        snapGrid = swiper.snapGrid,
        slidesGrid = swiper.slidesGrid,
        previousIndex = swiper.previousIndex,
        activeIndex = swiper.activeIndex,
        rtl = swiper.rtlTranslate,
        wrapperEl = swiper.wrapperEl;

    if (swiper.animating && params.preventInteractionOnTransition) {
      return false;
    }

    var skip = Math.min(swiper.params.slidesPerGroupSkip, slideIndex);
    var snapIndex = skip + Math.floor((slideIndex - skip) / swiper.params.slidesPerGroup);
    if (snapIndex >= snapGrid.length) snapIndex = snapGrid.length - 1;

    if ((activeIndex || params.initialSlide || 0) === (previousIndex || 0) && runCallbacks) {
      swiper.emit('beforeSlideChangeStart');
    }

    var translate = -snapGrid[snapIndex]; // Update progress

    swiper.updateProgress(translate); // Normalize slideIndex

    if (params.normalizeSlideIndex) {
      for (var i = 0; i < slidesGrid.length; i += 1) {
        if (-Math.floor(translate * 100) >= Math.floor(slidesGrid[i] * 100)) {
          slideIndex = i;
        }
      }
    } // Directions locks


    if (swiper.initialized && slideIndex !== activeIndex) {
      if (!swiper.allowSlideNext && translate < swiper.translate && translate < swiper.minTranslate()) {
        return false;
      }

      if (!swiper.allowSlidePrev && translate > swiper.translate && translate > swiper.maxTranslate()) {
        if ((activeIndex || 0) !== slideIndex) return false;
      }
    }

    var direction;
    if (slideIndex > activeIndex) direction = 'next';else if (slideIndex < activeIndex) direction = 'prev';else direction = 'reset'; // Update Index

    if (rtl && -translate === swiper.translate || !rtl && translate === swiper.translate) {
      swiper.updateActiveIndex(slideIndex); // Update Height

      if (params.autoHeight) {
        swiper.updateAutoHeight();
      }

      swiper.updateSlidesClasses();

      if (params.effect !== 'slide') {
        swiper.setTranslate(translate);
      }

      if (direction !== 'reset') {
        swiper.transitionStart(runCallbacks, direction);
        swiper.transitionEnd(runCallbacks, direction);
      }

      return false;
    }

    if (params.cssMode) {
      var isH = swiper.isHorizontal();
      var t = -translate;

      if (rtl) {
        t = wrapperEl.scrollWidth - wrapperEl.offsetWidth - t;
      }

      if (speed === 0) {
        wrapperEl[isH ? 'scrollLeft' : 'scrollTop'] = t;
      } else {
        // eslint-disable-next-line
        if (wrapperEl.scrollTo) {
          var _wrapperEl$scrollTo;

          wrapperEl.scrollTo((_wrapperEl$scrollTo = {}, _wrapperEl$scrollTo[isH ? 'left' : 'top'] = t, _wrapperEl$scrollTo.behavior = 'smooth', _wrapperEl$scrollTo));
        } else {
          wrapperEl[isH ? 'scrollLeft' : 'scrollTop'] = t;
        }
      }

      return true;
    }

    if (speed === 0) {
      swiper.setTransition(0);
      swiper.setTranslate(translate);
      swiper.updateActiveIndex(slideIndex);
      swiper.updateSlidesClasses();
      swiper.emit('beforeTransitionStart', speed, internal);
      swiper.transitionStart(runCallbacks, direction);
      swiper.transitionEnd(runCallbacks, direction);
    } else {
      swiper.setTransition(speed);
      swiper.setTranslate(translate);
      swiper.updateActiveIndex(slideIndex);
      swiper.updateSlidesClasses();
      swiper.emit('beforeTransitionStart', speed, internal);
      swiper.transitionStart(runCallbacks, direction);

      if (!swiper.animating) {
        swiper.animating = true;

        if (!swiper.onSlideToWrapperTransitionEnd) {
          swiper.onSlideToWrapperTransitionEnd = function transitionEnd(e) {
            if (!swiper || swiper.destroyed) return;
            if (e.target !== this) return;
            swiper.$wrapperEl[0].removeEventListener('transitionend', swiper.onSlideToWrapperTransitionEnd);
            swiper.$wrapperEl[0].removeEventListener('webkitTransitionEnd', swiper.onSlideToWrapperTransitionEnd);
            swiper.onSlideToWrapperTransitionEnd = null;
            delete swiper.onSlideToWrapperTransitionEnd;
            swiper.transitionEnd(runCallbacks, direction);
          };
        }

        swiper.$wrapperEl[0].addEventListener('transitionend', swiper.onSlideToWrapperTransitionEnd);
        swiper.$wrapperEl[0].addEventListener('webkitTransitionEnd', swiper.onSlideToWrapperTransitionEnd);
      }
    }

    return true;
  }

  function slideToLoop(index, speed, runCallbacks, internal) {
    if (index === void 0) {
      index = 0;
    }

    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    var newIndex = index;

    if (swiper.params.loop) {
      newIndex += swiper.loopedSlides;
    }

    return swiper.slideTo(newIndex, speed, runCallbacks, internal);
  }

  /* eslint no-unused-vars: "off" */
  function slideNext(speed, runCallbacks, internal) {
    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    var params = swiper.params,
        animating = swiper.animating;
    var increment = swiper.activeIndex < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup;

    if (params.loop) {
      if (animating && params.loopPreventsSlide) return false;
      swiper.loopFix(); // eslint-disable-next-line

      swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
    }

    return swiper.slideTo(swiper.activeIndex + increment, speed, runCallbacks, internal);
  }

  /* eslint no-unused-vars: "off" */
  function slidePrev(speed, runCallbacks, internal) {
    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    var params = swiper.params,
        animating = swiper.animating,
        snapGrid = swiper.snapGrid,
        slidesGrid = swiper.slidesGrid,
        rtlTranslate = swiper.rtlTranslate;

    if (params.loop) {
      if (animating && params.loopPreventsSlide) return false;
      swiper.loopFix(); // eslint-disable-next-line

      swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
    }

    var translate = rtlTranslate ? swiper.translate : -swiper.translate;

    function normalize(val) {
      if (val < 0) return -Math.floor(Math.abs(val));
      return Math.floor(val);
    }

    var normalizedTranslate = normalize(translate);
    var normalizedSnapGrid = snapGrid.map(function (val) {
      return normalize(val);
    });
    var currentSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate)];
    var prevSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate) - 1];

    if (typeof prevSnap === 'undefined' && params.cssMode) {
      snapGrid.forEach(function (snap) {
        if (!prevSnap && normalizedTranslate >= snap) prevSnap = snap;
      });
    }

    var prevIndex;

    if (typeof prevSnap !== 'undefined') {
      prevIndex = slidesGrid.indexOf(prevSnap);
      if (prevIndex < 0) prevIndex = swiper.activeIndex - 1;
    }

    return swiper.slideTo(prevIndex, speed, runCallbacks, internal);
  }

  /* eslint no-unused-vars: "off" */
  function slideReset(speed, runCallbacks, internal) {
    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    var swiper = this;
    return swiper.slideTo(swiper.activeIndex, speed, runCallbacks, internal);
  }

  /* eslint no-unused-vars: "off" */
  function slideToClosest(speed, runCallbacks, internal, threshold) {
    if (speed === void 0) {
      speed = this.params.speed;
    }

    if (runCallbacks === void 0) {
      runCallbacks = true;
    }

    if (threshold === void 0) {
      threshold = 0.5;
    }

    var swiper = this;
    var index = swiper.activeIndex;
    var skip = Math.min(swiper.params.slidesPerGroupSkip, index);
    var snapIndex = skip + Math.floor((index - skip) / swiper.params.slidesPerGroup);
    var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;

    if (translate >= swiper.snapGrid[snapIndex]) {
      // The current translate is on or after the current snap index, so the choice
      // is between the current index and the one after it.
      var currentSnap = swiper.snapGrid[snapIndex];
      var nextSnap = swiper.snapGrid[snapIndex + 1];

      if (translate - currentSnap > (nextSnap - currentSnap) * threshold) {
        index += swiper.params.slidesPerGroup;
      }
    } else {
      // The current translate is before the current snap index, so the choice
      // is between the current index and the one before it.
      var prevSnap = swiper.snapGrid[snapIndex - 1];
      var _currentSnap = swiper.snapGrid[snapIndex];

      if (translate - prevSnap <= (_currentSnap - prevSnap) * threshold) {
        index -= swiper.params.slidesPerGroup;
      }
    }

    index = Math.max(index, 0);
    index = Math.min(index, swiper.slidesGrid.length - 1);
    return swiper.slideTo(index, speed, runCallbacks, internal);
  }

  function slideToClickedSlide() {
    var swiper = this;
    var params = swiper.params,
        $wrapperEl = swiper.$wrapperEl;
    var slidesPerView = params.slidesPerView === 'auto' ? swiper.slidesPerViewDynamic() : params.slidesPerView;
    var slideToIndex = swiper.clickedIndex;
    var realIndex;

    if (params.loop) {
      if (swiper.animating) return;
      realIndex = parseInt($(swiper.clickedSlide).attr('data-swiper-slide-index'), 10);

      if (params.centeredSlides) {
        if (slideToIndex < swiper.loopedSlides - slidesPerView / 2 || slideToIndex > swiper.slides.length - swiper.loopedSlides + slidesPerView / 2) {
          swiper.loopFix();
          slideToIndex = $wrapperEl.children("." + params.slideClass + "[data-swiper-slide-index=\"" + realIndex + "\"]:not(." + params.slideDuplicateClass + ")").eq(0).index();
          nextTick(function () {
            swiper.slideTo(slideToIndex);
          });
        } else {
          swiper.slideTo(slideToIndex);
        }
      } else if (slideToIndex > swiper.slides.length - slidesPerView) {
        swiper.loopFix();
        slideToIndex = $wrapperEl.children("." + params.slideClass + "[data-swiper-slide-index=\"" + realIndex + "\"]:not(." + params.slideDuplicateClass + ")").eq(0).index();
        nextTick(function () {
          swiper.slideTo(slideToIndex);
        });
      } else {
        swiper.slideTo(slideToIndex);
      }
    } else {
      swiper.slideTo(slideToIndex);
    }
  }

  var slide = {
    slideTo: slideTo,
    slideToLoop: slideToLoop,
    slideNext: slideNext,
    slidePrev: slidePrev,
    slideReset: slideReset,
    slideToClosest: slideToClosest,
    slideToClickedSlide: slideToClickedSlide
  };

  function loopCreate() {
    var swiper = this;
    var document = getDocument();
    var params = swiper.params,
        $wrapperEl = swiper.$wrapperEl; // Remove duplicated slides

    $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass).remove();
    var slides = $wrapperEl.children("." + params.slideClass);

    if (params.loopFillGroupWithBlank) {
      var blankSlidesNum = params.slidesPerGroup - slides.length % params.slidesPerGroup;

      if (blankSlidesNum !== params.slidesPerGroup) {
        for (var i = 0; i < blankSlidesNum; i += 1) {
          var blankNode = $(document.createElement('div')).addClass(params.slideClass + " " + params.slideBlankClass);
          $wrapperEl.append(blankNode);
        }

        slides = $wrapperEl.children("." + params.slideClass);
      }
    }

    if (params.slidesPerView === 'auto' && !params.loopedSlides) params.loopedSlides = slides.length;
    swiper.loopedSlides = Math.ceil(parseFloat(params.loopedSlides || params.slidesPerView, 10));
    swiper.loopedSlides += params.loopAdditionalSlides;

    if (swiper.loopedSlides > slides.length) {
      swiper.loopedSlides = slides.length;
    }

    var prependSlides = [];
    var appendSlides = [];
    slides.each(function (el, index) {
      var slide = $(el);

      if (index < swiper.loopedSlides) {
        appendSlides.push(el);
      }

      if (index < slides.length && index >= slides.length - swiper.loopedSlides) {
        prependSlides.push(el);
      }

      slide.attr('data-swiper-slide-index', index);
    });

    for (var _i = 0; _i < appendSlides.length; _i += 1) {
      $wrapperEl.append($(appendSlides[_i].cloneNode(true)).addClass(params.slideDuplicateClass));
    }

    for (var _i2 = prependSlides.length - 1; _i2 >= 0; _i2 -= 1) {
      $wrapperEl.prepend($(prependSlides[_i2].cloneNode(true)).addClass(params.slideDuplicateClass));
    }
  }

  function loopFix() {
    var swiper = this;
    swiper.emit('beforeLoopFix');
    var activeIndex = swiper.activeIndex,
        slides = swiper.slides,
        loopedSlides = swiper.loopedSlides,
        allowSlidePrev = swiper.allowSlidePrev,
        allowSlideNext = swiper.allowSlideNext,
        snapGrid = swiper.snapGrid,
        rtl = swiper.rtlTranslate;
    var newIndex;
    swiper.allowSlidePrev = true;
    swiper.allowSlideNext = true;
    var snapTranslate = -snapGrid[activeIndex];
    var diff = snapTranslate - swiper.getTranslate(); // Fix For Negative Oversliding

    if (activeIndex < loopedSlides) {
      newIndex = slides.length - loopedSlides * 3 + activeIndex;
      newIndex += loopedSlides;
      var slideChanged = swiper.slideTo(newIndex, 0, false, true);

      if (slideChanged && diff !== 0) {
        swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
      }
    } else if (activeIndex >= slides.length - loopedSlides) {
      // Fix For Positive Oversliding
      newIndex = -slides.length + activeIndex + loopedSlides;
      newIndex += loopedSlides;

      var _slideChanged = swiper.slideTo(newIndex, 0, false, true);

      if (_slideChanged && diff !== 0) {
        swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
      }
    }

    swiper.allowSlidePrev = allowSlidePrev;
    swiper.allowSlideNext = allowSlideNext;
    swiper.emit('loopFix');
  }

  function loopDestroy() {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl,
        params = swiper.params,
        slides = swiper.slides;
    $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + ",." + params.slideClass + "." + params.slideBlankClass).remove();
    slides.removeAttr('data-swiper-slide-index');
  }

  var loop = {
    loopCreate: loopCreate,
    loopFix: loopFix,
    loopDestroy: loopDestroy
  };

  function setGrabCursor(moving) {
    var swiper = this;
    if (swiper.support.touch || !swiper.params.simulateTouch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode) return;
    var el = swiper.el;
    el.style.cursor = 'move';
    el.style.cursor = moving ? '-webkit-grabbing' : '-webkit-grab';
    el.style.cursor = moving ? '-moz-grabbin' : '-moz-grab';
    el.style.cursor = moving ? 'grabbing' : 'grab';
  }

  function unsetGrabCursor() {
    var swiper = this;

    if (swiper.support.touch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode) {
      return;
    }

    swiper.el.style.cursor = '';
  }

  var grabCursor = {
    setGrabCursor: setGrabCursor,
    unsetGrabCursor: unsetGrabCursor
  };

  function appendSlide(slides) {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl,
        params = swiper.params;

    if (params.loop) {
      swiper.loopDestroy();
    }

    if (_typeof(slides) === 'object' && 'length' in slides) {
      for (var i = 0; i < slides.length; i += 1) {
        if (slides[i]) $wrapperEl.append(slides[i]);
      }
    } else {
      $wrapperEl.append(slides);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && swiper.support.observer)) {
      swiper.update();
    }
  }

  function prependSlide(slides) {
    var swiper = this;
    var params = swiper.params,
        $wrapperEl = swiper.$wrapperEl,
        activeIndex = swiper.activeIndex;

    if (params.loop) {
      swiper.loopDestroy();
    }

    var newActiveIndex = activeIndex + 1;

    if (_typeof(slides) === 'object' && 'length' in slides) {
      for (var i = 0; i < slides.length; i += 1) {
        if (slides[i]) $wrapperEl.prepend(slides[i]);
      }

      newActiveIndex = activeIndex + slides.length;
    } else {
      $wrapperEl.prepend(slides);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && swiper.support.observer)) {
      swiper.update();
    }

    swiper.slideTo(newActiveIndex, 0, false);
  }

  function addSlide(index, slides) {
    var swiper = this;
    var $wrapperEl = swiper.$wrapperEl,
        params = swiper.params,
        activeIndex = swiper.activeIndex;
    var activeIndexBuffer = activeIndex;

    if (params.loop) {
      activeIndexBuffer -= swiper.loopedSlides;
      swiper.loopDestroy();
      swiper.slides = $wrapperEl.children("." + params.slideClass);
    }

    var baseLength = swiper.slides.length;

    if (index <= 0) {
      swiper.prependSlide(slides);
      return;
    }

    if (index >= baseLength) {
      swiper.appendSlide(slides);
      return;
    }

    var newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + 1 : activeIndexBuffer;
    var slidesBuffer = [];

    for (var i = baseLength - 1; i >= index; i -= 1) {
      var currentSlide = swiper.slides.eq(i);
      currentSlide.remove();
      slidesBuffer.unshift(currentSlide);
    }

    if (_typeof(slides) === 'object' && 'length' in slides) {
      for (var _i = 0; _i < slides.length; _i += 1) {
        if (slides[_i]) $wrapperEl.append(slides[_i]);
      }

      newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + slides.length : activeIndexBuffer;
    } else {
      $wrapperEl.append(slides);
    }

    for (var _i2 = 0; _i2 < slidesBuffer.length; _i2 += 1) {
      $wrapperEl.append(slidesBuffer[_i2]);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && swiper.support.observer)) {
      swiper.update();
    }

    if (params.loop) {
      swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
    } else {
      swiper.slideTo(newActiveIndex, 0, false);
    }
  }

  function removeSlide(slidesIndexes) {
    var swiper = this;
    var params = swiper.params,
        $wrapperEl = swiper.$wrapperEl,
        activeIndex = swiper.activeIndex;
    var activeIndexBuffer = activeIndex;

    if (params.loop) {
      activeIndexBuffer -= swiper.loopedSlides;
      swiper.loopDestroy();
      swiper.slides = $wrapperEl.children("." + params.slideClass);
    }

    var newActiveIndex = activeIndexBuffer;
    var indexToRemove;

    if (_typeof(slidesIndexes) === 'object' && 'length' in slidesIndexes) {
      for (var i = 0; i < slidesIndexes.length; i += 1) {
        indexToRemove = slidesIndexes[i];
        if (swiper.slides[indexToRemove]) swiper.slides.eq(indexToRemove).remove();
        if (indexToRemove < newActiveIndex) newActiveIndex -= 1;
      }

      newActiveIndex = Math.max(newActiveIndex, 0);
    } else {
      indexToRemove = slidesIndexes;
      if (swiper.slides[indexToRemove]) swiper.slides.eq(indexToRemove).remove();
      if (indexToRemove < newActiveIndex) newActiveIndex -= 1;
      newActiveIndex = Math.max(newActiveIndex, 0);
    }

    if (params.loop) {
      swiper.loopCreate();
    }

    if (!(params.observer && swiper.support.observer)) {
      swiper.update();
    }

    if (params.loop) {
      swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
    } else {
      swiper.slideTo(newActiveIndex, 0, false);
    }
  }

  function removeAllSlides() {
    var swiper = this;
    var slidesIndexes = [];

    for (var i = 0; i < swiper.slides.length; i += 1) {
      slidesIndexes.push(i);
    }

    swiper.removeSlide(slidesIndexes);
  }

  var manipulation = {
    appendSlide: appendSlide,
    prependSlide: prependSlide,
    addSlide: addSlide,
    removeSlide: removeSlide,
    removeAllSlides: removeAllSlides
  };

  function onTouchStart(event) {
    var swiper = this;
    var document = getDocument();
    var window = getWindow();
    var data = swiper.touchEventsData;
    var params = swiper.params,
        touches = swiper.touches;

    if (swiper.animating && params.preventInteractionOnTransition) {
      return;
    }

    var e = event;
    if (e.originalEvent) e = e.originalEvent;
    var $targetEl = $(e.target);

    if (params.touchEventsTarget === 'wrapper') {
      if (!$targetEl.closest(swiper.wrapperEl).length) return;
    }

    data.isTouchEvent = e.type === 'touchstart';
    if (!data.isTouchEvent && 'which' in e && e.which === 3) return;
    if (!data.isTouchEvent && 'button' in e && e.button > 0) return;
    if (data.isTouched && data.isMoved) return; // change target el for shadow root componenet

    var swipingClassHasValue = !!params.noSwipingClass && params.noSwipingClass !== '';

    if (swipingClassHasValue && e.target && e.target.shadowRoot && event.path && event.path[0]) {
      $targetEl = $(event.path[0]);
    }

    if (params.noSwiping && $targetEl.closest(params.noSwipingSelector ? params.noSwipingSelector : "." + params.noSwipingClass)[0]) {
      swiper.allowClick = true;
      return;
    }

    if (params.swipeHandler) {
      if (!$targetEl.closest(params.swipeHandler)[0]) return;
    }

    touches.currentX = e.type === 'touchstart' ? e.targetTouches[0].pageX : e.pageX;
    touches.currentY = e.type === 'touchstart' ? e.targetTouches[0].pageY : e.pageY;
    var startX = touches.currentX;
    var startY = touches.currentY; // Do NOT start if iOS edge swipe is detected. Otherwise iOS app cannot swipe-to-go-back anymore

    var edgeSwipeDetection = params.edgeSwipeDetection || params.iOSEdgeSwipeDetection;
    var edgeSwipeThreshold = params.edgeSwipeThreshold || params.iOSEdgeSwipeThreshold;

    if (edgeSwipeDetection && (startX <= edgeSwipeThreshold || startX >= window.innerWidth - edgeSwipeThreshold)) {
      return;
    }

    extend$1(data, {
      isTouched: true,
      isMoved: false,
      allowTouchCallbacks: true,
      isScrolling: undefined,
      startMoving: undefined
    });
    touches.startX = startX;
    touches.startY = startY;
    data.touchStartTime = now();
    swiper.allowClick = true;
    swiper.updateSize();
    swiper.swipeDirection = undefined;
    if (params.threshold > 0) data.allowThresholdMove = false;

    if (e.type !== 'touchstart') {
      var preventDefault = true;
      if ($targetEl.is(data.formElements)) preventDefault = false;

      if (document.activeElement && $(document.activeElement).is(data.formElements) && document.activeElement !== $targetEl[0]) {
        document.activeElement.blur();
      }

      var shouldPreventDefault = preventDefault && swiper.allowTouchMove && params.touchStartPreventDefault;

      if ((params.touchStartForcePreventDefault || shouldPreventDefault) && !$targetEl[0].isContentEditable) {
        e.preventDefault();
      }
    }

    swiper.emit('touchStart', e);
  }

  function onTouchMove(event) {
    var document = getDocument();
    var swiper = this;
    var data = swiper.touchEventsData;
    var params = swiper.params,
        touches = swiper.touches,
        rtl = swiper.rtlTranslate;
    var e = event;
    if (e.originalEvent) e = e.originalEvent;

    if (!data.isTouched) {
      if (data.startMoving && data.isScrolling) {
        swiper.emit('touchMoveOpposite', e);
      }

      return;
    }

    if (data.isTouchEvent && e.type !== 'touchmove') return;
    var targetTouch = e.type === 'touchmove' && e.targetTouches && (e.targetTouches[0] || e.changedTouches[0]);
    var pageX = e.type === 'touchmove' ? targetTouch.pageX : e.pageX;
    var pageY = e.type === 'touchmove' ? targetTouch.pageY : e.pageY;

    if (e.preventedByNestedSwiper) {
      touches.startX = pageX;
      touches.startY = pageY;
      return;
    }

    if (!swiper.allowTouchMove) {
      // isMoved = true;
      swiper.allowClick = false;

      if (data.isTouched) {
        extend$1(touches, {
          startX: pageX,
          startY: pageY,
          currentX: pageX,
          currentY: pageY
        });
        data.touchStartTime = now();
      }

      return;
    }

    if (data.isTouchEvent && params.touchReleaseOnEdges && !params.loop) {
      if (swiper.isVertical()) {
        // Vertical
        if (pageY < touches.startY && swiper.translate <= swiper.maxTranslate() || pageY > touches.startY && swiper.translate >= swiper.minTranslate()) {
          data.isTouched = false;
          data.isMoved = false;
          return;
        }
      } else if (pageX < touches.startX && swiper.translate <= swiper.maxTranslate() || pageX > touches.startX && swiper.translate >= swiper.minTranslate()) {
        return;
      }
    }

    if (data.isTouchEvent && document.activeElement) {
      if (e.target === document.activeElement && $(e.target).is(data.formElements)) {
        data.isMoved = true;
        swiper.allowClick = false;
        return;
      }
    }

    if (data.allowTouchCallbacks) {
      swiper.emit('touchMove', e);
    }

    if (e.targetTouches && e.targetTouches.length > 1) return;
    touches.currentX = pageX;
    touches.currentY = pageY;
    var diffX = touches.currentX - touches.startX;
    var diffY = touches.currentY - touches.startY;
    if (swiper.params.threshold && Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2)) < swiper.params.threshold) return;

    if (typeof data.isScrolling === 'undefined') {
      var touchAngle;

      if (swiper.isHorizontal() && touches.currentY === touches.startY || swiper.isVertical() && touches.currentX === touches.startX) {
        data.isScrolling = false;
      } else {
        // eslint-disable-next-line
        if (diffX * diffX + diffY * diffY >= 25) {
          touchAngle = Math.atan2(Math.abs(diffY), Math.abs(diffX)) * 180 / Math.PI;
          data.isScrolling = swiper.isHorizontal() ? touchAngle > params.touchAngle : 90 - touchAngle > params.touchAngle;
        }
      }
    }

    if (data.isScrolling) {
      swiper.emit('touchMoveOpposite', e);
    }

    if (typeof data.startMoving === 'undefined') {
      if (touches.currentX !== touches.startX || touches.currentY !== touches.startY) {
        data.startMoving = true;
      }
    }

    if (data.isScrolling) {
      data.isTouched = false;
      return;
    }

    if (!data.startMoving) {
      return;
    }

    swiper.allowClick = false;

    if (!params.cssMode && e.cancelable) {
      e.preventDefault();
    }

    if (params.touchMoveStopPropagation && !params.nested) {
      e.stopPropagation();
    }

    if (!data.isMoved) {
      if (params.loop) {
        swiper.loopFix();
      }

      data.startTranslate = swiper.getTranslate();
      swiper.setTransition(0);

      if (swiper.animating) {
        swiper.$wrapperEl.trigger('webkitTransitionEnd transitionend');
      }

      data.allowMomentumBounce = false; // Grab Cursor

      if (params.grabCursor && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
        swiper.setGrabCursor(true);
      }

      swiper.emit('sliderFirstMove', e);
    }

    swiper.emit('sliderMove', e);
    data.isMoved = true;
    var diff = swiper.isHorizontal() ? diffX : diffY;
    touches.diff = diff;
    diff *= params.touchRatio;
    if (rtl) diff = -diff;
    swiper.swipeDirection = diff > 0 ? 'prev' : 'next';
    data.currentTranslate = diff + data.startTranslate;
    var disableParentSwiper = true;
    var resistanceRatio = params.resistanceRatio;

    if (params.touchReleaseOnEdges) {
      resistanceRatio = 0;
    }

    if (diff > 0 && data.currentTranslate > swiper.minTranslate()) {
      disableParentSwiper = false;
      if (params.resistance) data.currentTranslate = swiper.minTranslate() - 1 + Math.pow(-swiper.minTranslate() + data.startTranslate + diff, resistanceRatio);
    } else if (diff < 0 && data.currentTranslate < swiper.maxTranslate()) {
      disableParentSwiper = false;
      if (params.resistance) data.currentTranslate = swiper.maxTranslate() + 1 - Math.pow(swiper.maxTranslate() - data.startTranslate - diff, resistanceRatio);
    }

    if (disableParentSwiper) {
      e.preventedByNestedSwiper = true;
    } // Directions locks


    if (!swiper.allowSlideNext && swiper.swipeDirection === 'next' && data.currentTranslate < data.startTranslate) {
      data.currentTranslate = data.startTranslate;
    }

    if (!swiper.allowSlidePrev && swiper.swipeDirection === 'prev' && data.currentTranslate > data.startTranslate) {
      data.currentTranslate = data.startTranslate;
    } // Threshold


    if (params.threshold > 0) {
      if (Math.abs(diff) > params.threshold || data.allowThresholdMove) {
        if (!data.allowThresholdMove) {
          data.allowThresholdMove = true;
          touches.startX = touches.currentX;
          touches.startY = touches.currentY;
          data.currentTranslate = data.startTranslate;
          touches.diff = swiper.isHorizontal() ? touches.currentX - touches.startX : touches.currentY - touches.startY;
          return;
        }
      } else {
        data.currentTranslate = data.startTranslate;
        return;
      }
    }

    if (!params.followFinger || params.cssMode) return; // Update active index in free mode

    if (params.freeMode || params.watchSlidesProgress || params.watchSlidesVisibility) {
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();
    }

    if (params.freeMode) {
      // Velocity
      if (data.velocities.length === 0) {
        data.velocities.push({
          position: touches[swiper.isHorizontal() ? 'startX' : 'startY'],
          time: data.touchStartTime
        });
      }

      data.velocities.push({
        position: touches[swiper.isHorizontal() ? 'currentX' : 'currentY'],
        time: now()
      });
    } // Update progress


    swiper.updateProgress(data.currentTranslate); // Update translate

    swiper.setTranslate(data.currentTranslate);
  }

  function onTouchEnd(event) {
    var swiper = this;
    var data = swiper.touchEventsData;
    var params = swiper.params,
        touches = swiper.touches,
        rtl = swiper.rtlTranslate,
        $wrapperEl = swiper.$wrapperEl,
        slidesGrid = swiper.slidesGrid,
        snapGrid = swiper.snapGrid;
    var e = event;
    if (e.originalEvent) e = e.originalEvent;

    if (data.allowTouchCallbacks) {
      swiper.emit('touchEnd', e);
    }

    data.allowTouchCallbacks = false;

    if (!data.isTouched) {
      if (data.isMoved && params.grabCursor) {
        swiper.setGrabCursor(false);
      }

      data.isMoved = false;
      data.startMoving = false;
      return;
    } // Return Grab Cursor


    if (params.grabCursor && data.isMoved && data.isTouched && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
      swiper.setGrabCursor(false);
    } // Time diff


    var touchEndTime = now();
    var timeDiff = touchEndTime - data.touchStartTime; // Tap, doubleTap, Click

    if (swiper.allowClick) {
      swiper.updateClickedSlide(e);
      swiper.emit('tap click', e);

      if (timeDiff < 300 && touchEndTime - data.lastClickTime < 300) {
        swiper.emit('doubleTap doubleClick', e);
      }
    }

    data.lastClickTime = now();
    nextTick(function () {
      if (!swiper.destroyed) swiper.allowClick = true;
    });

    if (!data.isTouched || !data.isMoved || !swiper.swipeDirection || touches.diff === 0 || data.currentTranslate === data.startTranslate) {
      data.isTouched = false;
      data.isMoved = false;
      data.startMoving = false;
      return;
    }

    data.isTouched = false;
    data.isMoved = false;
    data.startMoving = false;
    var currentPos;

    if (params.followFinger) {
      currentPos = rtl ? swiper.translate : -swiper.translate;
    } else {
      currentPos = -data.currentTranslate;
    }

    if (params.cssMode) {
      return;
    }

    if (params.freeMode) {
      if (currentPos < -swiper.minTranslate()) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      if (currentPos > -swiper.maxTranslate()) {
        if (swiper.slides.length < snapGrid.length) {
          swiper.slideTo(snapGrid.length - 1);
        } else {
          swiper.slideTo(swiper.slides.length - 1);
        }

        return;
      }

      if (params.freeModeMomentum) {
        if (data.velocities.length > 1) {
          var lastMoveEvent = data.velocities.pop();
          var velocityEvent = data.velocities.pop();
          var distance = lastMoveEvent.position - velocityEvent.position;
          var time = lastMoveEvent.time - velocityEvent.time;
          swiper.velocity = distance / time;
          swiper.velocity /= 2;

          if (Math.abs(swiper.velocity) < params.freeModeMinimumVelocity) {
            swiper.velocity = 0;
          } // this implies that the user stopped moving a finger then released.
          // There would be no events with distance zero, so the last event is stale.


          if (time > 150 || now() - lastMoveEvent.time > 300) {
            swiper.velocity = 0;
          }
        } else {
          swiper.velocity = 0;
        }

        swiper.velocity *= params.freeModeMomentumVelocityRatio;
        data.velocities.length = 0;
        var momentumDuration = 1000 * params.freeModeMomentumRatio;
        var momentumDistance = swiper.velocity * momentumDuration;
        var newPosition = swiper.translate + momentumDistance;
        if (rtl) newPosition = -newPosition;
        var doBounce = false;
        var afterBouncePosition;
        var bounceAmount = Math.abs(swiper.velocity) * 20 * params.freeModeMomentumBounceRatio;
        var needsLoopFix;

        if (newPosition < swiper.maxTranslate()) {
          if (params.freeModeMomentumBounce) {
            if (newPosition + swiper.maxTranslate() < -bounceAmount) {
              newPosition = swiper.maxTranslate() - bounceAmount;
            }

            afterBouncePosition = swiper.maxTranslate();
            doBounce = true;
            data.allowMomentumBounce = true;
          } else {
            newPosition = swiper.maxTranslate();
          }

          if (params.loop && params.centeredSlides) needsLoopFix = true;
        } else if (newPosition > swiper.minTranslate()) {
          if (params.freeModeMomentumBounce) {
            if (newPosition - swiper.minTranslate() > bounceAmount) {
              newPosition = swiper.minTranslate() + bounceAmount;
            }

            afterBouncePosition = swiper.minTranslate();
            doBounce = true;
            data.allowMomentumBounce = true;
          } else {
            newPosition = swiper.minTranslate();
          }

          if (params.loop && params.centeredSlides) needsLoopFix = true;
        } else if (params.freeModeSticky) {
          var nextSlide;

          for (var j = 0; j < snapGrid.length; j += 1) {
            if (snapGrid[j] > -newPosition) {
              nextSlide = j;
              break;
            }
          }

          if (Math.abs(snapGrid[nextSlide] - newPosition) < Math.abs(snapGrid[nextSlide - 1] - newPosition) || swiper.swipeDirection === 'next') {
            newPosition = snapGrid[nextSlide];
          } else {
            newPosition = snapGrid[nextSlide - 1];
          }

          newPosition = -newPosition;
        }

        if (needsLoopFix) {
          swiper.once('transitionEnd', function () {
            swiper.loopFix();
          });
        } // Fix duration


        if (swiper.velocity !== 0) {
          if (rtl) {
            momentumDuration = Math.abs((-newPosition - swiper.translate) / swiper.velocity);
          } else {
            momentumDuration = Math.abs((newPosition - swiper.translate) / swiper.velocity);
          }

          if (params.freeModeSticky) {
            // If freeModeSticky is active and the user ends a swipe with a slow-velocity
            // event, then durations can be 20+ seconds to slide one (or zero!) slides.
            // It's easy to see this when simulating touch with mouse events. To fix this,
            // limit single-slide swipes to the default slide duration. This also has the
            // nice side effect of matching slide speed if the user stopped moving before
            // lifting finger or mouse vs. moving slowly before lifting the finger/mouse.
            // For faster swipes, also apply limits (albeit higher ones).
            var moveDistance = Math.abs((rtl ? -newPosition : newPosition) - swiper.translate);
            var currentSlideSize = swiper.slidesSizesGrid[swiper.activeIndex];

            if (moveDistance < currentSlideSize) {
              momentumDuration = params.speed;
            } else if (moveDistance < 2 * currentSlideSize) {
              momentumDuration = params.speed * 1.5;
            } else {
              momentumDuration = params.speed * 2.5;
            }
          }
        } else if (params.freeModeSticky) {
          swiper.slideToClosest();
          return;
        }

        if (params.freeModeMomentumBounce && doBounce) {
          swiper.updateProgress(afterBouncePosition);
          swiper.setTransition(momentumDuration);
          swiper.setTranslate(newPosition);
          swiper.transitionStart(true, swiper.swipeDirection);
          swiper.animating = true;
          $wrapperEl.transitionEnd(function () {
            if (!swiper || swiper.destroyed || !data.allowMomentumBounce) return;
            swiper.emit('momentumBounce');
            swiper.setTransition(params.speed);
            setTimeout(function () {
              swiper.setTranslate(afterBouncePosition);
              $wrapperEl.transitionEnd(function () {
                if (!swiper || swiper.destroyed) return;
                swiper.transitionEnd();
              });
            }, 0);
          });
        } else if (swiper.velocity) {
          swiper.updateProgress(newPosition);
          swiper.setTransition(momentumDuration);
          swiper.setTranslate(newPosition);
          swiper.transitionStart(true, swiper.swipeDirection);

          if (!swiper.animating) {
            swiper.animating = true;
            $wrapperEl.transitionEnd(function () {
              if (!swiper || swiper.destroyed) return;
              swiper.transitionEnd();
            });
          }
        } else {
          swiper.updateProgress(newPosition);
        }

        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      } else if (params.freeModeSticky) {
        swiper.slideToClosest();
        return;
      }

      if (!params.freeModeMomentum || timeDiff >= params.longSwipesMs) {
        swiper.updateProgress();
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      }

      return;
    } // Find current slide


    var stopIndex = 0;
    var groupSize = swiper.slidesSizesGrid[0];

    for (var i = 0; i < slidesGrid.length; i += i < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup) {
      var _increment = i < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;

      if (typeof slidesGrid[i + _increment] !== 'undefined') {
        if (currentPos >= slidesGrid[i] && currentPos < slidesGrid[i + _increment]) {
          stopIndex = i;
          groupSize = slidesGrid[i + _increment] - slidesGrid[i];
        }
      } else if (currentPos >= slidesGrid[i]) {
        stopIndex = i;
        groupSize = slidesGrid[slidesGrid.length - 1] - slidesGrid[slidesGrid.length - 2];
      }
    } // Find current slide size


    var ratio = (currentPos - slidesGrid[stopIndex]) / groupSize;
    var increment = stopIndex < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;

    if (timeDiff > params.longSwipesMs) {
      // Long touches
      if (!params.longSwipes) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      if (swiper.swipeDirection === 'next') {
        if (ratio >= params.longSwipesRatio) swiper.slideTo(stopIndex + increment);else swiper.slideTo(stopIndex);
      }

      if (swiper.swipeDirection === 'prev') {
        if (ratio > 1 - params.longSwipesRatio) swiper.slideTo(stopIndex + increment);else swiper.slideTo(stopIndex);
      }
    } else {
      // Short swipes
      if (!params.shortSwipes) {
        swiper.slideTo(swiper.activeIndex);
        return;
      }

      var isNavButtonTarget = swiper.navigation && (e.target === swiper.navigation.nextEl || e.target === swiper.navigation.prevEl);

      if (!isNavButtonTarget) {
        if (swiper.swipeDirection === 'next') {
          swiper.slideTo(stopIndex + increment);
        }

        if (swiper.swipeDirection === 'prev') {
          swiper.slideTo(stopIndex);
        }
      } else if (e.target === swiper.navigation.nextEl) {
        swiper.slideTo(stopIndex + increment);
      } else {
        swiper.slideTo(stopIndex);
      }
    }
  }

  function onResize() {
    var swiper = this;
    var params = swiper.params,
        el = swiper.el;
    if (el && el.offsetWidth === 0) return; // Breakpoints

    if (params.breakpoints) {
      swiper.setBreakpoint();
    } // Save locks


    var allowSlideNext = swiper.allowSlideNext,
        allowSlidePrev = swiper.allowSlidePrev,
        snapGrid = swiper.snapGrid; // Disable locks on resize

    swiper.allowSlideNext = true;
    swiper.allowSlidePrev = true;
    swiper.updateSize();
    swiper.updateSlides();
    swiper.updateSlidesClasses();

    if ((params.slidesPerView === 'auto' || params.slidesPerView > 1) && swiper.isEnd && !swiper.isBeginning && !swiper.params.centeredSlides) {
      swiper.slideTo(swiper.slides.length - 1, 0, false, true);
    } else {
      swiper.slideTo(swiper.activeIndex, 0, false, true);
    }

    if (swiper.autoplay && swiper.autoplay.running && swiper.autoplay.paused) {
      swiper.autoplay.run();
    } // Return locks after resize


    swiper.allowSlidePrev = allowSlidePrev;
    swiper.allowSlideNext = allowSlideNext;

    if (swiper.params.watchOverflow && snapGrid !== swiper.snapGrid) {
      swiper.checkOverflow();
    }
  }

  function onClick(e) {
    var swiper = this;

    if (!swiper.allowClick) {
      if (swiper.params.preventClicks) e.preventDefault();

      if (swiper.params.preventClicksPropagation && swiper.animating) {
        e.stopPropagation();
        e.stopImmediatePropagation();
      }
    }
  }

  function onScroll() {
    var swiper = this;
    var wrapperEl = swiper.wrapperEl,
        rtlTranslate = swiper.rtlTranslate;
    swiper.previousTranslate = swiper.translate;

    if (swiper.isHorizontal()) {
      if (rtlTranslate) {
        swiper.translate = wrapperEl.scrollWidth - wrapperEl.offsetWidth - wrapperEl.scrollLeft;
      } else {
        swiper.translate = -wrapperEl.scrollLeft;
      }
    } else {
      swiper.translate = -wrapperEl.scrollTop;
    } // eslint-disable-next-line


    if (swiper.translate === -0) swiper.translate = 0;
    swiper.updateActiveIndex();
    swiper.updateSlidesClasses();
    var newProgress;
    var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();

    if (translatesDiff === 0) {
      newProgress = 0;
    } else {
      newProgress = (swiper.translate - swiper.minTranslate()) / translatesDiff;
    }

    if (newProgress !== swiper.progress) {
      swiper.updateProgress(rtlTranslate ? -swiper.translate : swiper.translate);
    }

    swiper.emit('setTranslate', swiper.translate, false);
  }

  var dummyEventAttached = false;

  function dummyEventListener() {}

  function attachEvents() {
    var swiper = this;
    var document = getDocument();
    var params = swiper.params,
        touchEvents = swiper.touchEvents,
        el = swiper.el,
        wrapperEl = swiper.wrapperEl,
        device = swiper.device,
        support = swiper.support;
    swiper.onTouchStart = onTouchStart.bind(swiper);
    swiper.onTouchMove = onTouchMove.bind(swiper);
    swiper.onTouchEnd = onTouchEnd.bind(swiper);

    if (params.cssMode) {
      swiper.onScroll = onScroll.bind(swiper);
    }

    swiper.onClick = onClick.bind(swiper);
    var capture = !!params.nested; // Touch Events

    if (!support.touch && support.pointerEvents) {
      el.addEventListener(touchEvents.start, swiper.onTouchStart, false);
      document.addEventListener(touchEvents.move, swiper.onTouchMove, capture);
      document.addEventListener(touchEvents.end, swiper.onTouchEnd, false);
    } else {
      if (support.touch) {
        var passiveListener = touchEvents.start === 'touchstart' && support.passiveListener && params.passiveListeners ? {
          passive: true,
          capture: false
        } : false;
        el.addEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
        el.addEventListener(touchEvents.move, swiper.onTouchMove, support.passiveListener ? {
          passive: false,
          capture: capture
        } : capture);
        el.addEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);

        if (touchEvents.cancel) {
          el.addEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener);
        }

        if (!dummyEventAttached) {
          document.addEventListener('touchstart', dummyEventListener);
          dummyEventAttached = true;
        }
      }

      if (params.simulateTouch && !device.ios && !device.android || params.simulateTouch && !support.touch && device.ios) {
        el.addEventListener('mousedown', swiper.onTouchStart, false);
        document.addEventListener('mousemove', swiper.onTouchMove, capture);
        document.addEventListener('mouseup', swiper.onTouchEnd, false);
      }
    } // Prevent Links Clicks


    if (params.preventClicks || params.preventClicksPropagation) {
      el.addEventListener('click', swiper.onClick, true);
    }

    if (params.cssMode) {
      wrapperEl.addEventListener('scroll', swiper.onScroll);
    } // Resize handler


    if (params.updateOnWindowResize) {
      swiper.on(device.ios || device.android ? 'resize orientationchange observerUpdate' : 'resize observerUpdate', onResize, true);
    } else {
      swiper.on('observerUpdate', onResize, true);
    }
  }

  function detachEvents() {
    var swiper = this;
    var document = getDocument();
    var params = swiper.params,
        touchEvents = swiper.touchEvents,
        el = swiper.el,
        wrapperEl = swiper.wrapperEl,
        device = swiper.device,
        support = swiper.support;
    var capture = !!params.nested; // Touch Events

    if (!support.touch && support.pointerEvents) {
      el.removeEventListener(touchEvents.start, swiper.onTouchStart, false);
      document.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
      document.removeEventListener(touchEvents.end, swiper.onTouchEnd, false);
    } else {
      if (support.touch) {
        var passiveListener = touchEvents.start === 'onTouchStart' && support.passiveListener && params.passiveListeners ? {
          passive: true,
          capture: false
        } : false;
        el.removeEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
        el.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
        el.removeEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);

        if (touchEvents.cancel) {
          el.removeEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener);
        }
      }

      if (params.simulateTouch && !device.ios && !device.android || params.simulateTouch && !support.touch && device.ios) {
        el.removeEventListener('mousedown', swiper.onTouchStart, false);
        document.removeEventListener('mousemove', swiper.onTouchMove, capture);
        document.removeEventListener('mouseup', swiper.onTouchEnd, false);
      }
    } // Prevent Links Clicks


    if (params.preventClicks || params.preventClicksPropagation) {
      el.removeEventListener('click', swiper.onClick, true);
    }

    if (params.cssMode) {
      wrapperEl.removeEventListener('scroll', swiper.onScroll);
    } // Resize handler


    swiper.off(device.ios || device.android ? 'resize orientationchange observerUpdate' : 'resize observerUpdate', onResize);
  }

  var events = {
    attachEvents: attachEvents,
    detachEvents: detachEvents
  };

  function setBreakpoint() {
    var swiper = this;
    var activeIndex = swiper.activeIndex,
        initialized = swiper.initialized,
        _swiper$loopedSlides = swiper.loopedSlides,
        loopedSlides = _swiper$loopedSlides === void 0 ? 0 : _swiper$loopedSlides,
        params = swiper.params,
        $el = swiper.$el;
    var breakpoints = params.breakpoints;
    if (!breakpoints || breakpoints && Object.keys(breakpoints).length === 0) return; // Get breakpoint for window width and update parameters

    var breakpoint = swiper.getBreakpoint(breakpoints);

    if (breakpoint && swiper.currentBreakpoint !== breakpoint) {
      var breakpointOnlyParams = breakpoint in breakpoints ? breakpoints[breakpoint] : undefined;

      if (breakpointOnlyParams) {
        ['slidesPerView', 'spaceBetween', 'slidesPerGroup', 'slidesPerGroupSkip', 'slidesPerColumn'].forEach(function (param) {
          var paramValue = breakpointOnlyParams[param];
          if (typeof paramValue === 'undefined') return;

          if (param === 'slidesPerView' && (paramValue === 'AUTO' || paramValue === 'auto')) {
            breakpointOnlyParams[param] = 'auto';
          } else if (param === 'slidesPerView') {
            breakpointOnlyParams[param] = parseFloat(paramValue);
          } else {
            breakpointOnlyParams[param] = parseInt(paramValue, 10);
          }
        });
      }

      var breakpointParams = breakpointOnlyParams || swiper.originalParams;
      var wasMultiRow = params.slidesPerColumn > 1;
      var isMultiRow = breakpointParams.slidesPerColumn > 1;

      if (wasMultiRow && !isMultiRow) {
        $el.removeClass(params.containerModifierClass + "multirow " + params.containerModifierClass + "multirow-column");
        swiper.emitContainerClasses();
      } else if (!wasMultiRow && isMultiRow) {
        $el.addClass(params.containerModifierClass + "multirow");

        if (breakpointParams.slidesPerColumnFill === 'column') {
          $el.addClass(params.containerModifierClass + "multirow-column");
        }

        swiper.emitContainerClasses();
      }

      var directionChanged = breakpointParams.direction && breakpointParams.direction !== params.direction;
      var needsReLoop = params.loop && (breakpointParams.slidesPerView !== params.slidesPerView || directionChanged);

      if (directionChanged && initialized) {
        swiper.changeDirection();
      }

      extend$1(swiper.params, breakpointParams);
      extend$1(swiper, {
        allowTouchMove: swiper.params.allowTouchMove,
        allowSlideNext: swiper.params.allowSlideNext,
        allowSlidePrev: swiper.params.allowSlidePrev
      });
      swiper.currentBreakpoint = breakpoint;
      swiper.emit('_beforeBreakpoint', breakpointParams);

      if (needsReLoop && initialized) {
        swiper.loopDestroy();
        swiper.loopCreate();
        swiper.updateSlides();
        swiper.slideTo(activeIndex - loopedSlides + swiper.loopedSlides, 0, false);
      }

      swiper.emit('breakpoint', breakpointParams);
    }
  }

  function getBreakpoints(breakpoints) {
    var window = getWindow(); // Get breakpoint for window width

    if (!breakpoints) return undefined;
    var breakpoint = false;
    var points = Object.keys(breakpoints).map(function (point) {
      if (typeof point === 'string' && point.indexOf('@') === 0) {
        var minRatio = parseFloat(point.substr(1));
        var value = window.innerHeight * minRatio;
        return {
          value: value,
          point: point
        };
      }

      return {
        value: point,
        point: point
      };
    });
    points.sort(function (a, b) {
      return parseInt(a.value, 10) - parseInt(b.value, 10);
    });

    for (var i = 0; i < points.length; i += 1) {
      var _points$i = points[i],
          point = _points$i.point,
          value = _points$i.value;

      if (value <= window.innerWidth) {
        breakpoint = point;
      }
    }

    return breakpoint || 'max';
  }

  var breakpoints = {
    setBreakpoint: setBreakpoint,
    getBreakpoint: getBreakpoints
  };

  function addClasses() {
    var swiper = this;
    var classNames = swiper.classNames,
        params = swiper.params,
        rtl = swiper.rtl,
        $el = swiper.$el,
        device = swiper.device;
    var suffixes = [];
    suffixes.push('initialized');
    suffixes.push(params.direction);

    if (params.freeMode) {
      suffixes.push('free-mode');
    }

    if (params.autoHeight) {
      suffixes.push('autoheight');
    }

    if (rtl) {
      suffixes.push('rtl');
    }

    if (params.slidesPerColumn > 1) {
      suffixes.push('multirow');

      if (params.slidesPerColumnFill === 'column') {
        suffixes.push('multirow-column');
      }
    }

    if (device.android) {
      suffixes.push('android');
    }

    if (device.ios) {
      suffixes.push('ios');
    }

    if (params.cssMode) {
      suffixes.push('css-mode');
    }

    suffixes.forEach(function (suffix) {
      classNames.push(params.containerModifierClass + suffix);
    });
    $el.addClass(classNames.join(' '));
    swiper.emitContainerClasses();
  }

  function removeClasses() {
    var swiper = this;
    var $el = swiper.$el,
        classNames = swiper.classNames;
    $el.removeClass(classNames.join(' '));
    swiper.emitContainerClasses();
  }

  var classes = {
    addClasses: addClasses,
    removeClasses: removeClasses
  };

  function loadImage(imageEl, src, srcset, sizes, checkForComplete, callback) {
    var window = getWindow();
    var image;

    function onReady() {
      if (callback) callback();
    }

    var isPicture = $(imageEl).parent('picture')[0];

    if (!isPicture && (!imageEl.complete || !checkForComplete)) {
      if (src) {
        image = new window.Image();
        image.onload = onReady;
        image.onerror = onReady;

        if (sizes) {
          image.sizes = sizes;
        }

        if (srcset) {
          image.srcset = srcset;
        }

        if (src) {
          image.src = src;
        }
      } else {
        onReady();
      }
    } else {
      // image already loaded...
      onReady();
    }
  }

  function preloadImages() {
    var swiper = this;
    swiper.imagesToLoad = swiper.$el.find('img');

    function onReady() {
      if (typeof swiper === 'undefined' || swiper === null || !swiper || swiper.destroyed) return;
      if (swiper.imagesLoaded !== undefined) swiper.imagesLoaded += 1;

      if (swiper.imagesLoaded === swiper.imagesToLoad.length) {
        if (swiper.params.updateOnImagesReady) swiper.update();
        swiper.emit('imagesReady');
      }
    }

    for (var i = 0; i < swiper.imagesToLoad.length; i += 1) {
      var imageEl = swiper.imagesToLoad[i];
      swiper.loadImage(imageEl, imageEl.currentSrc || imageEl.getAttribute('src'), imageEl.srcset || imageEl.getAttribute('srcset'), imageEl.sizes || imageEl.getAttribute('sizes'), true, onReady);
    }
  }

  var images = {
    loadImage: loadImage,
    preloadImages: preloadImages
  };

  function checkOverflow() {
    var swiper = this;
    var params = swiper.params;
    var wasLocked = swiper.isLocked;
    var lastSlidePosition = swiper.slides.length > 0 && params.slidesOffsetBefore + params.spaceBetween * (swiper.slides.length - 1) + swiper.slides[0].offsetWidth * swiper.slides.length;

    if (params.slidesOffsetBefore && params.slidesOffsetAfter && lastSlidePosition) {
      swiper.isLocked = lastSlidePosition <= swiper.size;
    } else {
      swiper.isLocked = swiper.snapGrid.length === 1;
    }

    swiper.allowSlideNext = !swiper.isLocked;
    swiper.allowSlidePrev = !swiper.isLocked; // events

    if (wasLocked !== swiper.isLocked) swiper.emit(swiper.isLocked ? 'lock' : 'unlock');

    if (wasLocked && wasLocked !== swiper.isLocked) {
      swiper.isEnd = false;
      if (swiper.navigation) swiper.navigation.update();
    }
  }

  var checkOverflow$1 = {
    checkOverflow: checkOverflow
  };

  var defaults = {
    init: true,
    direction: 'horizontal',
    touchEventsTarget: 'container',
    initialSlide: 0,
    speed: 300,
    cssMode: false,
    updateOnWindowResize: true,
    nested: false,
    // Overrides
    width: null,
    height: null,
    //
    preventInteractionOnTransition: false,
    // ssr
    userAgent: null,
    url: null,
    // To support iOS's swipe-to-go-back gesture (when being used in-app).
    edgeSwipeDetection: false,
    edgeSwipeThreshold: 20,
    // Free mode
    freeMode: false,
    freeModeMomentum: true,
    freeModeMomentumRatio: 1,
    freeModeMomentumBounce: true,
    freeModeMomentumBounceRatio: 1,
    freeModeMomentumVelocityRatio: 1,
    freeModeSticky: false,
    freeModeMinimumVelocity: 0.02,
    // Autoheight
    autoHeight: false,
    // Set wrapper width
    setWrapperSize: false,
    // Virtual Translate
    virtualTranslate: false,
    // Effects
    effect: 'slide',
    // 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
    // Breakpoints
    breakpoints: undefined,
    // Slides grid
    spaceBetween: 0,
    slidesPerView: 1,
    slidesPerColumn: 1,
    slidesPerColumnFill: 'column',
    slidesPerGroup: 1,
    slidesPerGroupSkip: 0,
    centeredSlides: false,
    centeredSlidesBounds: false,
    slidesOffsetBefore: 0,
    // in px
    slidesOffsetAfter: 0,
    // in px
    normalizeSlideIndex: true,
    centerInsufficientSlides: false,
    // Disable swiper and hide navigation when container not overflow
    watchOverflow: false,
    // Round length
    roundLengths: false,
    // Touches
    touchRatio: 1,
    touchAngle: 45,
    simulateTouch: true,
    shortSwipes: true,
    longSwipes: true,
    longSwipesRatio: 0.5,
    longSwipesMs: 300,
    followFinger: true,
    allowTouchMove: true,
    threshold: 0,
    touchMoveStopPropagation: false,
    touchStartPreventDefault: true,
    touchStartForcePreventDefault: false,
    touchReleaseOnEdges: false,
    // Unique Navigation Elements
    uniqueNavElements: true,
    // Resistance
    resistance: true,
    resistanceRatio: 0.85,
    // Progress
    watchSlidesProgress: false,
    watchSlidesVisibility: false,
    // Cursor
    grabCursor: false,
    // Clicks
    preventClicks: true,
    preventClicksPropagation: true,
    slideToClickedSlide: false,
    // Images
    preloadImages: true,
    updateOnImagesReady: true,
    // loop
    loop: false,
    loopAdditionalSlides: 0,
    loopedSlides: null,
    loopFillGroupWithBlank: false,
    loopPreventsSlide: true,
    // Swiping/no swiping
    allowSlidePrev: true,
    allowSlideNext: true,
    swipeHandler: null,
    // '.swipe-handler',
    noSwiping: true,
    noSwipingClass: 'swiper-no-swiping',
    noSwipingSelector: null,
    // Passive Listeners
    passiveListeners: true,
    // NS
    containerModifierClass: 'swiper-container-',
    // NEW
    slideClass: 'swiper-slide',
    slideBlankClass: 'swiper-slide-invisible-blank',
    slideActiveClass: 'swiper-slide-active',
    slideDuplicateActiveClass: 'swiper-slide-duplicate-active',
    slideVisibleClass: 'swiper-slide-visible',
    slideDuplicateClass: 'swiper-slide-duplicate',
    slideNextClass: 'swiper-slide-next',
    slideDuplicateNextClass: 'swiper-slide-duplicate-next',
    slidePrevClass: 'swiper-slide-prev',
    slideDuplicatePrevClass: 'swiper-slide-duplicate-prev',
    wrapperClass: 'swiper-wrapper',
    // Callbacks
    runCallbacksOnInit: true,
    // Internals
    _emitClasses: false
  };

  function _defineProperties$1(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$1(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$1(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$1(Constructor, staticProps);
    return Constructor;
  }
  var prototypes = {
    modular: modular,
    eventsEmitter: eventsEmitter,
    update: update,
    translate: translate,
    transition: transition$1,
    slide: slide,
    loop: loop,
    grabCursor: grabCursor,
    manipulation: manipulation,
    events: events,
    breakpoints: breakpoints,
    checkOverflow: checkOverflow$1,
    classes: classes,
    images: images
  };
  var extendedDefaults = {};

  var Swiper = /*#__PURE__*/function () {
    function Swiper() {
      var el;
      var params;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      if (args.length === 1 && args[0].constructor && args[0].constructor === Object) {
        params = args[0];
      } else {
        el = args[0];
        params = args[1];
      }

      if (!params) params = {};
      params = extend$1({}, params);
      if (el && !params.el) params.el = el; // Swiper Instance

      var swiper = this;
      swiper.support = getSupport();
      swiper.device = getDevice({
        userAgent: params.userAgent
      });
      swiper.browser = getBrowser();
      swiper.eventsListeners = {};
      swiper.eventsAnyListeners = [];

      if (typeof swiper.modules === 'undefined') {
        swiper.modules = {};
      }

      Object.keys(swiper.modules).forEach(function (moduleName) {
        var module = swiper.modules[moduleName];

        if (module.params) {
          var moduleParamName = Object.keys(module.params)[0];
          var moduleParams = module.params[moduleParamName];
          if (_typeof(moduleParams) !== 'object' || moduleParams === null) return;
          if (!(moduleParamName in params && 'enabled' in moduleParams)) return;

          if (params[moduleParamName] === true) {
            params[moduleParamName] = {
              enabled: true
            };
          }

          if (_typeof(params[moduleParamName]) === 'object' && !('enabled' in params[moduleParamName])) {
            params[moduleParamName].enabled = true;
          }

          if (!params[moduleParamName]) params[moduleParamName] = {
            enabled: false
          };
        }
      }); // Extend defaults with modules params

      var swiperParams = extend$1({}, defaults);
      swiper.useParams(swiperParams); // Extend defaults with passed params

      swiper.params = extend$1({}, swiperParams, extendedDefaults, params);
      swiper.originalParams = extend$1({}, swiper.params);
      swiper.passedParams = extend$1({}, params); // add event listeners

      if (swiper.params && swiper.params.on) {
        Object.keys(swiper.params.on).forEach(function (eventName) {
          swiper.on(eventName, swiper.params.on[eventName]);
        });
      }

      if (swiper.params && swiper.params.onAny) {
        swiper.onAny(swiper.params.onAny);
      } // Save Dom lib


      swiper.$ = $; // Find el

      var $el = $(swiper.params.el);
      el = $el[0];

      if (!el) {
        return undefined;
      }

      if ($el.length > 1) {
        var swipers = [];
        $el.each(function (containerEl) {
          var newParams = extend$1({}, params, {
            el: containerEl
          });
          swipers.push(new Swiper(newParams));
        });
        return swipers;
      }

      el.swiper = swiper; // Find Wrapper

      var $wrapperEl;

      if (el && el.shadowRoot && el.shadowRoot.querySelector) {
        $wrapperEl = $(el.shadowRoot.querySelector("." + swiper.params.wrapperClass)); // Children needs to return slot items

        $wrapperEl.children = function (options) {
          return $el.children(options);
        };
      } else {
        $wrapperEl = $el.children("." + swiper.params.wrapperClass);
      } // Extend Swiper


      extend$1(swiper, {
        $el: $el,
        el: el,
        $wrapperEl: $wrapperEl,
        wrapperEl: $wrapperEl[0],
        // Classes
        classNames: [],
        // Slides
        slides: $(),
        slidesGrid: [],
        snapGrid: [],
        slidesSizesGrid: [],
        // isDirection
        isHorizontal: function isHorizontal() {
          return swiper.params.direction === 'horizontal';
        },
        isVertical: function isVertical() {
          return swiper.params.direction === 'vertical';
        },
        // RTL
        rtl: el.dir.toLowerCase() === 'rtl' || $el.css('direction') === 'rtl',
        rtlTranslate: swiper.params.direction === 'horizontal' && (el.dir.toLowerCase() === 'rtl' || $el.css('direction') === 'rtl'),
        wrongRTL: $wrapperEl.css('display') === '-webkit-box',
        // Indexes
        activeIndex: 0,
        realIndex: 0,
        //
        isBeginning: true,
        isEnd: false,
        // Props
        translate: 0,
        previousTranslate: 0,
        progress: 0,
        velocity: 0,
        animating: false,
        // Locks
        allowSlideNext: swiper.params.allowSlideNext,
        allowSlidePrev: swiper.params.allowSlidePrev,
        // Touch Events
        touchEvents: function touchEvents() {
          var touch = ['touchstart', 'touchmove', 'touchend', 'touchcancel'];
          var desktop = ['mousedown', 'mousemove', 'mouseup'];

          if (swiper.support.pointerEvents) {
            desktop = ['pointerdown', 'pointermove', 'pointerup'];
          }

          swiper.touchEventsTouch = {
            start: touch[0],
            move: touch[1],
            end: touch[2],
            cancel: touch[3]
          };
          swiper.touchEventsDesktop = {
            start: desktop[0],
            move: desktop[1],
            end: desktop[2]
          };
          return swiper.support.touch || !swiper.params.simulateTouch ? swiper.touchEventsTouch : swiper.touchEventsDesktop;
        }(),
        touchEventsData: {
          isTouched: undefined,
          isMoved: undefined,
          allowTouchCallbacks: undefined,
          touchStartTime: undefined,
          isScrolling: undefined,
          currentTranslate: undefined,
          startTranslate: undefined,
          allowThresholdMove: undefined,
          // Form elements to match
          formElements: 'input, select, option, textarea, button, video, label',
          // Last click time
          lastClickTime: now(),
          clickTimeout: undefined,
          // Velocities
          velocities: [],
          allowMomentumBounce: undefined,
          isTouchEvent: undefined,
          startMoving: undefined
        },
        // Clicks
        allowClick: true,
        // Touches
        allowTouchMove: swiper.params.allowTouchMove,
        touches: {
          startX: 0,
          startY: 0,
          currentX: 0,
          currentY: 0,
          diff: 0
        },
        // Images
        imagesToLoad: [],
        imagesLoaded: 0
      }); // Install Modules

      swiper.useModules();
      swiper.emit('_swiper'); // Init

      if (swiper.params.init) {
        swiper.init();
      } // Return app instance


      return swiper;
    }

    var _proto = Swiper.prototype;

    _proto.emitContainerClasses = function emitContainerClasses() {
      var swiper = this;
      if (!swiper.params._emitClasses || !swiper.el) return;
      var classes = swiper.el.className.split(' ').filter(function (className) {
        return className.indexOf('swiper-container') === 0 || className.indexOf(swiper.params.containerModifierClass) === 0;
      });
      swiper.emit('_containerClasses', classes.join(' '));
    };

    _proto.getSlideClasses = function getSlideClasses(slideEl) {
      var swiper = this;
      return slideEl.className.split(' ').filter(function (className) {
        return className.indexOf('swiper-slide') === 0 || className.indexOf(swiper.params.slideClass) === 0;
      }).join(' ');
    };

    _proto.emitSlidesClasses = function emitSlidesClasses() {
      var swiper = this;
      if (!swiper.params._emitClasses || !swiper.el) return;
      swiper.slides.each(function (slideEl) {
        var classNames = swiper.getSlideClasses(slideEl);
        swiper.emit('_slideClass', slideEl, classNames);
      });
    };

    _proto.slidesPerViewDynamic = function slidesPerViewDynamic() {
      var swiper = this;
      var params = swiper.params,
          slides = swiper.slides,
          slidesGrid = swiper.slidesGrid,
          swiperSize = swiper.size,
          activeIndex = swiper.activeIndex;
      var spv = 1;

      if (params.centeredSlides) {
        var slideSize = slides[activeIndex].swiperSlideSize;
        var breakLoop;

        for (var i = activeIndex + 1; i < slides.length; i += 1) {
          if (slides[i] && !breakLoop) {
            slideSize += slides[i].swiperSlideSize;
            spv += 1;
            if (slideSize > swiperSize) breakLoop = true;
          }
        }

        for (var _i = activeIndex - 1; _i >= 0; _i -= 1) {
          if (slides[_i] && !breakLoop) {
            slideSize += slides[_i].swiperSlideSize;
            spv += 1;
            if (slideSize > swiperSize) breakLoop = true;
          }
        }
      } else {
        for (var _i2 = activeIndex + 1; _i2 < slides.length; _i2 += 1) {
          if (slidesGrid[_i2] - slidesGrid[activeIndex] < swiperSize) {
            spv += 1;
          }
        }
      }

      return spv;
    };

    _proto.update = function update() {
      var swiper = this;
      if (!swiper || swiper.destroyed) return;
      var snapGrid = swiper.snapGrid,
          params = swiper.params; // Breakpoints

      if (params.breakpoints) {
        swiper.setBreakpoint();
      }

      swiper.updateSize();
      swiper.updateSlides();
      swiper.updateProgress();
      swiper.updateSlidesClasses();

      function setTranslate() {
        var translateValue = swiper.rtlTranslate ? swiper.translate * -1 : swiper.translate;
        var newTranslate = Math.min(Math.max(translateValue, swiper.maxTranslate()), swiper.minTranslate());
        swiper.setTranslate(newTranslate);
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
      }

      var translated;

      if (swiper.params.freeMode) {
        setTranslate();

        if (swiper.params.autoHeight) {
          swiper.updateAutoHeight();
        }
      } else {
        if ((swiper.params.slidesPerView === 'auto' || swiper.params.slidesPerView > 1) && swiper.isEnd && !swiper.params.centeredSlides) {
          translated = swiper.slideTo(swiper.slides.length - 1, 0, false, true);
        } else {
          translated = swiper.slideTo(swiper.activeIndex, 0, false, true);
        }

        if (!translated) {
          setTranslate();
        }
      }

      if (params.watchOverflow && snapGrid !== swiper.snapGrid) {
        swiper.checkOverflow();
      }

      swiper.emit('update');
    };

    _proto.changeDirection = function changeDirection(newDirection, needUpdate) {
      if (needUpdate === void 0) {
        needUpdate = true;
      }

      var swiper = this;
      var currentDirection = swiper.params.direction;

      if (!newDirection) {
        // eslint-disable-next-line
        newDirection = currentDirection === 'horizontal' ? 'vertical' : 'horizontal';
      }

      if (newDirection === currentDirection || newDirection !== 'horizontal' && newDirection !== 'vertical') {
        return swiper;
      }

      swiper.$el.removeClass("" + swiper.params.containerModifierClass + currentDirection).addClass("" + swiper.params.containerModifierClass + newDirection);
      swiper.emitContainerClasses();
      swiper.params.direction = newDirection;
      swiper.slides.each(function (slideEl) {
        if (newDirection === 'vertical') {
          slideEl.style.width = '';
        } else {
          slideEl.style.height = '';
        }
      });
      swiper.emit('changeDirection');
      if (needUpdate) swiper.update();
      return swiper;
    };

    _proto.init = function init() {
      var swiper = this;
      if (swiper.initialized) return;
      swiper.emit('beforeInit'); // Set breakpoint

      if (swiper.params.breakpoints) {
        swiper.setBreakpoint();
      } // Add Classes


      swiper.addClasses(); // Create loop

      if (swiper.params.loop) {
        swiper.loopCreate();
      } // Update size


      swiper.updateSize(); // Update slides

      swiper.updateSlides();

      if (swiper.params.watchOverflow) {
        swiper.checkOverflow();
      } // Set Grab Cursor


      if (swiper.params.grabCursor) {
        swiper.setGrabCursor();
      }

      if (swiper.params.preloadImages) {
        swiper.preloadImages();
      } // Slide To Initial Slide


      if (swiper.params.loop) {
        swiper.slideTo(swiper.params.initialSlide + swiper.loopedSlides, 0, swiper.params.runCallbacksOnInit);
      } else {
        swiper.slideTo(swiper.params.initialSlide, 0, swiper.params.runCallbacksOnInit);
      } // Attach events


      swiper.attachEvents(); // Init Flag

      swiper.initialized = true; // Emit

      swiper.emit('init');
      swiper.emit('afterInit');
    };

    _proto.destroy = function destroy(deleteInstance, cleanStyles) {
      if (deleteInstance === void 0) {
        deleteInstance = true;
      }

      if (cleanStyles === void 0) {
        cleanStyles = true;
      }

      var swiper = this;
      var params = swiper.params,
          $el = swiper.$el,
          $wrapperEl = swiper.$wrapperEl,
          slides = swiper.slides;

      if (typeof swiper.params === 'undefined' || swiper.destroyed) {
        return null;
      }

      swiper.emit('beforeDestroy'); // Init Flag

      swiper.initialized = false; // Detach events

      swiper.detachEvents(); // Destroy loop

      if (params.loop) {
        swiper.loopDestroy();
      } // Cleanup styles


      if (cleanStyles) {
        swiper.removeClasses();
        $el.removeAttr('style');
        $wrapperEl.removeAttr('style');

        if (slides && slides.length) {
          slides.removeClass([params.slideVisibleClass, params.slideActiveClass, params.slideNextClass, params.slidePrevClass].join(' ')).removeAttr('style').removeAttr('data-swiper-slide-index');
        }
      }

      swiper.emit('destroy'); // Detach emitter events

      Object.keys(swiper.eventsListeners).forEach(function (eventName) {
        swiper.off(eventName);
      });

      if (deleteInstance !== false) {
        swiper.$el[0].swiper = null;
        deleteProps(swiper);
      }

      swiper.destroyed = true;
      return null;
    };

    Swiper.extendDefaults = function extendDefaults(newDefaults) {
      extend$1(extendedDefaults, newDefaults);
    };

    Swiper.installModule = function installModule(module) {
      if (!Swiper.prototype.modules) Swiper.prototype.modules = {};
      var name = module.name || Object.keys(Swiper.prototype.modules).length + "_" + now();
      Swiper.prototype.modules[name] = module;
    };

    Swiper.use = function use(module) {
      if (Array.isArray(module)) {
        module.forEach(function (m) {
          return Swiper.installModule(m);
        });
        return Swiper;
      }

      Swiper.installModule(module);
      return Swiper;
    };

    _createClass$1(Swiper, null, [{
      key: "extendedDefaults",
      get: function get() {
        return extendedDefaults;
      }
    }, {
      key: "defaults",
      get: function get() {
        return defaults;
      }
    }]);

    return Swiper;
  }();

  Object.keys(prototypes).forEach(function (prototypeGroup) {
    Object.keys(prototypes[prototypeGroup]).forEach(function (protoMethod) {
      Swiper.prototype[protoMethod] = prototypes[prototypeGroup][protoMethod];
    });
  });
  Swiper.use([Resize, Observer$1]);

  function _extends$1() {
    _extends$1 = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends$1.apply(this, arguments);
  }
  var Navigation = {
    update: function update() {
      // Update Navigation Buttons
      var swiper = this;
      var params = swiper.params.navigation;
      if (swiper.params.loop) return;
      var _swiper$navigation = swiper.navigation,
          $nextEl = _swiper$navigation.$nextEl,
          $prevEl = _swiper$navigation.$prevEl;

      if ($prevEl && $prevEl.length > 0) {
        if (swiper.isBeginning) {
          $prevEl.addClass(params.disabledClass);
        } else {
          $prevEl.removeClass(params.disabledClass);
        }

        $prevEl[swiper.params.watchOverflow && swiper.isLocked ? 'addClass' : 'removeClass'](params.lockClass);
      }

      if ($nextEl && $nextEl.length > 0) {
        if (swiper.isEnd) {
          $nextEl.addClass(params.disabledClass);
        } else {
          $nextEl.removeClass(params.disabledClass);
        }

        $nextEl[swiper.params.watchOverflow && swiper.isLocked ? 'addClass' : 'removeClass'](params.lockClass);
      }
    },
    onPrevClick: function onPrevClick(e) {
      var swiper = this;
      e.preventDefault();
      if (swiper.isBeginning && !swiper.params.loop) return;
      swiper.slidePrev();
    },
    onNextClick: function onNextClick(e) {
      var swiper = this;
      e.preventDefault();
      if (swiper.isEnd && !swiper.params.loop) return;
      swiper.slideNext();
    },
    init: function init() {
      var swiper = this;
      var params = swiper.params.navigation;
      if (!(params.nextEl || params.prevEl)) return;
      var $nextEl;
      var $prevEl;

      if (params.nextEl) {
        $nextEl = $(params.nextEl);

        if (swiper.params.uniqueNavElements && typeof params.nextEl === 'string' && $nextEl.length > 1 && swiper.$el.find(params.nextEl).length === 1) {
          $nextEl = swiper.$el.find(params.nextEl);
        }
      }

      if (params.prevEl) {
        $prevEl = $(params.prevEl);

        if (swiper.params.uniqueNavElements && typeof params.prevEl === 'string' && $prevEl.length > 1 && swiper.$el.find(params.prevEl).length === 1) {
          $prevEl = swiper.$el.find(params.prevEl);
        }
      }

      if ($nextEl && $nextEl.length > 0) {
        $nextEl.on('click', swiper.navigation.onNextClick);
      }

      if ($prevEl && $prevEl.length > 0) {
        $prevEl.on('click', swiper.navigation.onPrevClick);
      }

      extend$1(swiper.navigation, {
        $nextEl: $nextEl,
        nextEl: $nextEl && $nextEl[0],
        $prevEl: $prevEl,
        prevEl: $prevEl && $prevEl[0]
      });
    },
    destroy: function destroy() {
      var swiper = this;
      var _swiper$navigation2 = swiper.navigation,
          $nextEl = _swiper$navigation2.$nextEl,
          $prevEl = _swiper$navigation2.$prevEl;

      if ($nextEl && $nextEl.length) {
        $nextEl.off('click', swiper.navigation.onNextClick);
        $nextEl.removeClass(swiper.params.navigation.disabledClass);
      }

      if ($prevEl && $prevEl.length) {
        $prevEl.off('click', swiper.navigation.onPrevClick);
        $prevEl.removeClass(swiper.params.navigation.disabledClass);
      }
    }
  };
  var Navigation$1 = {
    name: 'navigation',
    params: {
      navigation: {
        nextEl: null,
        prevEl: null,
        hideOnClick: false,
        disabledClass: 'swiper-button-disabled',
        hiddenClass: 'swiper-button-hidden',
        lockClass: 'swiper-button-lock'
      }
    },
    create: function create() {
      var swiper = this;
      bindModuleMethods(swiper, {
        navigation: _extends$1({}, Navigation)
      });
    },
    on: {
      init: function init(swiper) {
        swiper.navigation.init();
        swiper.navigation.update();
      },
      toEdge: function toEdge(swiper) {
        swiper.navigation.update();
      },
      fromEdge: function fromEdge(swiper) {
        swiper.navigation.update();
      },
      destroy: function destroy(swiper) {
        swiper.navigation.destroy();
      },
      click: function click(swiper, e) {
        var _swiper$navigation3 = swiper.navigation,
            $nextEl = _swiper$navigation3.$nextEl,
            $prevEl = _swiper$navigation3.$prevEl;

        if (swiper.params.navigation.hideOnClick && !$(e.target).is($prevEl) && !$(e.target).is($nextEl)) {
          var isHidden;

          if ($nextEl) {
            isHidden = $nextEl.hasClass(swiper.params.navigation.hiddenClass);
          } else if ($prevEl) {
            isHidden = $prevEl.hasClass(swiper.params.navigation.hiddenClass);
          }

          if (isHidden === true) {
            swiper.emit('navigationShow');
          } else {
            swiper.emit('navigationHide');
          }

          if ($nextEl) {
            $nextEl.toggleClass(swiper.params.navigation.hiddenClass);
          }

          if ($prevEl) {
            $prevEl.toggleClass(swiper.params.navigation.hiddenClass);
          }
        }
      }
    }
  };

  function _extends$2() {
    _extends$2 = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends$2.apply(this, arguments);
  }
  var Scrollbar = {
    setTranslate: function setTranslate() {
      var swiper = this;
      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
      var scrollbar = swiper.scrollbar,
          rtl = swiper.rtlTranslate,
          progress = swiper.progress;
      var dragSize = scrollbar.dragSize,
          trackSize = scrollbar.trackSize,
          $dragEl = scrollbar.$dragEl,
          $el = scrollbar.$el;
      var params = swiper.params.scrollbar;
      var newSize = dragSize;
      var newPos = (trackSize - dragSize) * progress;

      if (rtl) {
        newPos = -newPos;

        if (newPos > 0) {
          newSize = dragSize - newPos;
          newPos = 0;
        } else if (-newPos + dragSize > trackSize) {
          newSize = trackSize + newPos;
        }
      } else if (newPos < 0) {
        newSize = dragSize + newPos;
        newPos = 0;
      } else if (newPos + dragSize > trackSize) {
        newSize = trackSize - newPos;
      }

      if (swiper.isHorizontal()) {
        $dragEl.transform("translate3d(" + newPos + "px, 0, 0)");
        $dragEl[0].style.width = newSize + "px";
      } else {
        $dragEl.transform("translate3d(0px, " + newPos + "px, 0)");
        $dragEl[0].style.height = newSize + "px";
      }

      if (params.hide) {
        clearTimeout(swiper.scrollbar.timeout);
        $el[0].style.opacity = 1;
        swiper.scrollbar.timeout = setTimeout(function () {
          $el[0].style.opacity = 0;
          $el.transition(400);
        }, 1000);
      }
    },
    setTransition: function setTransition(duration) {
      var swiper = this;
      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
      swiper.scrollbar.$dragEl.transition(duration);
    },
    updateSize: function updateSize() {
      var swiper = this;
      if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
      var scrollbar = swiper.scrollbar;
      var $dragEl = scrollbar.$dragEl,
          $el = scrollbar.$el;
      $dragEl[0].style.width = '';
      $dragEl[0].style.height = '';
      var trackSize = swiper.isHorizontal() ? $el[0].offsetWidth : $el[0].offsetHeight;
      var divider = swiper.size / swiper.virtualSize;
      var moveDivider = divider * (trackSize / swiper.size);
      var dragSize;

      if (swiper.params.scrollbar.dragSize === 'auto') {
        dragSize = trackSize * divider;
      } else {
        dragSize = parseInt(swiper.params.scrollbar.dragSize, 10);
      }

      if (swiper.isHorizontal()) {
        $dragEl[0].style.width = dragSize + "px";
      } else {
        $dragEl[0].style.height = dragSize + "px";
      }

      if (divider >= 1) {
        $el[0].style.display = 'none';
      } else {
        $el[0].style.display = '';
      }

      if (swiper.params.scrollbar.hide) {
        $el[0].style.opacity = 0;
      }

      extend$1(scrollbar, {
        trackSize: trackSize,
        divider: divider,
        moveDivider: moveDivider,
        dragSize: dragSize
      });
      scrollbar.$el[swiper.params.watchOverflow && swiper.isLocked ? 'addClass' : 'removeClass'](swiper.params.scrollbar.lockClass);
    },
    getPointerPosition: function getPointerPosition(e) {
      var swiper = this;

      if (swiper.isHorizontal()) {
        return e.type === 'touchstart' || e.type === 'touchmove' ? e.targetTouches[0].clientX : e.clientX;
      }

      return e.type === 'touchstart' || e.type === 'touchmove' ? e.targetTouches[0].clientY : e.clientY;
    },
    setDragPosition: function setDragPosition(e) {
      var swiper = this;
      var scrollbar = swiper.scrollbar,
          rtl = swiper.rtlTranslate;
      var $el = scrollbar.$el,
          dragSize = scrollbar.dragSize,
          trackSize = scrollbar.trackSize,
          dragStartPos = scrollbar.dragStartPos;
      var positionRatio;
      positionRatio = (scrollbar.getPointerPosition(e) - $el.offset()[swiper.isHorizontal() ? 'left' : 'top'] - (dragStartPos !== null ? dragStartPos : dragSize / 2)) / (trackSize - dragSize);
      positionRatio = Math.max(Math.min(positionRatio, 1), 0);

      if (rtl) {
        positionRatio = 1 - positionRatio;
      }

      var position = swiper.minTranslate() + (swiper.maxTranslate() - swiper.minTranslate()) * positionRatio;
      swiper.updateProgress(position);
      swiper.setTranslate(position);
      swiper.updateActiveIndex();
      swiper.updateSlidesClasses();
    },
    onDragStart: function onDragStart(e) {
      var swiper = this;
      var params = swiper.params.scrollbar;
      var scrollbar = swiper.scrollbar,
          $wrapperEl = swiper.$wrapperEl;
      var $el = scrollbar.$el,
          $dragEl = scrollbar.$dragEl;
      swiper.scrollbar.isTouched = true;
      swiper.scrollbar.dragStartPos = e.target === $dragEl[0] || e.target === $dragEl ? scrollbar.getPointerPosition(e) - e.target.getBoundingClientRect()[swiper.isHorizontal() ? 'left' : 'top'] : null;
      e.preventDefault();
      e.stopPropagation();
      $wrapperEl.transition(100);
      $dragEl.transition(100);
      scrollbar.setDragPosition(e);
      clearTimeout(swiper.scrollbar.dragTimeout);
      $el.transition(0);

      if (params.hide) {
        $el.css('opacity', 1);
      }

      if (swiper.params.cssMode) {
        swiper.$wrapperEl.css('scroll-snap-type', 'none');
      }

      swiper.emit('scrollbarDragStart', e);
    },
    onDragMove: function onDragMove(e) {
      var swiper = this;
      var scrollbar = swiper.scrollbar,
          $wrapperEl = swiper.$wrapperEl;
      var $el = scrollbar.$el,
          $dragEl = scrollbar.$dragEl;
      if (!swiper.scrollbar.isTouched) return;
      if (e.preventDefault) e.preventDefault();else e.returnValue = false;
      scrollbar.setDragPosition(e);
      $wrapperEl.transition(0);
      $el.transition(0);
      $dragEl.transition(0);
      swiper.emit('scrollbarDragMove', e);
    },
    onDragEnd: function onDragEnd(e) {
      var swiper = this;
      var params = swiper.params.scrollbar;
      var scrollbar = swiper.scrollbar,
          $wrapperEl = swiper.$wrapperEl;
      var $el = scrollbar.$el;
      if (!swiper.scrollbar.isTouched) return;
      swiper.scrollbar.isTouched = false;

      if (swiper.params.cssMode) {
        swiper.$wrapperEl.css('scroll-snap-type', '');
        $wrapperEl.transition('');
      }

      if (params.hide) {
        clearTimeout(swiper.scrollbar.dragTimeout);
        swiper.scrollbar.dragTimeout = nextTick(function () {
          $el.css('opacity', 0);
          $el.transition(400);
        }, 1000);
      }

      swiper.emit('scrollbarDragEnd', e);

      if (params.snapOnRelease) {
        swiper.slideToClosest();
      }
    },
    enableDraggable: function enableDraggable() {
      var swiper = this;
      if (!swiper.params.scrollbar.el) return;
      var document = getDocument();
      var scrollbar = swiper.scrollbar,
          touchEventsTouch = swiper.touchEventsTouch,
          touchEventsDesktop = swiper.touchEventsDesktop,
          params = swiper.params,
          support = swiper.support;
      var $el = scrollbar.$el;
      var target = $el[0];
      var activeListener = support.passiveListener && params.passiveListeners ? {
        passive: false,
        capture: false
      } : false;
      var passiveListener = support.passiveListener && params.passiveListeners ? {
        passive: true,
        capture: false
      } : false;

      if (!support.touch) {
        target.addEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
        document.addEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
        document.addEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener);
      } else {
        target.addEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
        target.addEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
        target.addEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener);
      }
    },
    disableDraggable: function disableDraggable() {
      var swiper = this;
      if (!swiper.params.scrollbar.el) return;
      var document = getDocument();
      var scrollbar = swiper.scrollbar,
          touchEventsTouch = swiper.touchEventsTouch,
          touchEventsDesktop = swiper.touchEventsDesktop,
          params = swiper.params,
          support = swiper.support;
      var $el = scrollbar.$el;
      var target = $el[0];
      var activeListener = support.passiveListener && params.passiveListeners ? {
        passive: false,
        capture: false
      } : false;
      var passiveListener = support.passiveListener && params.passiveListeners ? {
        passive: true,
        capture: false
      } : false;

      if (!support.touch) {
        target.removeEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
        document.removeEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
        document.removeEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener);
      } else {
        target.removeEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
        target.removeEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
        target.removeEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener);
      }
    },
    init: function init() {
      var swiper = this;
      if (!swiper.params.scrollbar.el) return;
      var scrollbar = swiper.scrollbar,
          $swiperEl = swiper.$el;
      var params = swiper.params.scrollbar;
      var $el = $(params.el);

      if (swiper.params.uniqueNavElements && typeof params.el === 'string' && $el.length > 1 && $swiperEl.find(params.el).length === 1) {
        $el = $swiperEl.find(params.el);
      }

      var $dragEl = $el.find("." + swiper.params.scrollbar.dragClass);

      if ($dragEl.length === 0) {
        $dragEl = $("<div class=\"" + swiper.params.scrollbar.dragClass + "\"></div>");
        $el.append($dragEl);
      }

      extend$1(scrollbar, {
        $el: $el,
        el: $el[0],
        $dragEl: $dragEl,
        dragEl: $dragEl[0]
      });

      if (params.draggable) {
        scrollbar.enableDraggable();
      }
    },
    destroy: function destroy() {
      var swiper = this;
      swiper.scrollbar.disableDraggable();
    }
  };
  var Scrollbar$1 = {
    name: 'scrollbar',
    params: {
      scrollbar: {
        el: null,
        dragSize: 'auto',
        hide: false,
        draggable: false,
        snapOnRelease: true,
        lockClass: 'swiper-scrollbar-lock',
        dragClass: 'swiper-scrollbar-drag'
      }
    },
    create: function create() {
      var swiper = this;
      bindModuleMethods(swiper, {
        scrollbar: _extends$2({
          isTouched: false,
          timeout: null,
          dragTimeout: null
        }, Scrollbar)
      });
    },
    on: {
      init: function init(swiper) {
        swiper.scrollbar.init();
        swiper.scrollbar.updateSize();
        swiper.scrollbar.setTranslate();
      },
      update: function update(swiper) {
        swiper.scrollbar.updateSize();
      },
      resize: function resize(swiper) {
        swiper.scrollbar.updateSize();
      },
      observerUpdate: function observerUpdate(swiper) {
        swiper.scrollbar.updateSize();
      },
      setTranslate: function setTranslate(swiper) {
        swiper.scrollbar.setTranslate();
      },
      setTransition: function setTransition(swiper, duration) {
        swiper.scrollbar.setTransition(duration);
      },
      destroy: function destroy(swiper) {
        swiper.scrollbar.destroy();
      }
    }
  };

  /**
   * Home: Services Tabs
   */
  Swiper.use([Navigation$1]);

  var initProcessTabs = function initProcessTabs() {
    // Get tabs
    var tabsOurProcess = document.querySelectorAll('.process-menu__link');

    if (tabsOurProcess) {
      var tabsOurProcessLength;
      var i;

      (function () {
        /**
         * Clear Tabs Active Status
         */
        var clearTabsStatus = function clearTabsStatus() {
          tabsOurProcess.forEach(function (el) {
            el.classList.remove('active');
          });
        };
        /**
         * Home: Our Process Slider
         */


        var sliderProcess = new Swiper('.process-slider', {
          loop: false,
          slidesPerView: 1,
          preloadImages: false,
          lazy: true,
          autoHeight: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          }
        });
        /**
         * Slider Action
         */

        sliderProcess.on('slideChange', function () {
          var slideId = sliderProcess.activeIndex; // Clear Active Tab Status

          clearTabsStatus(); // Active Tab

          tabsOurProcess[slideId].classList.add('active');
        });
        /**
         * Tabs Action
         */

        tabsOurProcessLength = tabsOurProcess.length;

        for (i = 0; i < tabsOurProcessLength; i++) {
          tabsOurProcess[i].addEventListener('click', function (e) {
            e.preventDefault();
          });
          tabsOurProcess[i].addEventListener('mouseover', function (e) {
            e.preventDefault(); // Clear Active Tab Status

            clearTabsStatus(); // Active Tab

            var obj = e.currentTarget;
            obj.classList.add('active'); // Show Start Points

            var points = obj.dataset.index; // Slider

            sliderProcess.slideTo(points - 1);
          });
        }
      })();
    }
  };

  /**
   * Home: Services Tabs
   */
  Swiper.use([Navigation$1]);

  var initOurProcessTabs = function initOurProcessTabs() {
    // Get tabs
    var tabsOurProcess = document.querySelectorAll('.our-process-menu__link');

    if (tabsOurProcess) {
      var tabsOurProcessLength;
      var i;

      (function () {
        /**
         * Clear Tabs Active Status
         */
        var clearTabsStatus = function clearTabsStatus() {
          tabsOurProcess.forEach(function (el) {
            el.classList.remove('active');
          });
        };
        /**
         * Home: Our Process Slider
         */


        var sliderProcess = new Swiper('.our-process-menu-slider', {
          loop: false,
          slidesPerView: 1,
          preloadImages: false,
          lazy: true,
          autoHeight: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          }
        });
        /**
         * Slider Action
         */

        sliderProcess.on('slideChange', function () {
          var slideId = sliderProcess.activeIndex; // Clear Active Tab Status

          clearTabsStatus(); // Active Tab

          tabsOurProcess[slideId].classList.add('active');
        });
        /**
         * Tabs Action
         */

        tabsOurProcessLength = tabsOurProcess.length;

        for (i = 0; i < tabsOurProcessLength; i++) {
          tabsOurProcess[i].addEventListener('click', function (e) {
            e.preventDefault();
          });
          tabsOurProcess[i].addEventListener('mouseover', function (e) {
            e.preventDefault(); // Clear Active Tab Status

            clearTabsStatus(); // Active Tab

            var obj = e.currentTarget;
            obj.classList.add('active'); // Show Start Points

            var points = obj.dataset.slideindex; // Slider

            sliderProcess.slideTo(points - 1);
          });
        }
      })();
    }
  };

  /**
   * Home: Services Tabs
   */
  var initServiceTabs = function initServiceTabs() {
    // Get all tabs
    var tabsServices = document.querySelectorAll('.services-tabs__link');
    var tabsServicesContent = document.querySelectorAll('.services-content__item');

    if (tabsServices) {
      var tabsServicesLength = tabsServices.length;

      for (var i = 0; i < tabsServicesLength; i++) {
        /**
         * Set Active Status
         */
        tabsServices[i].addEventListener('mousemove', function (e) {
          e.preventDefault();
          tabsServices.forEach(function (el) {
            el.classList.remove('active');
          });
          tabsServicesContent.forEach(function (el) {
            el.classList.remove('active');
          });
          var obj = e.currentTarget;
          obj.classList.add('active');
          var link = obj.hash;

          if (link) {
            var actualTab = document.querySelector(link);

            if (actualTab) {
              actualTab.classList.add('active');
            }
          }
        });
        tabsServices[i].addEventListener('click', function (e) {
          e.preventDefault();
        });
      }
    }
  };

  /**
   * Home: Services Tabs
   */
  var initWorksTabs = function initWorksTabs() {
    // Get all tabs
    var tabsServices = document.querySelectorAll('.works-tabs-list__link');
    var tabsServicesContent = document.querySelectorAll('.works-card');

    if (tabsServices) {
      var tabsServicesLength = tabsServices.length;

      for (var i = 0; i < tabsServicesLength; i++) {
        /**
         * Set Active Status
         */
        tabsServices[i].addEventListener('mousemove', function (e) {
          tabsServices.forEach(function (el) {
            el.classList.remove('active');
          });
          tabsServicesContent.forEach(function (el) {
            el.classList.remove('active');
          });
          var obj = e.currentTarget;
          obj.classList.add('active');
          var link = obj.hash;
          var actualTab = document.querySelector(link);
          actualTab.classList.add('active');
        });
        tabsServices[i].addEventListener('click', function (e) {
          e.preventDefault();
        });
      }
    }
  };

  /**
   * Home: Testimonials Tabs
   */
  Swiper.use([Navigation$1]);

  var initTestimonialsTabs = function initTestimonialsTabs() {
    /**
     * More/Less Content
     */
    function shrinkFeedbackText() {
      var feedbackText = document.querySelectorAll('.feedback__text');

      if (feedbackText) {
        feedbackText.forEach(function (el) {
          var text = el.innerHTML;
          var textHeight = el.clientHeight;

          if (textHeight > 260) {
            el.classList.add('feedback__text-excerpt');
            var textAction = el.querySelector('.feedback-text-action');

            if (textAction) {
              textAction.addEventListener('click', function (e) {
                e.preventDefault();
                el.classList.remove('feedback__text-excerpt');
              });
            }
          }
        });
      }
    }

    shrinkFeedbackText(); // Get all tabs

    var tabsServices = document.querySelectorAll('.testimonials__link');
    var tabsServicesContent = document.querySelectorAll('.testimonials_tab-content__item');

    if (tabsServices) {
      (function () {
        var tabsServicesContentLen = tabsServicesContent.length;
        var slider = [];

        for (var i = 0; i < tabsServicesContentLen; i++) {
          // Testimonials Slider
          var testimonialsSlider = tabsServicesContent[i].querySelector('.testimonials-slider');

          if (testimonialsSlider) {
            slider[i] = new Swiper(testimonialsSlider, {
              loop: false,
              variableWidth: true,
              slidesPerView: 1,
              spaceBetween: 20,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
              },
              breakpoints: {
                768: {
                  slidesPerView: 3,
                  spaceBetween: 30
                },
                528: {
                  slidesPerView: 2,
                  spaceBetween: 20
                }
              }
            });
          }
        }

        var tabsServicesLen = tabsServices.length;

        for (var _i = 0; _i < tabsServicesLen; _i++) {
          /**
           * Set Active Status
           */
          tabsServices[_i].addEventListener('mousemove', function (e) {
            e.preventDefault();
            tabsServices.forEach(function (el) {
              el.classList.remove('active');
            });
            tabsServicesContent.forEach(function (el) {
              el.classList.remove('active');
            });
            var obj = e.currentTarget;
            obj.classList.add('active');
            var link = obj.hash;

            if (link) {
              var actualTab = document.querySelector(link);

              if (actualTab) {
                actualTab.classList.add('active'); // Check slider (Is it on home page?)

                if (slider.length > 0) {
                  // Update slider
                  slider[actualTab.dataset.tabid].update(true);
                }
              }
            }

            shrinkFeedbackText();
          });

          tabsServices[_i].addEventListener('click', function (e) {
            e.preventDefault();
          });
        }
      })();
    }
  };

  /**
   * FAQ Tabs
   */
  var tabsFAQ = function tabsFAQ() {
    // Get all tabs
    var tabsServices = document.querySelectorAll('.faq-tabs-list__link');
    var tabsServicesContent = document.querySelectorAll('.faq-tabs__card');

    if (tabsServices) {
      var tabsServicesLen = tabsServices.length;

      for (var i = 0; i < tabsServicesLen; i++) {
        /**
         * Set Active Status
         */
        tabsServices[i].addEventListener('mousemove', function (e) {
          e.preventDefault();
          tabsServices.forEach(function (el) {
            el.classList.remove('active');
          });
          tabsServicesContent.forEach(function (el) {
            el.classList.remove('active');
          });
          var obj = e.currentTarget;
          obj.classList.add('active');
          var link = obj.dataset.tabid;
          var actualTab = document.querySelectorAll("[data-tab=\"".concat(link, "\"]"));
          var actualTabLen = actualTab.length;

          for (var id = 0; id < actualTabLen; id++) {
            actualTab[id].classList.add('active');
          }
        });
        tabsServices[i].addEventListener('click', function (e) {
          e.preventDefault();
        });
      }
    }
  };

  /* KeyshapeJS v1.1.0 (c) 2018-2019 Pixofield Ltd | pixofield.com/keyshapejs/mit-license */
  window.KeyshapeJS = function () {
    function t(a) {
      return "undefined" !== typeof a;
    }

    function x(a, b) {
      return a && 0 == a.indexOf(b);
    }

    function _H(a) {
      if (!isFinite(a)) throw Error("Non-finite value");
    }

    function R(a) {
      if (14 >= a) return 16;
      var b = S[a];
      b || (b = t(ca[a]) ? 0 | (a.toLowerCase().indexOf("color") == a.length - 5 ? 48 : 0) : 1);
      return b;
    }

    function K(a) {
      return 0 <= a ? Math.pow(a, 1 / 3) : -Math.pow(-a, 1 / 3);
    }

    function da(a, b, c, d) {
      if (0 == a) return 0 == b ? b = -d / c : (a = Math.sqrt(c * c - 4 * b * d), d = (-c + a) / (2 * b), 0 <= d && 1 >= d ? b = d : (d = (-c - a) / (2 * b), b = 0 <= d && 1 >= d ? d : 0)), b;
      var e = c / a - b * b / (a * a) / 3;
      c = b * b * b / (a * a * a) / 13.5 - b * c / (a * a) / 3 + d / a;
      var n = c * c / 4 + e * e * e / 27;
      b = -b / (3 * a);

      if (0 >= n) {
        if (0 == e && 0 == c) return -K(d / a);
        a = Math.sqrt(c * c / 4 - n);
        d = Math.acos(-c / 2 / a);
        c = Math.cos(d / 3);
        d = Math.sqrt(3) * Math.sin(d / 3);
        a = K(a);
        e = 2 * a * c + b;
        if (0 <= e && 1 >= e) return e;
        e = -a * (c + d) + b;
        if (0 <= e && 1 >= e) return e;
        e = a * (d - c) + b;
        if (0 <= e && 1 >= e) return e;
      } else {
        a = K(-c / 2 + Math.sqrt(n));
        c = K(-c / 2 - Math.sqrt(n));
        d = a + c + b;
        if (0 <= d && 1 >= d) return d;
        d = -(a + c) / 2 + b;
        if (0 <= d && 1 >= d) return d;
      }

      return 0;
    }

    function ea(a, b) {
      if (48 == a && "number" === typeof b) return "rgba(" + (b >>> 24) + "," + (b >>> 16 & 255) + "," + (b >>> 8 & 255) + "," + (b & 255) / 255 + ")";
      if (64 == a) return b = b.map(function (a) {
        return a + "px";
      }), b.join(",");

      if (96 == a) {
        a = "";

        for (var c = b.length, d = 0; d < c; d += 2) {
          a += b[d], a += b[d + 1].join(",");
        }

        return a;
      }

      if (80 == a) {
        if (0 == b[0]) return "none";
        a = "";
        c = b.length;

        for (d = 0; d < c;) {
          a += T[b[d]], 1 == b[d] ? a += "(" + b[d + 1] + ") " : 5 == b[d] ? (a += "(" + b[d + 1] + "px " + b[d + 2] + "px " + b[d + 3] + "px rgba(" + (b[d + 4] >>> 24) + "," + (b[d + 4] >> 16 & 255) + "," + (b[d + 4] >> 8 & 255) + "," + (b[d + 4] & 255) / 255 + ")) ", d += 3) : a = 2 == b[d] ? a + ("(" + b[d + 1] + "px) ") : 7 == b[d] ? a + ("(" + b[d + 1] + "deg) ") : a + ("(" + (0 > b[d + 1] ? 0 : b[d + 1]) + ") "), d += 2;
        }

        return a;
      }

      return 32 == a ? b + "px" : b;
    }

    function y(a) {
      return 0 >= a ? 0 : 255 <= a ? 255 : a;
    }

    function fa(a, b, c, d) {
      if (16 == a || 32 == a) return (c - b) * d + b;
      if (0 == a) return .5 > d ? b : c;

      if (48 == a) {
        if ("number" === typeof b && "number" === typeof c) {
          var e = 1 - d;
          return (y(e * (b >>> 24) + d * (c >>> 24)) << 24 | y(e * (b >>> 16 & 255) + d * (c >>> 16 & 255)) << 16 | y(e * (b >>> 8 & 255) + d * (c >>> 8 & 255)) << 8 | y(e * (b & 255) + d * (c & 255))) >>> 0;
        }

        return .5 > d ? b : c;
      }

      if (64 == a) {
        0 == b.length && (b = [0]);
        0 == c.length && (c = [0]);
        var n = b.length;
        b.length != c.length && (n = b.length * c.length);
        var l = [];

        for (a = 0; a < n; ++a) {
          var f = b[a % b.length];
          var h = (c[a % c.length] - f) * d + f;
          0 > h && (h = 0);
          l.push(h);
        }

        return l;
      }

      if (96 == a) {
        if (b.length != c.length) return .5 > d ? b : c;
        n = b.length;
        l = [];

        for (a = 0; a < n; a += 2) {
          if (b[a] !== c[a]) return .5 > d ? b : c;
          l[a] = b[a];
          l[a + 1] = [];

          for (f = 0; f < b[a + 1].length; ++f) {
            l[a + 1].push((c[a + 1][f] - b[a + 1][f]) * d + b[a + 1][f]);
          }
        }

        return l;
      }

      if (80 == a) {
        n = b.length;
        if (n != c.length) return .5 > d ? b : c;
        l = [];

        for (a = 0; a < n;) {
          if (b[a] != c[a] || 1 == b[a]) return .5 > d ? b : c;
          l[a] = b[a];
          l[a + 1] = (c[a + 1] - b[a + 1]) * d + b[a + 1];

          if (5 == b[a]) {
            l[a + 2] = (c[a + 2] - b[a + 2]) * d + b[a + 2];
            l[a + 3] = (c[a + 3] - b[a + 3]) * d + b[a + 3];
            e = 1 - d;
            var g = b[a + 4],
                q = c[a + 4];
            h = e * (g >>> 24) + d * (q >>> 24);
            var m = e * (g >> 16 & 255) + d * (q >> 16 & 255);
            f = e * (g >> 8 & 255) + d * (q >> 8 & 255);
            e = e * (g & 255) + d * (q & 255);
            l[a + 4] = (y(m) << 16 | y(f) << 8 | y(e)) + 16777216 * (y(h) | 0);
            a += 3;
          }

          a += 2;
        }

        return l;
      }

      return 0;
    }

    function U(a, b) {
      a: {
        var c = a + b[2];
        var d = b[4].length;

        for (var e = 0; e < d; ++e) {
          if (c < b[4][e]) {
            c = e;
            break a;
          }
        }

        c = d - 1;
      }

      d = b[2];
      e = b[4][c - 1] - d;
      a = (a - e) / (b[4][c] - d - e);
      if (b[6] && b[6].length > c - 1) if (d = b[6][c - 1], 1 == d[0]) {
        if (0 >= a) a = 0;else if (1 <= a) a = 1;else {
          e = d[1];
          var n = d[3];
          a = da(3 * e - 3 * n + 1, -6 * e + 3 * n, 3 * e, -a);
          a = 3 * a * (1 - a) * (1 - a) * d[2] + 3 * a * a * (1 - a) * d[4] + a * a * a;
        }
      } else 2 == d[0] ? (d = d[1], a = Math.ceil(a * d) / d) : 3 == d[0] && (d = d[1], a = Math.floor(a * d) / d);
      return fa(b[1] & 240, b[5][c - 1], b[5][c], a);
    }

    function L() {
      u || (v = new Date().getTime() + V);
    }

    function O(a) {
      if (a || !E) {
        for (var b = !1, c = 0; c < w.length; ++c) {
          w[c].J(a) && (b = !0);
        }

        if (a) for (; 0 < I.length;) {
          if (a = I.shift(), c = a[0], 1 == a[1]) c.onfinish && (c.onfinish(), b = !0), c.I();else if (2 == a[1] && c.onloop) c.onloop();
        }
        return b;
      }
    }

    function W() {
      L();
      O(!0) && !u ? (E = !0, M(W)) : E = !1;
    }

    function N() {
      E || (E = !0, M(W));
    }

    function X(a, b) {
      var c = [];
      a.split(b).forEach(function (a) {
        c.push(parseFloat(a));
      });
      return c;
    }

    function A(a) {
      -1 == a.indexOf(",") && (a = a.replace(" ", ","));
      return X(a, ",");
    }

    function Y(a) {
      a._ks || (a._ks = {});

      if (!a._ks.transform) {
        for (var b = a._ks.transform = [], c = 0; 14 >= c; ++c) {
          b[c] = 0;
        }

        b[10] = 1;
        b[11] = 1;

        if (a = a.getAttribute("transform")) {
          a = a.trim().split(") ");

          for (c = a.length - 2; 0 <= c; --c) {
            if (x(a[c], "translate(")) {
              for (var d = 0; d < c; d++) {
                a.shift();
              }

              break;
            }
          }

          c = a.shift();
          x(c, "translate(") && (c = A(c.substring(10)), b[1] = c[0], b[2] = t(c[1]) ? c[1] : 0, c = a.shift());
          x(c, "rotate(") && (c = A(c.substring(7)), b[6] = c[0], c = a.shift());
          x(c, "skewX(") && (c = A(c.substring(6)), b[7] = c[0], c = a.shift());
          x(c, "skewY(") && (c = A(c.substring(6)), b[8] = c[0], c = a.shift());
          x(c, "scale(") && (c = A(c.substring(6)), b[10] = c[0], b[11] = t(c[1]) ? c[1] : c[0], c = a.shift());
          x(c, "translate(") && (c = A(c.substring(10)), b[13] = c[0], b[14] = t(c[1]) ? c[1] : 0);
        }
      }
    }

    function Z(a) {
      this.l = a;
      this.A = [];
      this.C = [];
      this.v = 0;
      this.s = this.a = this.c = null;
      this.h = this.f = this.g = 0;
      this.b = 1;
      this.i = this.F = this.o = !1;
    }

    function J(a, b, c) {
      b = a[b];
      void 0 === b && (b = a[c]);
      return b;
    }

    function ha(a) {
      return Array.isArray(a) ? a : x(a, "cubic-bezier(") ? (a = a.substring(13, a.length - 1).split(","), [1, parseFloat(a[0]), parseFloat(a[1]), parseFloat(a[2]), parseFloat(a[3])]) : x(a, "steps(") ? (a = a.substring(6, a.length - 1).split(","), [a[1] && "start" == a[1].trim() ? 2 : 3, parseFloat(a[0])]) : [0];
    }

    function ia(a) {
      a = a.trim();
      return x(a, "#") ? (parseInt(a.substring(1), 16) << 8) + 255 : x(a, "rgba(") ? (a = a.substring(5, a.length - 1), a = a.split(","), (parseInt(a[0], 10) << 24) + (parseInt(a[1], 10) << 16) + (parseInt(a[2], 10) << 8) + 255 * parseFloat(a[3]) << 0) : a;
    }

    function aa(a) {
      !1 === a.i && (w.push(a), a.i = !0, !1 !== a.l.autoplay && a.play());
      return this;
    }

    function P(a) {
      if (!0 === a.i) {
        a._cancel();

        var b = w.indexOf(a);
        -1 < b && w.splice(b, 1);
        b = I.indexOf(a);
        -1 < b && I.splice(b, 1);
        a.i = !1;
      }

      return this;
    }

    var Q = Error("Not in timeline list"),
        ba = "mpath posX posY    rotate skewX skewY  scaleX scaleY  anchorX anchorY".split(" "),
        ja = " translate translate    rotate skewX skewY  scale scale  translate translate".split(" "),
        T = "none url blur brightness contrast drop-shadow grayscale hue-rotate invert opacity saturate sepia".split(" "),
        M = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || null;
    M || (M = function M(a) {
      window.setTimeout(a, 16);
    });
    var S = {
      d: 97,
      fill: 48,
      fillOpacity: 16,
      filter: 80,
      height: 33,
      opacity: 16,
      offsetDistance: 33,
      stroke: 48,
      strokeDasharray: 64,
      strokeDashoffset: 32,
      strokeOpacity: 16,
      strokeWidth: 32,
      transform: 1,
      width: 33
    },
        ca = window.getComputedStyle(document.documentElement),
        E = !1,
        v = new Date().getTime(),
        u,
        V = 0,
        w = [],
        I = [];
    Z.prototype = {
      B: function B(a) {
        var b = 0;

        if (null !== this.c) {
          var c = this.j();
          0 < this.b && null !== c && c >= this.f ? this.h ? (this.c = v - this.g / this.b, this.h--, b = 2) : (b = 1, a ? this.a = c : this.a = this.s ? Math.max(this.s, this.f) : this.f) : 0 > this.b && null !== c && c <= this.g ? this.h && Infinity != this.f ? (this.c = v - this.f / this.b, this.h--, b = 2) : (this.h = 0, b = 1, a ? this.a = c : this.a = this.s ? Math.min(this.s, this.g) : this.g) : null !== c && 0 != this.b && (a && null !== this.a && (this.c = v - this.a / this.b), this.a = null);
        }

        this.s = this.j();
        return b;
      },
      J: function J(a) {
        a && (this.o && (this.o = !1, null === this.c && (0 != this.b && null !== this.a ? (this.c = v - this.a / this.b, this.a = null) : this.c = v)), null === this.a && null !== this.c && (a = this.B(!1), 0 != a && I.push([this, a])));
        a = this.j();
        if (null === a) return !1;

        for (var b = this.A, c = this.C, d = 0; d < b.length; ++d) {
          for (var e = b[d], n = !1, l = 0; l < c[d].length; ++l) {
            var f = c[d][l],
                h = f[0];

            if (null !== h) {
              var g = f[2];
              var q = f[4].length,
                  m = f[4][q - 1] - g;
              g = 0 == m ? f[5][q - 1] : a <= g ? f[5][0] : a >= g + f[3] ? 0 == f[3] % m ? f[5][q - 1] : U(f[3] % m, f) : U((a - g) % m, f);
              0 == h ? (e._ks.mpath = f[8], e._ks.transform[h] = g, n = !0) : 14 >= h ? (e._ks.transform[h] = g, n = !0) : (g = ea(f[1] & 240, g), f[1] & 1 ? e.setAttribute(h, g) : e.style[h] = g);
            }
          }

          if (n) {
            Y(e);
            n = e._ks.transform;
            l = "";
            if (f = e._ks.mpath) g = n[0], 0 > g && (g = 0), 100 < g && (g = 100), g = g * f[2] / 100, h = f[1].getPointAtLength(g), l = "translate(" + h.x + "," + h.y + ") ", f[0] && (.5 > g ? (g = h, h = f[1].getPointAtLength(.5)) : g = f[1].getPointAtLength(g - .5), l += "rotate(" + 180 * Math.atan2(h.y - g.y, h.x - g.x) / Math.PI + ") ");

            for (f = 1; f < n.length; ++f) {
              h = n[f], h != (10 == f || 11 == f ? 1 : 0) && (l += " " + ja[f] + "(", l = 2 >= f ? l + (1 == f ? h + ",0" : "0," + h) : 13 <= f ? l + (13 == f ? h + ",0" : "0," + h) : 10 <= f ? l + (10 == f ? h + ",1" : "1," + h) : l + h, l += ")");
            }

            e.setAttribute("transform", l);
          }
        }

        return "running" == this.m();
      },
      I: function I() {
        !1 !== this.l.autoremove && "finished" == this.m() && P(this);
      },
      D: function D() {
        if (!this.F) {
          this.F = !0;

          for (var a = this.A, b = this.C, c = 0; c < a.length; ++c) {
            for (var d = a[c], e = 0; e < b[c].length; ++e) {
              14 >= b[c][e][0] && Y(d);
            }
          }
        }
      },
      u: function u(a) {
        if ("number" == typeof a) return a;
        if (!t(this.l.markers) || !t(this.l.markers[a])) throw Error("Invalid marker: " + a);
        return +this.l.markers[a];
      },
      play: function play(a) {
        t(a) && null !== a && (a = this.u(a), _H(a), 0 > this.b && a < this.g && (a = this.g), 0 < this.b && a > this.f && (a = this.f), this.w(a, !0));
        if (!this.i) throw Q;
        a = this.j();
        if (0 < this.b && (null === a || a >= this.f)) this.a = this.g;else if (0 > this.b && (null === a || a <= this.g)) {
          if (Infinity == this.f) throw Error("Cannot seek to Infinity");
          this.a = this.f;
        } else 0 == this.b && null === a && (this.a = this.g);
        if (null === this.a) return this;
        this.c = null;
        this.o = !0;
        this.D();
        N();
        return this;
      },
      pause: function pause(a) {
        if (!this.i) throw Q;
        t(a) && (a = this.u(a), _H(a));

        if ("paused" != this.m()) {
          L();
          var b = this.j();
          if (null === b) if (0 <= this.b) this.a = this.g;else {
            if (Infinity == this.f) throw Error("Cannot seek to Infinity");
            this.a = this.f;
          }
          null !== this.c && null === this.a && (this.a = b);
          this.c = null;
          this.o = !1;
          this.B(!1);
          this.D();
          N();
        }

        t(a) && this.w(a, !0);
        return this;
      },
      range: function range(a, b) {
        if (0 == arguments.length) return {
          "in": this.g,
          out: this.f
        };
        var c = this.u(a),
            d = this.v;
        t(b) && (d = this.u(b));

        _H(c);

        if (0 > c || 0 > d || c >= d || isNaN(d)) throw Error("Invalid range");
        var e = this.m();
        this.g = c;
        this.f = d;
        "finished" == e && "running" == this.m() && this.play();
        return this;
      },
      loop: function loop(a) {
        if (!t(a)) return {
          count: this.h
        };
        this.h = !0 === a ? Infinity : Math.floor(a);
        if (0 > this.h || isNaN(this.h)) this.h = 0;
        return this;
      },
      j: function j() {
        return null !== this.a ? this.a : null === this.c ? null : (v - this.c) * this.b;
      },
      w: function w(a, b) {
        b && L();
        null !== a && (this.D(), null !== this.a || null === this.c || 0 == this.b ? (this.a = a, O(!1)) : this.c = v - a / this.b, this.i || (this.c = null), this.s = null, this.B(!0), N());
      },
      G: function G() {
        return this.j();
      },
      time: function time(a) {
        if (t(a)) {
          if (!this.i) throw Q;
          a = this.u(a);

          _H(a);

          this.w(a, !0);
          return this;
        }

        return this.G();
      },
      m: function m() {
        var a = this.j();
        return this.o ? "running" : null === a ? "idle" : null === this.c ? "paused" : 0 < this.b && a >= this.f || 0 > this.b && a <= this.g ? "finished" : "running";
      },
      state: function state() {
        return this.m();
      },
      duration: function duration() {
        return this.v;
      },
      H: function H(a) {
        _H(a);

        L();
        var b = this.j();
        this.b = a;
        null !== b && this.w(b, !1);
      },
      rate: function rate(a) {
        return t(a) ? (this.H(a), this) : this.b;
      },
      marker: function marker(a) {
        return t(this.l.markers) ? this.l.markers[a] : void 0;
      },
      _cancel: function _cancel() {
        if (!this.i || "idle" == this.m()) return this;
        this.c = this.a = null;
        this.o = !1;
        return this;
      }
    };
    return {
      version: "1.1.0",
      animate: function animate() {
        var a = {};

        if (1 == arguments.length % 2) {
          a = arguments[arguments.length - 1];
          var b = {};

          for (c in a) {
            b[c] = a[c];
          }

          a = b;
        }

        var c = new Z(a);
        a = arguments;

        for (var d = b = 0; d < a.length - 1; d += 2) {
          var e = a[d];
          var n = e instanceof Element ? e : document.getElementById(e.substring(1));
          if (!n) throw Error("Invalid target: " + e);
          e = n;
          n = a[d + 1];
          e._ks || (e._ks = {});

          for (var l = [], f = 0; f < n.length; ++f) {
            var h = n[f],
                g = J(h, "p", "property");
            if ("string" != typeof g || -1 != g.indexOf("-") || "" === g || !(0 < S[g] || 0 <= ba.indexOf(g))) throw Error("Invalid property: " + g);
            var q = ba.indexOf(g);
            "" !== g && 0 <= q && (g = q);
            q = R(g);
            var m = J(h, "t", "times");
            if (!m || 2 > m.length) throw Error("Not enough times");
            m = m.slice();
            if (!isFinite(m[0]) || 0 > m[0]) throw Error("Invalid time: " + m[0]);

            for (var B = 1; B < m.length; ++B) {
              if (!isFinite(m[B]) || 0 > m[B] || m[B] < m[B - 1]) throw Error("Invalid time: " + m[B]);
            }

            B = m[0];
            var v = m[m.length - 1] - B,
                y = h.iterations || 0;
            1 > y && (y = 1);
            v *= y;
            b < v + B && (b = v + B);
            var u = J(h, "v", "values");
            if (!u || u.length != m.length) throw Error("Values do not match times");
            u = u.slice();

            for (var C = g, k = u, w = R(C) & 240, p = 0; p < k.length; ++p) {
              if (96 == w) {
                for (var G = k[p].substring(6, k[p].length - 2).match(/[A-DF-Za-df-z][-+0-9eE., ]*/ig), A = [], r = 0; r < G.length; ++r) {
                  A.push(G[r][0]);

                  for (var z = 1 < G[r].trim().length ? G[r].substring(1).split(",") : [], F = 0; F < z.length; ++F) {
                    z[F] = parseFloat(z[F]);
                  }

                  A.push(z);
                }

                k[p] = A;
              } else if (48 == w) x(k[p], "#") ? (G = 9 == k[p].length, k[p] = parseInt(k[p].substring(1), 16), G || (k[p] = 256 * k[p] | 255)) : x(k[p], "url(") || "none" == k[p] || (console.warn("unsupported color: " + k[p]), k[p] = 0);else if (80 == w) {
                G = k;
                A = p;
                r = k[p];
                if ("none" == r) r = [0];else {
                  z = [];

                  for (var D = r.indexOf("("); 0 < D;) {
                    if (F = T.indexOf(r.substring(0, D)), 0 <= F) {
                      z.push(F);
                      var E = r.indexOf(") ");
                      0 > E && (E = r.length - 1);
                      D = r.substring(D + 1, E).split(" ");
                      5 == F ? (z.push(parseFloat(D[0])), z.push(parseFloat(D[1])), z.push(parseFloat(D[2])), z.push(ia(D[3]))) : 1 == F ? z.push(D[0]) : z.push(parseFloat(D[0]));
                      r = r.substring(E + 1).trim();
                      D = r.indexOf("(");
                    } else break;
                  }

                  r = z;
                }
                G[A] = r;
              } else 64 == w ? "none" != k[p] ? /^[0-9 .]*$/.test(k[p]) ? k[p] = X(k[p], " ") : (console.warn("unsupported value: " + k[p]), k[p] = [0]) : k[p] = [0] : 32 == w ? (_H(k[p]), k[p] = parseFloat(k[p])) : 0 === C && (k[p] = parseFloat(k[p]));
            }

            C = J(h, "e", "easing");
            k = m.length;

            for (C || (C = []); C.length < k;) {
              C.push([1, 0, 0, .58, 1]);
            }

            for (k = 0; k < C.length; ++k) {
              C[k] = ha(C[k]);
            }

            q = [g, q, B, v, m, u, C, y];
            m = J(h, "mp", "motionPath");
            t(m) && 0 === g && (q[8] = [], q[8][0] = h.motionRotate, h = document.createElementNS("http://www.w3.org/2000/svg", "path"), m || (m = "M0,0"), h.setAttribute("d", m), q[8][1] = h, q[8][2] = h.getTotalLength());
            l.push(q);
          }

          0 < l.length && (c.A.push(e), c.C.push(l));
        }

        c.v = b;
        c.g = 0;
        c.f = c.v;
        aa(c);
        return c;
      },
      add: aa,
      remove: P,
      removeAll: function removeAll() {
        for (var a = w.length - 1; 0 <= a; --a) {
          P(w[a]);
        }

        return this;
      },
      timelines: function timelines() {
        return w.slice();
      },
      globalPlay: function globalPlay() {
        u && (V = u - new Date().getTime(), u = void 0, N());
        return this;
      },
      globalPause: function globalPause() {
        u || (u = v, O(!1));
        return this;
      },
      globalState: function globalState() {
        return u ? "paused" : "running";
      }
    };
  }();

  var sceneCTA = function sceneCTA() {
    if (!KeyshapeJS.version.indexOf('1.') != 0) {
      /**
       * Animation for CTA Section
       */
      var _sceneCTA = document.getElementById('sceneCTA');

      var sceneCTAlaunch = true;
      /**
       * Animation Frames
       */

      var getCTASceneAnimation = function getCTASceneAnimation() {
        window.ks = document.ks = KeyshapeJS;

        (function (ks) {
          ks.animate('#Sun', [{
            p: 'mpath',
            t: [1500, 4200],
            v: ['0%', '100%'],
            e: [[0], [0]],
            mp: 'M0,0L340,0'
          }, {
            p: 'scaleX',
            t: [1500, 2200, 3200, 4200],
            v: [1, 1.3, 1.2, 1],
            e: [[0], [0], [0], [0]]
          }, {
            p: 'scaleY',
            t: [1500, 2200, 3200, 4200],
            v: [1, 1.3, 1.2, 1],
            e: [[0], [0], [0], [0]]
          }], '#Tree', [{
            p: 'skewX',
            t: [4900, 5000, 5100, 5200, 5300, 5500],
            v: [0, 6, -6, 6, -6, 0],
            e: [[1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [0]]
          }, {
            p: 'skewY',
            t: [4900, 5000, 5100, 5200, 5300, 5500],
            v: [0, 0, 0, 0, 0, 0],
            e: [[1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [0]]
          }], '#_a0', [{
            p: 'opacity',
            t: [500, 800, 2300, 3000, 3600],
            v: [0, 1, 0.9, 0.5, 1],
            e: [[0], [0], [0], [0], [0]]
          }, {
            p: 'd',
            t: [1500, 2500, 2900, 3500, 4000],
            v: ["path('M-6.9,-76.5L-38.3,-62.5L141.9,-10.5L197.5,-33.5C197.5,-33.5,-6.9,-76.5,-6.9,-76.5Z')", "path('M-6.4,-77.1L-70.4,-77.1L10.6,-16.1L54.6,-37.1C54.6,-37.1,-6.4,-77.1,-6.4,-77.1Z')", "path('M-8.6,-76L-69.4,-76L-89.2,-36.1L-8.6,-37.8C-8.6,-37.8,-8.6,-76,-8.6,-76Z')", "path('M-7.4,-75.6L-68.4,-75.6L-160.4,-38.1L-94.7,-23.5C-94.7,-23.5,-7.4,-75.6,-7.4,-75.6Z')", "path('M-5.4,-76.2L-69.4,-76.2L-197.3,-14.4L-107.4,-7.1C-107.4,-7.1,-5.4,-76.2,-5.4,-76.2Z')"],
            e: [[1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [1, 0, 0, 1, 1], [0]]
          }], '#Left-Branch', [{
            p: 'scaleX',
            t: [900, 1300],
            v: [0, -1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }, {
            p: 'scaleY',
            t: [900, 1300],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }], '#Fill-31', [{
            p: 'fill',
            t: [2000, 4700],
            v: ['#02256c', '#ffec61'],
            e: [[1, 0, 0, 1, 1], [0]]
          }], '#Fill-32', [{
            p: 'fill',
            t: [1500, 2000, 4400],
            v: ['#ffec61', '#ffec61', '#02256c'],
            e: [[0], [1, 0, 1, 1, 1], [0]]
          }], '#Main-branch', [{
            p: 'scaleX',
            t: [400, 1000],
            v: [0, -1],
            e: [[1, 0, 2.7, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [400, 1000],
            v: [0, 1],
            e: [[1, 0, 2.7, 1, 0], [0]]
          }], '#Fill-33', [{
            p: 'fill',
            t: [1500, 2000, 4400],
            v: ['#ffec61', '#ffec61', '#02256c'],
            e: [[0], [1, 0, 1, 1, 1], [0]]
          }], '#Fill-34', [{
            p: 'fill',
            t: [1500, 2000, 4700],
            v: ['#02256c', '#02256c', '#ffec61'],
            e: [[0], [1, 0, 1, 1, 1], [0]]
          }], '#Right-Branch', [{
            p: 'scaleX',
            t: [800, 1200],
            v: [0, -1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }, {
            p: 'scaleY',
            t: [800, 1200],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }], '#Fill-64', [{
            p: 'fill',
            t: [2000, 4400],
            v: ['#ffec61', '#02256c'],
            e: [[1, 0, 1, 1, 1], [0]]
          }], '#Fill-65', [{
            p: 'fill',
            t: [1300, 1500, 2000, 4700],
            v: ['#02256c', '#ffec61', '#02256c', '#ffec61'],
            e: [[0], [0], [1, 0, 1, 1, 1], [0]]
          }], '#Left-Tree', [{
            p: 'scaleX',
            t: [1000, 1500],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }, {
            p: 'scaleY',
            t: [1000, 1500],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }], '#Mid-Tree', [{
            p: 'scaleX',
            t: [700, 1000],
            v: [0, 1],
            e: [[1, 0, 2, 0.5, 0.3], [0]]
          }, {
            p: 'scaleY',
            t: [700, 1000],
            v: [0, 1],
            e: [[1, 0, 2, 0.5, 0.3], [0]]
          }], '#Right-Tree', [{
            p: 'scaleX',
            t: [1200, 1700],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }, {
            p: 'scaleY',
            t: [1200, 1700],
            v: [0, 1],
            e: [[1, 1, 0, 0.4, 0.8], [0]]
          }], {
            autoremove: false
          }).range(0, 7000);
          if (document.location.search.substr(1).split('&').indexOf('global=paused') >= 0) ks.globalPause();
        })(KeyshapeJS);
      };
      /**
       * Check Visibility
       */


      window.addEventListener('scroll', function () {
        showCTASceneAnimation();
      });
      /**
       * Show Animation
       */

      var showCTASceneAnimation = function showCTASceneAnimation() {
        // Animation Trigger
        var offset = -100;

        if (sceneCTAlaunch) {
          if (checkVisible(_sceneCTA, offset)) {
            getCTASceneAnimation();
            sceneCTAlaunch = false;
          }
        }
      };

      showCTASceneAnimation();
    }
  };

  //import { scrollMaster } from './helpers/helpers';
  var sceneIntro = function sceneIntro() {
    /**
     * Intro Animation
     */
    if (!KeyshapeJS.version.indexOf('1.') != 0) {
      var playerIntro = KeyshapeJS; // scrollMaster(function () {
      //   playerIntro.globalPlay();
      // }, function () {
      //   playerIntro.globalPause();
      // });

      /**
       * Intro Header Animation
       */

      var showIntroTitle = function showIntroTitle() {
        var introHeader = document.querySelector('.intro-header');

        if (introHeader) {
          introHeader.classList.add('active');
        }
      };
      /**
       * Intro Header Animation
       */


      var showIntroButtons = function showIntroButtons() {
        var introMenu = document.querySelector('.intro-menu');

        if (introMenu) {
          setTimeout(function () {
            introMenu.classList.add('active');
          }, 1500);
        }
      };
      /**
       * Animation Frames
       */


      var showIntroSceneAnimation = function showIntroSceneAnimation() {
        (function (playerIntro) {
          playerIntro.animate("#Vector-Book", [{
            p: 'scaleX',
            t: [0, 800],
            v: [.5, 1],
            e: [[1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [0, 800],
            v: [.5, 1],
            e: [[1, 0, 1, 1, 1], [0]]
          }, {
            p: 'opacity',
            t: [0, 800],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Path-10-Copy", [{
            p: 'd',
            t: [0, 1200, 1700, 3100, 6300, 7700, 9200, 10400, 10900, 12300, 15500, 16900, 18200, 19400, 19900, 21300, 24500, 25900, 27400, 28600, 29100, 30500, 33700, 35100],
            v: ["path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L443,334.1L43.4,478.1Z')", "path('M43.4,478.1L57.6,641.6C58.4,650.4,66.1,656.9,74.9,656.1L468.5,622.5L468.5,622.5L427.5,230L43.4,478.1Z')"],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval", [{
            p: 'scaleX',
            t: [0, 1700, 3600, 6300, 8200, 9200, 10900, 12800, 15500, 17400, 18200, 19900, 21800, 24500, 26400, 27400, 29100, 31000, 33700, 35600],
            v: [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }, {
            p: 'scaleY',
            t: [0, 1700, 3600, 6300, 8200, 9200, 10900, 12800, 15500, 17400, 18200, 19900, 21800, 24500, 26400, 27400, 29100, 31000, 33700, 35600],
            v: [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-13", [{
            p: 'scaleX',
            t: [800, 3500, 4400, 8300, 10000, 12700, 13600, 17500, 19000, 21700, 22600, 26500, 28200, 30900, 31800, 35700],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [800, 3500, 4400, 8300, 10000, 12700, 13600, 17500, 19000, 21700, 22600, 26500, 28200, 30900, 31800, 35700],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Path-13-Copy", [{
            p: 'scaleX',
            t: [800, 2800, 4800, 8300, 10000, 12000, 14000, 17500, 19000, 21000, 23000, 26500, 28200, 30200, 32200, 35700],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [800, 2800, 4800, 8300, 10000, 12000, 14000, 17500, 19000, 21000, 23000, 26500, 28200, 30200, 32200, 35700],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Path-13-Copy-2", [{
            p: 'scaleX',
            t: [800, 2700, 9300, 10000, 11900, 18500, 19000, 20900, 27500, 28200, 30100, 36700],
            v: [0, .5, 1, 0, .5, 1, 0, .5, 1, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [800, 2700, 9300, 10000, 11900, 18500, 19000, 20900, 27500, 28200, 30100, 36700],
            v: [0, .5, 1, 0, .5, 1, 0, .5, 1, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Path-13-Copy-5", [{
            p: 'scaleX',
            t: [800, 2700, 4700, 7300, 8200, 10000, 11900, 13900, 16500, 17400, 19000, 20900, 22900, 25500, 26400, 28200, 30100, 32100, 34700, 35600],
            v: [0, .5, 1, .6, 1, 0, .5, 1, .6, 1, 0, .5, 1, .6, 1, 0, .5, 1, .6, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [800, 2700, 4700, 7300, 8200, 10000, 11900, 13900, 16500, 17400, 19000, 20900, 22900, 25500, 26400, 28200, 30100, 32100, 34700, 35600],
            v: [0, .5, 1, .6, 1, 0, .5, 1, .6, 1, 0, .5, 1, .6, 1, 0, .5, 1, .6, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-6", [{
            p: 'scaleX',
            t: [800, 2700, 4700, 7300, 8000, 10000, 11900, 13900, 16500, 17200, 19000, 20900, 22900, 25500, 26200, 28200, 30100, 32100, 34700, 35400],
            v: [0, .7, 1, .7, 1, 0, .7, 1, .7, 1, 0, .7, 1, .7, 1, 0, .7, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [800, 2700, 4700, 7300, 8000, 10000, 11900, 13900, 16500, 17200, 19000, 20900, 22900, 25500, 26200, 28200, 30100, 32100, 34700, 35400],
            v: [0, .7, 1, .7, 1, 0, .7, 1, .7, 1, 0, .7, 1, .7, 1, 0, .7, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-7", [{
            p: 'opacity',
            t: [1000, 1400, 1800, 2300, 2900, 3300, 3700, 4200, 7500, 7900, 8300, 8800, 10200, 10600, 11000, 11500, 12100, 12500, 12900, 13400, 16700, 17100, 17500, 18000, 19200, 19600, 20000, 20500, 21100, 21500, 21900, 22400, 25700, 26100, 26500, 27000, 28400, 28800, 29200, 29700, 30300, 30700, 31100, 31600, 34900, 35300, 35700, 36200],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-8", [{
            p: 'opacity',
            t: [1000, 1600, 1800, 2300, 2900, 3500, 3700, 4200, 7500, 8100, 8300, 8800, 10200, 10800, 11000, 11500, 12100, 12700, 12900, 13400, 16700, 17300, 17500, 18000, 19200, 19800, 20000, 20500, 21100, 21700, 21900, 22400, 25700, 26300, 26500, 27000, 28400, 29000, 29200, 29700, 30300, 30900, 31100, 31600, 34900, 35500, 35700, 36200],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-9", [{
            p: 'opacity',
            t: [800, 1500, 1800, 2300, 2700, 3400, 3700, 4200, 7300, 8000, 8300, 8800, 10000, 10700, 11000, 11500, 11900, 12600, 12900, 13400, 16500, 17200, 17500, 18000, 19000, 19700, 20000, 20500, 20900, 21600, 21900, 22400, 25500, 26200, 26500, 27000, 28200, 28900, 29200, 29700, 30100, 30800, 31100, 31600, 34700, 35400, 35700, 36200],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-10", [{
            p: 'opacity',
            t: [800, 1200, 1800, 2300, 2700, 3100, 3700, 4200, 7300, 7700, 8300, 8800, 10000, 10400, 11000, 11500, 11900, 12300, 12900, 13400, 16500, 16900, 17500, 18000, 19000, 19400, 20000, 20500, 20900, 21300, 21900, 22400, 25500, 25900, 26500, 27000, 28200, 28600, 29200, 29700, 30100, 30500, 31100, 31600, 34700, 35100, 35700, 36200],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-2", [{
            p: 'scaleX',
            t: [800, 1800, 2700, 4800, 10000, 11000, 11900, 14000, 19000, 20000, 20900, 23000, 28200, 29200, 30100, 32200],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [800, 1800, 2700, 4800, 10000, 11000, 11900, 14000, 19000, 20000, 20900, 23000, 28200, 29200, 30100, 32200],
            v: [0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1, 0, 1, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Oval-2", [{
            p: 'scaleX',
            t: [800, 1800, 2700, 3700, 4400, 8300, 10000, 11000, 11900, 12900, 13600, 17500, 19000, 20000, 20900, 21900, 22600, 26500, 28200, 29200, 30100, 31100, 31800, 35700],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [800, 1800, 2700, 3700, 4400, 8300, 10000, 11000, 11900, 12900, 13600, 17500, 19000, 20000, 20900, 21900, 22600, 26500, 28200, 29200, 30100, 31100, 31800, 35700],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Oval-3", [{
            p: 'scaleX',
            t: [0, 800, 1700, 3600, 8800, 9200, 10000, 10900, 12800, 18000, 18200, 19000, 19900, 21800, 27000, 27400, 28200, 29100, 31000, 36200],
            v: [0, 1, .7, 0, 1, 0, 1, .7, 0, 1, 0, 1, .7, 0, 1, 0, 1, .7, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [0, 800, 1700, 3600, 8800, 9200, 10000, 10900, 12800, 18000, 18200, 19000, 19900, 21800, 27000, 27400, 28200, 29100, 31000, 36200],
            v: [0, 1, .7, 0, 1, 0, 1, .7, 0, 1, 0, 1, .7, 0, 1, 0, 1, .7, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Oval-4", [{
            p: 'scaleX',
            t: [0, 800, 1700, 3300, 7800, 9200, 10000, 10900, 12500, 17000, 18200, 19000, 19900, 21500, 26000, 27400, 28200, 29100, 30700, 35200],
            v: [0, 1, .7, .7, 1, 0, 1, .7, .7, 1, 0, 1, .7, .7, 1, 0, 1, .7, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [0, 800, 1700, 3300, 7800, 9200, 10000, 10900, 12500, 17000, 18200, 19000, 19900, 21500, 26000, 27400, 28200, 29100, 30700, 35200],
            v: [0, 1, .7, .7, 1, 0, 1, .7, .7, 1, 0, 1, .7, .7, 1, 0, 1, .7, .7, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Oval-5", [{
            p: 'scaleX',
            t: [0, 800, 1700, 3800, 8800, 9200, 10000, 10900, 13000, 18000, 18200, 19000, 19900, 22000, 27000, 27400, 28200, 29100, 31200, 36200],
            v: [0, 1, .7, .8, 1, 0, 1, .7, .8, 1, 0, 1, .7, .8, 1, 0, 1, .7, .8, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [0, 800, 1700, 3800, 8800, 9200, 10000, 10900, 13000, 18000, 18200, 19000, 19900, 22000, 27000, 27400, 28200, 29100, 31200, 36200],
            v: [0, 1, .7, .8, 1, 0, 1, .7, .8, 1, 0, 1, .7, .8, 1, 0, 1, .7, .8, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Path-12", [{
            p: 'd',
            t: [300, 1800, 5800, 9500, 11000, 15000, 18500, 20000, 24000, 27700, 29200, 33200],
            v: ["path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L605.5,399L693,364L785,349L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L605.5,399L693,364L785,349L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L605.5,399L693,364L785,349L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L605.5,399L693,364L785,349L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L529.5,459L605.5,479L637,368L693,364L763,262.5L849,291L873.1,570.5C873.9,579.4,867.4,587.1,858.6,587.9C761.7,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')"],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-12-Copy", [{
            p: 'd',
            t: [800, 1800, 3800, 5800, 10000, 11000, 13000, 15000, 19000, 20000, 22000, 24000, 28200, 29200, 31200, 33200],
            v: ["path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,418L794,423.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,418L794,423.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,418L794,423.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,418L794,423.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')", "path('M469,622.5L536.5,519L612.5,530L646,463.5L700,460L775,390.5L859.5,403L873.2,570.6C873.9,579.4,867.4,587.1,858.6,587.9C761.8,596.2,663.4,605.9,563.5,617C532.4,619.8,500.9,621.6,469,622.5Z')"],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-13-Copy-11", [{
            p: 'scaleX',
            t: [0, 1800, 2800, 5800, 9200, 11000, 12000, 15000, 18200, 20000, 21000, 24000, 27400, 29200, 30200, 33200],
            v: [0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [0, 1800, 2800, 5800, 9200, 11000, 12000, 15000, 18200, 20000, 21000, 24000, 27400, 29200, 30200, 33200],
            v: [0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Path-13-Copy-12", [{
            p: 'scaleX',
            t: [0, 1800, 2800, 5800, 9200, 11000, 12000, 15000, 18200, 20000, 21000, 24000, 27400, 29200, 30200, 33200],
            v: [0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }, {
            p: 'scaleY',
            t: [0, 1800, 2800, 5800, 9200, 11000, 12000, 15000, 18200, 20000, 21000, 24000, 27400, 29200, 30200, 33200],
            v: [0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1, 0, 0, .5, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 0], [0]]
          }], "#Oval-7", [{
            p: 'scaleX',
            t: [800, 2100, 2700, 4000, 7300, 8600, 10000, 11300, 11900, 13200, 16500, 17800, 19000, 20300, 20900, 22200, 25500, 26800, 28200, 29500, 30100, 31400, 34700, 36000],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [800, 2100, 2700, 4000, 7300, 8600, 10000, 11300, 11900, 13200, 16500, 17800, 19000, 20300, 20900, 22200, 25500, 26800, 28200, 29500, 30100, 31400, 34700, 36000],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Oval-8", [{
            p: 'scaleX',
            t: [300, 2200, 4200, 6800, 8800, 9500, 11400, 13400, 16000, 18000, 18500, 20400, 22400, 25000, 27000, 27700, 29600, 31600, 34200, 36200],
            v: [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [300, 2200, 4200, 6800, 8800, 9500, 11400, 13400, 16000, 18000, 18500, 20400, 22400, 25000, 27000, 27700, 29600, 31600, 34200, 36200],
            v: [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Phone-annd-Code", [{
            p: 'mpath',
            t: [2300, 2400, 2500, 2800],
            v: ['0%', '13.3%', '63.3%', '100%'],
            e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]],
            mp: "M677.7,310L677.7,350L677.7,200L677.7,310"
          }, {
            p: 'scaleX',
            t: [0, 800],
            v: [1.5, 1],
            e: [[1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [0, 800],
            v: [1.5, 1],
            e: [[1, 0, 1, 1, 1], [0]]
          }, {
            p: 'opacity',
            t: [0, 800],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Code-2", [{
            p: 'opacity',
            t: [3400, 4500],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Rectangle", [{
            p: 'fill',
            t: [4600, 5000, 5200, 5300, 5400, 5600, 6000, 6100, 6800, 7500, 7600, 7700, 8000, 8700, 9100, 9300, 9400, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .4, .6, .7], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .4, .7, .7], [1, .3, .4, .7, .7], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-2", [{
            p: 'fill',
            t: [4600, 5000, 5200, 5300, 5400, 5600, 6000, 6100, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9500, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .7, .6, .9], [1, .4, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-4", [{
            p: 'fill',
            t: [4600, 5000, 5200, 5300, 5400, 5600, 6000, 6100, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .4, .7, .7], [1, .3, .4, .7, .7], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-5", [{
            p: 'fill',
            t: [4600, 5000, 5200, 5300, 5400, 5600, 6000, 6100, 6800, 7600, 7700, 8000, 8700, 9100, 9300, 9400, 9500, 10000, 10100, 10200, 10300, 10500, 10900, 11000, 11100, 11600, 11700, 11800, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20300, 20900, 21000, 21100, 21700, 21800, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .6, .7, .9], [1, .3, .4, .7, .7], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .7, .6, .9], [1, .4, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .4, 1, 1, 1], [0], [1, 0, .4, .4, .8], [1, .3, .7, .7, .9], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [1, 0, .8, .7, 1], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Rectangle-6", [{
            p: 'fill',
            t: [4600, 5000, 5200, 5300, 5400, 5600, 6000, 6100, 6800, 7600, 7700, 8000, 8700, 9100, 9300, 9400, 9500, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .6, .7, .9], [1, .3, .4, .7, .7], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .7, .6, .9], [1, .4, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .5, .6, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-2", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17500, 17600, 17900, 18400, 18700, 18800, 19400, 19800, 19900, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#18236a', '#2f71c7', '#2f71c721', '#18236a', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#18236a', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .5, 1, 1, 1], [1, 0, .4, .4, .7], [1, .3, .4, .6, .7], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [1, 0, .4, .4, .7], [1, .3, .5, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, .4, .4, .8], [1, .3, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Path-6-3", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .5, 1, 1, 1], [1, 0, .4, .4, .7], [1, .3, .4, .6, .7], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-4", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .5, 1, 1, 1], [1, 0, .4, .4, .7], [1, .3, .4, .6, .7], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-9", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .5, .6, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-7", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-8", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .3, .7, .7], [1, .3, .5, .7, .8], [1, .4, .5, .7, .8], [1, .4, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-9", [{
            p: 'fill',
            t: [4900, 5000, 5200, 5300, 5400, 5600, 6000, 6400, 6800, 7600, 7700, 8300, 8700, 9100, 9300, 9400, 9800, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .5, .4, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .3, .4, .7, .7], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-10", [{
            p: 'fill',
            t: [4900, 5000, 5200, 5300, 5400, 5600, 6000, 6400, 6800, 7600, 7700, 8300, 8700, 9100, 9300, 9400, 9800, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .5, .4, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .3, .4, .7, .7], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-5", [{
            p: 'fill',
            t: [4900, 5000, 5200, 5300, 5400, 5600, 6000, 6400, 6800, 7600, 7700, 8300, 8700, 9100, 9300, 9400, 9800, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .5, .4, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .3, .5, .7, .8], [1, .3, .4, .7, .7], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-11", [{
            p: 'fill',
            t: [4900, 5000, 5200, 5300, 5400, 5600, 6000, 6400, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9800, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .5, .4, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .5, .6, .8], [1, .3, .4, .7, .7], [1, .5, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-12", [{
            p: 'fill',
            t: [4900, 5000, 5200, 5300, 5400, 5600, 6000, 6400, 6800, 7200, 7500, 7600, 7700, 8300, 8700, 9100, 9300, 9400, 9800, 10000, 10100, 10200, 10500, 10600, 11000, 11100, 11600, 11700, 12100, 12200, 12500, 13200, 16400, 17200, 17600, 17900, 18300, 18700, 18800, 19100, 19800, 20200, 20600, 20900, 21000, 21100, 21700, 22100, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#18236a', '#2f71c7', '#2f71c721', '#18236a', '#2f71c740', '#2f71c7', '#244a99', '#18236a', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[1, 0, .5, .4, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .3, .5, .7, .8], [1, .6, 1, 1, 1], [0], [0], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .7, .7, .9], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [0], [0], [1, 0, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .5, .4, .8], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .5, 1, 1, 1], [1, 0, .7, .6, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]]
          }], "#Rectangle-13", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[1, 0, .5, .5, .8], [1, .3, .6, .7, .9], [1, .6, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .8], [1, .5, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .4, .4, .8], [1, .3, .4, .6, .7], [1, .4, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .6, .7, .8], [1, .6, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, .6, .5, .9], [1, .4, .5, .7, .8], [1, .6, 1, 1, 1], [1, 0, .5, .5, .8], [1, .5, 1, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-6", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-7", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-8", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-14", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-9", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17500, 17600, 17900, 18200, 18700, 18800, 19400, 19700, 19800, 20200, 20500, 20900, 21000, 21100, 21700, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#18236a', '#2f71c7', '#2f71c721', '#18236a', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#18236a', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-10", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7100, 7600, 7700, 8300, 8600, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 11000, 11100, 11600, 11700, 12000, 12200, 12500, 13200, 16400, 17100, 17600, 17900, 18300, 18600, 18700, 18800, 20200, 20900, 21000, 21100, 21700, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#18236a', '#2f71c7', '#2f71c721', '#18236a', '#18236a', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [1, 0, 0, 1, 1], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-10", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-15", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6800, 7600, 7700, 8700, 9100, 9300, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-16", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9400, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-11", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9400, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-17", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8700, 9100, 9300, 10000, 10100, 10200, 10500, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-18", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8200, 8700, 9100, 9300, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-19", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8200, 8700, 9100, 9300, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12000, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-20", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-21", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6800, 7600, 7700, 8700, 9100, 9300, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12000, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 19000, 19100, 20200, 20900, 21000, 21100, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2a61b4', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-11", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12000, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-12", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8200, 8700, 9100, 9300, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12000, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-13", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7600, 7700, 8200, 8700, 9100, 9300, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12000, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-14", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10700, 10900, 11000, 11100, 11600, 11700, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-15", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10700, 10900, 11000, 11100, 11600, 11700, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-12", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10700, 10900, 11000, 11100, 11600, 11700, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-13", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10700, 10900, 11000, 11100, 11600, 11700, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-22", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7600, 7700, 8400, 8700, 9100, 9300, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-16", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9800, 9900, 10000, 10100, 10200, 10500, 10700, 10900, 11000, 11100, 11600, 11700, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 19800, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#18236a', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-23", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17500, 17600, 17900, 18400, 18700, 18800, 19400, 19800, 19900, 20200, 20700, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#18236a', '#2f71c7', '#2f71c721', '#18236a', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#18236a', '#2f71c791', '#18236a', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-24", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 19400, 19900, 20200, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-17", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 13200, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#18236a', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-25", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7500, 7600, 7700, 8400, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#18236a', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Rectangle-26", [{
            p: 'fill',
            t: [5000, 5200, 5300, 5400, 5600, 6000, 6500, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9900, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 19400, 19900, 20200, 20900, 21000, 21100, 21700, 22200, 22500, 23200],
            v: ['#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-18", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 19400, 19700, 20200, 20900, 21000, 21100, 21700, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-14", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 19400, 19700, 20200, 20900, 21000, 21100, 21700, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-15", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 19400, 19700, 20200, 20900, 21000, 21100, 21700, 22000, 22500, 23200],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#18236a', '#18236a', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#18236a', '#18236a', '#2f71c752', '#18236a'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-16", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-6-19", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-17", [{
            p: 'fill',
            t: [4800, 5000, 5200, 5300, 5400, 5600, 6000, 6300, 6800, 7500, 7600, 7700, 8700, 9100, 9300, 9400, 9700, 10000, 10100, 10200, 10500, 10900, 11000, 11100, 11600, 12200, 12500, 16400, 17600, 17900, 18700, 18800, 20200, 20900, 21000, 21100, 22500],
            v: ['#18236a', '#2f71c7', '#2f71c79c', '#2f71c7', '#2f71c7', '#2f71c7', '#18236a', '#18236a', '#2f71c7', '#18236a', '#2f71c7', '#2f71c7', '#2f71c75c', '#2f71c7', '#2f71c7a6', '#18236a', '#18236a', '#2f71c7', '#2f71c7', '#2f71c785', '#2f71c7', '#18236a', '#2f71c7', '#2f71c791', '#2f71c7', '#2f71c73d', '#2f71c726', '#2f71c785', '#2f71c7', '#2f71c721', '#2f71c740', '#2f71c7', '#2f71c791', '#2f71c733', '#2f71c7bd', '#2f71c76b', '#2f71c752'],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Code-1", [{
            p: 'opacity',
            t: [0, 800, 3900, 4900, 6200, 7200, 10300, 11300],
            v: [0, 1, 0, 1, 0, 1, 0, 1],
            e: [[0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#App-Screen", [{
            p: 'mpath',
            t: [3000, 3400],
            v: ['0%', '100%'],
            e: [[1, 0, 1, 1, 1], [0]],
            mp: "M93.1,42L-110.1,42"
          }], "#Rectangle-36", [{
            p: 'scaleX',
            t: [1800, 4400, 9000, 15900, 19600, 20600, 22100, 24700, 29300, 36200, 39900, 40900],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [1800, 4400, 9000, 15900, 19600, 20600, 22100, 24700, 29300, 36200, 39900, 40900],
            v: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }], "#Rectangle-Copy", [{
            p: 'scaleX',
            t: [1800, 4400, 7800, 9000, 13300, 16800, 17800, 20600, 22100, 24700, 28100, 29300, 33600, 37100, 38100, 40900],
            v: [0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [1800, 4400, 7800, 9000, 13300, 16800, 17800, 20600, 22100, 24700, 28100, 29300, 33600, 37100, 38100, 40900],
            v: [0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }], "#Rectangle-Copy-2", [{
            p: 'scaleX',
            t: [1800, 4400, 8500, 9000, 14800, 15300, 17100, 20600, 22100, 24700, 28800, 29300, 35100, 35600, 37400, 40900],
            v: [0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [1800, 4400, 8500, 9000, 14800, 15300, 17100, 20600, 22100, 24700, 28800, 29300, 35100, 35600, 37400, 40900],
            v: [0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 0, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }], "#Oval-26", [{
            p: 'scaleX',
            t: [2800, 3800, 10800, 11800, 18800, 19800, 26800, 27800],
            v: [0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [2800, 3800, 10800, 11800, 18800, 19800, 26800, 27800],
            v: [0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }], "#Path-5", [{
            p: 'd',
            t: [0, 5800, 6800, 8000, 13800, 14800, 16000, 21800, 22800, 24000, 29800, 30800],
            v: ["path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,49.2L120.9,114.6L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,49.2L120.9,114.6L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,49.2L120.9,114.6L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,49.2L120.9,114.6L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')", "path('M66.7,25L95.4,78.7L120.9,94.9L131.9,128.8L161.6,134.5L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.4,2.2,92.1L25,15.6L25,15.6L66.7,25Z')"],
            e: [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Path-5-Copy", [{
            p: 'd',
            t: [0, 6800, 8000, 14800, 16000, 22800, 24000, 30800],
            v: ["path('M57.9,59.9L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,59.9Z')", "path('M57.9,69.8L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,69.8Z')", "path('M57.9,59.9L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,59.9Z')", "path('M57.9,69.8L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,69.8Z')", "path('M57.9,59.9L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,59.9Z')", "path('M57.9,69.8L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,69.8Z')", "path('M57.9,59.9L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,59.9Z')", "path('M57.9,69.8L85,99.6L115.2,118.3L123.5,136.1L157.4,149.1L175.7,191.3L6.9,103.2C2.9,101.1,.9,96.5,2.2,92.1L15.1,48.5L15.1,48.5L57.9,69.8Z')"],
            e: [[0], [0], [0], [0], [0], [0], [0], [0]]
          }], "#Oval-27", [{
            p: 'scaleX',
            t: [2800, 3800, 10800, 11800, 18800, 19800, 26800, 27800],
            v: [0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }, {
            p: 'scaleY',
            t: [2800, 3800, 10800, 11800, 18800, 19800, 26800, 27800],
            v: [0, 1, 0, 1, 0, 1, 0, 1],
            e: [[1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0], [1, 0, 1, 1, 1], [0]]
          }], {
            autoremove: false
          }).range(0, 42100).loop(true);
        })(KeyshapeJS);
      };

      var sceneIntro = document.getElementById('sceneIntro');

      if (sceneIntro) {
        showIntroTitle();
        showIntroSceneAnimation();
        showIntroButtons();
      }
    }
  };

  var sceneProcess = function sceneProcess() {
    if (!KeyshapeJS.version.indexOf('1.') != 0) {
      /**
       * Animation for Our Process Section
       */
      var sceneProcess = document.getElementById('sceneProcess');
      var sceneProcesslaunch = true;
      /**
       * Show Animation
       */

      var showProcessSceneAnimation = function showProcessSceneAnimation() {
        var offset = -300;

        if (sceneProcesslaunch) {
          if (checkVisible(sceneProcess, offset)) {
            getProcessSceneAnimation();
            sceneProcesslaunch = false;
          }
        }
      };
      /**
       * Animation Frames
       */


      var getProcessSceneAnimation = function getProcessSceneAnimation() {
        window.ks = document.ks = KeyshapeJS;

        (function (ks) {
          ks.animate("#Sun-1", [{
            p: 'fillOpacity',
            t: [0, 1000, 3500, 4500, 7000, 8000, 10500],
            v: [.1, .3, .1, .3, .1, .3, .1],
            e: [[1, 0, 0, .6, 1], [0], [1, 0, 0, .6, 1], [0], [1, 0, 0, .6, 1], [0], [0]]
          }], "#Sun-2", [{
            p: 'fillOpacity',
            t: [0, 500, 1500, 3500, 4000, 5000, 7000, 7500, 8500, 10500],
            v: [.1, 0, .3, .1, 0, .3, .1, 0, .3, .1],
            e: [[0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0]]
          }], "#Sun-3", [{
            p: 'fillOpacity',
            t: [0, 1000, 2000, 3500, 4500, 5500, 7000, 8000, 9000, 10500],
            v: [.1, 0, .3, .1, 0, .3, .1, 0, .3, .1],
            e: [[0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0]]
          }], "#Sun-4", [{
            p: 'fillOpacity',
            t: [0, 1500, 2500, 3500, 5000, 6000, 7000, 8500, 9500, 10500],
            v: [.1, 0, .3, .1, 0, .3, .1, 0, .3, .1],
            e: [[0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0], [1, 0, 0, .6, 1], [0], [0]]
          }], "#Rocket", [{
            p: 'mpath',
            t: [0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10000],
            v: ['0%', '6.8%', '13.6%', '20.4%', '27.2%', '34%', '40.8%', '47.6%', '54.4%', '100%'],
            e: [[1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [1, .4, 0, .6, 1], [0]],
            mp: "M747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,288.6L747,328.6L747,60.5"
          }, {
            p: 'scaleX',
            t: [9000, 10000],
            v: [1, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleY',
            t: [9000, 10000],
            v: [1, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }], "#Rocket-2", [{
            p: 'opacity',
            t: [0, 500, 10000, 10500],
            v: [1, 0, 0, 1],
            e: [[0], [0], [0], [0]]
          }], "#Text-Launch", [{
            p: 'opacity',
            t: [8000, 8500],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Text-Test", [{
            p: 'opacity',
            t: [7000, 7500],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Text-Develop", [{
            p: 'opacity',
            t: [5000, 5500],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Text-Design", [{
            p: 'opacity',
            t: [3000, 3500],
            v: [0, 1],
            e: [[0], [0]]
          }], "#Text-Discover", [{
            p: 'opacity',
            t: [1000, 1500],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }], "#Flag---1", [{
            p: 'rotate',
            t: [0, 1000],
            v: [-10, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleX',
            t: [0, 1000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleY',
            t: [0, 1000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }], "#Flag---2", [{
            p: 'rotate',
            t: [2000, 3000],
            v: [-10, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleX',
            t: [2000, 3000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleY',
            t: [2000, 3000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }], "#Flag---3", [{
            p: 'rotate',
            t: [4000, 5000],
            v: [-10, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleX',
            t: [4000, 5000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleY',
            t: [4000, 5000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }], "#Flag---4", [{
            p: 'rotate',
            t: [6000, 7000],
            v: [-10, 0],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleX',
            t: [6000, 7000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }, {
            p: 'scaleY',
            t: [6000, 7000],
            v: [0, 1],
            e: [[1, .4, 0, 0, 1], [0]]
          }], {
            autoremove: false
          }).range(0, 10500).loop(true);
          if (document.location.search.substr(1).split('&').indexOf('global=paused') >= 0) ks.globalPause();
        })(KeyshapeJS);
      };
      /**
       * Check Visibility
       */


      window.addEventListener('scroll', function () {
        showProcessSceneAnimation();
      });
      showProcessSceneAnimation();
    }
  };

  /**
   * Blog: Intro Slider
   */
  Swiper.use([Navigation$1]);

  var sliderBlog = function sliderBlog() {
    /**
     * Intro Slider
     */
    var elSliderIntro = document.querySelector('.blog-intro__slider');

    if (elSliderIntro) {
      var sliderIntro = new Swiper(elSliderIntro, {
        loop: true,
        centeredSlides: true,
        spaceBetween: 0,
        slidesPerView: 3,
        preloadImages: false,
        lazy: true,
        autoplay: {
          delay: 3000,
          disableOnInteraction: false
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        breakpoints: {
          768: {
            spaceBetween: 0
          }
        }
      });
    }
    /**
     * Featured Posts Slider
     */


    var elSliderFeatured = document.querySelector('.featured-posts__slider');

    if (elSliderFeatured) {
      var SliderFeatured = new Swiper(elSliderFeatured, {
        loop: true,
        centeredSlides: true,
        spaceBetween: 0,
        slidesPerView: 1,
        preloadImages: false,
        lazy: true,
        autoplay: {
          delay: 3000,
          disableOnInteraction: false
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        }
      });
    }
    /**
     * Recent Posts Slider
     */


    var elSimilarPosts = document.querySelector('.similar-posts__slides');

    if (elSimilarPosts) {
      var SliderSim = new Swiper(elSimilarPosts, {
        loop: true,
        centeredSlides: false,
        spaceBetween: 45,
        preloadImages: false,
        lazy: true,
        autoplay: {
          delay: 3000,
          disableOnInteraction: false
        },
        navigation: {
          nextEl: '.similar-posts__btn-next',
          prevEl: '.similar-posts__btn-prev'
        },
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 20
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 30
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 45
          }
        }
      });
    }
  };

  /**
   * Home: Works Slider
   */
  Swiper.use([Scrollbar$1]);

  var sliderWorksList = function sliderWorksList() {
    var elSlider = document.querySelector('.works-list-slider');

    if (elSlider) {
      (function () {
        var elData = document.querySelector('.works-data');
        var elDataItems = elData.querySelectorAll('.filter-all');
        var items = [];
        /**
         * Scroll to active work
         */

        var worksPosts = document.querySelector('.works-posts');
        /**
         * Hide Works
         */

        var hideWorks = function hideWorks() {
          if (worksPosts) {
            var worksPostsLinks = worksPosts.querySelectorAll('.works-tabs-list__link');
            var worksPostsCards = worksPosts.querySelectorAll('.works-card');

            if (worksPostsLinks && worksPostsCards) {
              worksPostsCards.forEach(function (el) {
                el.classList.remove('active');
                el.classList.add('hide');
              });
              worksPostsLinks.forEach(function (el) {
                var hash = el.hash;
                var card = worksPosts.querySelector(hash);

                if (card) {
                  card.classList.remove('hide');
                }
              });
            }
          }
        };

        var updateSlider = function updateSlider(target) {
          target.classList.add('active');

          if (sliderWorks.slides) {
            sliderWorks.removeAllSlides();
            var filterClass = '.filter-' + target.hash.split('#')[1];
            var elFilteredItems = document.querySelectorAll(filterClass);
            var filteredItems = [];
            elFilteredItems.forEach(function (el) {
              var item = el.innerHTML;
              filteredItems.push(item);
            });
            sliderWorks.addSlide(1, filteredItems);
            sliderWorks.update(true);
            initWorksTabs();
          } else {
            sliderWorks = new Swiper(elSlider, {
              direction: 'vertical',
              mousewheel: true,
              mousewheelControl: true,
              slidesPerView: 'auto',
              autoHeight: true,
              scrollbar: {
                el: '.swiper-scrollbar',
                hide: false,
                draggable: true
              }
            });
            elSlider.style.height = ''; // Restyle Slider

            var elActionPanel = document.querySelector('.works-tabs-list__action');
            elActionPanel.style.display = '';
            var elWrapper = elSlider.querySelector('.swiper-wrapper');
            elWrapper.style.flexDirection = '';
            var elScrollBar = elSlider.querySelector('.swiper-scrollbar');
            elScrollBar.style.display = '';
          }

          if (worksPosts) {
            var worksPostsLinks = worksPosts.querySelectorAll('.works-tabs-list__link');

            if (worksPostsLinks) {
              worksPostsLinks.forEach(function (el) {
                el.addEventListener('mousemove', function (e) {
                  window.scrollTo(0, 0);
                });
              });
            }
          }
        };

        elDataItems.forEach(function (el) {
          var item = el.innerHTML;
          items.push(item);
        });
        /**
         * Vertical Slider
         */

        var sliderWorks = new Swiper(elSlider, {
          direction: 'vertical',
          mousewheel: true,
          mousewheelControl: true,
          slidesPerView: 'auto',
          autoHeight: true,
          scrollbar: {
            el: '.swiper-scrollbar',
            hide: false,
            draggable: true
          }
        });
        sliderWorks.addSlide(1, items);
        sliderWorks.update(true);
        initWorksTabs();
        var btnExpand = document.querySelector('.btn-works-list-expand');

        if (btnExpand) {
          btnExpand.addEventListener('click', function (e) {
            e.preventDefault();
            sliderWorks.destroy(true, true);
            elSlider.style.height = 'auto'; // Restyle Slider

            var elActionPanel = document.querySelector('.works-tabs-list__action');
            elActionPanel.style.display = 'none';
            var elWrapper = elSlider.querySelector('.swiper-wrapper');
            elWrapper.style.flexDirection = 'column';
            var elScrollBar = elSlider.querySelector('.swiper-scrollbar');
            elScrollBar.style.display = 'none';
          });
        }
        /**
         * Filter
         */


        var elFilter = document.querySelector('.works-posts__filter');
        var elFilterItems = elFilter.querySelectorAll('.tab-menu__link');
        var elFilterItemsLen = elFilterItems.length;

        for (var i = 0; i < elFilterItemsLen; i++) {
          elFilterItems[i].addEventListener('mousemove', function (e) {
            e.preventDefault();
            var target = e.currentTarget;
            elFilterItems.forEach(function (el) {
              el.classList.remove('active');
            });
            updateSlider(target);
            hideWorks();
          });
          elFilterItems[i].addEventListener('click', function (e) {
            e.preventDefault();
          });

          if (worksPosts) {
            var worksPostsLinks = worksPosts.querySelectorAll('.works-tabs-list__link');

            if (worksPostsLinks) {
              worksPostsLinks.forEach(function (el) {
                el.addEventListener('mousemove', function (e) {
                  window.scrollTo(0, 0);
                });
              });
            }
          }
        }
        /**
         * Mobile Filter
         */


        var elMobFilter = document.querySelector('.works-filter');

        if (elMobFilter) {
          var elMobFilterBtn = elMobFilter.querySelector('.btn');
          var elMobFilterBtnText = elMobFilterBtn.querySelector('.btn__text');
          var elMobFilterOptions = elMobFilter.querySelector('.works-filter__options');
          var elMobFilterLinks = elMobFilter.querySelectorAll('.works-filter__link ');

          if (elMobFilterBtn && elMobFilterOptions && elMobFilterLinks) {
            elMobFilterBtn.addEventListener('click', function (e) {
              e.preventDefault();
              var status = elMobFilterOptions.classList.contains('active');

              if (status) {
                elMobFilterOptions.classList.remove('active');
                elMobFilterBtn.classList.remove('active');
                return;
              } else {
                elMobFilterOptions.classList.add('active');
                elMobFilterBtn.classList.add('active');
              }
            });
            elMobFilterLinks.forEach(function (el) {
              el.addEventListener('click', function (e) {
                e.preventDefault;
                elMobFilterLinks.forEach(function (el) {
                  el.classList.remove('active');
                });
                var target = e.currentTarget;
                elMobFilterBtnText.innerHTML = target.innerHTML;
                elMobFilterOptions.classList.remove('active');
                elMobFilterBtn.classList.remove('active');
                updateSlider(target);
                hideWorks();
              });
            });
          }
        }
      })();
    }
  };

  /**
   * Home: Works Slider (Mobile)
   */

  var sliderWorks = function sliderWorks() {
    var worksSlider = document.querySelector('.mobile-works-slider');

    if (worksSlider) {
      var _sliderWorks = new Swiper(worksSlider, {
        loop: true,
        centeredSlides: true,
        spaceBetween: 20,
        slidesPerView: 1,
        preloadImages: false,
        lazy: true,
        autoplay: {
          delay: 3000,
          disableOnInteraction: false
        }
      });
    }
  };

  /**
   * Home: Works Slider (Mobile)
   */
  Swiper.use([Navigation$1]);

  var sliderInsight = function sliderInsight() {
    var insSlider = document.querySelector('.insight-slider');

    if (insSlider) {
      var sliderIns = new Swiper(insSlider, {
        loop: false,
        variableWidth: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        breakpoints: {
          768: {
            slidesPerView: 3,
            spaceBetween: 30
          },
          528: {
            slidesPerView: 2,
            spaceBetween: 20
          }
        }
      });
    }
  };

  var StickyMenu = function StickyMenu() {
    var elSticky = document.querySelector('.sticky');

    if (elSticky) {
      var sticky = new Sticky('.sticky');
    }
  };

  /**
   * Load methods, helpers, polyfills etc.
   */
  /**
   * The DOMContentLoaded event fires when the initial HTML document has been completely loaded and parsed,
   * without waiting for stylesheets, images, and subframes to finish loading.
   *
   * https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event
   */

  document.addEventListener('DOMContentLoaded', function () {
    initNav(); // Tabs

    initProcessTabs();
    initOurProcessTabs();
    initServiceTabs();
    initWorksTabs();
    initTestimonialsTabs();
    tabsFAQ(); // Animation

    sceneIntro();
    sceneCTA();
    sceneProcess(); // Modal

    Modal();
    ModalContact();
    ModalVideo(); // Sliders

    sliderBlog();
    sliderWorksList();
    sliderWorks();
    sliderInsight(); // Action

    btnExploreMore(); // Sticky

    StickyMenu();
  });

}());
