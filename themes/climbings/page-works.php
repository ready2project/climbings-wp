<?php

/**
 * Works Page
 * @package climbings
 */

get_header();
?>



<!-- Works Raw Data -->
<div class="works-data">

  <?php
  /**
   * Get All Works
   */

  $args = array(
    'post_type' => 'works',
  );
  $the_query = new WP_Query($args);

  if ($the_query->have_posts()) {
    $counter = 1;

    while ($the_query->have_posts()) :
      setup_postdata($the_query->the_post());

      if ($counter === 1) {
        $active = ' active';
      } else {
        $active = '';
      }

      $resp = get_field('filter');

      $classes = 'filter-all ';
      foreach ($resp as $value) {
        $value = strtolower('filter-' . str_replace(' ', '-', $value));
        $classes .= $value . ' ';
      }
  ?>

      <div class="<?php echo $classes; ?>">
        <div class="swiper-slide">
          <div class="works-tabs-list__item">
            <a class="works-tabs-list__link<?php echo $active; ?>" href="#works-tab-<?php the_ID(); ?>">
              <?php the_title(); ?>
            </a>
          </div>
        </div>
      </div>

  <?php
      $counter++;
    endwhile;

    // Restore original Query & Post Data
    wp_reset_query();
    wp_reset_postdata();
  }
  ?>
</div><!-- /.works-data -->

<!-- Works -->
<main class="works-posts">
  <div class="container container-full">

    <header class="works__header">
      <div class="row">
        <div class="col-md-4">
          <h1 class="t2">Works</h1>
        </div>
        <div class="col-md-8">

          <!-- Works Filter | Select Box-->
          <div class="works-filter">
            <div class="works-filter__select">
              <a class="btn btn-secondary btn-icon" href="#">
                <span class="btn__text">All Projects</span>
                <i class="i i-arrow-down"></i>
              </a>
            </div>

            <div class="works-filter__options">
              <ul class="works-filter-list">
                <li class="works-filter__item">
                  <a href="#all" class="works-filter__link active">All Projects</a>
                </li>
                <li class="works-filter__item">
                  <a href="#design" class="works-filter__link">Web/UI Design</a>
                </li>
                <li class="works-filter__item">
                  <a href="#branding" class="works-filter__link">Branding</a>
                </li>
                <li class="works-filter__item">
                  <a href="#graphics" class="works-filter__link">Graphics</a>
                </li>
              </ul>
            </div>
          </div>

          <!-- Works Filter -->
          <div class="works-posts__filter">
            <ul class="tab-menu tab-menu-alt">
              <li class="tab-menu__item">
                <a href="#all" class="tab-menu__link active">All Projects</a>
              </li>
              <li class="tab-menu__item">
                <a href="#design" class="tab-menu__link">Web/UI Design</a>
              </li>
              <li class="tab-menu__item">
                <a href="#branding" class="tab-menu__link">Branding</a>
              </li>
              <li class="tab-menu__item">
                <a href="#graphics" class="tab-menu__link">Graphics</a>
              </li>
            </ul>
          </div><!-- /.works-posts__filter -->

        </div>
      </div>
    </header>

    <!-- Tabs -->
    <div class="works-tabs">
      <div class="row">
        <div class="col-md-4" data-sticky-container>

          <!-- Tabs Nav -->
          <div class="works-tabs__nav sticky" data-margin-top="80" data-sticky-class="sticky-active">

            <!-- Tabs List -->
            <div class="works-tabs-list works-tabs-list-main">
              <div class="works-list-slider swiper-container">
                <div class="swiper-wrapper">

                </div>

                <!-- Add Scrollbar -->
                <div class="swiper-scrollbar"></div>
              </div>

              <div class="works-tabs-list__action">
                <a href="#" class="btn btn-short btn-works-list-expand"><i class="i i-arrow-down"></i></a>
              </div>

            </div><!-- /.works-tabs-list -->
          </div><!-- /.works-tabs__nav -->
        </div>
        <div class="col-md-8">

          <!-- Tabs Content -->
          <div class="works-tabs__content">



            <?php
            $args = array(
              'post_type' => 'works',
            );
            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) {
              while ($the_query->have_posts()) :
                setup_postdata($the_query->the_post());
            ?>

                <!-- Tabs Card -->
                <div class="works-card" id="works-tab-<?php the_ID(); ?>">

                  <!-- Card Header -->
                  <div class="works-card__header">
                    <?php
                    $logo = get_field('logo');
                    if (!empty($logo['url'])) : ?>
                      <div class="works-card__logo">
                        <img src="<?php echo $logo['url']; ?>" alt="">
                      </div>
                    <?php endif; ?>

                    <h3 class="page-card__title">
                      <?php the_title(); ?>
                    </h3>

                    <div class="works-card__subtitle">
                      <p>
                        <?php the_field('subtitle'); ?>
                      </p>
                    </div>

                    <div class="works-card__dsc">
                      <?php the_field('description'); ?>
                    </div>
                  </div>

                  <!-- Card Content -->
                  <div class="works-card__content">
                    <h3 class="caps-heading">Responsibilities</h3>

                    <?php
                    $resp = get_field('responsibilities');
                    ?>

                    <!-- Card Tags -->
                    <ul class="works-card-tags__list">
                      <?php foreach ($resp as $value) : ?>
                        <li class="works-card-tags__item">
                          <?php echo $value; ?>
                        </li>
                      <?php endforeach; ?>
                    </ul>

                    <!-- Card Action -->
                    <div class="works-card__action">
                      <a class="btn btn-lg btn-icon modal-link" href="#" data-target="work-<?php the_ID(); ?>">
                        <span class="btn__text">View the Project</span>
                        <i class="i i-arrow-right"></i>
                      </a>
                    </div>

                    <!-- Card Thumbnail -->
                    <div class="works-card__thumb">
                      <?php
                      $url = get_the_post_thumbnail_url();
                      ?>

                      <?php if ($url) : ?>
                        <img loading="lazy" src="<?php echo $url; ?>" alt="Climbings | Works |  <?php the_title(); ?>">
                      <?php endif; ?>
                    </div>

                  </div>
                </div><!-- /.works-card -->



            <?php
              endwhile;

              // Restore original Query & Post Data
              wp_reset_query();
              wp_reset_postdata();
            }
            ?>

          </div><!-- /.works-tabs__content -->
        </div><!-- /.works-tabs -->

      </div>
    </div>
  </div>
</main><!-- /.works -->


<?php
$args = array(
  'post_type' => 'works',
);
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
  while ($the_query->have_posts()) :
    setup_postdata($the_query->the_post());
?>

    <!-- Modal Window: Works Details -->
    <div class="modal modal-work" id="work-<?php the_ID(); ?>">
      <div class="container">
        <div class="modal-work__header">
          <a href="#" class="btn btn-short btn-hide btn-alt modal-hide" data-target="work-<?php the_ID(); ?>"><i class="i i-hide"></i></a>
        </div>
        <div class="modal-work__body">

          <!-- Tabs Card -->
          <div class="works-card">

            <!-- Card Header -->
            <div class="works-card__header">
              <h3 class="page-card__title"><?php the_title(); ?></h3>

              <div class="works-card__subtitle">
                <p>
                  <?php the_field('subtitle'); ?>
                </p>
              </div>

              <div class="works-card__dsc">
                <?php the_field('description'); ?>
              </div>
            </div>

            <!-- Card Content -->
            <div class="works-card__content">
              <h3 class="caps-heading">Responsibilities</h3>

              <?php
              $resp = get_field('responsibilities');
              ?>

              <!-- Card Tags -->
              <ul class="works-card-tags__list">
                <?php foreach ($resp as $value) : ?>
                  <li class="works-card-tags__item">
                    <?php echo $value; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div><!-- /.works-card -->

          <!-- Preview -->
          <div class="modal-work__preview">
            <?php
            $screens = get_field('screens');
            foreach ($screens as $item) :
            ?>
              <img loading="lazy" src="<?php echo $item['url']; ?>" alt="Climbings | Works |  <?php the_title(); ?>">
            <?php endforeach; ?>
          </div>

        </div><!-- /.modal-work-body -->
      </div><!-- /.container -->
    </div><!-- /.modal-work -->
<?php
  endwhile;

  // Restore original Query & Post Data
  wp_reset_query();
  wp_reset_postdata();
}
?>

<?php
get_footer();
