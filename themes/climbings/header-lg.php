<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package climbings
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <!-- Meta Tags -->
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Fonts Preloading / Caching -->
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Black.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Book.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/greycliff/subset-GreycliffCF-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/greycliff/subset-GreycliffCF-ExtraBold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<!-- Main Navigation -->
<nav class="nav nav-lg">
  <div class="col-logo">
    <!-- Logo -->
    <a class="logo" href="<?php echo get_home_url(); ?>" title="Climbings. Index Page" accesskey="1">
      <svg width="149" height="36">
        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#logo-alt"></use>
      </svg>
    </a>
  </div>
  <div class="col-nav">

    <div class="search-field">
      <form action="/" method="GET">
        <img class="search-field__icon" src="<?php echo get_template_directory_uri(); ?>/assets/dist/images/icon-search.svg" alt="Climbings | Icon - Search" width="23" height="21">
        <input class="form__input" type="text" placeholder="Search" id="s" name="s" id="search" value="<?php the_search_query(); ?>">
      </form>
    </div>

    <!-- Menu Button -->
    <a href="#" class="btn-nav" id="btnNav">
      <span class="btn-nav__label">Menu</span>

      <span class="hamburger hamburger-squeeze">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </span>
    </a>
  </div>
</nav>


<?php get_template_part('template-parts/menu/main-menu', 'none'); ?>
