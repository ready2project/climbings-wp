<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package climbings
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <!-- Meta Tags -->
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Fonts Preloading / Caching -->
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Black.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/circularstd/subset-CircularStd-Book.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/greycliff/subset-GreycliffCF-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/dist/fonts/greycliff/subset-GreycliffCF-ExtraBold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


  <?php get_template_part('template-parts/menu/main-menu', 'none'); ?>


  <?php
  if (!is_front_page()) : ?>

    <!-- Header -->
    <header class="header header-404">
      <div class="container">
        <div class="row">
          <div class="col">

            <!-- Logo -->
            <a class="logo" href="<?php echo get_home_url(); ?>" title="Climbings. Index Page" accesskey="1">
              <svg width="206" height="50">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#logo"></use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </header>

  <?php endif; ?>