<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *

 *
 * @package climbings
 */

?>


<?php
get_template_part('template-parts/section-cta', 'none');
get_template_part('template-parts/modal-contact-form', 'none');
?>


<!-- Footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col">

        <?php get_template_part('template-parts/menu/footer-menu', 'none'); ?>

        <!-- Footer Logo -->
        <a href="<?php echo get_home_url(); ?>" class="logo-footer" aria-label="Climbings. Index Page">
          <svg width="71" height="79">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/dist/images/sprite.svg#logo-symbol"></use>
          </svg>
        </a>

        <!-- Footer Copy -->
        <p class="copy">&copy; Climbings LLC 2019–<?php echo date('Y'); ?></p>
      </div>
    </div>

  </div>
</footer>

<?php wp_footer(); ?>
</body>

</html>